<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends IWEB_Controller {
	
	function __construct()
	{
		parent::__construct();	
		$this->load->model('home_model');			
	}
	
	public function index($msg='')
	{
		
		if(get_authenticateUserID()>0)
		{
			redirect('home/dashboard');
		}
		
		
		$data['error']='';
		$data['username']='';
		$data['password']='';
		
		$data['msg']=$msg;
		$this->load->view('common/login',$data);
	}
	
	function chklogin()
	{
	
		$this->form_validation->set_rules('username', 'Username', 'required|alpha_numeric');
		$this->form_validation->set_rules('password', 'Password', 'required|min_length[8]|max_length[12]');
		
		
			if($this->form_validation->run() == FALSE){
			
			
					if(validation_errors())
					{
						$data["error"] = validation_errors();
					}else{
						$data["error"] = "";
					}
					
					
					$data['username']=$this->input->post('username');
					$data['password']=$this->input->post('password');
					$data['msg']='';
					
			
			}
			
			
			else
			{
			
					$chk=$this->home_model->chk_login();
					
					if($chk=='1')
					{
						
						redirect('home/dashboard');
						
					}
					
					else
					{
					
						$data['error']='Email or Password are wrong.';
						
						
						$data['username']=$this->input->post('username');
						$data['password']=$this->input->post('password');
						
						$data['msg']='';
						
					}
			
			
			}
					
					
			$this->load->view('common/login',$data);		
	
	
	}
	
	function logout()	
	{
	
		$this->session->sess_destroy();		
		$this->session->unset_userdata();		
		$this->session->set_userdata(array('team_id' => ''));		
		redirect('home/index/logout');
	}
	
	
	function forgot_password()
	{
		
		
		$data["email"] = "";
		$this->form_validation->set_rules('email', 'Email', 'required|valid_email');
		
		if($this->form_validation->run() == FALSE){			
			if(validation_errors())
			{
				$data["error"] = validation_errors();
			}else{
				$data["error"] = "";
			}
					
			$data["email"] = $this->input->post('email');
		}
		
		else
		{
			$data["email"] = $this->input->post('email');
			
			$chk_mail=$this->home_model->forgot_email();
			
			if($chk_mail==0)
			{
					
				$data['error']='email_not_found';
				
			
			
			}
			elseif($chk_mail==2)
			{
				$data['error']='record_not_found';	
				
			
			}
			else
			{
				$data['error']='success';	
				
			
			}
			
		
		}
		
		$this->load->view('common/forgot_password',$data);	
		
		
	}
	
	
	
	function dashboard($msg='')
	{
		$data=array();
                $data['msg']=$msg;
	
		$this->template->write_view('header','common/header',$data,TRUE);
		$this->template->write_view('content','common/dashboard',$data,TRUE);
		$this->template->write_view('sidebar','common/sidebar',$data,TRUE);
		$this->template->write_view('footer','common/footer',$data,TRUE);
		$this->template->render();		
		
	}
	
}
