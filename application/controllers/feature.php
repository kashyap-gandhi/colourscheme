<?php
class Feature extends IWEB_Controller {

	function Feature()
	{
		parent::__construct();
		$this->load->model('feature_model');	
                
                 if(!check_role_permission('manage_feature')) { 
                    redirect('home/dashboard/notauthorize');
                }
	}
	
	
	
	function index($msg='')
	{
		redirect('feature/footage');
	}
	
	
	
	
	
	///===coat type===
	
	function coat($offset=0,$msg='')
	{
	
		$this->load->library('pagination');

		$limit = '15';
		
		$config['base_url'] = site_url('feature/coat/');
		$config['total_rows'] = $this->feature_model->get_total_coat_count();
		$config['per_page'] = $limit;		
		$this->pagination->initialize($config);		
		$data['page_link'] = $this->pagination->create_links();
		$data['result'] = $this->feature_model->get_all_coat($offset, $limit);
		$data['msg'] = $msg;
		$data['offset'] = $offset;
		$this->template->write_view('header','common/header',$data,TRUE);
		$this->template->write_view('content','feature/list_coat',$data,TRUE);
		$this->template->write_view('sidebar','common/sidebar',$data,TRUE);
		$this->template->write_view('footer','common/footer',$data,TRUE);
		$this->template->render();	
	}
	
	
	function add_coat()
	{

		$this->form_validation->set_rules('rate_type', 'Name', 'required|alpha_space|min_length[3]|max_length[50]');
		$this->form_validation->set_rules('quotetype_id', 'Quote Type', 'required');
		
		if($this->input->post('rate')!='') {
			$this->form_validation->set_rules('rate', 'Rate', 'numeric');
		}
		
	
		if($this->form_validation->run() == FALSE){			
			if(validation_errors())
			{
				$data["error"] = validation_errors();
			}else{
				$data["error"] = "";
			}
			$data["featurecoat_id"] = $this->input->post('featurecoat_id');
			$data["rate_type"] = $this->input->post('rate_type');
			$data["quotetype_id"] = $this->input->post('quotetype_id');
			$data["rate"] = $this->input->post('rate');
			
			if($this->input->post('offset')=="")
			{
				$limit = '15';
				$totalRows = $this->feature_model->get_total_coat_count();
				$data["offset"] = (int)($totalRows/$limit)*$limit;
			}else{
				$data["offset"] = $this->input->post('offset');
			}
				
				$this->template->write_view('header','common/header',$data,TRUE);
				$this->template->write_view('content','feature/add_coat',$data,TRUE);
				$this->template->write_view('sidebar','common/sidebar',$data,TRUE);
				$this->template->write_view('footer','common/footer',$data,TRUE);
				$this->template->render();
		}else{
			if($this->input->post('featurecoat_id'))
			{
				$this->feature_model->coat_update();
				$msg = "update";
			}else{
				$this->feature_model->coat_insert();
				$msg = "insert";
			}
			$offset = $this->input->post('offset');
			redirect('feature/coat/'.$offset.'/'.$msg);
		}				
		
		
	
	}
	
	
	function edit_coat($id,$offset=0)
	{
		
		$coat_detail=$this->feature_model->get_coat_detail($id);
		
		if(empty($coat_detail)) { redirect('feature/coat/'.$offset.'/notfound'); }
		
		$data['featurecoat_id']=$id;
		
		$data["rate_type"] = $coat_detail->rate_type;
		$data["quotetype_id"] = $coat_detail->quotetype_id;
		$data["rate"] = $coat_detail->rate;
		
		
		$data['offset']=$offset;
		$data['error']='';
		
		
		$this->template->write_view('header','common/header',$data,TRUE);
		$this->template->write_view('content','feature/add_coat',$data,TRUE);
		$this->template->write_view('sidebar','common/sidebar',$data,TRUE);
		$this->template->write_view('footer','common/footer',$data,TRUE);
		$this->template->render();
	
	}
	
	
	function coat_action()
	{
	
		$offset=$this->input->post('offset');
		$action=$this->input->post('action');
		$featurecoat_id=$this->input->post('chk');
		
		if($action=='delete')
		{	
			foreach($featurecoat_id as $id)
			{
				$this->feature_model->delete_coat($id);
			}
			
			redirect('feature/coat/'.$offset.'/delete');
		}
		
	}
	
	
	
	///===footage type===
	
	function footage($offset=0,$msg='')
	{
	
		$this->load->library('pagination');

		$limit = '15';
		
		$config['base_url'] = site_url('feature/footage/');
		$config['total_rows'] = $this->feature_model->get_total_footage_count();
		$config['per_page'] = $limit;		
		$this->pagination->initialize($config);		
		$data['page_link'] = $this->pagination->create_links();
		$data['result'] = $this->feature_model->get_all_footage($offset, $limit);
		$data['msg'] = $msg;
		$data['offset'] = $offset;
		$this->template->write_view('header','common/header',$data,TRUE);
		$this->template->write_view('content','feature/list_footage',$data,TRUE);
		$this->template->write_view('sidebar','common/sidebar',$data,TRUE);
		$this->template->write_view('footer','common/footer',$data,TRUE);
		$this->template->render();	
	}
	
	
	function add_footage()
	{

		$this->form_validation->set_rules('description', 'Name', 'required|feature_check|min_length[3]|max_length[50]');
		$this->form_validation->set_rules('quotetype_id', 'Quote Type', 'required');
		
		if($this->input->post('default_footage')!='') {
			$this->form_validation->set_rules('default_footage', 'Default Footage', 'numeric');
		}
		
	
		if($this->form_validation->run() == FALSE){			
			if(validation_errors())
			{
				$data["error"] = validation_errors();
			}else{
				$data["error"] = "";
			}
			$data["feature_id"] = $this->input->post('feature_id');
			$data["description"] = $this->input->post('description');
			$data["quotetype_id"] = $this->input->post('quotetype_id');
			$data["default_footage"] = $this->input->post('default_footage');
			
			if($this->input->post('offset')=="")
			{
				$limit = '15';
				$totalRows = $this->feature_model->get_total_footage_count();
				$data["offset"] = (int)($totalRows/$limit)*$limit;
			}else{
				$data["offset"] = $this->input->post('offset');
			}
				
				$this->template->write_view('header','common/header',$data,TRUE);
				$this->template->write_view('content','feature/add_footage',$data,TRUE);
				$this->template->write_view('sidebar','common/sidebar',$data,TRUE);
				$this->template->write_view('footer','common/footer',$data,TRUE);
				$this->template->render();
		}else{
			if($this->input->post('feature_id'))
			{
				$this->feature_model->footage_update();
				$msg = "update";
			}else{
				$this->feature_model->footage_insert();
				$msg = "insert";
			}
			$offset = $this->input->post('offset');
			redirect('feature/footage/'.$offset.'/'.$msg);
		}				
		
		
	
	}
	
	
	function edit_footage($id,$offset=0)
	{
		
		$footage_detail=$this->feature_model->get_footage_detail($id);
		
		if(empty($footage_detail)) { redirect('feature/footage/'.$offset.'/notfound'); }
		
		$data['feature_id']=$id;
		
		$data["description"] = $footage_detail->description;
		$data["quotetype_id"] = $footage_detail->quotetype_id;
		$data["default_footage"] = $footage_detail->default_footage;
		
		
		$data['offset']=$offset;
		$data['error']='';
		
		
		$this->template->write_view('header','common/header',$data,TRUE);
		$this->template->write_view('content','feature/add_footage',$data,TRUE);
		$this->template->write_view('sidebar','common/sidebar',$data,TRUE);
		$this->template->write_view('footer','common/footer',$data,TRUE);
		$this->template->render();
	
	}
	
	
	function footage_action()
	{
	
		$offset=$this->input->post('offset');
		$action=$this->input->post('action');
		$feature_id=$this->input->post('chk');
		
		if($action=='delete')
		{	
			foreach($feature_id as $id)
			{
				$this->feature_model->delete_footage($id);
			}
			
			redirect('feature/footage/'.$offset.'/delete');
		}
		
	}
	
	
	
}
?>