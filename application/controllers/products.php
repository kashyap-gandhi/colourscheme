<?php
class Products extends IWEB_Controller {

	function Products()
	{
		parent::__construct();
		$this->load->model('products_model');	
                
                if(!check_role_permission('manage_product')) { 
                    redirect('home/dashboard/notauthorize');
                }
                
                
	}
	
	
	
	function index($msg='')
	{
		redirect('products/manage');
	}
	
	
	
		
	///===manage===
	
	function managecost($product_id,$offset=0,$msg='')
	{
	
		$product_detail=$this->products_model->get_product_detail($product_id);		
		if(empty($product_detail)) { redirect('products/manage/'.$offset.'/notfound'); }
		
		$data['product_id']=$product_id;
		$data['product_detail']=$product_detail;
		
		
		
		
		$this->load->library('pagination');

		$limit = '15';
		
		$config['base_url'] = site_url('products/managecost/'.$product_id.'/');
		$config['total_rows'] = $this->products_model->get_total_product_cost_count($product_id);
		$config['per_page'] = $limit;		
		$this->pagination->initialize($config);		
		$data['page_link'] = $this->pagination->create_links();
		$data['result'] = $this->products_model->get_all_product_cost($product_id,$offset, $limit);
		$data['msg'] = $msg;
		$data['offset'] = $offset;
		$this->template->write_view('header','common/header',$data,TRUE);
		$this->template->write_view('content','products/list_product_cost',$data,TRUE);
		$this->template->write_view('sidebar','common/sidebar',$data,TRUE);
		$this->template->write_view('footer','common/footer',$data,TRUE);
		$this->template->render();		
		
	
	}
	
	
	function add_cost($product_id)
	{
		$product_detail=$this->products_model->get_product_detail($product_id);		
		if(empty($product_detail)) { redirect('products/manage/'.$offset.'/notfound'); }
		
		$data['product_detail']=$product_detail;
		
		
		$data['product_id']=$product_id;
		
                $markup_cost_array=array(0,5,10,15,20,25,30,35,40);		
		$data['all_markup_cost']=$markup_cost_array;
                
                
                
                
		$this->form_validation->set_rules('product_id', 'Product', 'required');
		$this->form_validation->set_rules('start_date', 'Start Date', 'required');
		
		if($this->input->post('end_date')!='') {
			$this->form_validation->set_rules('end_date', 'End Date', 'required');
		}
		$this->form_validation->set_rules('cost', 'Cost', 'required|numeric');
		
		
	
		if($this->form_validation->run() == FALSE){			
			if(validation_errors())
			{
				$data["error"] = validation_errors();
			}else{
				$data["error"] = "";
			}
			$data["productcost_id"] = $this->input->post('productcost_id');
			//$data["product_id"] = $this->input->post('product_id');
			$data["start_date"] = $this->input->post('start_date');
			$data["end_date"] = $this->input->post('end_date');
			$data["cost"] = $this->input->post('cost');
                        $data['pst_tax']=$this->input->post('pst_tax');
                        $data['markup_cost']=$this->input->post('markup_cost');
			$data["active"] = $this->input->post('active');
                        
			
			
			
			if($this->input->post('offset')=="")
			{
				$limit = '15';
				$totalRows = $this->products_model->get_total_product_cost_count($product_id);
				$data["offset"] = (int)($totalRows/$limit)*$limit;
			}else{
				$data["offset"] = $this->input->post('offset');
			}
				
				$this->template->write_view('header','common/header',$data,TRUE);
				$this->template->write_view('content','products/add_product_cost',$data,TRUE);
				$this->template->write_view('sidebar','common/sidebar',$data,TRUE);
				$this->template->write_view('footer','common/footer',$data,TRUE);
				$this->template->render();
		}else{
			if($this->input->post('productcost_id'))
			{
				$this->products_model->product_cost_update();
				$msg = "update";
			}else{
				$this->products_model->product_cost_insert();
				$msg = "insert";
			}
			$offset = $this->input->post('offset');
			redirect('products/managecost/'.$product_id.'/'.$offset.'/'.$msg);
		}				
		
		
	
	}
	
	
	function edit_cost($product_id,$id,$offset=0)
	{
		
		$product_detail=$this->products_model->get_product_detail($product_id);		
		if(empty($product_detail)) { redirect('products/manage/'.$offset.'/notfound'); }
		
		$data['product_detail']=$product_detail;
		
		
		$productcost_detail=$this->products_model->get_product_cost_detail($id);
		
		if(empty($productcost_detail)) { redirect('products/managecost/'.$offset.'/notfound'); }
		
		$data['productcost_id']=$id;
                
                
                
                $markup_cost_array=array(0,5,10,15,20,25,30,35,40);		
		$data['all_markup_cost']=$markup_cost_array;
                
                
		
		$data["product_id"] = $product_id; ///$productcost_detail->product_id;
		$data["start_date"] = $productcost_detail->start_date;
		$data["end_date"] = $productcost_detail->end_date;
		$data["cost"] = $productcost_detail->cost;
                $data['pst_tax']=$productcost_detail->pst_tax;
                $data['markup_cost']=$productcost_detail->markup_cost;
		$data["active"] = $productcost_detail->active;
		
		
		$data['offset']=$offset;
		$data['error']='';
		
		
		$this->template->write_view('header','common/header',$data,TRUE);
		$this->template->write_view('content','products/add_product_cost',$data,TRUE);
		$this->template->write_view('sidebar','common/sidebar',$data,TRUE);
		$this->template->write_view('footer','common/footer',$data,TRUE);
		$this->template->render();
	
	}
	
	
	function cost_action($product_id)
	{
		
		$product_detail=$this->products_model->get_product_detail($product_id);		
		if(empty($product_detail)) { redirect('products/manage/'.$offset.'/notfound'); }
		
		$data['product_detail']=$product_detail;
		
		
		$offset=$this->input->post('offset');
		$action=$this->input->post('action');
		$productcost_id=$this->input->post('chk');
		
		if($action=='active')
		{	
			foreach($productcost_id as $id)
			{
				$this->products_model->active_cost($id);
			}
			
			redirect('products/managecost/'.$product_id.'/'.$offset.'/active');
		
                        
                } elseif($action=='inactive') {	
			
                        foreach($productcost_id as $id)
			{
				$this->products_model->deactive_cost($id);
			}
			
			redirect('products/managecost/'.$product_id.'/'.$offset.'/inactive');
                        
		} elseif($action=='delete') {	
			foreach($productcost_id as $id)
			{
				$this->products_model->delete_cost($id);
			}
			
			redirect('products/managecost/'.$product_id.'/'.$offset.'/delete');
		}
		
	}
	
	///===manage===
        
        
        
        
	function search($option,$keyword,$offset=0,$msg='')
	{
		$this->load->library('pagination');

		$limit = '15';
                
                $option=trim($option);
                $keyword=trim($keyword);
                
                
                $config['uri_segment'] = 5;
		
		$config['base_url'] = site_url('products/search/'.$option.'/'.$keyword);
		$config['total_rows'] = $this->products_model->get_total_product_count($option,$keyword);
		$config['per_page'] = $limit;		
		$this->pagination->initialize($config);		
		$data['page_link'] = $this->pagination->create_links();
		$data['result'] = $this->products_model->get_all_product($offset, $limit,$option,$keyword);
		$data['msg'] = $msg;
		$data['offset'] = $offset;
                
                $data['option']=$option;
                $data['keyword']=$keyword;
		
		$this->template->write_view('header','common/header',$data,TRUE);
		$this->template->write_view('content','products/list_product',$data,TRUE);
		$this->template->write_view('sidebar','common/sidebar',$data,TRUE);
		$this->template->write_view('footer','common/footer',$data,TRUE);
		$this->template->render();
	}
	
        
        
	
	function manage($offset=0,$msg='')
	{
		$this->load->library('pagination');

		$limit = '15';
                $option='';
                $keyword='';
		
		
		$config['base_url'] = site_url('products/manage/');
		$config['total_rows'] = $this->products_model->get_total_product_count($option,$keyword);
		$config['per_page'] = $limit;		
		$this->pagination->initialize($config);		
		$data['page_link'] = $this->pagination->create_links();
		$data['result'] = $this->products_model->get_all_product($offset, $limit,$option,$keyword);
		$data['msg'] = $msg;
		$data['offset'] = $offset;
                
                $data['option']=$option;
                $data['keyword']=$keyword;
                
		$this->template->write_view('header','common/header',$data,TRUE);
		$this->template->write_view('content','products/list_product',$data,TRUE);
		$this->template->write_view('sidebar','common/sidebar',$data,TRUE);
		$this->template->write_view('footer','common/footer',$data,TRUE);
		$this->template->render();		
		
	
	}
	
	
	function add_product()
	{
		
		$this->form_validation->set_rules('quotetype_id', 'Quote Type', 'required');
		$this->form_validation->set_rules('productquality_id', 'Quality Type', 'required');
		
		$this->form_validation->set_rules('paintbrand', 'Paint Brand', 'required|feature_check|min_length[3]|max_length[50]');
		$this->form_validation->set_rules('description', 'Description', 'required|feature_check|min_length[3]|max_length[100]');
		$this->form_validation->set_rules('spreadqty', 'Spread Quantity', 'required|is_natural');
		
		
	
		if($this->form_validation->run() == FALSE){			
			if(validation_errors())
			{
				$data["error"] = validation_errors();
			}else{
				$data["error"] = "";
			}
			$data["product_id"] = $this->input->post('product_id');
			$data["paintbrand"] = $this->input->post('paintbrand');
			$data["spreadqty"] = $this->input->post('spreadqty');
			$data["description"] = $this->input->post('description');
			$data["quotetype_id"] = $this->input->post('quotetype_id');
			$data["productquality_id"] = $this->input->post('productquality_id');
			
			
			if($this->input->post('offset')=="")
			{
				$limit = '15';
				$totalRows = $this->products_model->get_total_product_count();
				$data["offset"] = (int)($totalRows/$limit)*$limit;
			}else{
				$data["offset"] = $this->input->post('offset');
			}
				
				$this->template->write_view('header','common/header',$data,TRUE);
				$this->template->write_view('content','products/add_product',$data,TRUE);
				$this->template->write_view('sidebar','common/sidebar',$data,TRUE);
				$this->template->write_view('footer','common/footer',$data,TRUE);
				$this->template->render();
		}else{
			if($this->input->post('product_id'))
			{
				$this->products_model->product_update();
				$msg = "update";
			}else{
				$this->products_model->product_insert();
				$msg = "insert";
			}
			$offset = $this->input->post('offset');
			redirect('products/manage/'.$offset.'/'.$msg);
		}				
		
		
	
	}
	
	
	function edit_product($id,$offset=0)
	{
		
		$product_detail=$this->products_model->get_product_detail($id);
		
		if(empty($product_detail)) { redirect('products/manage/'.$offset.'/notfound'); }
		
		$data['product_id']=$id;
		
		$data["paintbrand"] = $product_detail->paintbrand;
		$data["spreadqty"] = $product_detail->spreadqty;
		$data["description"] = $product_detail->description;
		$data["quotetype_id"] = $product_detail->quotetype_id;
		$data["productquality_id"] = $product_detail->productquality_id;
		
		
		$data['offset']=$offset;
		$data['error']='';
		
		
		$this->template->write_view('header','common/header',$data,TRUE);
		$this->template->write_view('content','products/add_product',$data,TRUE);
		$this->template->write_view('sidebar','common/sidebar',$data,TRUE);
		$this->template->write_view('footer','common/footer',$data,TRUE);
		$this->template->render();
	
	}
	
	
	function product_action()
	{
		$offset=$this->input->post('offset');
		$action=$this->input->post('action');
		$product_id=$this->input->post('chk');
		
		if($action=='delete')
		{	
			foreach($product_id as $id)
			{
				$this->products_model->delete_product($id);
			}
			
			redirect('products/manage/'.$offset.'/delete');
		}
		
	}
	
	
	
	///===managetype===
	
	function managetype($offset=0,$msg='')
	{
		$this->load->library('pagination');

		$limit = '15';
		
		$config['base_url'] = site_url('products/managetype/');
		$config['total_rows'] = $this->products_model->get_total_type_count();
		$config['per_page'] = $limit;		
		$this->pagination->initialize($config);		
		$data['page_link'] = $this->pagination->create_links();
		$data['result'] = $this->products_model->get_all_type($offset, $limit);
		$data['msg'] = $msg;
		$data['offset'] = $offset;
		$this->template->write_view('header','common/header',$data,TRUE);
		$this->template->write_view('content','products/list_type',$data,TRUE);
		$this->template->write_view('sidebar','common/sidebar',$data,TRUE);
		$this->template->write_view('footer','common/footer',$data,TRUE);
		$this->template->render();		
		
	
	}
	
	
	function add_type()
	{
		$this->form_validation->set_rules('quality_desc', 'Name', 'required|alpha_space|min_length[3]|max_length[50]');
	
		if($this->form_validation->run() == FALSE){			
			if(validation_errors())
			{
				$data["error"] = validation_errors();
			}else{
				$data["error"] = "";
			}
			$data["productquality_id"] = $this->input->post('productquality_id');
			$data["quality_desc"] = $this->input->post('quality_desc');
			
			if($this->input->post('offset')=="")
			{
				$limit = '15';
				$totalRows = $this->products_model->get_total_type_count();
				$data["offset"] = (int)($totalRows/$limit)*$limit;
			}else{
				$data["offset"] = $this->input->post('offset');
			}
				
				$this->template->write_view('header','common/header',$data,TRUE);
				$this->template->write_view('content','products/add_type',$data,TRUE);
				$this->template->write_view('sidebar','common/sidebar',$data,TRUE);
				$this->template->write_view('footer','common/footer',$data,TRUE);
				$this->template->render();
		}else{
			if($this->input->post('productquality_id'))
			{
				$this->products_model->type_update();
				$msg = "update";
			}else{
				$this->products_model->type_insert();
				$msg = "insert";
			}
			$offset = $this->input->post('offset');
			redirect('products/managetype/'.$offset.'/'.$msg);
		}				
		
		
	
	}
	
	
	function edit_type($id,$offset=0)
	{
		
		$type_detail=$this->products_model->get_type_detail($id);
		
		if(empty($type_detail)) { redirect('products/managetype/'.$offset.'/notfound'); }
		
		$data['productquality_id']=$id;
		
		$data["quality_desc"] = $type_detail->quality_desc;
		
		
		$data['offset']=$offset;
		$data['error']='';
		
		
		$this->template->write_view('header','common/header',$data,TRUE);
		$this->template->write_view('content','products/add_type',$data,TRUE);
		$this->template->write_view('sidebar','common/sidebar',$data,TRUE);
		$this->template->write_view('footer','common/footer',$data,TRUE);
		$this->template->render();
	
	}
	
	
	function type_action()
	{
		$offset=$this->input->post('offset');
		$action=$this->input->post('action');
		$productquality_id=$this->input->post('chk');
		
		if($action=='delete')
		{	
			foreach($productquality_id as $id)
			{
				$this->products_model->delete_type($id);
			}
			
			redirect('products/managetype/'.$offset.'/delete');
		}
		
	}
	
}
?>