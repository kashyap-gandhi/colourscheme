<?php
class User extends IWEB_Controller {

	function User()
	{
		parent::__construct();
		$this->load->model('user_model');	
                
                if(!check_role_permission('manage_team')) { 
                    redirect('home/dashboard/notauthorize');
                }
	}
	
	
	
	function index($msg='')
	{
		redirect('user/teams');
	}
	
	
	
	///===roles===
	
	function wages($offset=0,$msg='')
	{
	
		$this->load->library('pagination');

		$limit = '15';
		
		$config['base_url'] = site_url('user/wages/');
		$config['total_rows'] = $this->user_model->get_total_wage_count();
		$config['per_page'] = $limit;		
		$this->pagination->initialize($config);		
		$data['page_link'] = $this->pagination->create_links();
		$data['result'] = $this->user_model->get_all_wage($offset, $limit);
		$data['msg'] = $msg;
		$data['offset'] = $offset;
		$this->template->write_view('header','common/header',$data,TRUE);
		$this->template->write_view('content','team/list_wage',$data,TRUE);
		$this->template->write_view('sidebar','common/sidebar',$data,TRUE);
		$this->template->write_view('footer','common/footer',$data,TRUE);
		$this->template->render();		
		
	
	}
	
	
	function add_wage()
	{
	
		
		$this->form_validation->set_rules('team_id', 'Team / Employee', 'required');
		$this->form_validation->set_rules('wage', 'Wage', 'required|numeric');
		$this->form_validation->set_rules('burden', 'Burden', 'required|numeric');
		$this->form_validation->set_rules('beginwage', 'Begin Date', 'required');
		$this->form_validation->set_rules('endwage', 'End Date', 'required');
	
		if($this->form_validation->run() == FALSE){			
			if(validation_errors())
			{
				$data["error"] = validation_errors();
			}else{
				$data["error"] = "";
			}
			$data["wage_id"] = $this->input->post('wage_id');
			$data["team_id"] = $this->input->post('team_id');
			$data["wage"] = $this->input->post('wage');
			$data["burden"] = $this->input->post('burden');
			$data["beginwage"] = $this->input->post('beginwage');
			$data["endwage"] = $this->input->post('endwage');
			
			if($this->input->post('offset')=="")
			{
				$limit = '15';
				$totalRows = $this->user_model->get_total_wage_count();
				$data["offset"] = (int)($totalRows/$limit)*$limit;
			}else{
				$data["offset"] = $this->input->post('offset');
			}
				
				$this->template->write_view('header','common/header',$data,TRUE);
				$this->template->write_view('content','team/add_wage',$data,TRUE);
				$this->template->write_view('sidebar','common/sidebar',$data,TRUE);
				$this->template->write_view('footer','common/footer',$data,TRUE);
				$this->template->render();
		}else{
			if($this->input->post('wage_id'))
			{
				$this->user_model->wage_update();
				$msg = "update";
			}else{
				$this->user_model->wage_insert();
				$msg = "insert";
			}
			$offset = $this->input->post('offset');
			redirect('user/wages/'.$offset.'/'.$msg);
		}				
		
		
	
	}
	
	
	function edit_wage($id,$offset=0)
	{
		
		
		$wage_detail=$this->user_model->get_wage_detail($id);
		
		if(empty($wage_detail)) { redirect('user/wages/'.$offset.'/notfound'); }
		
		$data['wage_id']=$id;
		
		$data["team_id"] = $wage_detail->team_id;
		$data["wage"] = $wage_detail->wage;
		$data["burden"] = $wage_detail->burden;
		$data["beginwage"] = $wage_detail->beginwage;
		$data["endwage"] = $wage_detail->endwage;		
		
		$data['offset']=$offset;
		$data['error']='';
		
		
		$this->template->write_view('header','common/header',$data,TRUE);
		$this->template->write_view('content','team/add_wage',$data,TRUE);
		$this->template->write_view('sidebar','common/sidebar',$data,TRUE);
		$this->template->write_view('footer','common/footer',$data,TRUE);
		$this->template->render();
	
	}
	
	
	function wage_action()
	{
		
		
		$offset=$this->input->post('offset');
		$action=$this->input->post('action');
                
                $action_name='';             
                
                if($this->input->post('action_from')!=''){
                    $action_name=$this->input->post('action_from');
                }
                
                $action_id=0;
                if($this->input->post('action_id')>0){
                    $action_id=$this->input->post('action_id');
                }
                
                
                
		$wage_id=$this->input->post('chk');
		
		if($action=='delete')
		{	
			foreach($wage_id as $id)
			{
				$this->db->delete('wagehistory',array('wage_id'=>$id));
			}
			
                        if($action_name!='' && $action_id>0){
                            redirect('user/'.$action_name.'/'.$action_id.'/'.$offset.'/deletewage');
                        } else {
                            redirect('user/wages/'.$offset.'/delete');
                        }
		}
		
	}
	
	
	
	///===roles===
	
	function roles($offset=0,$msg='')
	{
	
		$this->load->library('pagination');

		$limit = '15';
		
		$config['base_url'] = site_url('user/roles/');
		$config['total_rows'] = $this->user_model->get_total_role_count();
		$config['per_page'] = $limit;		
		$this->pagination->initialize($config);		
		$data['page_link'] = $this->pagination->create_links();
		$data['result'] = $this->user_model->get_all_role($offset, $limit);
		$data['msg'] = $msg;
		$data['offset'] = $offset;
		$this->template->write_view('header','common/header',$data,TRUE);
		$this->template->write_view('content','team/list_role',$data,TRUE);
		$this->template->write_view('sidebar','common/sidebar',$data,TRUE);
		$this->template->write_view('footer','common/footer',$data,TRUE);
		$this->template->render();		
		
	
	}
	
	
	function add_role()
	{
	
		
		$this->form_validation->set_rules('role', 'Name', 'required|alpha_space|min_length[3]|max_length[50]');
	
		if($this->form_validation->run() == FALSE){			
			if(validation_errors())
			{
				$data["error"] = validation_errors();
			}else{
				$data["error"] = "";
			}
			$data["role_id"] = $this->input->post('role_id');
			$data["role"] = $this->input->post('role');
			
			if($this->input->post('offset')=="")
			{
				$limit = '15';
				$totalRows = $this->user_model->get_total_role_count();
				$data["offset"] = (int)($totalRows/$limit)*$limit;
			}else{
				$data["offset"] = $this->input->post('offset');
			}
				
				$this->template->write_view('header','common/header',$data,TRUE);
				$this->template->write_view('content','team/add_role',$data,TRUE);
				$this->template->write_view('sidebar','common/sidebar',$data,TRUE);
				$this->template->write_view('footer','common/footer',$data,TRUE);
				$this->template->render();
		}else{
			if($this->input->post('role_id'))
			{
				$this->user_model->role_update();
				$msg = "update";
			}else{
				$this->user_model->role_insert();
				$msg = "insert";
			}
			$offset = $this->input->post('offset');
			redirect('user/roles/'.$offset.'/'.$msg);
		}				
		
		
	
	}
	
	
	function edit_role($id,$offset=0)
	{
		
		
		$role_detail=$this->user_model->get_role_detail($id);
		
		if(empty($role_detail)) { redirect('user/roles/'.$offset.'/notfound'); }
		
		$data['role_id']=$id;
		
		$data["role"] = $role_detail->role;
		
		
		$data['offset']=$offset;
		$data['error']='';
		
		
		$this->template->write_view('header','common/header',$data,TRUE);
		$this->template->write_view('content','team/add_role',$data,TRUE);
		$this->template->write_view('sidebar','common/sidebar',$data,TRUE);
		$this->template->write_view('footer','common/footer',$data,TRUE);
		$this->template->render();
	
	}
	
	
	function role_action($role_id,$action,$offset=0)
	{
		
		
		if($action=='delete')
		{	
			
			$check_role_use=$this->db->query("select * from ".$this->db->dbprefix('team')." where role1='".$role_id."' or role2='".$role_id."'");
			
			if($check_role_use->num_rows()>0) { 
				$msg="cannot";
			} else { 
				$this->user_model->delete_role($role_id);
				$msg="delete";
			}
			
			
			redirect('user/roles/'.$offset.'/'.$msg);
		}
		
	}
	
	
	///====rights access===
	
	
	function assign_rights($id,$offset=0)
	{
		
		
		if($id=='')
		{
			redirect('user/teams');
		}
		
			
			$data['team_id']=$id;
			$data['offset']=$offset;
		
			$data['assign_rights']=$this->user_model->get_assign_rights($id);
			$data['rights']=$this->user_model->get_rights();	
			
			$team_detail=$this->user_model->get_team_detail($id);
		
		
			$data["team_detail"] = $team_detail;
				
		

			
			$this->template->write_view('header','common/header',$data,TRUE);
			$this->template->write_view('content','team/assign_rights',$data,TRUE);
			$this->template->write_view('sidebar','common/sidebar',$data,TRUE);
			$this->template->write_view('footer','common/footer',$data,TRUE);
			$this->template->render();
		
	}
	
	function add_rights($id)
	{
		
		$this->user_model->add_rights();
		
		redirect('user/teams/'.$this->input->post('offset').'/rights');
		
	
	}
	
		
	//////////////================front user===============
	

	function team_login($offset=0,$msg='')
	{
	
		$this->load->library('pagination');

		$limit = '15';
		
		$config['base_url'] = site_url('user/team_login/');
		$config['total_rows'] = $this->user_model->get_total_team_login_count();
		$config['per_page'] = $limit;		
		$this->pagination->initialize($config);		
		$data['page_link'] = $this->pagination->create_links();
		$data['result'] = $this->user_model->get_all_team_login($offset, $limit);
		$data['msg'] = $msg;
		$data['offset'] = $offset;
		$this->template->write_view('header','common/header',$data,TRUE);
		$this->template->write_view('content','team/list_team_login',$data,TRUE);
		$this->template->write_view('sidebar','common/sidebar',$data,TRUE);
		$this->template->write_view('footer','common/footer',$data,TRUE);
		$this->template->render();		
		
	
	}
	
	function team_login_action()
	{
		
		$offset=$this->input->post('offset');
		$action=$this->input->post('action');
		$team_login_id=$this->input->post('chk');
		
		if($action=='delete')
		{	
			foreach($team_login_id as $id)
			{
				$this->db->delete('team_login',array('login_id'=>$id));
			}
			
			redirect('user/team_login/'.$offset.'/delete');
		}
				
	}
	
        
        function searchteam($option,$keyword,$offset=0,$msg='')
	{
	
		$this->load->library('pagination');

		$limit = '15';
                
                $option=trim($option);
                $keyword=trim($keyword);
                
                
                 $config['uri_segment'] = 5;
		
		$config['base_url'] = site_url('user/searchteam/'.$option.'/'.$keyword);
		$config['total_rows'] = $this->user_model->get_total_team_count($option,$keyword);
		$config['per_page'] = $limit;		
		$this->pagination->initialize($config);		
		$data['page_link'] = $this->pagination->create_links();
		$data['result'] = $this->user_model->get_all_team($offset, $limit,$option,$keyword);
		$data['msg'] = $msg;
		$data['offset'] = $offset;
                
                $data['option']=$option;
                $data['keyword']=$keyword;
                
		$this->template->write_view('header','common/header',$data,TRUE);
		$this->template->write_view('content','team/list_team',$data,TRUE);
		$this->template->write_view('sidebar','common/sidebar',$data,TRUE);
		$this->template->write_view('footer','common/footer',$data,TRUE);
		$this->template->render();		
		
	
	}
	
	
	
	function teams($offset=0,$msg='')
	{
	
		$this->load->library('pagination');

		$limit = '15';
                
                $option='';
                $keyword='';
		
		$config['base_url'] = site_url('user/teams/');
		$config['total_rows'] = $this->user_model->get_total_team_count($option,$keyword);
		$config['per_page'] = $limit;		
		$this->pagination->initialize($config);		
		$data['page_link'] = $this->pagination->create_links();
		$data['result'] = $this->user_model->get_all_team($offset, $limit,$option,$keyword);
		$data['msg'] = $msg;
		$data['offset'] = $offset;
                
                $data['option']=$option;
                $data['keyword']=$keyword;
                
                
		$this->template->write_view('header','common/header',$data,TRUE);
		$this->template->write_view('content','team/list_team',$data,TRUE);
		$this->template->write_view('sidebar','common/sidebar',$data,TRUE);
		$this->template->write_view('footer','common/footer',$data,TRUE);
		$this->template->render();		
		
	
	}
	
	
	
	function email_check($email)
	{
		$chk_email = $this->user_model->email_unique($email);
		if($chk_email == TRUE)
		{
			return TRUE;
		}
		else
		{
			$this->form_validation->set_message('email_check', 'There is an existing account associated with this email address.');
			return FALSE;
		}
	}	
	
	function username_check($username)
	{
		$username = $this->user_model->user_unique($username);
		if($username == TRUE)
		{
			return TRUE;
		}
		else
		{
			$this->form_validation->set_message('username_check', 'There is an existing account associated with this username.');
			return FALSE;
		}
	}	
	
	
	function add_team()
	{
		
                $wage_history=array();
		if($this->input->post('team_id')>0) {
                    $wage_history=$this->user_model->get_team_all_wage($this->input->post('team_id'));
                }
                
                $data['wage_history']=$wage_history;
                
                $data['msg']='';
                
		
		$this->form_validation->set_rules('email', 'Email', 'required|valid_email|callback_email_check');
		$this->form_validation->set_rules('username', 'User Name', 'required|alpha|callback_username_check');
		
		$this->form_validation->set_rules('name', 'Name', 'required|alpha_space|min_length[3]|max_length[50]');

		if($this->input->post('team_id')>0) {
			
			if($this->input->post('password')!='') { $this->form_validation->set_rules('password', 'Password', 'required|min_length[8]|max_length[15]'); }
		} else {
			$this->form_validation->set_rules('password', 'Password', 'required|min_length[8]|max_length[15]');
		}
		$this->form_validation->set_rules('role1', 'Primary Role', 'required');
		
		$this->form_validation->set_rules('active', 'Status', 'required');	
	
		if($this->form_validation->run() == FALSE){			
			if(validation_errors())
			{
				$data["error"] = validation_errors();
			}else{
				$data["error"] = "";
			}
			$data["team_id"] = $this->input->post('team_id');
			$data["email"] = $this->input->post('email');
			$data["username"] = $this->input->post('username');
			$data["name"] = $this->input->post('name');
			$data["password"] = $this->input->post('password');
			$data["ex_password"] = $this->input->post('ex_password');
			$data["role1"] = $this->input->post('role1');
			$data["role2"] = $this->input->post('role2');
			
			$data["begin_date"] = $this->input->post('begin_date');
			$data["end_date"] = $this->input->post('end_date');
			
			$data["active"] = $this->input->post('active');
			
			if($this->input->post('offset')=="")
			{
				$limit = '15';
				$totalRows = $this->user_model->get_total_team_count();
				$data["offset"] = (int)($totalRows/$limit)*$limit;
			}else{
				$data["offset"] = $this->input->post('offset');
			}
				
				$this->template->write_view('header','common/header',$data,TRUE);
				$this->template->write_view('content','team/add_team',$data,TRUE);
				$this->template->write_view('sidebar','common/sidebar',$data,TRUE);
				$this->template->write_view('footer','common/footer',$data,TRUE);
				$this->template->render();
		}else{
			if($this->input->post('team_id'))
			{
				$this->user_model->team_update();
				$msg = "update";
			}else{
				$this->user_model->team_insert();
				$msg = "insert";
			}
			$offset = $this->input->post('offset');
			redirect('user/teams/'.$offset.'/'.$msg);
		}				
		
		
	
	}
	
	
	function edit_team($id,$offset=0,$msg='')
	{
		
		
		$admin_detail=$this->user_model->get_team_detail($id);
		
		if(empty($admin_detail)) { redirect('user/teams/'.$offset.'/notfound'); }
		
		$data['team_id']=$id;
		
		
		$data["email"] = $admin_detail->email;
		$data["username"] = $admin_detail->username;
		$data["name"] = $admin_detail->name;
		$data["role1"] = $admin_detail->role1;
		$data["role2"] = $admin_detail->role2;
		$data["begin_date"] = $admin_detail->begin_date;
		$data["end_date"] = $admin_detail->end_date;
		$data["password"] = '';
		$data["ex_password"] = $admin_detail->password;
		$data["active"] = $admin_detail->active;
		
		$data['offset']=$offset;
		$data['error']='';
                $data['msg']=$msg;
                
                
                $data['wage_history']=$this->user_model->get_team_all_wage($id);
                
		
		
		$this->template->write_view('header','common/header',$data,TRUE);
		$this->template->write_view('content','team/add_team',$data,TRUE);
		$this->template->write_view('sidebar','common/sidebar',$data,TRUE);
		$this->template->write_view('footer','common/footer',$data,TRUE);
		$this->template->render();
	
	}
	
	
	function team_action()
	{
		
		$offset=$this->input->post('offset');
		$action=$this->input->post('action');
		$team_id=$this->input->post('chk');
		
	
		
		if($action=='delete')
		{	
			foreach($team_id as $id)
			{
				$this->user_model->delete_team($id);
			}
			
			redirect('user/teams/'.$offset.'/delete');
		}
		
		if($action=='active')
		{	
			foreach($team_id as $id)
			{
				$this->db->query("update ".$this->db->dbprefix('team')." set active=1 where team_id='".$id."'");				
			}
			
			redirect('user/teams/'.$offset.'/active');
		}
		
		if($action=='inactive')
		{
			foreach($team_id as $id)
			{
				$this->db->query("update ".$this->db->dbprefix('team')." set active=0 where team_id='".$id."'");			
			}
			
			redirect('user/teams/'.$offset.'/inactive');
		}
		
		
		
	}
	
}
?>