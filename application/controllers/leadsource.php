<?php
class Leadsource extends IWEB_Controller {

	function Leadsource()
	{
		parent::__construct();
		$this->load->model('leadsource_model');	 
                
                 if(!check_role_permission('manage_leadsource')) { 
                    redirect('home/dashboard/notauthorize');
                }
	}
	
	
	
	function index($msg='')
	{
		redirect('leadsource/manage');
	}
	
	

	///===manage===
	
	function manage($offset=0,$msg='')
	{
		$this->load->library('pagination');

		$limit = '15';
		
		$config['base_url'] = site_url('leadsource/manage/');
		$config['total_rows'] = $this->leadsource_model->get_total_leadsource_count();
		$config['per_page'] = $limit;		
		$this->pagination->initialize($config);		
		$data['page_link'] = $this->pagination->create_links();
		$data['result'] = $this->leadsource_model->get_all_leadsource($offset, $limit);
		$data['msg'] = $msg;
		$data['offset'] = $offset;
		$this->template->write_view('header','common/header',$data,TRUE);
		$this->template->write_view('content','leadsource/list_leadsource',$data,TRUE);
		$this->template->write_view('sidebar','common/sidebar',$data,TRUE);
		$this->template->write_view('footer','common/footer',$data,TRUE);
		$this->template->render();		
	}	
	
	
	function add_lead()
	{
		$this->form_validation->set_rules('leadsource', 'Name', 'required|alpha_space|min_length[3]|max_length[50]');
		
		if($this->input->post('quantity')!='') {
			$this->form_validation->set_rules('quantity', 'Quantity', 'numeric|max_length[10]');
		}
		if($this->input->post('design')!='') {
			$this->form_validation->set_rules('design', 'Design Cost', 'numeric|max_length[10]');
		}
		if($this->input->post('printing')!='') {
			$this->form_validation->set_rules('printing', 'Printing Cost', 'numeric|max_length[10]');
		}
		if($this->input->post('distribution_cost')!='') {
			$this->form_validation->set_rules('distribution_cost', 'Distribution Cost', 'numeric|max_length[10]');
		}
		
		
		if($this->form_validation->run() == FALSE){			
			if(validation_errors())
			{
				$data["error"] = validation_errors();
			}else{
				$data["error"] = "";
			}
			$data["leadsource_id"] = $this->input->post('leadsource_id');
			$data["leadsource"] = $this->input->post('leadsource');			
			$data["beginpgmdate"] = $this->input->post('beginpgmdate');
			$data["endpgmdate"] = $this->input->post('endpgmdate');
			$data["leadsourcetype_id"] = $this->input->post('leadsourcetype_id');
			$data["design"] = $this->input->post('design');
			$data["quantity"] = $this->input->post('quantity');
			$data["printing"] = $this->input->post('printing');
			$data["distribution_cost"] = $this->input->post('distribution_cost');
			
			if($this->input->post('offset')=="")
			{
				$limit = '15';
				$totalRows = $this->leadsource_model->get_total_leadsource_count();
				$data["offset"] = (int)($totalRows/$limit)*$limit;
			}else{
				$data["offset"] = $this->input->post('offset');
			}
				
				$this->template->write_view('header','common/header',$data,TRUE);
				$this->template->write_view('content','leadsource/add_lead',$data,TRUE);
				$this->template->write_view('sidebar','common/sidebar',$data,TRUE);
				$this->template->write_view('footer','common/footer',$data,TRUE);
				$this->template->render();
		}else{
			if($this->input->post('leadsource_id'))
			{
				$this->leadsource_model->lead_update();
				$msg = "update";
			}else{
				$this->leadsource_model->lead_insert();
				$msg = "insert";
			}
			$offset = $this->input->post('offset');
			redirect('leadsource/manage/'.$offset.'/'.$msg);
		}				
		
		
	
	}
	
	
	function edit_lead($id,$offset=0)
	{
		
		$type_detail=$this->leadsource_model->get_lead_detail($id);
		
		if(empty($type_detail)) { redirect('leadsource/manage/'.$offset.'/notfound'); }
		
		$data['leadsource_id']=$id;
		
		$data["leadsource"] = $type_detail->leadsource;
		$data["beginpgmdate"] = $type_detail->beginpgmdate;
		$data["endpgmdate"] = $type_detail->endpgmdate;
		$data["leadsourcetype_id"] = $type_detail->leadsourcetype_id;
		$data["design"] = $type_detail->design;
		$data["quantity"] = $type_detail->quantity;
		$data["printing"] = $type_detail->printing;
		$data["distribution_cost"] = $type_detail->distribution_cost;
		
		
		
		$data['offset']=$offset;
		$data['error']='';
		
		
		$this->template->write_view('header','common/header',$data,TRUE);
		$this->template->write_view('content','leadsource/add_lead',$data,TRUE);
		$this->template->write_view('sidebar','common/sidebar',$data,TRUE);
		$this->template->write_view('footer','common/footer',$data,TRUE);
		$this->template->render();
	
	}
	
	
	function lead_action()
	{
		$offset=$this->input->post('offset');
		$action=$this->input->post('action');
		$leadsource_id=$this->input->post('chk');
		
		if($action=='delete')
		{	
			foreach($leadsource_id as $id)
			{
				$this->leadsource_model->delete_lead($id);
			}
			
			redirect('leadsource/manage/'.$offset.'/delete');
		}
		
	}
	
	
	
	///===managetype===
	
	function managetype($offset=0,$msg='')
	{
		$this->load->library('pagination');

		$limit = '15';
		
		$config['base_url'] = site_url('leadsource/managetype/');
		$config['total_rows'] = $this->leadsource_model->get_total_type_count();
		$config['per_page'] = $limit;		
		$this->pagination->initialize($config);		
		$data['page_link'] = $this->pagination->create_links();
		$data['result'] = $this->leadsource_model->get_all_type($offset, $limit);
		$data['msg'] = $msg;
		$data['offset'] = $offset;
		$this->template->write_view('header','common/header',$data,TRUE);
		$this->template->write_view('content','leadsource/list_type',$data,TRUE);
		$this->template->write_view('sidebar','common/sidebar',$data,TRUE);
		$this->template->write_view('footer','common/footer',$data,TRUE);
		$this->template->render();		
		
	
	}
	
	
	function add_type()
	{
		$this->form_validation->set_rules('leadsourcetype', 'Name', 'required|alpha_space|min_length[3]|max_length[50]');
	
		if($this->form_validation->run() == FALSE){			
			if(validation_errors())
			{
				$data["error"] = validation_errors();
			}else{
				$data["error"] = "";
			}
			$data["leadsourcetype_id"] = $this->input->post('leadsourcetype_id');
			$data["leadsourcetype"] = $this->input->post('leadsourcetype');
			
			if($this->input->post('offset')=="")
			{
				$limit = '15';
				$totalRows = $this->leadsource_model->get_total_type_count();
				$data["offset"] = (int)($totalRows/$limit)*$limit;
			}else{
				$data["offset"] = $this->input->post('offset');
			}
				
				$this->template->write_view('header','common/header',$data,TRUE);
				$this->template->write_view('content','leadsource/add_type',$data,TRUE);
				$this->template->write_view('sidebar','common/sidebar',$data,TRUE);
				$this->template->write_view('footer','common/footer',$data,TRUE);
				$this->template->render();
		}else{
			if($this->input->post('leadsourcetype_id'))
			{
				$this->leadsource_model->type_update();
				$msg = "update";
			}else{
				$this->leadsource_model->type_insert();
				$msg = "insert";
			}
			$offset = $this->input->post('offset');
			redirect('leadsource/managetype/'.$offset.'/'.$msg);
		}				
		
		
	
	}
	
	
	function edit_type($id,$offset=0)
	{
		
		$type_detail=$this->leadsource_model->get_type_detail($id);
		
		if(empty($type_detail)) { redirect('leadsource/managetype/'.$offset.'/notfound'); }
		
		$data['leadsourcetype_id']=$id;
		
		$data["leadsourcetype"] = $type_detail->leadsourcetype;
		
		
		$data['offset']=$offset;
		$data['error']='';
		
		
		$this->template->write_view('header','common/header',$data,TRUE);
		$this->template->write_view('content','leadsource/add_type',$data,TRUE);
		$this->template->write_view('sidebar','common/sidebar',$data,TRUE);
		$this->template->write_view('footer','common/footer',$data,TRUE);
		$this->template->render();
	
	}
	
	
	function type_action()
	{
		$offset=$this->input->post('offset');
		$action=$this->input->post('action');
		$leadsourcetype_id=$this->input->post('chk');
		
		if($action=='delete')
		{	
			foreach($leadsourcetype_id as $id)
			{
				$this->leadsource_model->delete_type($id);
			}
			
			redirect('leadsource/managetype/'.$offset.'/delete');
		}
		
	}
	
	
}
?>