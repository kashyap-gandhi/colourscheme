<?php
class Setting extends IWEB_Controller {

	function Setting()
	{
		parent::__construct();	
		$this->load->model('setting_model');
                
                if(!check_role_permission('system_setting')) { 
                    redirect('home/dashboard/notauthorize');
                }
                
                
	}
	
	
	function index()
	{
		redirect('setting/site');	
	}
	
	
	function edit_email_template($id,$offset=0)
	{
            
                if(!check_role_permission('email_templates_setting')) { 
                    redirect('home/dashboard/notauthorize');
                }
            
            
            
			if($id=='' || $id==0) {			
				redirect('setting/email_template');
			}
		
			$email_template = $this->setting_model->get_one_email_template($id);
			
			if(!$email_template) {
				redirect('setting/email_template');			
			}
			
			$data["error"] = "";
			$data["offset"] = $offset;
			$data["email_template_id"] = $email_template->email_template_id;
			$data["from_name"] = $email_template->from_name;
			$data["from_address"] = $email_template->from_address;
			$data["reply_address"] = $email_template->reply_address;
			$data["subject"] = $email_template->subject;
			$data["message"] = $email_template->message;
			$data["task"] = $email_template->task;	
			
			$this->template->write_view('header','common/header',$data,TRUE);
			$this->template->write_view('content','setting/add_email_template',$data,TRUE);
			$this->template->write_view('sidebar','common/sidebar',$data,TRUE);
			$this->template->write_view('footer','common/footer',$data,TRUE);
			$this->template->render();
	
	
	}
	
	function add_email_template($id=0)
	{
            
            
             if(!check_role_permission('email_templates_setting')) { 
                    redirect('home/dashboard/notauthorize');
                }
                
                
		
		$email_template = $this->setting_model->get_one_email_template($id);
		$data["offset"] = $this->input->post('offset');
		
		
		$this->form_validation->set_rules('from_name', 'From Name', 'required');
		$this->form_validation->set_rules('from_address', 'From Address', 'required|valid_email');
		$this->form_validation->set_rules('reply_address', 'Reply Address', 'required|valid_email');
		$this->form_validation->set_rules('subject', 'Subject', 'required');
		$this->form_validation->set_rules('message', 'Message', 'required');
		if($this->form_validation->run() == FALSE){			
			if(validation_errors())
			{
				$data["error"] = validation_errors();
			}else{
				$data["error"] = "";
			}
			if($this->input->post('email_template_id'))
			{
				$data["email_template_id"] = $this->input->post('email_template_id');
				$data["from_name"] = $this->input->post('from_name');
				$data["from_address"] = $this->input->post('from_address');
				$data["reply_address"] = $this->input->post('reply_address');
				$data["subject"] = $this->input->post('subject');
				$data["message"] = $this->input->post('message');
				$data["task"] = $this->input->post('task');
				
			}else{
				
				$data["email_template_id"] = $email_template->email_template_id;
				$data["from_name"] = $email_template->from_name;
				$data["from_address"] = $email_template->from_address;
				$data["reply_address"] = $email_template->reply_address;
				$data["subject"] = $email_template->subject;
				$data["message"] = $email_template->message;
				$data["task"] = $email_template->task;
			}		
			
		}else{		
			
			if($this->input->post('email_template_id'))
			{
				$this->setting_model->email_template_update();
				$msg='update';
			} else {
				$this->setting_model->email_template_insert();
				$msg='insert';
			}
			
			redirect('setting/email_template/'.$this->input->post('offset').'/'.$msg);
			
		}		
			$this->template->write_view('header','common/header',$data,TRUE);
			$this->template->write_view('content','setting/add_email_template',$data,TRUE);
			$this->template->write_view('sidebar','common/sidebar',$data,TRUE);
			$this->template->write_view('footer','common/footer',$data,TRUE);
			$this->template->render();	
					
	}	
	
	function email_template($offset=0,$msg='')
	{
            
            
             if(!check_role_permission('email_templates_setting')) { 
                    redirect('home/dashboard/notauthorize');
                }
                
                
	
		$this->load->library('pagination');

		$limit = '15';
		
		$config['base_url'] = site_url('setting/email_template/');
		$config['total_rows'] = $this->setting_model->get_total_email_template_count();
		$config['per_page'] = $limit;		
		$this->pagination->initialize($config);		
		$data['page_link'] = $this->pagination->create_links();
		$data['result'] = $this->setting_model->get_all_email_template($offset, $limit);
		$data['msg'] = $msg;
		$data['offset'] = $offset;
		$this->template->write_view('header','common/header',$data,TRUE);
		$this->template->write_view('content','setting/list_email_template',$data,TRUE);
		$this->template->write_view('sidebar','common/sidebar',$data,TRUE);
		$this->template->write_view('footer','common/footer',$data,TRUE);
		$this->template->render();
	}
	
	function email()
	{
		
            
             if(!check_role_permission('email_setting')) { 
                    redirect('home/dashboard/notauthorize');
                }
                
                
		
		$email_setting = $this->setting_model->get_email_setting();
		$data["email_setting_id"] = $email_setting->email_setting_id;
		$data["mailer"] = $email_setting->mailer;
		$data["sendmail_path"] = $email_setting->sendmail_path;
		$data["smtp_port"] = $email_setting->smtp_port;
		$data["smtp_host"] = $email_setting->smtp_host;
		$data["smtp_email"] = $email_setting->smtp_email;
		$data["smtp_password"] = $email_setting->smtp_password;
				
		$data['msg']='';
				
		$this->form_validation->set_rules('mailer', 'Mailer Type', 'required');
		
		if($this->input->post('mailer')=='sendmail')
		{		
			$this->form_validation->set_rules('sendmail_path', 'Sendmail Path', 'required');
		}

		if($this->input->post('mailer')=='smtp')
		{
			$this->form_validation->set_rules('smtp_port', 'Smtp Port', 'required|is_natural_no_zero');
			$this->form_validation->set_rules('smtp_host', 'Smtp Host', 'required');
			$this->form_validation->set_rules('smtp_email', 'Smtp Email', 'required|valid_email');
			$this->form_validation->set_rules('smtp_password', 'Smtp Password', 'required');
		}
		
		
		if($this->form_validation->run() == FALSE)
		{			
			if(validation_errors())
			{
				$data["error"] = validation_errors();
			}else{
				$data["error"] = "";
			}
			if($this->input->post('email_setting_id'))
			{
				$data["email_setting_id"] = $this->input->post('email_setting_id');
				$data["mailer"] = $this->input->post('mailer');
				$data["sendmail_path"] = $this->input->post('sendmail_path');
				$data["smtp_port"] = $this->input->post('smtp_port');
				$data["smtp_host"] = $this->input->post('smtp_host');
				$data["smtp_email"] = $this->input->post('smtp_email');
				$data["smtp_password"] = $this->input->post('smtp_password');
			}
						
			
		}
		
		else
		{
			
			$update=$this->setting_model->update_email_setting();
			$data['msg']='Email Setting has been updated successfully.';
			$data['error']='';
			
			$data["email_setting_id"] = $this->input->post('email_setting_id');
			$data["mailer"] = $this->input->post('mailer');
			$data["sendmail_path"] = $this->input->post('sendmail_path');
			$data["smtp_port"] = $this->input->post('smtp_port');
			$data["smtp_host"] = $this->input->post('smtp_host');
			$data["smtp_email"] = $this->input->post('smtp_email');
			$data["smtp_password"] = $this->input->post('smtp_password');
			
		
		}		
		
			$this->template->write_view('header','common/header',$data,TRUE);
			$this->template->write_view('content','setting/email',$data,TRUE);
			$this->template->write_view('sidebar','common/sidebar',$data,TRUE);
			$this->template->write_view('footer','common/footer',$data,TRUE);
			$this->template->render();
	
	}
	
	
	
	
	function site()
	{
            
            
              if(!check_role_permission('site_setting')) { 
                    redirect('home/dashboard/notauthorize');
                }
		
		$site_setting = $this->setting_model->get_site_setting();
		$data["site_id"] = $site_setting->site_setting_id;
		$data["site_name"] = $site_setting->site_name;
		$data["currency_symbol"] = $site_setting->currency_symbol;
		$data["currency_code"] = $site_setting->currency_code;
		$data["date_format"] = $site_setting->date_format;
		$data["time_format"] = $site_setting->time_format;				
		$data["date_time_format"] =$site_setting->date_time_format;
		$data["site_timezone"] =$site_setting->site_timezone;
		$data["site_version"] = $site_setting->site_version;
		
		$data['msg']='';
				
				
		$this->form_validation->set_rules('site_name', 'Site Name', 'required');
		$this->form_validation->set_rules('currency_symbol', 'Currency Symbol', 'required');
		$this->form_validation->set_rules('currency_code', 'Currency Code', 'required');
		$this->form_validation->set_rules('date_format', 'Date Format', 'required');
		$this->form_validation->set_rules('time_format', 'Time Format', 'required');
		$this->form_validation->set_rules('date_time_format', 'DateTime Format', 'required');
		$this->form_validation->set_rules('site_timezone', 'Site Timezone', 'required');
		
		
		
		
		if($this->form_validation->run() == FALSE)
		{			
			if(validation_errors())
			{
				$data["error"] = validation_errors();
			}else{
				$data["error"] = "";
			}
			
			
			if($this->input->post('site_id'))
			{
				
				$data["site_name"] = $this->input->post('site_name');
				$data["currency_symbol"] = $this->input->post('currency_symbol');
				$data["currency_code"] = $this->input->post('currency_code');
				$data["date_format"] = $this->input->post('date_format');
				$data["time_format"] = $this->input->post('time_format');
				$data["date_time_format"] = $this->input->post('date_time_format');
				$data["site_timezone"] = $this->input->post('site_timezone');
				
				
			}
			
		
			
			
			
		}
		
		else
		{
		
			
			$update=$this->setting_model->update_site_setting();
			$data['msg']='Site Setting has been updated successfully.';
			$data['error']='';
			
			$data["site_name"] = $this->input->post('site_name');
			$data["currency_symbol"] = $this->input->post('currency_symbol');
			$data["currency_code"] = $this->input->post('currency_code');
			$data["date_format"] = $this->input->post('date_format');
			$data["time_format"] = $this->input->post('time_format');
			$data["date_time_format"] = $this->input->post('date_time_format');
			$data["site_timezone"] = $this->input->post('site_timezone');
			
		
		
		}		
		
			$this->template->write_view('header','common/header',$data,TRUE);
			$this->template->write_view('sidebar','common/sidebar',$data,TRUE);
			$this->template->write_view('content','setting/site',$data,TRUE);			
			$this->template->write_view('footer','common/footer',$data,TRUE);
			$this->template->render();
	
	}
	
	
}

?>