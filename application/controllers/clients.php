<?php
class Clients extends IWEB_Controller {

	function Clients()
	{
		parent::__construct();
		$this->load->model('clients_model');	
		$this->load->model('quotes_model');	
                
                if(!check_role_permission('manage_client')) { 
                    redirect('home/dashboard/notauthorize');
                }
		
	}
	
	
	function index($msg='')
	{
		redirect('clients/manage');
	}
	
	
	function quotes($client_id,$offset=0,$msg='')
	{
		$client_detail=$this->clients_model->get_client_detail($client_id);
		
		if(empty($client_detail)) { redirect('clients/manage/'.$offset.'/notfound'); }
		
		$data['client_id']=$client_id;
		$data['client_detail']=$client_detail;
		
		
		$this->load->library('pagination');

		$limit = '15';
		
		$config['base_url'] = site_url('quotes/quotes/');
		$config['total_rows'] = $this->clients_model->get_total_quote_count($client_id);
		$config['per_page'] = $limit;		
		$this->pagination->initialize($config);		
		$data['page_link'] = $this->pagination->create_links();
		$data['result'] = $this->clients_model->get_all_quote($offset, $limit,$client_id);
		$data['msg'] = $msg;
		$data['offset'] = $offset;
		
		$this->template->write_view('header','common/header',$data,TRUE);
		$this->template->write_view('content','clients/list_quote',$data,TRUE);
		$this->template->write_view('sidebar','common/sidebar',$data,TRUE);
		$this->template->write_view('footer','common/footer',$data,TRUE);
		$this->template->render();
	}
	
	
	function quote_action($client_id)
	{
		$offset=$this->input->post('offset');
		$action=$this->input->post('action');
		$quote_id=$this->input->post('chk');
		
		if($action=='delete')
		{	
			foreach($quote_id as $id)
			{
				$this->quotes_model->delete_quote($id);
			}
			
			redirect('clients/quotes/'.$client_id.'/'.$offset.'/delete');
		}
		
	}
	
	//====clients===============
	
	
        
        
	function search($option,$keyword,$offset=0,$msg='')
	{
		$this->load->library('pagination');

		$limit = '15';
                
                $option=trim($option);
                $keyword=trim($keyword);
                
                
                $config['uri_segment'] = 5;
		
		$config['base_url'] = site_url('clients/search/'.$option.'/'.$keyword);
		$config['total_rows'] = $this->clients_model->get_total_client_count($option,$keyword);
		$config['per_page'] = $limit;		
		$this->pagination->initialize($config);		
		$data['page_link'] = $this->pagination->create_links();
		$data['result'] = $this->clients_model->get_all_client($offset, $limit,$option,$keyword);
		$data['msg'] = $msg;
		$data['offset'] = $offset;
                
                $data['option']=$option;
                $data['keyword']=$keyword;
		
		$this->template->write_view('header','common/header',$data,TRUE);
		$this->template->write_view('content','clients/list_client',$data,TRUE);
		$this->template->write_view('sidebar','common/sidebar',$data,TRUE);
		$this->template->write_view('footer','common/footer',$data,TRUE);
		$this->template->render();
	}
	
        
        
	
	function manage($offset=0,$msg='')
	{
		$this->load->library('pagination');

		$limit = '15';
                
                $option='';
                $keyword='';
		
		$config['base_url'] = site_url('clients/manage/');
		$config['total_rows'] = $this->clients_model->get_total_client_count($option,$keyword);
		$config['per_page'] = $limit;		
		$this->pagination->initialize($config);		
		$data['page_link'] = $this->pagination->create_links();
		$data['result'] = $this->clients_model->get_all_client($offset, $limit,$option,$keyword);
		$data['msg'] = $msg;
		$data['offset'] = $offset;
                
                $data['option']=$option;
                $data['keyword']=$keyword;
                
		
		$this->template->write_view('header','common/header',$data,TRUE);
		$this->template->write_view('content','clients/list_client',$data,TRUE);
		$this->template->write_view('sidebar','common/sidebar',$data,TRUE);
		$this->template->write_view('footer','common/footer',$data,TRUE);
		$this->template->render();
	}
	
	function client_action()
	{
		
		$offset=$this->input->post('offset');
		$action=$this->input->post('action');
		$client_id=$this->input->post('chk');
		
		if($action=='delete')
		{	
			foreach($client_id as $id)
			{
				$this->clients_model->delete_client($id);
			}
			
			redirect('clients/manage/'.$offset.'/delete');
		}
		
	}
	

	function email_check($email)
	{
		$chk_email = $this->clients_model->email_unique($email);
		if($chk_email == TRUE)
		{
			return TRUE;
		}
		else
		{
			$this->form_validation->set_message('email_check', 'There is an existing account associated with this email address.');
			return FALSE;
		}
	}	
	
	function cellphone_check($number)
	{
		if($number!='') {
			$number = $this->clients_model->cellphone_unique($number);
			if($number == TRUE)
			{
				return TRUE;
			}
			else
			{
				$this->form_validation->set_message('cellphone_check', 'There is an existing account associated with this Cell Phone.');
				return FALSE;
			}
		} else {
			return TRUE;
		}
	}	
	
	
	function homephone_check($number)
	{
		if($number!='') {
			$number = $this->clients_model->homephone_unique($number);
			if($number == TRUE)
			{
				return TRUE;
			}
			else
			{
				$this->form_validation->set_message('homephone_check', 'There is an existing account associated with this Home Phone.');
				return FALSE;
			}
		} else {
			return TRUE;
		}
	}	
	
	
	
	function businessphone_check($number)
	{
		if($number!='') {
			$number = $this->clients_model->businessphone_unique($number);
			if($number == TRUE)
			{
				return TRUE;
			}
			else
			{
				$this->form_validation->set_message('businessphone_check', 'There is an existing account associated with this Business Phone.');
				return FALSE;
			}
		} else {
			return TRUE;
		}
	}	
	
	function faxphone_check($number)
	{
		if($number!='') {
			$number = $this->clients_model->faxphone_unique($number);
			if($number == TRUE)
			{
				return TRUE;
			}
			else
			{
				$this->form_validation->set_message('faxphone_check', 'There is an existing account associated with this Fax Phone.');
				return FALSE;
			}
		} else {
			return TRUE;
		}
	}
	
	
	function add_client()
	{
		$this->form_validation->set_rules('first_name', 'Company', 'required|company_check');
		$this->form_validation->set_rules('last_name', 'Contact', 'required|company_check');
		
		$this->form_validation->set_rules('email', 'Email', 'required|valid_email|callback_email_check');
		
		$this->form_validation->set_rules('cell_phone', 'Cell Phone', 'required|contact_check|callback_cellphone_check');		
		$this->form_validation->set_rules('home_phone', 'Home Phone', 'contact_check|callback_homephone_check');
		$this->form_validation->set_rules('business_phone', 'Business Phone', 'contact_check|callback_businessphone_check');		
		$this->form_validation->set_rules('fax', 'Fax', 'contact_check|callback_faxphone_check');
		
		
		$this->form_validation->set_rules('address', 'Address', 'required');
		$this->form_validation->set_rules('city', 'City', 'required');
		$this->form_validation->set_rules('province', 'Province', 'required');
		$this->form_validation->set_rules('postal_code', 'Postal Code', 'required');
		
	
		if($this->form_validation->run() == FALSE){			
			if(validation_errors())
			{
				$data["error"] = validation_errors();
			}else{
				$data["error"] = "";
			}
			$data["client_id"] = $this->input->post('client_id');
			
			$data["first_name"] = $this->input->post('first_name');
			$data["last_name"] = $this->input->post('last_name');
			
			$data["email"] = $this->input->post('email');
			
			$data["cell_phone"] = $this->input->post('cell_phone');
			$data["home_phone"] = $this->input->post('home_phone');
			$data["business_phone"] = $this->input->post('business_phone');
			$data["fax"] = $this->input->post('fax');
			
			
			$data["address"] = $this->input->post('address');
			$data["city"] = $this->input->post('city');			
			$data["province"] = $this->input->post('province');
			$data["postal_code"] = $this->input->post('postal_code');			
			
			$data["source"] = $this->input->post('source');
			
			
			if($this->input->post('offset')=="")
			{
				//$limit = '15';
				//$totalRows = $this->clients_model->get_total_client_count();
				//$data["offset"] = (int)($totalRows/$limit)*$limit;
				$data["offset"] = 0;
			}else{
				$data["offset"] = $this->input->post('offset');
			}
				
				$this->template->write_view('header','common/header',$data,TRUE);
				$this->template->write_view('content','clients/add_client',$data,TRUE);
				$this->template->write_view('sidebar','common/sidebar',$data,TRUE);
				$this->template->write_view('footer','common/footer',$data,TRUE);
				$this->template->render();
		}else{
			if($this->input->post('client_id'))
			{
				$this->clients_model->client_update();
				$msg = "update";
			}else{
				$this->clients_model->client_insert();
				$msg = "insert";
			}
			$offset = $this->input->post('offset');
			redirect('clients/manage/'.$offset.'/'.$msg);
		}				
		
		
	
	}
	
	
	function edit_client($id,$offset=0)
	{
		
		
		$client_detail=$this->clients_model->get_client_detail($id);
		
		if(empty($client_detail)) { redirect('clients/manage/'.$offset.'/notfound'); }
		
		$data['client_id']=$id;		
		
		$data["first_name"] = $client_detail->first_name;
		$data["last_name"] = $client_detail->last_name;
		$data["email"] = $client_detail->email;
		
		$data["cell_phone"] = $client_detail->cell_phone;
		$data["home_phone"] = $client_detail->home_phone;
		$data["business_phone"] = $client_detail->business_phone;
		$data["fax"] = $client_detail->fax;		
		
		$data["address"] = $client_detail->address;
		$data["city"] = $client_detail->city;
		$data["province"] = $client_detail->province;
		$data["postal_code"] = $client_detail->postal_code;
		
		$data["source"] = $client_detail->source;
		
		$data['offset']=$offset;
		$data['error']='';
		
		
		$this->template->write_view('header','common/header',$data,TRUE);
		$this->template->write_view('content','clients/add_client',$data,TRUE);
		$this->template->write_view('sidebar','common/sidebar',$data,TRUE);
		$this->template->write_view('footer','common/footer',$data,TRUE);
		$this->template->render();
	
	}
	
	
	
}
?>