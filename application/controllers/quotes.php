<?php
class Quotes extends IWEB_Controller {

	function Quotes()
	{
		parent::__construct();
		$this->load->model('quotes_model');	
                
                 if(!check_role_permission('manage_quote')) { 
                    redirect('home/dashboard/notauthorize');
                }
	}
	
	
	
	function index($msg='')
	{
		redirect('quotes/manage');
	}
	
        
        function getemployeewage(){
            
            $wage=0;
            
            if($_POST) {
                
                
                 $team_id=$this->input->post('team_id');
                 
                
                 
                 $query=$this->db->query("select (wage+burden) as totalwage from ".$this->db->dbprefix("wagehistory")." where team_id=".$team_id." order by wage_id desc limit 1");
                 
                 if($query->num_rows()>0){
			$res=$query->row();
                        $wage= $res->totalwage; 
		}
                 
                
                
            }
            echo $wage; die;
           
            
            
        }
        
	
	function add_timecharge($msg='')
	{
		$data['error']='';
		$data['page_link']='';
		$data['result']='';
		
		$data['msg']=$msg;
		
		$data['timecode_id']='';
		/*$data["quote_id"] = '';
		$data["team_id"] = '';
		$data["role_id"] = '';
		$data["chargetype_id"] = '';
		
		$data['date']=date('d-m-Y');
		$data["begin_time"] = '';
		$data["end_time"] = '';
		$data["break"] = '';
		$data["hours"] = '';
		
		$data["resource_cost"] = '';*/
		
		
		
		
			
		
		$this->form_validation->set_rules('quote_id', 'Quote', 'required|trim');
		$this->form_validation->set_rules('team_id', 'Team', 'required|trim');
		$this->form_validation->set_rules('role_id', 'Role', 'required|trim');
		$this->form_validation->set_rules('chargetype_id', 'Charge Type', 'required|trim');
		
		$this->form_validation->set_rules('date', 'Date', 'required|trim');		
		$this->form_validation->set_rules('begin_time', 'Begin Time', 'required|trim');
		$this->form_validation->set_rules('end_time', 'End Time', 'required|trim');
		$this->form_validation->set_rules('break', 'Break', 'required|trim');
		$this->form_validation->set_rules('hours', 'Hours', 'required|trim');
		
		$this->form_validation->set_rules('resource_cost', 'Resource Cost', 'required|trim|numeric');
		
	
		if($this->form_validation->run() == FALSE){	
			
			$time_error='';	
			
			if($_POST) {		
			 if($this->input->post('begin_time')>=$this->input->post('end_time'))
			 {
				$time_error=1;
			 }
			}
				
			if(validation_errors() || $time_error==1)
			{
				$time_text="";
				if($time_error==1) {
					$time_text="<p>End Time must be greater than Begin Time.</p>";
				}
				$data["error"] = validation_errors().$time_text;
			}else{
				$data["error"] = "";
			}
			$data["timecode_id"] = $this->input->post('timecode_id');
			
			$data["quote_id"] = $this->input->post('quote_id');
			$data["team_id"] = $this->input->post('team_id');
			$data["role_id"] = $this->input->post('role_id');
			$data["chargetype_id"] = $this->input->post('chargetype_id');
			
			$data["date"] = $this->input->post('date');
			if($this->input->post('date')=='')
			{
				$data['date']=date('d-m-Y');
			}
		
			$data["begin_time"] = $this->input->post('begin_time');
			$data["end_time"] = $this->input->post('end_time');
			$data["break"] = $this->input->post('break');
			$data["hours"] = $this->input->post('hours');
			
			$data["resource_cost"] = $this->input->post('resource_cost');
			
				
				
		}else{
			if($this->input->post('timecode_id'))
			{
				$this->quotes_model->timecharge_update();
				$msg = "update";
			}else{
				$this->quotes_model->timecharge_insert();
				$msg = "insert";
			}
			$offset = $this->input->post('offset');
			redirect('quotes/add_timecharge/'.$msg);
		}				
		
		$this->template->write_view('header','common/header',$data,TRUE);
		$this->template->write_view('content','quotes/add_timecharge',$data,TRUE);
		$this->template->write_view('sidebar','common/sidebar',$data,TRUE);
		$this->template->write_view('footer','common/footer',$data,TRUE);
		$this->template->render();
	}
	
	///===manage===
	
	function finder()
	{
		
		$data['error']='';
		$data['page_link']='';
		
		
		$result='';
		
		
		$cell_phone='';
		$home_phone='';
		$business_phone='';
		$fax='';
		
		$first_name='';
		$last_name='';
		$email='';
		
		$address='';
		$city='';
		$province='';
		$postal_code='';
		
		
		if($_POST) {
			
			$cell_phone=$this->input->post('cell_phone');
			$home_phone=$this->input->post('home_phone');
			$business_phone=$this->input->post('business_phone');
			$fax=$this->input->post('fax');
			
			$first_name=$this->input->post('first_name');
			$last_name=$this->input->post('last_name');
			$email=$this->input->post('email');
			
			$address=$this->input->post('address');
			$city=$this->input->post('city');
			$province=$this->input->post('province');
			$postal_code=$this->input->post('postal_code');
			
			$result = $this->quotes_model->find_client();
		
		}
                
                
                
                $data['quote_paint_1']=quote_paint(1);
                $data['quote_paint_2']=quote_paint(2);
		
		
		$data['cell_phone']=$cell_phone;
		$data['home_phone']=$home_phone;
		$data['business_phone']=$business_phone;
		$data['fax']=$fax;
		
		$data['first_name']=$first_name;
		$data['last_name']=$last_name;
		$data['email']=$email;
		
		$data['address']=$address;
		$data['city']=$city;
		$data['province']=$province;
		$data['postal_code']=$postal_code;
		
		$data['result']=$result;
		
		$this->template->write_view('header','common/header',$data,TRUE);
		$this->template->write_view('content','quotes/finder',$data,TRUE);
		$this->template->write_view('sidebar','common/sidebar',$data,TRUE);
		$this->template->write_view('footer','common/footer',$data,TRUE);
		$this->template->render();
	}
	
	
	function add_client()
	{
		$status="fail";
		$client_id=0;
		
		
		if($_POST) {
			
			$cell_phone=$this->input->post('cell_phone');
			$home_phone=$this->input->post('home_phone');
			$business_phone=$this->input->post('business_phone');
			$fax=$this->input->post('fax');
			
			$first_name=$this->input->post('first_name');
			$last_name=$this->input->post('last_name');
			$email=$this->input->post('email');
			
			$address=$this->input->post('address');
			$city=$this->input->post('city');
			$province=$this->input->post('province');
			$postal_code=$this->input->post('postal_code');
			
			$client_id = $this->quotes_model->add_client();
				
			$status="success";
		}
		
		if($client_id=='' || $client_id==0) {
			$status="fail";
		}
		
		echo json_encode(array("status"=>$status,'client_id'=>$client_id));
		die;
	}
	
	function add_client_quote()
	{
		$status="fail";
		$quote_id=0;
		
		
		if($_POST) {
			
			$client_id=$this->input->post('client_id');
			$quotetype_id=$this->input->post('quotetype_id');
			$bustype_id=$this->input->post('bustype_id');
			$team_id=$this->input->post('team_id');
			
			$quote_id = $this->quotes_model->add_client_quote();
				
			$status="success";
		}
		
		if($quote_id=='' || $quote_id==0) {
			$status="fail";
		}
		
		echo json_encode(array("status"=>$status,'quote_id'=>$quote_id));
		die;
	}
	
        function search($option,$keyword,$offset=0,$msg='')
	{
	
		$this->load->library('pagination');

		$limit = '15';
                
                $option=trim($option);
                $keyword=trim($keyword);
                
                
                $config['uri_segment'] = 5;
		
		$config['base_url'] = site_url('quotes/manage/');
		$config['total_rows'] = $this->quotes_model->get_total_quote_count($option,$keyword);
		$config['per_page'] = $limit;		
		$this->pagination->initialize($config);		
		$data['page_link'] = $this->pagination->create_links();
		$data['result'] = $this->quotes_model->get_all_quote($offset, $limit,$option,$keyword);
		$data['msg'] = $msg;
		$data['offset'] = $offset;
                
                $data['option']=$option;
                $data['keyword']=$keyword;
                
		
		$this->template->write_view('header','common/header',$data,TRUE);
		$this->template->write_view('content','quotes/list_quote',$data,TRUE);
		$this->template->write_view('sidebar','common/sidebar',$data,TRUE);
		$this->template->write_view('footer','common/footer',$data,TRUE);
		$this->template->render();
	}
	
        
	function manage($offset=0,$msg='')
	{
		$this->load->library('pagination');

		$limit = '15';
                
                 $option='';
                $keyword='';
		
		$config['base_url'] = site_url('quotes/manage/');
		$config['total_rows'] = $this->quotes_model->get_total_quote_count($option,$keyword);
		$config['per_page'] = $limit;		
		$this->pagination->initialize($config);		
		$data['page_link'] = $this->pagination->create_links();
		$data['result'] = $this->quotes_model->get_all_quote($offset, $limit,$option,$keyword);
		$data['msg'] = $msg;
		$data['offset'] = $offset;
                
                $data['option']=$option;
                $data['keyword']=$keyword;
                
		
		$this->template->write_view('header','common/header',$data,TRUE);
		$this->template->write_view('content','quotes/list_quote',$data,TRUE);
		$this->template->write_view('sidebar','common/sidebar',$data,TRUE);
		$this->template->write_view('footer','common/footer',$data,TRUE);
		$this->template->render();
	}
	
	function quote_action()
	{
		$offset=$this->input->post('offset');
		$action=$this->input->post('action');
		$quote_id=$this->input->post('chk');
		
		if($action=='delete')
		{	
			foreach($quote_id as $id)
			{
				$this->quotes_model->delete_quote($id);
			}
			
			redirect('quotes/manage/'.$offset.'/delete');
		}
		
	}
	
	///====step 1=====
	
	function feature_config()
	{
		$status="fail";
		
		$html='';
		$config_id=0;
		
		
		if($_POST) {
			
			$feature_id=$this->input->post('feature_id');
			
			$feature_configs=$this->quotes_model->get_feature_configs($feature_id);
			
			$cnt=0;
			
			if(!empty($feature_configs)){
			
				foreach($feature_configs as $config) {
				
					$html.='<option value="'.$config->feature_configure_id.'" data-rate="'.$config->rate.'">'.$config->rate.'</option>';
					
					if($cnt==0) { $config_id=$config->feature_configure_id; } 
					$cnt++;
				
				}
			}
			
			
			
			
				
			$status="success";
		}
		
		if($config_id=='' || $config_id==0) {
			$status="fail";
		}
		
		echo json_encode(array("status"=>$status,'html'=>$html,'config_id'=>$config_id));
		die;
	}
	
	
	function rate_config()
	{
		$status="fail";
		
		$html='';
	
		
		
		if($_POST) {
			
			$config_id=$this->input->post('config_id');
			
			$feature_configs=$this->quotes_model->get_rate_configs($config_id);
			
			$cnt=0;
			
			if(!empty($feature_configs)){
		
				$status="success";
				$html=$feature_configs;
				
			}
			
		
		}
		
		
		
		echo json_encode(array("status"=>$status,'html'=>$html));
		die;
	}
	
	
	function calculatesqfttime(){
		$status="fail";
		
		$sqfootage=0; $totTime=0; $sqfootagec=0; $totTimec=0;
	
		
		
		if($_POST) {
			
			
			$feature_id=$this->input->post('feature_id');
			
			$length=$this->input->post('length');
			$width=$this->input->post('width');
			$height=$this->input->post('height');
			$quantity=$this->input->post('quantity');
			$addsqft=$this->input->post('addsqft');
			$multiplyer=$this->input->post('multiplyer');
			$sheen=$this->input->post('sheen');
			$coat_type=$this->input->post('coat_type');
			$rate=$this->input->post('rate');
			$commit=$this->input->post('commit');
                        
                        
                        $PaintQuality=$this->input->post('PaintQuality');
                        $jobrate_p=$this->input->post('jobrate');
                        
			
			$spread=0;
                        
                        if ($PaintQuality != '') {

                            $product_detail = product_by_productcost_id($PaintQuality);

                            if (!empty($product_detail)) {

                                $spread = $product_detail->spreadqty;
                            }
                        }
                        
                        
                        
                        $jobrate=0;
                        
                        if($jobrate_p!=''){
                            $jobrate=$jobrate_p;
                        }
                        
			
					
			$quantity_feature=unserialize(QUANTITY_FEATURE);
			
			$quote_sqfootage_formula=unserialize(QUOTE_SQFOOTAGE_FORMULA);
			$feature_sqfootage=unserialize(FEATURE_SQFOOTAGE);
			
			//===for sqfootage=========
			foreach($quote_sqfootage_formula as $key=>$sqft_formula){
				
					$fids=$feature_sqfootage[$key];
					
					if(in_array($feature_id,$fids)){
						
						
						//echo $sqft_formula."<br/>";
						
						$replace_key=array(
						'[LengthVal]'=>$length,
						'[HeightVal]'=>$height,
						'[WidthVal]'=>$width,
						'[Coats]'=>$multiplyer,
						'[Spread]'=>$spread,
						'[UpG]'=>$addsqft,
						'[Rate]'=>$rate,
						'[Quantity]'=>$quantity						
						);
						
						 $final_formula=trim(strtr($sqft_formula,$replace_key));
						//echo "<br /><br />";
						
						
						$sqfootage=round(eval("return {$final_formula};"),2);
						
						//if($commit==true || $commit==1) { $sqfootagec=$sqfootage; }
						$sqfootagec=$sqfootage;
						
						
					}
				
			}
			
			
			
			
			$quote_totaltime_formula=unserialize(QUOTE_TOTALTIME_FORMULA);
			$feature_totaltime=unserialize(FEATURE_TOTALTIME);
			
			
			
			//===for tottime=========
			foreach($quote_totaltime_formula as $key=>$time_formula){
					
					
					$fids=$feature_totaltime[$key];

					
					if(in_array($feature_id,$fids)){
						
						
						//echo $time_formula; echo "<br /><br />";
						
						if($quantity>0){ 
							
							if(in_array($feature_id,$quantity_feature)){
								
								$time_formula=$quote_totaltime_formula['TOT5'];
								
							}							
						}	
						
						//echo $time_formula; echo "<br /><br />";
						
						
						$replace_key=array(
						'[LengthVal]'=>$length,
						'[HeightVal]'=>$height,
						'[WidthVal]'=>$width,
						'[Coats]'=>$multiplyer,
						'[Spread]'=>$spread,
						'[UpG]'=>$addsqft,
						'[Rate]'=>$rate,
						'[Quantity]'=>$quantity,
						'[Sqfootage]'=>$sqfootage,
						'[JobRate]'=>$jobrate						
						);
						
						$final_formula=trim(strtr($time_formula,$replace_key));
						//echo "<br /><br />";
						
						
						$totTime=round(eval("return {$final_formula};"),2);
						
						//die;
						//if($commit==true || $commit==1) { $totTimec=$totTime; }
                                                $totTimec=$totTime;
						
											
					
				}
			}
			
			
			$status="success";
		
		}
		
		echo json_encode(array("status"=>$status,'sqfootage'=>$sqfootage,'totTime'=>$totTime,'sqfootagec'=>$sqfootagec,'totTimec'=>$totTimec));
		die;
	}
	
	
	function add_quote($quote_id=0,$msg='')
	{
		$data['quote_id']=$quote_id;
		
		
		$quote_details=$this->quotes_model->quote_details($quote_id);		
		
		if(!$quote_details){
			redirect('quotes/manage/0/notfound');
		}
		
		$data['quote_details']=$quote_details;
		
		
		$data["error"] = "";
		
		
		$quotetype_id=$quote_details->quotetype_id;
		$bustype_id=$quote_details->bustype_id;
		
		$data['quotetype_id']=$quotetype_id;
		$data['bustype_id']=$bustype_id;
		
		$data['QuoteContact']=$quote_details->QuoteContact;
		$data['QuoteAddress']=$quote_details->QuoteAddress;
		$data['QuoteContactPhone']=$quote_details->QuoteContactPhone;
		$data['short_description']=$quote_details->short_description;
		
		$data['leadsource_id']=$quote_details->leadsource_id;
		$data['ReferralName']=$quote_details->ReferralName;
		
		$data['rate']=$quote_details->rate;	
		$data['date']=$quote_details->date;	
		$data['PaintQuality']=$quote_details->PaintQuality;		
		
		$data['team_id']=$quote_details->team_id;		
		$data['wonreason_id']=$quote_details->wonreason_id;
		$data['salesstage_id']=$quote_details->salesstage_id;
		$data['Probability']=$quote_details->Probability;
		
		
		
		
		
		///====post part=====
		
		if($_POST){
		
			
			$this->form_validation->set_rules('quote_id', 'Quote', 'required|trim');
			//$this->form_validation->set_rules('QuoteContact', 'Contact', 'required|trim');
			$this->form_validation->set_rules('QuoteAddress', 'Location', 'required|trim');
			$this->form_validation->set_rules('QuoteContactPhone', 'Phone', 'required|trim');			
			//$this->form_validation->set_rules('short_description', 'Description', 'required|trim');	
				
			//$this->form_validation->set_rules('leadsource_id', 'Lead', 'required|trim');
			
			//$this->form_validation->set_rules('PaintQuality', 'Paint', 'required|trim');
			//$this->form_validation->set_rules('rate', 'Job Rate', 'required|trim');
			
			$this->form_validation->set_rules('team_id', 'Sales Lead', 'required|trim');
			//$this->form_validation->set_rules('salesstage_id', 'Sales Stage', 'required|trim');
			$this->form_validation->set_rules('Probability', 'Probability', 'required|trim');
			$this->form_validation->set_rules('wonreason_id', 'Win / Loss', 'required|trim');
			
			
			
			if($this->form_validation->run() == FALSE){	
				
				if(validation_errors())
				{					
					$data["error"] = validation_errors();
				}else{
					$data["error"] = "";
				}
				
				
				$data['QuoteContact']=$this->input->post('QuoteContact');	
				$data['QuoteAddress']=$this->input->post('QuoteAddress');	
				$data['QuoteContactPhone']=$this->input->post('QuoteContactPhone');	
				$data['short_description']=$this->input->post('short_description');	
				
				$data['leadsource_id']=$this->input->post('leadsource_id');	
				$data['ReferralName']=$this->input->post('ReferralName');	
				
				$data['rate']=$this->input->post('rate');		
				$data['date']=$this->input->post('date');		
				$data['PaintQuality']=$this->input->post('PaintQuality');			
				
				$data['team_id']=$this->input->post('team_id');			
				$data['wonreason_id']=$this->input->post('wonreason_id');	
				$data['salesstage_id']=$this->input->post('salesstage_id');	
				$data['Probability']=$this->input->post('Probability');	
			
					
			}else{
				
				$this->quotes_model->quote_detail_update();
				
						
				$msg='update';
				redirect('quotes/add_quote/'.$quote_id.'/'.$msg);
			}				
		
			
		}		
		///====post part=====
		
		
		$room_details=$this->quotes_model->room_details($quote_id);				
		$data['room_details']=$room_details;
		
		
		
		
		$data['offset']='';
		$data['msg']=$msg;
		
		$multiplyer_array=array(1,2,3,4);
		$data['all_multiplyer']=$multiplyer_array;
		
		$probability_array=array(0,10,25,50,75,100);
		$data['all_probability']=$probability_array;
		
		
		$prep_array=array(0,0.12,0.25,0.50,0.75,1,2,3,4,5,6,7,8,9,10,15,20,25,30,35);
		
		if($quotetype_id==1){
			$prep_array=array(1.25,2,3,4,5,6,7,8,9,10,15,20,25,30,35);
		} elseif($quotetype_id==2){
			$prep_array=array(1.50,2,3,4,5,6,7,8,9,10,15,20,25,30,35);
		}
		
		
		$sc_array=array(0,0.12,0.25,0.50,0.75,1,2,3,4,5,6,7,8,9,10,15,20,25,30,35);		
		
		$clostes_array=array(0,1,2,3,4,5,6,7,8,9,10,15,20,25,30,35);
		$misc_array=array(0,1,2,3,4,5,6,7,8,9,10,15,20,25,30,35);
		
		$data['all_prep']=$prep_array;
		$data['all_sc']=$sc_array;
		$data['all_clostes']=$clostes_array;
		$data['all_misc']=$misc_array;
		
		
		
		
		
		$data['all_sheen']=sheen_list();	
		
		
		$data['quote_paint']=quote_paint($quotetype_id);
		$data['all_room_type']=quotetype_room_list($quotetype_id);
		$data['all_feature_type']=quotetype_feature_list($quotetype_id);
		$data['all_feature_coat']=quotetype_featurecoat_list($quotetype_id);
		
		
		
		
		
		$this->template->write_view('header','common/header',$data,TRUE);
		$this->template->write_view('content','quotes/add_quote',$data,TRUE);
		$this->template->write_view('sidebar','common/sidebar',$data,TRUE);
		$this->template->write_view('footer','common/footer',$data,TRUE);
		$this->template->render();
	}
	
	
	function step2($quote_id,$msg='')
	{
		$data['quote_id']=$quote_id;
		
		
		$quote_details=$this->quotes_model->quote_details($quote_id);		
		
		if(!$quote_details){
			redirect('quotes/manage/0/notfound');
		}
		
		$data['quote_details']=$quote_details;
		
		
		$data['error']='';
		
		
		$quotetype_id=$quote_details->quotetype_id;
		$bustype_id=$quote_details->bustype_id;
		
		$data['quotetype_id']=$quotetype_id;
		$data['bustype_id']=$bustype_id;
		
		
		///====post part=====
		if($_POST)
		{
			//pr($_POST); die;
			$this->quotes_model->quote_order_update();				
						
			$msg='update';
			redirect('quotes/step2/'.$quote_id.'/'.$msg);
		}
		///====post part=====
		
		$data['commit_material']=$this->quotes_model->get_commited_material($quote_id);		
		$data['optional_material']=$this->quotes_model->get_optional_material($quote_id);		
		
		$order_details=$this->quotes_model->order_details($quote_id);				
		$data['order_details']=$order_details;
		
		$data['msg']=$msg;
		
		
	
		$markup_cost_array=array(0,5,10,15,20,25,30,35,40);		
		$data['all_markup_cost']=$markup_cost_array;
	
		
		
		$data['all_sheen']=sheen_list();		
		$data['all_product_list']=quotetype_product_list($quotetype_id);
		
		
		
		
		$this->template->write_view('header','common/header',$data,TRUE);
		$this->template->write_view('content','quotes/add_quote2',$data,TRUE);
		$this->template->write_view('sidebar','common/sidebar',$data,TRUE);
		$this->template->write_view('footer','common/footer',$data,TRUE);
		$this->template->render();
	}
	
	function step3($quote_id,$msg='')
	{
		$data['quote_id']=$quote_id;
		
		$quote_details=$this->quotes_model->quote_details($quote_id);		
		
		if(!$quote_details){
			redirect('quotes/manage/'.$offset.'/notfound');
		}
		
		$data['quote_details']=$quote_details;
		
                
                
		
		$data['offset']='';
		$data['error']='';
		$data['msg']=$msg;
		
                
                ///====post part=====
		if($_POST)
		{
			//pr($_POST); die;
			$this->quotes_model->quote_financial_update();				
						
			$msg='update';
			redirect('quotes/step3/'.$quote_id.'/'.$msg);
		}
		///====post part=====
		
		
		
		$other_service_array=array('sundries'=>'Sundries','miscmaterials'=>'Misc Materials','rentals'=>'Rentals','powerwash'=>'Power Wash','misc'=>'Misc','other'=>'Other');
		$other_service_pay_array=array(0,12.50,25,50,75,100,15,200,250,300,350,400,450,500,600,700,800,900,1000);
		
		$other_service_task_array=array('Wallpaper Removal','Misc');
		
		
		$discount_array=array(0,5,10,15,20,25);
		
		$data['other_service']=$other_service_array;
		$data['other_service_pay']=$other_service_pay_array;
		$data['other_task']=$other_service_task_array;
		$data['all_discount']=$discount_array;
		
		
		
                $data['total_commit_hours']=quotetotalhours_by_id($quote_id,1,0);
                $data['total_optional_hours_1']=quotetotalhours_by_id($quote_id,0,1);
                $data['total_optional_hours_2']=quotetotalhours_by_id($quote_id,0,2);
                $data['total_optional_hours_3']=quotetotalhours_by_id($quote_id,0,3);
                $data['total_optional_hours_4']=quotetotalhours_by_id($quote_id,0,4);
                
                /*$data['total_commit_prep']=quotetotalprep_by_id($quote_id,1);
                $data['total_optional_prep']=quotetotalprep_by_id($quote_id,0);
                
                $data['total_commit_closets']=quotetotalclosets_by_id($quote_id,1);
                $data['total_optional_closets']=quotetotalclosets_by_id($quote_id,0);
                
                $data['total_commit_misc']=quotetotalmisc_by_id($quote_id,1);
                $data['total_optional_misc']=quotetotalmisc_by_id($quote_id,0);
                
                $data['total_commit_setclean']=quotetotalsetclean_by_id($quote_id,1);
                $data['total_optional_setclean']=quotetotalsetclean_by_id($quote_id,0);*/
                
                
                $data['total_commit_material']=quotetotalmaterial_by_id($quote_id,1,0);
                $data['total_optional_material_1']=quotetotalmaterial_by_id($quote_id,0,1);
                $data['total_optional_material_2']=quotetotalmaterial_by_id($quote_id,0,2);
                $data['total_optional_material_3']=quotetotalmaterial_by_id($quote_id,0,3);
                $data['total_optional_material_4']=quotetotalmaterial_by_id($quote_id,0,4);
                
                
		
		
		$this->template->write_view('header','common/header',$data,TRUE);
		$this->template->write_view('content','quotes/add_quote3',$data,TRUE);
		$this->template->write_view('sidebar','common/sidebar',$data,TRUE);
		$this->template->write_view('footer','common/footer',$data,TRUE);
		$this->template->render();
	}
	
	
	function step4($quote_id,$msg='')
	{
		$data['quote_id']=$quote_id;
		
		$quote_details=$this->quotes_model->quote_details($quote_id);		
		
		if(!$quote_details){
			redirect('quotes/manage/'.$offset.'/notfound');
		}
		
		$data['quote_details']=$quote_details;
		
		
		
		$data['offset']='';
		$data['error']='';
		$data['msg']=$msg;
                
                
                ///====post part=====
		if($_POST)
		{
			//pr($_POST); die;
			$this->quotes_model->quote_step_4_financial_update();				
						
			$msg='update';
			redirect('quotes/step4/'.$quote_id.'/'.$msg);
		}
		///====post part=====
		
		
                
		$other_service_array=array('sundries'=>'Sundries','miscmaterials'=>'Misc Materials','rentals'=>'Rentals','powerwash'=>'Power Wash','misc'=>'Misc','other'=>'Other');
		$other_service_pay_array=array(0,12.50,25,50,75,100,15,200,250,300,350,400,450,500,600,700,800,900,1000);
		
		$other_service_task_array=array('Wallpaper Removal','Misc');
		
		
		$discount_array=array(0,5,10,15,20,25);
		
		$data['other_service']=$other_service_array;
		$data['other_service_pay']=$other_service_pay_array;
		$data['other_task']=$other_service_task_array;
		$data['all_discount']=$discount_array;
                
                
                
                $data['total_commit_hours']=quotetotalhours_by_id($quote_id,1,0);
                $data['total_optional_hours_1']=quotetotalhours_by_id($quote_id,0,1);
                $data['total_optional_hours_2']=quotetotalhours_by_id($quote_id,0,2);
                $data['total_optional_hours_3']=quotetotalhours_by_id($quote_id,0,3);
                $data['total_optional_hours_4']=quotetotalhours_by_id($quote_id,0,4);
                
                /*$data['total_commit_prep']=quotetotalprep_by_id($quote_id,1);
                $data['total_optional_prep']=quotetotalprep_by_id($quote_id,0);
                
                $data['total_commit_closets']=quotetotalclosets_by_id($quote_id,1);
                $data['total_optional_closets']=quotetotalclosets_by_id($quote_id,0);
                
                $data['total_commit_misc']=quotetotalmisc_by_id($quote_id,1);
                $data['total_optional_misc']=quotetotalmisc_by_id($quote_id,0);
                
                $data['total_commit_setclean']=quotetotalsetclean_by_id($quote_id,1);
                $data['total_optional_setclean']=quotetotalsetclean_by_id($quote_id,0);*/
                
                
                $data['total_commit_material']=quotetotalmaterial_by_id($quote_id,1,0);
                $data['total_optional_material_1']=quotetotalmaterial_by_id($quote_id,0,1);
                $data['total_optional_material_2']=quotetotalmaterial_by_id($quote_id,0,2);
                $data['total_optional_material_3']=quotetotalmaterial_by_id($quote_id,0,3);
                $data['total_optional_material_4']=quotetotalmaterial_by_id($quote_id,0,4);
                
		
		$this->template->write_view('header','common/header',$data,TRUE);
		$this->template->write_view('content','quotes/add_quote4',$data,TRUE);
		$this->template->write_view('sidebar','common/sidebar',$data,TRUE);
		$this->template->write_view('footer','common/footer',$data,TRUE);
		$this->template->render();
	}
	
	
	function step5($quote_id,$msg='')
	{
		$data['quote_id']=$quote_id;
		
		
		$quote_details=$this->quotes_model->quote_details($quote_id);		
		
		if(!$quote_details){
			redirect('quotes/manage/'.$offset.'/notfound');
		}
		
		$data['quote_details']=$quote_details;
		
		
		$data['error']='';
		$data['msg']=$msg;
		
		$data['satisfactioncalldate']=$quote_details->satisfactioncalldate;
		$data['satisfactionrating']=$quote_details->satisfactionrating;
		$data['satisfactioncall']=$quote_details->satisfactioncall;
		$data['satisfactionleadpainter']=$quote_details->satisfactionleadpainter;
		$data['satisfactionname']=$quote_details->satisfactionname;
		$data['satisfactionnotes']=$quote_details->satisfactionnotes;
		
		
		///====post part=====
		
		if($_POST){
			
			
			$this->form_validation->set_rules('quote_id', 'Quote', 'required|trim');
			$this->form_validation->set_rules('satisfactioncalldate', 'Call Date', 'required|trim');
			$this->form_validation->set_rules('satisfactionrating', 'Satisfaction Rating', 'required|trim');
			$this->form_validation->set_rules('satisfactioncall', 'No. Of Revisits', 'required|trim');
			
			$this->form_validation->set_rules('satisfactionleadpainter', 'Team Lead', 'required|trim');		
			$this->form_validation->set_rules('satisfactionname', 'Contact Name', 'required|trim');
			$this->form_validation->set_rules('satisfactionnotes', 'Notes', 'required|trim');
			if($this->form_validation->run() == FALSE){	
				
				
				
					
				if(validation_errors())
				{					
					$data["error"] = validation_errors();
				}else{
					$data["error"] = "";
				}
				
				$data["satisfactioncalldate"] = $this->input->post('satisfactioncalldate');			
				$data["satisfactionrating"] = $this->input->post('satisfactionrating');
				$data["satisfactioncall"] = $this->input->post('satisfactioncall');
				
				$data["satisfactionleadpainter"] = $this->input->post('satisfactionleadpainter');
				$data["satisfactionname"] = $this->input->post('satisfactionname');
				$data["satisfactionnotes"] = $this->input->post('satisfactionnotes');
				
				
				
					
					
			}else{
				
				$this->quotes_model->followup_update();
				
				$msg='update';
				redirect('quotes/step5/'.$quote_id.'/'.$msg);
			}				
		
			
		}		
		///====post part=====
	
		
		
		
		
		
		
		$this->template->write_view('header','common/header',$data,TRUE);
		$this->template->write_view('content','quotes/add_quote5',$data,TRUE);
		$this->template->write_view('sidebar','common/sidebar',$data,TRUE);
		$this->template->write_view('footer','common/footer',$data,TRUE);
		$this->template->render();
	}
	
	function step6($quote_id,$offset=0,$msg='')
	{
		$data['quote_id']=$quote_id;
		
		$quote_details=$this->quotes_model->quote_details($quote_id);		
		
		if(!$quote_details){
			redirect('quotes/manage/'.$offset.'/notfound');
		}
		
		$data['quote_details']=$quote_details;
		
		$data['error']='';
		
		
		///=====add part====
		
		$data['msg']=$msg;
		
		$data['timecode_id']='';
		
		
		$data["team_id"] = '';
		$data["role_id"] = '';
		$data["chargetype_id"] = '';		
		$data['date']=date('d-m-Y H:i:s');
		$data["begin_time"] = '';
		$data["end_time"] = '';
		$data["break"] = '';
		$data["hours"] = '';		
		$data["resource_cost"] = '';
		
		
		if($_POST){
		
		
			$this->form_validation->set_rules('quote_id', 'Quote', 'required|trim');
			$this->form_validation->set_rules('team_id', 'Team', 'required|trim');
			$this->form_validation->set_rules('role_id', 'Role', 'required|trim');
			$this->form_validation->set_rules('chargetype_id', 'Charge Type', 'required|trim');
			
			$this->form_validation->set_rules('date', 'Date', 'required|trim');		
			$this->form_validation->set_rules('begin_time', 'Begin Time', 'required|trim');
			$this->form_validation->set_rules('end_time', 'End Time', 'required|trim');
			$this->form_validation->set_rules('break', 'Break', 'required|trim');
			$this->form_validation->set_rules('hours', 'Hours', 'required|trim');
			
			$this->form_validation->set_rules('resource_cost', 'Resource Cost', 'required|trim|numeric');
			
	
			if($this->form_validation->run() == FALSE){	
				
				$time_error='';	
			
				if($_POST) {		
				 if($this->input->post('begin_time')>=$this->input->post('end_time'))
				 {
					$time_error=1;
				 }
				}
					
				if(validation_errors() || $time_error==1)
				{
					$time_text="";
					if($time_error==1) {
						$time_text="<p>End Time must be greater than Begin Time.</p>";
					}
					$data["error"] = validation_errors().$time_text;
				}else{
					$data["error"] = "";
				}
				$data["timecode_id"] = $this->input->post('timecode_id');
				
				
				$data["team_id"] = $this->input->post('team_id');
				$data["role_id"] = $this->input->post('role_id');
				$data["chargetype_id"] = $this->input->post('chargetype_id');
				
				$data["date"] = $this->input->post('date');
				if($this->input->post('date')=='')
				{
					$data['date']=date('d-m-Y');
				}
			
				$data["begin_time"] = $this->input->post('begin_time');
				$data["end_time"] = $this->input->post('end_time');
				$data["break"] = $this->input->post('break');
				$data["hours"] = $this->input->post('hours');
				
				$data["resource_cost"] = $this->input->post('resource_cost');
				
					
					
			}else{
				if($this->input->post('timecode_id'))
				{
					$this->quotes_model->timecharge_update();
					$msg = "update";
				}else{
					$this->quotes_model->timecharge_insert();
					$msg = "insert";
				}
				$offset = $this->input->post('offset');
				redirect('quotes/step6/'.$quote_id.'/'.$offset.'/'.$msg);
			}				
		
		}
		///====add part=====	
		
		
		
		$data['quote_first']=$this->quotes_model->quote_first($quote_id);
		$data['quote_last']=$this->quotes_model->quote_last($quote_id);
		
		
		$this->load->library('pagination');

		$limit = '15';
		
		$config['base_url'] = site_url('quotes/step6/'.$quote_id);
		$config['total_rows'] = $this->quotes_model->get_quote_total_timecharge_count($quote_id);
		$config['per_page'] = $limit;		
		$this->pagination->initialize($config);		
		$data['page_link'] = $this->pagination->create_links();
		$data['result'] = $this->quotes_model->get_quote_all_timecharge($offset, $limit,$quote_id);
		$data['msg'] = $msg;
		$data['offset'] = $offset;
		
		
		
		$this->template->write_view('header','common/header',$data,TRUE);
		$this->template->write_view('content','quotes/add_quote6',$data,TRUE);
		$this->template->write_view('sidebar','common/sidebar',$data,TRUE);
		$this->template->write_view('footer','common/footer',$data,TRUE);
		$this->template->render();
	}
	
	
	function timecharge_action($quote_id,$timecode_id,$action,$offset=0)
	{
		
		if($action=='delete')
		{	
			
			$this->quotes_model->delete_timecharge($timecode_id);			
			
			redirect('quotes/step6/'.$quote_id.'/'.$offset.'/delete');
		}
		
	}
	
	function complete_quote($id=0,$is_complete=0)
	{
		$status='fail'; $msg='Unable to update quote.';
		if($_POST){
			
			if(isset($_POST['quote_id'])) {
				
				$id=$_POST['quote_id'];
				
				if(isset($_POST['is_complete'])) {
					$is_complete=$_POST['is_complete'];
				}
				
				
				if($this->quotes_model->complete_quote($id,$is_complete))
				{
					$status='success';
					$msg='Quote has been completed successfully.';
				}
				
			} else {
				$msg='Cannot find quote details.';
			}
		} else {
			$msg='Cannot find quote details.';
		}	
		
		echo json_encode(array('status'=>$status,'msg'=>$msg));
		
	}
	
	
	///===managetype===
	
	function managetype($offset=0,$msg='')
	{
		$this->load->library('pagination');

		$limit = '15';
		
		$config['base_url'] = site_url('quotes/managetype/');
		$config['total_rows'] = $this->quotes_model->get_total_type_count();
		$config['per_page'] = $limit;		
		$this->pagination->initialize($config);		
		$data['page_link'] = $this->pagination->create_links();
		$data['result'] = $this->quotes_model->get_all_type($offset, $limit);
		$data['msg'] = $msg;
		$data['offset'] = $offset;
		$this->template->write_view('header','common/header',$data,TRUE);
		$this->template->write_view('content','quotes/list_type',$data,TRUE);
		$this->template->write_view('sidebar','common/sidebar',$data,TRUE);
		$this->template->write_view('footer','common/footer',$data,TRUE);
		$this->template->render();		
		
	
	}
	
	
	function add_type()
	{
		$this->form_validation->set_rules('quotetype', 'Name', 'required|alpha_space|min_length[3]|max_length[50]');
	
		if($this->form_validation->run() == FALSE){			
			if(validation_errors())
			{
				$data["error"] = validation_errors();
			}else{
				$data["error"] = "";
			}
			$data["quotetype_id"] = $this->input->post('quotetype_id');
			$data["quotetype"] = $this->input->post('quotetype');
			
			if($this->input->post('offset')=="")
			{
				$limit = '15';
				$totalRows = $this->quotes_model->get_total_type_count();
				$data["offset"] = (int)($totalRows/$limit)*$limit;
			}else{
				$data["offset"] = $this->input->post('offset');
			}
				
				$this->template->write_view('header','common/header',$data,TRUE);
				$this->template->write_view('content','quotes/add_type',$data,TRUE);
				$this->template->write_view('sidebar','common/sidebar',$data,TRUE);
				$this->template->write_view('footer','common/footer',$data,TRUE);
				$this->template->render();
		}else{
			if($this->input->post('quotetype_id'))
			{
				$this->quotes_model->type_update();
				$msg = "update";
			}else{
				$this->quotes_model->type_insert();
				$msg = "insert";
			}
			$offset = $this->input->post('offset');
			redirect('quotes/managetype/'.$offset.'/'.$msg);
		}				
		
		
	
	}
	
	
	function edit_type($id,$offset=0)
	{
		
		$type_detail=$this->quotes_model->get_type_detail($id);
		
		if(empty($type_detail)) { redirect('quotes/managetype/'.$offset.'/notfound'); }
		
		$data['quotetype_id']=$id;
		
		$data["quotetype"] = $type_detail->quotetype;
		
		
		$data['offset']=$offset;
		$data['error']='';
		
		
		$this->template->write_view('header','common/header',$data,TRUE);
		$this->template->write_view('content','quotes/add_type',$data,TRUE);
		$this->template->write_view('sidebar','common/sidebar',$data,TRUE);
		$this->template->write_view('footer','common/footer',$data,TRUE);
		$this->template->render();
	
	}
	
	
	function type_action()
	{
		$offset=$this->input->post('offset');
		$action=$this->input->post('action');
		$quotetype_id=$this->input->post('chk');
		
		if($action=='delete')
		{	
			foreach($quotetype_id as $id)
			{
				$this->quotes_model->delete_type($id);
			}
			
			redirect('quotes/managetype/'.$offset.'/delete');
		}
		
	}
	
	
}
?>