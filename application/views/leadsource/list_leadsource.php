<?php $site_setting=site_setting();?>
<div class="page-header">
				<div class="pull-left">
					<h4><i class="icon-cogs"></i>LeadSources</h4>
				</div>
				<div class="pull-right">
					<ul class="bread">
						<li><a href="<?php echo site_url('leadsource/manage');?>">LeadSources</a><span class="divider">/</span></li>
                        <li class="active">LeadSources</li>
					</ul>
				</div>
			</div>
            
            
            
            
            <div class="container-fluid" id="content-area">
            
            
            <div class="row-fluid">
								<div class="span12">
                                
                                
                                
                                
                                <?php if($msg!='') { ?>
<div class="alert <?php if($msg=='notfound' || $msg=='cannot') {?>alert-danger<?php } else { ?>alert-success<?php } ?>">
											<button type="button" class="close" data-dismiss="alert">&times;</button>
											<strong><?php if($msg=='notfound' || $msg=='cannot') {?>Warning<?php } else { ?>Success<?php } ?>!</strong>
<?php if($msg=='active') { ?>LeadSources has been activated successfully. <?php } ?>
	<?php if($msg=='inactive') { ?>LeadSources has been inactivated successfully. <?php } ?>
	<?php if($msg=='delete') { ?>LeadSources detail has been deleted successfully. <?php } ?>	
	<?php if($msg=='insert') { ?>New LeadSources has been added successfully. <?php } ?>	
	<?php if($msg=='update') { ?>LeadSources detail has been updated successfully. <?php } ?>	
    <?php if($msg=='notfound') { ?>LeadSources records not found. <?php } ?>
    <?php if($msg=='cannot') { ?>Cannot delete LeadSources because it used in many records. <?php } ?>		
  
</div> 
<?php } ?>	
                                
                                
                                
									<div class="box">
										<div class="box-head">
											<i class="icon-table"></i>
											<span>Manage LeadSources</span>
										</div>
                                        

									                                        

	 <form name="frm_listlead" id="frm_listlead" action="<?php echo site_url('leadsource/lead_action');?>" method="post">
<input type="hidden" name="action" id="action" />
<input type="hidden" name="offset" id="offset" value="<?php echo $offset; ?>" />	
                                        
                                        <div class="box-body box-body-nopadding">
											<div class="highlight-toolbar">
												<div class="pull-left">
													
												</div>
                                                
                                                <div class="pull-right"><div class="btn-toolbar">
													<div class="btn-group">
														<a href="<?php echo site_url('leadsource/add_lead');?>" class='button button-basic button-icon' rel="tooltip" title="Add"><i class="icon-plus-sign"></i></a>
                                                          <a href="javascript:void(0)" class='button button-basic button-icon' rel="tooltip" title="Delete" onclick="setaction('chk[]','delete', 'If LeadSources is linked to any records then it will not delete record. \n\nAre you sure, you want to delete selected record(s)?', 'frm_listlead')"><i class="icon-trash"></i></a>
												
													</div>
												</div></div>
												
											</div>
											<table class="table table-nomargin table-striped table-bordered table-hover table-pagination">
												<thead>
													<tr>
														<th><input type="checkbox" class="checkall" /></th> 
                                                        <th>Name</th>
                                                        <th>Type</th>
                                                        <th>Begin</th>
                                                        <th>End</th>
                                                        <th>Quantity</th>
                                                        <th>Design(<?php echo $site_setting->currency_symbol;?>)</th>                                                        
                                                        <th>Printing(<?php echo $site_setting->currency_symbol;?>)</th>
                                                        <th>Dist. Cost(<?php echo $site_setting->currency_symbol;?>)</th>														
														<th>Action</th>														
													</tr>
												</thead>
												<tbody>
									<?php if($result) { 
											
											foreach($result as $res) {
											
										?>
										
											<tr> 
												<td><input type="checkbox" id='chk' name="chk[]" value="<?php echo $res->leadsource_id;?>" /></td> 
												<td><?php echo anchor('leadsource/edit_lead/'.$res->leadsource_id.'/'.$offset,$res->leadsource,' rel="tooltip" title="Edit" ');?></td>												
                                                <td><?php if($res->leadsourcetype!='') { echo $res->leadsourcetype; } ?></td>
                                                
                                                <td><?php if($res->beginpgmdate!='') { echo date($site_setting->date_time_format,strtotime($res->beginpgmdate)); } ?></td> 
                                                <td><?php if($res->endpgmdate!='') { echo date($site_setting->date_time_format,strtotime($res->endpgmdate)); } ?></td> 
                                                
                                                <td><?php echo $res->quantity;?></td>
                                                
                                                <td><?php echo $res->design;?></td>                                                 
                                                <td><?php echo $res->printing;?></td> 
                                                <td><?php echo $res->distribution_cost;?></td>
                                                
                                                <td>                                               
                                                <a href="<?php echo site_url('leadsource/edit_lead/'.$res->leadsource_id.'/'.$offset);?>" class='button button-basic button-icon' rel="tooltip" title="Edit"><i class="icon-edit"></i></a>
                                               </td>
                                                
											</tr> 
											 
										 
										
										<?php   } ?>
										<?php } else { ?>
										<tr><td colspan="10" align="center" valign="middle"  style="text-align:center;">No LeadSources has been added yet.</td></tr>
										<?php } ?>
														
													</tbody>
												</table>
												<div class="bottom-table">
													<div class="pull-left">
														
													</div>
													<div class="pull-right"><div class="pagination pagination-custom">
														<?php echo $page_link; ?>
													</div></div>
												</div>
											</div>
										
                                        </form>
                                    
                                        
                                        </div>
									</div>
								</div>
            
            
            </div>