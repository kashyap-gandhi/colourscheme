<?php $site_setting=site_setting();
 ?>

<div class="page-header">
				<div class="pull-left">
					<h4><i class="icon-cogs"></i> <?php if($leadsourcetype_id!='') {?>Edit LeadSources Type<?php } else { ?>Add LeadSources Type<?php }  ?></h4>
				</div>
				<div class="pull-right">
					<ul class="bread">
						<li><a href="<?php echo site_url('leadsource/manage');?>">LeadSources</a><span class="divider">/</span></li>
                        <li class="active">LeadSources Type</li>
					</ul>
				</div>
			</div>
            
            
            <div class="container-fluid" id="content-area">
				
<div class="row-fluid">
                <div class="span12">
                
                 <?php if($error!=''){ ?>

		<div class="alert alert-error">
											<button type="button" class="close" data-dismiss="alert">&times;</button>
											<strong>Warning !</strong> <?php echo $error;?>
										</div>    <?php }?>
                                        
                                        
    
					
						<div class="box">
							<div class="box-head">
								<i class="icon-list-ul"></i>
								<span>Change LeadSources Type details</span>
							</div>
							<div class="box-body box-body-nopadding">
                             <?php
									$attributes = array('name'=>'frm_addleadsourcetype','class'=>'form-horizontal form-bordered');
									echo form_open('leadsource/add_type',$attributes);
								  ?> 
                                  
								
									<div class="control-group">
										<label for="textfield" class="control-label">Name</label>
										<div class="controls">
											<input name="leadsourcetype" id="leadsourcetype" type="text" value="<?php echo $leadsourcetype; ?>" placeholder="Name" class="input-xlarge">
                                            
										</div>
									</div>
									
                                    
                                    
									<div class="form-actions">
										 <?php if($leadsourcetype_id=='') { ?>
												<button type="submit" class="button button-basic-blue">Save</button>
											<?php } else { ?>
											<button type="submit" class="button button-basic-blue">Save changes</button>
											<?php } ?>
                                            <button type="button" class="button button-basic" onClick="window.location.href='<?php echo site_url('leadsource/managetype');?>'">Cancel</button>
                                           
											<input type="hidden" name="leadsourcetype_id" id="leadsourcetype_id" value="<?php echo $leadsourcetype_id; ?>" />
											<input type="hidden" name="offset" id="offset" value="<?php echo $offset; ?>" />
										
									</div>
                                   
                                   
								</form>
							</div>
						</div>
					</div>
				</div>
				
				
				
			</div>