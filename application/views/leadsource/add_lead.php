<?php $site_setting=site_setting();
$leadsource_type_list=leadsource_type_list();
 ?>

<div class="page-header">
				<div class="pull-left">
					<h4><i class="icon-cogs"></i> <?php if($leadsource_id!='') {?>Edit LeadSources<?php } else { ?>Add LeadSources<?php }  ?></h4>
				</div>
				<div class="pull-right">
					<ul class="bread">
						<li><a href="<?php echo site_url('leadsource/manage');?>">LeadSources</a><span class="divider">/</span></li>
                        <li class="active">LeadSources</li>
					</ul>
				</div>
			</div>
            
            
            <div class="container-fluid" id="content-area">
				
<div class="row-fluid">
                <div class="span12">
                
                 <?php if($error!=''){ ?>

		<div class="alert alert-error">
											<button type="button" class="close" data-dismiss="alert">&times;</button>
											<strong>Warning !</strong> <?php echo $error;?>
										</div>    <?php }?>
                                        
                                        
    
					
						<div class="box">
							<div class="box-head">
								<i class="icon-list-ul"></i>
								<span>Change LeadSources details</span>
							</div>
							<div class="box-body box-body-nopadding">
                             <?php
									$attributes = array('name'=>'frm_addleadsource','class'=>'form-horizontal form-bordered');
									echo form_open('leadsource/add_lead',$attributes);
								  ?> 
                                  
                                  
                                  <div class="control-group">
										<label for="textarea" class="control-label">LeadSource Type</label>
										<div class="controls">    
                                        
                  <select name="leadsourcetype_id" id="leadsourcetype_id" >
                  <option value="">Select</option>
				<?php if(isset($leadsource_type_list) && !empty($leadsource_type_list)) { 
						foreach($leadsource_type_list as $type) { ?>
                	<option value="<?php echo $type->leadsourcetype_id;?>" <?php if($leadsourcetype_id==$type->leadsourcetype_id) { ?> selected="selected" <?php } ?>><?php echo ucfirst($type->leadsourcetype); ?></option>
				<?php } } ?>
                  </select>
                  
                    </div>
				</div>
                
                
								
									<div class="control-group">
										<label for="textfield" class="control-label">Name</label>
										<div class="controls">
											<input name="leadsource" id="leadsource" type="text" value="<?php echo $leadsource; ?>" placeholder="Name" class="input-xlarge">
                                            
										</div>
									</div>
                                    
                                    
                                    
                                    <div class="control-group">
										<label for="textfield" class="control-label">Quantity</label>
										<div class="controls">
											<input name="quantity" id="quantity" type="text" value="<?php echo $quantity; ?>" placeholder="Quantity" class="input-small">
										</div>
									</div>
                                    
                                    
                                    
                                    
                                    <div class="control-group">
										<label for="textfield" class="control-label">Design Cost</label>
                                        <div class="controls">
										<div class="input-append">
											<input name="design" id="design" type="text" value="<?php echo $design; ?>" placeholder="Design Cost" class="input-small">
                                            <span class="add-on"><?php echo $site_setting->currency_symbol;?></span>
                                            </div>
										</div>
									</div>
                                    
                                    <div class="control-group">
										<label for="textfield" class="control-label">Printing Cost</label>
                                        <div class="controls">
										<div class="input-append">
											<input name="printing" id="printing" type="text" value="<?php echo $printing; ?>" placeholder="Printing Cost" class="input-small">
                                            <span class="add-on"><?php echo $site_setting->currency_symbol;?></span>
                                            </div>
										</div>
									</div>
                                    
                                    <div class="control-group">
										<label for="textfield" class="control-label">Distribution Cost</label>
                                        <div class="controls">
										<div class="input-append">
											<input name="distribution_cost" id="distribution_cost" type="text" value="<?php echo $distribution_cost; ?>" placeholder="Distribution Cost" class="input-small">
                                            <span class="add-on"><?php echo $site_setting->currency_symbol;?></span>
                                            </div>
										</div>
									</div>
                                                  
                          
                          
   	<!-- datepicker plugin -->
	<link rel="stylesheet" href="<?php echo base_url();?>css/bootstrap-datetimepicker.min.css">

    
	<!-- datepicker plugin -->
	<script src="<?php echo base_url();?>js/bootstrap-datetimepicker.min.js"></script>


                <script type="text/javascript">
  $(function() {
    $('.datetimepicker').datetimepicker();
  });
</script>      
                      
                      
                      <div class="control-group">
										<label for="password" class="control-label">Begin Date</label>
										<div class="controls">
							
                            
                             <div  class="input-append date datetimepicker">
    <input data-format="dd/MM/yyyy hh:mm:ss" name="beginpgmdate" id="beginpgmdate" type="text" value="<?php if($beginpgmdate!='') { echo date('d/m/Y H:i:s',strtotime($beginpgmdate)); } ?>" placeholder="Begin Date" class="input-xlarge" />
    <span class="add-on">
      <i data-time-icon="icon-time" data-date-icon="icon-calendar">
      </i>
    </span>
  </div>
                            
                            				
										</div>
									</div>
                      
                       <div class="control-group">
										<label for="password" class="control-label">End Date</label>
										<div class="controls">
                                        
                                        <div class="input-append date datetimepicker">
    <input data-format="dd/MM/yyyy hh:mm:ss" name="endpgmdate" id="endpgmdate" type="text" value="<?php if($endpgmdate!='') { echo date('d/m/Y H:i:s',strtotime($endpgmdate)); } ?>" placeholder="End Date" class="input-xlarge" />
    <span class="add-on">
      <i data-time-icon="icon-time" data-date-icon="icon-calendar">
      </i>
    </span>
  </div>
  
  
										
										</div>
									</div>
                      
                      
                  
                       
                                    
									
                                    
                                    
									<div class="form-actions">
										 <?php if($leadsource_id=='') { ?>
												<button type="submit" class="button button-basic-blue">Save</button>
											<?php } else { ?>
											<button type="submit" class="button button-basic-blue">Save changes</button>
											<?php } ?>
                                            <button type="button" class="button button-basic" onClick="window.location.href='<?php echo site_url('leadsource/manage');?>'">Cancel</button>
                                           
											<input type="hidden" name="leadsource_id" id="leadsource_id" value="<?php echo $leadsource_id; ?>" />
											<input type="hidden" name="offset" id="offset" value="<?php echo $offset; ?>" />
										
									</div>
                                   
                                   
								</form>
							</div>
						</div>
					</div>
				</div>
				
				
				
			</div>