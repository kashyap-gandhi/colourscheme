<div class="page-header">
				<div class="pull-left">
					<h4><i class="icon-file-alt"></i> Assign Rights </h4>
				</div>
				<div class="pull-right">
					<ul class="bread">
						<li><a href="<?php echo site_url('user/teams');?>">Teams / Employee</a><span class="divider">/</span></li>
						<li class='active'>Assign Rights</li>
					</ul>
				</div>
			</div>
            
            
   
   			<div class="container-fluid" id="content-area">
				<div class="row-fluid">
					<div class="span12">
						<div class="box">
							<div class="box-head">
								<i class="icon-list-ul"></i>
								<span>Manage Access Rights for :: <?php echo ucfirst($team_detail->name);
                                if($team_detail->email!='') {
                                echo '('.$team_detail->email.')'; } ?></span>
							</div>
							<div class="box-body box-body-nopadding">
								  <?php
									$attributes = array('name'=>'frm_assignrights','class'=>'form-horizontal');
									echo form_open('user/add_rights/'.$team_id,$attributes);
								  ?>
									
									
									
									
									<div class="control-group">
										<label class="control-label"><a href="javascript:void(0)" onclick="javascript:setchecked('rights_id[]',1)" style="color:#000;"><?php echo "Check All"; ?></a>&nbsp;&nbsp;|&nbsp;&nbsp;<a href="javascript:void(0)" onclick="javascript:setchecked('rights_id[]',0)" style="color:#000;"><?php echo "Clear All"; ?></a></label>
										<div class="controls">
											
                                            <?php												
												if($rights)
												{
													foreach($rights as $rig)
													{																
										?>														
																
                                            <label class='checkbox'>
												
                                                <input type="checkbox" name="rights_id[]" value="<?php echo $rig->rights_id; ?>" style="width:40px;" <?php if($assign_rights) { if(in_array($rig->rights_id,$assign_rights)) { ?> checked="checked" <?php } } ?> /> <?php echo str_replace('_',' ',$rig->rights_name); ?>
											</label>
											
                                            <?php 	} 		
												} 
											?>	
																	
																	
										</div>
									</div>
									
									
									<div class="form-actions">
                                    <input type="hidden" name="team_id" id="team_id" value="<?php echo $team_id; ?>" />													 									 				 <input type="hidden" name="offset" id="offset" value="<?php echo $offset; ?>" />
										<button type="submit" class="button button-basic-blue">Save changes</button>
										
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
                
                </div>         

