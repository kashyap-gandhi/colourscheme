<?php $site_setting = site_setting();
$role_list = role_list(); ?>

<div class="page-header">
    <div class="pull-left">
        <h4><i class="icon-cogs"></i> <?php if ($team_id == '') { ?>Add Team<?php } if ($team_id != '') {
    if ($team_id == $this->session->userdata('team_id')) { ?>My Account<?php } else { ?>Edit Team<?php }
} ?></h4>
    </div>
    <div class="pull-right">
        <ul class="bread">
            <li><a href="<?php echo site_url('user/teams'); ?>">Teams / Employee</a><span class="divider">/</span></li>				
            <li class='active'>Detail</li>
        </ul>
    </div>
</div>


<div class="container-fluid" id="content-area">

    <div class="row-fluid">
        <div class="span12">
            
            

<?php if($msg!='') { ?>
<div class="alert <?php if($msg=='notfound' || $msg=='cannot') {?>alert-danger<?php } else { ?>alert-success<?php } ?>">
<button type="button" class="close" data-dismiss="alert">&times;</button>
<strong><?php if($msg=='notfound' || $msg=='cannot') {?>Warning<?php } else { ?>Success<?php } ?>!</strong>

<?php if($msg=='deletewage') { ?>Wage detail has been deleted successfully. <?php } ?>	

</div> 
<?php } ?>	

            

<?php if ($error != '') { ?>

                <div class="alert alert-error">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    <strong>Warning !</strong> <?php echo $error; ?>
                </div>    <?php } ?>




            <div class="box">
                <div class="box-head">
                    <i class="icon-list-ul"></i>
                    <span>Change your details</span>
                </div>
                <div class="box-body box-body-nopadding">
                    <?php
                    $attributes = array('name' => 'frm_addadmin', 'class' => 'form-horizontal form-bordered');
                    echo form_open('user/add_team', $attributes);
                    ?> 


                    <div class="control-group">
                        <label for="textfield" class="control-label">Username</label>
                        <div class="controls">
                            <input name="username" id="username" type="text" value="<?php echo $username; ?>" placeholder="Username" class="input-xlarge">
                            <span class="help-inline">(Username must be unique.)</span> 
                        </div>
                    </div>
                    <div class="control-group">
                        <label for="password" class="control-label">Name</label>
                        <div class="controls">
                            <input name="name" id="name" type="text" value="<?php echo $name; ?>" placeholder="Name" class="input-xlarge">
                        </div>
                    </div>



                    <div class="control-group">
                        <label for="textarea" class="control-label">Password</label>
                        <div class="controls">
                            <input name="password" id="password" type="password" value="" placeholder="Password" class="input-xlarge">
<?php if ($team_id != '') { ?> <span class="help-inline">(Leave Blank if you dont want to change password.)</span> <?php } ?>
                            <input name="ex_password" id="ex_password" type="hidden" value="<?php echo $ex_password; ?>">
                        </div>
                    </div>

                    <div class="control-group">
                        <label for="password" class="control-label">Email</label>
                        <div class="controls">
                            <input name="email" id="email" type="text" value="<?php echo $email; ?>" placeholder="Email" class="input-xlarge">
                            <span class="help-inline">(Email must be unique.)</span> 
                        </div>
                    </div>





                    <div class="control-group">
                        <label for="textarea" class="control-label">Primary Role</label>
                        <div class="controls">    

                            <select name="role1" id="role1" >
                                <option value="">Select</option>
                                <?php if (isset($role_list) && !empty($role_list)) {
                                    foreach ($role_list as $role) { ?>
                                        <option value="<?php echo $role->role_id; ?>" <?php if ($role1 == $role->role_id) { ?> selected="selected" <?php } ?>><?php echo ucfirst($role->role); ?></option>
    <?php }
} ?>
                            </select>

                        </div>
                    </div>

                    <div class="control-group">
                        <label for="textarea" class="control-label">Secondary Role</label>
                        <div class="controls">    

                            <select name="role2" id="role2" >
                                <option value="">Select</option>
                                <?php if (isset($role_list) && !empty($role_list)) {
                                    foreach ($role_list as $role) { ?>
                                        <option value="<?php echo $role->role_id; ?>" <?php if ($role2 == $role->role_id) { ?> selected="selected" <?php } ?>><?php echo ucfirst($role->role); ?></option>
    <?php }
} ?>
                            </select>

                        </div>
                    </div>

                    <!-- datepicker plugin -->
                    <link rel="stylesheet" href="<?php echo base_url(); ?>css/bootstrap-datetimepicker.min.css">


                    <!-- datepicker plugin -->
                    <script src="<?php echo base_url(); ?>js/bootstrap-datetimepicker.min.js"></script>


                    <script type="text/javascript">
                        $(function() {
                            $('.datetimepicker').datetimepicker();
                        });
                    </script>      


                    <div class="control-group">
                        <label for="password" class="control-label">Begin Date</label>
                        <div class="controls">


                            <div  class="input-append date datetimepicker">
                                <input data-format="dd/MM/yyyy hh:mm:ss" name="begin_date" id="begin_date" type="text" value="<?php if ($begin_date != '') {
    echo date('d/m/Y H:i:s', strtotime($begin_date));
} ?>" placeholder="Begin Date" class="input-xlarge" />
                                <span class="add-on">
                                    <i data-time-icon="icon-time" data-date-icon="icon-calendar">
                                    </i>
                                </span>
                            </div>


                        </div>
                    </div>

                    <div class="control-group">
                        <label for="password" class="control-label">End Date</label>
                        <div class="controls">

                            <div class="input-append date datetimepicker">
                                <input data-format="dd/MM/yyyy hh:mm:ss" name="end_date" id="end_date" type="text" value="<?php if ($end_date != '') {
    echo date('d/m/Y H:i:s', strtotime($end_date));
} ?>" placeholder="End Date" class="input-xlarge" />
                                <span class="add-on">
                                    <i data-time-icon="icon-time" data-date-icon="icon-calendar">
                                    </i>
                                </span>
                            </div>



                        </div>
                    </div>




                    <div class="control-group">
                        <label for="textarea" class="control-label">Status</label>
                        <div class="controls">    

                            <select name="active" id="active" >
                                <option value="0" <?php if ($active == 0) { ?> selected="selected" <?php } ?>>Inactive</option>
                                <option value="1" <?php if ($active == 1) { ?> selected="selected" <?php } ?>>Active</option>
                            </select>

                        </div>
                    </div>







                    <div class="form-actions">
<?php if ($team_id == '') { ?>
                            <button type="submit" class="button button-basic-blue">Save</button>
<?php } else { ?>
                            <button type="submit" class="button button-basic-blue">Save changes</button>
<?php } ?>
                        <button type="button" class="button button-basic" onClick="window.location.href='<?php echo site_url('user/teams'); ?>'">Cancel</button>

                        <input type="hidden" name="team_id" id="team_id" value="<?php echo $team_id; ?>" />
                        <input type="hidden" name="offset" id="offset" value="<?php echo $offset; ?>" />

                    </div>


                    </form>
                </div>
            </div>
       
        
        
        
        
        
        <!---wage list--->
        
        
        

            <div class="box">
                <div class="box-head">
                    <i class="icon-table"></i>
                    <span>Wage History</span>
                </div>


                <form name="frm_listwages" id="frm_listwages" action="<?php echo site_url('user/wage_action'); ?>" method="post">
                    <input type="hidden" name="action" id="action" />
                    <input type="hidden" name="offset" id="offset" value="<?php echo $offset; ?>" />
                    
                    <input type="hidden" name="action_from" id="action_from" value="edit_team" />
                    <input type="hidden" name="action_id" id="action_id" value="<?php echo $team_id; ?>" />

                    <div class="box-body box-body-nopadding">
                        <div class="highlight-toolbar">
                            <div class="pull-left">

                            </div>

                            <div class="pull-right"><div class="btn-toolbar">
                                    <div class="btn-group">
                                        <a href="<?php echo site_url('user/add_wage'); ?>" target="_blank" class='button button-basic button-icon' rel="tooltip" title="Add"><i class="icon-plus-sign"></i></a>
                                        <a href="javascript:void(0)" class='button button-basic button-icon' rel="tooltip" title="Delete" onclick="setaction('chk[]','delete', 'Are you sure, you want to delete selected record(s)?', 'frm_listwages')"><i class="icon-trash"></i></a>

                                    </div>
                                </div></div>

                        </div>
                        <table class="table table-nomargin table-striped table-bordered table-hover table-pagination">
                            <thead>
                                <tr>
                                   
                                    <th><input type="checkbox" class="checkall" /></th> 
                                    <th>Wage(<?php echo $site_setting->currency_symbol; ?>)</th>
                                    <th>Burden(<?php echo $site_setting->currency_symbol; ?>)</th>
                                    <th>Begin</th>
                                    <th>End</th>
                                    <th>Action</th>														
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if ($wage_history) {

                                    foreach ($wage_history as $res) {
                                        ?>

                                        <tr> 
                                          

                                           <td><input type="checkbox" id='chk' name="chk[]" value="<?php echo $res->wage_id;?>" /></td>  


                                            <td><?php echo $res->wage; ?></td> 
                                            <td><?php echo $res->burden; ?></td>

                                            <td><?php if ($res->beginwage != '') {
                                            echo date($site_setting->date_time_format, strtotime($res->beginwage));
                                        } ?></td> 
                                            <td><?php if ($res->endwage != '') {
                                            echo date($site_setting->date_time_format, strtotime($res->endwage));
                                        } ?></td> 
                                            <td>                                               
                                                <a href="<?php echo site_url('user/edit_wage/' . $res->wage_id . '/' . $offset); ?>" target="_blank" class='button button-basic button-icon' rel="tooltip" title="Edit"><i class="icon-edit"></i></a>
                                            </td>

                                        </tr> 



    <?php } ?>
<?php } else { ?>
                                    <tr><td colspan="6" align="center" valign="middle"  style="text-align:center;">No Wage has been added yet.</td></tr>
<?php } ?>

                            </tbody>
                        </table>
                        <div class="bottom-table">
                            <div class="pull-left">

                            </div>
                            <div class="pull-right"><div class="pagination pagination-custom">

                                </div></div>
                        </div>
                    </div>

                </form>

            </div>
       
        
        
        <!---wage list--->
        
        
        
        
        
        
        
        
        
        
        
        
        </div>
    </div>



</div>