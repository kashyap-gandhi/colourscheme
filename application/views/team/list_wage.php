<?php $site_setting=site_setting();?>
<div class="page-header">
				<div class="pull-left">
					<h4><i class="icon-cogs"></i>Wage History</h4>
				</div>
				<div class="pull-right">
					<ul class="bread">
						<li><a href="<?php echo site_url('user/teams');?>">Teams / Employee</a><span class="divider">/</span></li>
                        <li class="active">Wages</li>
					</ul>
				</div>
			</div>
            
            
            
            
            <div class="container-fluid" id="content-area">
            
            
            <div class="row-fluid">
								<div class="span12">
                                
                                
                                
                                
                                <?php if($msg!='') { ?>
<div class="alert <?php if($msg=='notfound' || $msg=='cannot') {?>alert-danger<?php } else { ?>alert-success<?php } ?>">
											<button type="button" class="close" data-dismiss="alert">&times;</button>
											<strong><?php if($msg=='notfound' || $msg=='cannot') {?>Warning<?php } else { ?>Success<?php } ?>!</strong>

	<?php if($msg=='delete') { ?>Wage detail has been deleted successfully. <?php } ?>	
	<?php if($msg=='insert') { ?>New Wage has been added successfully. <?php } ?>	
	<?php if($msg=='update') { ?>Wage detail has been updated successfully. <?php } ?>	
    <?php if($msg=='notfound') { ?>Wage records not found. <?php } ?>
  
</div> 
<?php } ?>	
                                
                                
                                
									<div class="box">
										<div class="box-head">
											<i class="icon-table"></i>
											<span>Manage Wage History</span>
										</div>
                                        

										 <form name="frm_listwages" id="frm_listwages" action="<?php echo site_url('user/wage_action');?>" method="post">
<input type="hidden" name="action" id="action" />
<input type="hidden" name="offset" id="offset" value="<?php echo $offset; ?>" />

                                        
                                        <div class="box-body box-body-nopadding">
											<div class="highlight-toolbar">
												<div class="pull-left">
													
												</div>
                                                
                                                <div class="pull-right"><div class="btn-toolbar">
													<div class="btn-group">
														<a href="<?php echo site_url('user/add_wage');?>" class='button button-basic button-icon' rel="tooltip" title="Add"><i class="icon-plus-sign"></i></a>
                                                         <a href="javascript:void(0)" class='button button-basic button-icon' rel="tooltip" title="Delete" onclick="setaction('chk[]','delete', 'Are you sure, you want to delete selected record(s)?', 'frm_listwages')"><i class="icon-trash"></i></a>
												
													</div>
												</div></div>
												
											</div>
											<table class="table table-nomargin table-striped table-bordered table-hover table-pagination">
												<thead>
													<tr>
														<th><input type="checkbox" class="checkall" /></th> 
                                                        <th>Team/Employee</th>
														<th>Wage(<?php echo $site_setting->currency_symbol;?>)</th>
                                                        <th>Burden(<?php echo $site_setting->currency_symbol;?>)</th>
                                                        <th>Begin</th>
                                                        <th>End</th>
														<th>Action</th>														
													</tr>
												</thead>
												<tbody>
									<?php if($result) { 
											
											foreach($result as $res) {
											
										?>
										
											<tr> 
												<td><input type="checkbox" id='chk' name="chk[]" value="<?php echo $res->wage_id;?>" /></td> 
												
                                                <td><?php echo anchor('user/edit_team/'.$res->team_id.'/0',$res->name,' rel="tooltip" title="Edit" ');?></td> 
											
                                            	
                                                <td><?php echo $res->wage;?></td> 
                                                <td><?php echo $res->burden;?></td>
                                                 
                                                <td><?php if($res->beginwage!='') { echo date($site_setting->date_time_format,strtotime($res->beginwage)); } ?></td> 
                                                <td><?php if($res->endwage!='') { echo date($site_setting->date_time_format,strtotime($res->endwage)); } ?></td> 
												<td>                                               
                                                <a href="<?php echo site_url('user/edit_wage/'.$res->wage_id.'/'.$offset);?>" class='button button-basic button-icon' rel="tooltip" title="Edit"><i class="icon-edit"></i></a>
                                           </td>
                                                
											</tr> 
											 
										 
										
										<?php   } ?>
										<?php } else { ?>
										<tr><td colspan="7" align="center" valign="middle"  style="text-align:center;">No Wage has been added yet.</td></tr>
										<?php } ?>
														
													</tbody>
												</table>
												<div class="bottom-table">
													<div class="pull-left">
														
													</div>
													<div class="pull-right"><div class="pagination pagination-custom">
														<?php echo $page_link; ?>
													</div></div>
												</div>
											</div>
										
                                    </form>
                                        
                                        </div>
									</div>
								</div>
            
            
            </div>