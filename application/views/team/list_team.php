<?php $site_setting = site_setting(); ?>
<div class="page-header">
    <div class="pull-left">
        <h4><i class="icon-cogs"></i> Team / Employee</h4>
    </div>
    <div class="pull-right">
        <ul class="bread">
            <li>Team / Employee</li>
        </ul>
    </div>
</div>




<div class="container-fluid" id="content-area">


    <div class="row-fluid">
        <div class="span12">




            <?php if ($msg != '') { ?>
                <div class="alert <?php if ($msg == 'notfound') { ?>alert-danger<?php } else { ?>alert-success<?php } ?>">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    <strong><?php if ($msg == 'notfound') { ?>Warning<?php } else { ?>Success<?php } ?>!</strong>
                    <?php if ($msg == 'active') { ?>Team account has been activated successfully. <?php } ?>
                    <?php if ($msg == 'inactive') { ?>Team account has been inactivated successfully. <?php } ?>
                    <?php if ($msg == 'delete') { ?>Team account has been deleted successfully. <?php } ?>	
                    <?php if ($msg == 'insert') { ?>New Team has been added successfully. <?php } ?>	
                    <?php if ($msg == 'update') { ?>Team account has been updated successfully. <?php } ?>	
                    <?php if ($msg == 'rights') { ?>Team right access has been updated successfully. <?php } ?>	
                    <?php if ($msg == 'notfound') { ?>Team records not found. <?php } ?>	

                </div> 
            <?php } ?>	


            
            <script>
        
        function searchme(){
                
                
                var search_by=$("select#search_by option:selected").val();
                var search_value=$.trim($("input#search_value").val());
                
                if(search_by==0)
		{
                    window.location.href='<?php echo site_url('user/teams');?>/';
		}
		else
		{
			
			window.location.href='<?php echo site_url('user/searchteam');?>/'+search_by+'/'+search_value;
		}
	
            
        }
        
        </script>
            

        

            <div class="box">
                <div class="box-head">
                    <i class="icon-table"></i>
                    <span>Manage Team</span>
                </div>

                    <div class="box-body box-body-nopadding">
                        <div class="highlight-toolbar">
                            <div class="pull-left">

                                
                                
                                <select name="search_by" id="search_by" style="margin: 5px;">
                                    <option value="0">All</option>
                                    <option value="name" <?php if($option=='name'){?> selected <?php } ?>>Name</option>
                                    <option value="email" <?php if($option=='email'){?> selected <?php } ?>>Email</option>
                                    
                                   
                                </select>
                                <input type="text" value=" <?php if($keyword!=''){ echo $keyword; } ?>" name="search_value" id="search_value" style="margin: 5px;" />
                                <input type="button" name="search_btn" value="Search" class="button button-basic" onclick="searchme();" />
                                
                                
                            </div>

                            
                            
                <form name="frm_listadmin" id="frm_listadmin" action="<?php echo site_url('user/team_action'); ?>" method="post">
                    <input type="hidden" name="action" id="action" />
                    <input type="hidden" name="offset" id="offset" value="<?php echo $offset; ?>" />


                            <div class="pull-right"><div class="btn-toolbar">
                                    <div class="btn-group">
                                        <a href="<?php echo site_url('user/add_team'); ?>" class='button button-basic button-icon' rel="tooltip" title="Add"><i class="icon-plus-sign"></i></a>

                                        <a href="javascript:void(0)" class='button button-basic button-icon' rel="tooltip" title="Active" onclick="setaction('chk[]','active', 'Are you sure, you want to active selected record(s)?', 'frm_listadmin')"><i class="icon-ok"></i></a>
                                        <a href="javascript:void(0)" class='button button-basic button-icon' rel="tooltip" title="Inactive" onclick="setaction('chk[]','inactive', 'Are you sure, you want to inactive selected record(s)?', 'frm_listadmin')"><i class="icon-minus-sign"></i></a>
                                        <a href="javascript:void(0)" class='button button-basic button-icon' rel="tooltip" title="Delete" onclick="setaction('chk[]','delete', 'If Team or Employee is linked to any records then it will not delete record. \n\nAre you sure, you want to delete selected record(s)?', 'frm_listadmin')"><i class="icon-trash"></i></a>
                                    </div>
                                </div></div>

                        </div>
                        <table class="table table-nomargin table-striped table-bordered table-hover table-pagination">
                            <thead>
                                <tr>
                                    <th><input type="checkbox" class="checkall" /></th> 
                                    <th>Name</th>
                                    <th>Username</th>
                                    <th>Email</th>

                                    <?php /* $assign_rights=get_rights('assign_rights');

                                      if($assign_rights==1) {

                                      ?>

                                      <th>Rights</th>

                                      <?php } */ ?>  


                                    <th>Begin</th> 
                                    <th>End</th> 
                                    <th>Create</th> 

                                    <th>Status</th>
                                    <th>Action</th>														
                                </tr>
                            </thead>
                            <tbody>
<?php
if ($result) {

    foreach ($result as $res) {
        ?>

                                        <tr> 
                                            <td><input type="checkbox" id='chk' name="chk[]" value="<?php echo $res->team_id; ?>" /></td> 
                                            <td><?php echo anchor('user/edit_team/' . $res->team_id . '/' . $offset, $res->name, ' rel="tooltip" title="Edit" '); ?></td> 

                                            <td><?php echo $res->username; ?></td> 
                                            <td><?php echo $res->email; ?></td> 


        <?php /* $assign_rights=get_rights('assign_rights');

          if(	$assign_rights==1) {

          ?>

          <td>

          <?php

          echo anchor('user/assign_rights/'.$res->team_id.'/'.$offset,'<i class="icon-user"></i>',' class="button button-basic button-icon" rel="tooltip" title="Right Access" ');

          ?></td>

          <?php } */ ?>




                                            <td><?php if ($res->begin_date != '') {
                                        echo date($site_setting->date_time_format, strtotime($res->begin_date));
                                    } ?></td> 
                                            <td><?php if ($res->end_date != '') {
                                        echo date($site_setting->date_time_format, strtotime($res->end_date));
                                    } ?></td> 
                                            <td><?php if ($res->team_date != '') {
                                        echo date($site_setting->date_time_format, strtotime($res->team_date));
                                    } ?></td> 


                                            <td><?php if ($res->active == 1) { ?><a href="javascript:void(0)" class="button button-basic button-icon" rel="tooltip" title="Active"><i class="icon-ok"></i></a><?php } else { ?><a href="javascript:void(0)" class="button button-basic button-icon" rel="tooltip" title="Inactive"><i class="icon-minus-sign"></i></a><?php } ?></td> 
                                            <td>                                               
                                                <a href="<?php echo site_url('user/edit_team/' . $res->team_id . '/' . $offset); ?>" class='button button-basic button-icon' rel="tooltip" title="Edit"><i class="icon-edit"></i></a>
                                            </td>
                                        </tr> 



                                    <?php } ?>
                                <?php } else { ?>
                                    <tr><td colspan="10" align="center" valign="middle"  style="text-align:center;">No Team / Employee has been added yet.</td></tr>
<?php } ?>

                            </tbody>
                        </table>
                        <div class="bottom-table">
                            <div class="pull-left">

                            </div>
                            <div class="pull-right"><div class="pagination pagination-custom">
<?php echo $page_link; ?>
                                </div></div>
                        </div>
                    </div>

                </form>

            </div>
        </div>
    </div>


</div>