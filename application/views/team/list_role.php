<?php $site_setting=site_setting();?>
<div class="page-header">
				<div class="pull-left">
					<h4><i class="icon-cogs"></i>Team Roles</h4>
				</div>
				<div class="pull-right">
					<ul class="bread">
						<li><a href="<?php echo site_url('user/teams');?>">Teams / Employee</a><span class="divider">/</span></li>
                        <li class="active">Roles</li>
					</ul>
				</div>
			</div>
            
            
            
            
            <div class="container-fluid" id="content-area">
            
            
            <div class="row-fluid">
								<div class="span12">
                                
                                
                                
                                
                                <?php if($msg!='') { ?>
<div class="alert <?php if($msg=='notfound' || $msg=='cannot') {?>alert-danger<?php } else { ?>alert-success<?php } ?>">
											<button type="button" class="close" data-dismiss="alert">&times;</button>
											<strong><?php if($msg=='notfound' || $msg=='cannot') {?>Warning<?php } else { ?>Success<?php } ?>!</strong>
<?php if($msg=='active') { ?>Role has been activated successfully. <?php } ?>
	<?php if($msg=='inactive') { ?>Role has been inactivated successfully. <?php } ?>
	<?php if($msg=='delete') { ?>Role detail has been deleted successfully. <?php } ?>	
	<?php if($msg=='insert') { ?>New Role has been added successfully. <?php } ?>	
	<?php if($msg=='update') { ?>Role detail has been updated successfully. <?php } ?>	
    <?php if($msg=='notfound') { ?>Role records not found. <?php } ?>
    <?php if($msg=='cannot') { ?>Cannot delete role because it used in many records. <?php } ?>		
  
</div> 
<?php } ?>	
                                
                                
                                
									<div class="box">
										<div class="box-head">
											<i class="icon-table"></i>
											<span>Manage Roles</span>
										</div>
                                        

										
                                        
                                        <div class="box-body box-body-nopadding">
											<div class="highlight-toolbar">
												<div class="pull-left">
													
												</div>
                                                
                                                <div class="pull-right"><div class="btn-toolbar">
													<div class="btn-group">
														<a href="<?php echo site_url('user/add_role');?>" class='button button-basic button-icon' rel="tooltip" title="Add"><i class="icon-plus-sign"></i></a>
												
													</div>
												</div></div>
												
											</div>
											<table class="table table-nomargin table-striped table-bordered table-hover table-pagination">
												<thead>
													<tr>
														
                                                        <th>Name</th>
														
														<th>Action</th>														
													</tr>
												</thead>
												<tbody>
									<?php if($result) { 
											
											foreach($result as $res) {
											
										?>
										
											<tr> 
												
												<td><?php echo anchor('user/edit_role/'.$res->role_id.'/'.$offset,$res->role,' rel="tooltip" title="Edit" ');?></td> 
										
												<td>                                               
                                                <a href="<?php echo site_url('user/edit_role/'.$res->role_id.'/'.$offset);?>" class='button button-basic button-icon' rel="tooltip" title="Edit"><i class="icon-edit"></i></a>
                                               <?php echo anchor('user/role_action/'.$res->role_id.'/delete/'.$offset,'<i class="icon-trash"></i>',' class="button button-basic button-icon" rel="tooltip" title="Delete" ');?></td>
                                                
											</tr> 
											 
										 
										
										<?php   } ?>
										<?php } else { ?>
										<tr><td colspan="2" align="center" valign="middle"  style="text-align:center;">No Roles has been added yet.</td></tr>
										<?php } ?>
														
													</tbody>
												</table>
												<div class="bottom-table">
													<div class="pull-left">
														
													</div>
													<div class="pull-right"><div class="pagination pagination-custom">
														<?php echo $page_link; ?>
													</div></div>
												</div>
											</div>
										
                                    
                                        
                                        </div>
									</div>
								</div>
            
            
            </div>