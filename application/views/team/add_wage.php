<?php $site_setting=site_setting();
$team_list=team_list();
 ?>

<div class="page-header">
				<div class="pull-left">
					<h4><i class="icon-cogs"></i> <?php if($wage_id!='') {?>Edit Wage<?php } else { ?>Add Wage<?php }  ?></h4>
				</div>
				<div class="pull-right">
					<ul class="bread">
						<li><a href="<?php echo site_url('user/teams');?>">Teams / Employee</a><span class="divider">/</span></li>				
						<li class='active'>Wage History</li>
					</ul>
				</div>
			</div>
            
            
            <div class="container-fluid" id="content-area">
				
<div class="row-fluid">
                <div class="span12">
                
                 <?php if($error!=''){ ?>

		<div class="alert alert-error">
											<button type="button" class="close" data-dismiss="alert">&times;</button>
											<strong>Warning !</strong> <?php echo $error;?>
										</div>    <?php }?>
                                        
                                        
    
					
						<div class="box">
							<div class="box-head">
								<i class="icon-list-ul"></i>
								<span>Change role details</span>
							</div>
							<div class="box-body box-body-nopadding">
                             <?php
									$attributes = array('name'=>'frm_addwage','class'=>'form-horizontal form-bordered');
									echo form_open('user/add_wage',$attributes);
								  ?> 
                                  
								
									<div class="control-group">
										<label for="textarea" class="control-label">Team / Employee</label>
										<div class="controls">    
                                        
                  <select name="team_id" id="team_id" >
                  <option value="">Select</option>
				<?php if(isset($team_list) && !empty($team_list)) { 
						foreach($team_list as $team) { ?>
                	<option value="<?php echo $team->team_id;?>" <?php if($team_id==$team->team_id) { ?> selected="selected" <?php } ?>><?php echo ucfirst($team->name); if($team->email!='') { echo "&nbsp;&nbsp;(".$team->email.")"; } ?></option>
				<?php } } ?>
                  </select>
                  
                    </div>
				</div>
                                    
                       
                                    
                                                
                                    <div class="control-group">
										<label for="textfield" class="control-label">Wage</label>
                                        <div class="controls">
										<div class="input-append">
											<input name="wage" id="wage" type="text" value="<?php echo $wage; ?>" placeholder="Wage" class="input-small">
                                            <span class="add-on"><?php echo $site_setting->currency_symbol;?></span>
                                            </div>
										</div>
									</div>
									
                       
                      
                                  
                                    <div class="control-group">
										<label for="textfield" class="control-label">Burden</label>
                                        <div class="controls">
										<div class="input-append">
											<input name="burden" id="burden" type="text" value="<?php echo $burden; ?>" placeholder="Burden" class="input-small">
                                            <span class="add-on"><?php echo $site_setting->currency_symbol;?></span>
                                            </div>
										</div>
									</div>
                                    
                                    
                   
                          
                          
   	<!-- datepicker plugin -->
	<link rel="stylesheet" href="<?php echo base_url();?>css/bootstrap-datetimepicker.min.css">

    
	<!-- datepicker plugin -->
	<script src="<?php echo base_url();?>js/bootstrap-datetimepicker.min.js"></script>


                <script type="text/javascript">
  $(function() {
    $('.datetimepicker').datetimepicker();
  });
</script>      
                      
                      
                      <div class="control-group">
										<label for="password" class="control-label">Begin Date</label>
										<div class="controls">
							
                            
                             <div  class="input-append date datetimepicker">
    <input data-format="dd/MM/yyyy hh:mm:ss" name="beginwage" id="beginwage" type="text" value="<?php if($beginwage!='') { echo date('d/m/Y H:i:s',strtotime($beginwage)); } ?>" placeholder="Begin Wage" class="input-xlarge" />
    <span class="add-on">
      <i data-time-icon="icon-time" data-date-icon="icon-calendar">
      </i>
    </span>
  </div>
                            
                            				
										</div>
									</div>
                      
                       <div class="control-group">
										<label for="password" class="control-label">End Date</label>
										<div class="controls">
                                        
                                        <div class="input-append date datetimepicker">
    <input data-format="dd/MM/yyyy hh:mm:ss" name="endwage" id="endwage" type="text" value="<?php if($endwage!='') { echo date('d/m/Y H:i:s',strtotime($endwage)); } ?>" placeholder="End Wage" class="input-xlarge" />
    <span class="add-on">
      <i data-time-icon="icon-time" data-date-icon="icon-calendar">
      </i>
    </span>
  </div>
  
  
										
										</div>
									</div>
                      
                      
                  
                       
                         
                         
                               
                                    
                                    
									<div class="form-actions">
										 <?php if($wage_id=='') { ?>
												<button type="submit" class="button button-basic-blue">Save</button>
											<?php } else { ?>
											<button type="submit" class="button button-basic-blue">Save changes</button>
											<?php } ?>
                                            <button type="button" class="button button-basic" onClick="window.location.href='<?php echo site_url('user/wages');?>'">Cancel</button>
                                           
											<input type="hidden" name="wage_id" id="wage_id" value="<?php echo $wage_id; ?>" />
											<input type="hidden" name="offset" id="offset" value="<?php echo $offset; ?>" />
										
									</div>
                                   
                                   
								</form>
							</div>
						</div>
					</div>
				</div>
				
				
				
			</div>