<?php $site_setting=site_setting();?>
<div class="page-header">
				<div class="pull-left">
					<h4><i class="icon-cogs"></i>Team Login History</h4>
				</div>
				<div class="pull-right">
					<ul class="bread">
						<li><a href="<?php echo site_url('user/teams');?>">Teams / Employee</a><span class="divider">/</span></li>
                        <li class="active">Team Login</li>
					</ul>
				</div>
			</div>
            
            
            
            
            <div class="container-fluid" id="content-area">
            
            
            <div class="row-fluid">
								<div class="span12">
                                
                                
                                
                                
                                <?php if($msg!='') { ?>
<div class="alert alert-success">
											<button type="button" class="close" data-dismiss="alert">&times;</button>
											<strong>Success !</strong>
<?php if($msg=='delete') { ?>Team login details has been deleted successfully. <?php } ?>	
  
</div> 
<?php } ?>	
                                
                                
                                
									<div class="box">
										<div class="box-head">
											<i class="icon-table"></i>
											<span>Watch Team Login</span>
										</div>
                                        
 <form name="frm_listadminlogin" id="frm_listadminlogin" action="<?php echo site_url('user/team_login_action');?>" method="post">
<input type="hidden" name="action" id="action" />
<input type="hidden" name="offset" id="offset" value="<?php echo $offset; ?>" />
										
                                        
                                        <div class="box-body box-body-nopadding">
											<div class="highlight-toolbar">
												<div class="pull-left">
													
												</div>
                                                
                                                <div class="pull-right"><div class="btn-toolbar">
													<div class="btn-group">
														
                                                        <a href="javascript:void(0)" class='button button-basic button-icon' rel="tooltip" title="Delete" onclick="setaction('chk[]','delete', 'Are you sure, you want to delete selected record(s)?', 'frm_listadminlogin')"><i class="icon-trash"></i></a>
													</div>
												</div></div>
												
											</div>
											<table class="table table-nomargin table-striped table-bordered table-hover table-pagination">
												<thead>
													<tr>
														<th><input type="checkbox" class="checkall" /></th> 
                                                        <th>Name</th>
														<th>Username</th>
														<th>Email</th>
                                                        
                                                        <th>Login Date</th> 
                                                        <th>Login IP</th> 
                                                       														
													</tr>
												</thead>
												<tbody>
									<?php if($result) { 
											
											foreach($result as $res) {
											
										?>
										
											<tr> 
												<td><input type="checkbox" id='chk' name="chk[]" value="<?php echo $res->login_id;?>" /></td> 
												<td><?php echo anchor('user/edit_team/'.$res->team_id.'/'.$offset,$res->name,' rel="tooltip" title="Edit" ');?></td> 
												
												<td><?php echo $res->username;?></td> 
												<td><?php echo $res->email;?></td> 
                                                
                                               

												<td><?php if($res->login_date!='') { echo date($site_setting->date_time_format,strtotime($res->login_date)); } ?></td> 
                                                <td><?php  echo $res->login_ip;  ?></td> 
                                               
                                                
												
											</tr> 
											 
										 
										
										<?php   } ?>
										<?php } else { ?>
										<tr><td colspan="6" align="center" valign="middle"  style="text-align:center;">No Team / Employee have take Login into system.</td></tr>
										<?php } ?>
														
													</tbody>
												</table>
												<div class="bottom-table">
													<div class="pull-left">
														
													</div>
													<div class="pull-right"><div class="pagination pagination-custom">
														<?php echo $page_link; ?>
													</div></div>
												</div>
											</div>
										
                                        </form>
                                        
                                        </div>
									</div>
								</div>
            
            
            </div>