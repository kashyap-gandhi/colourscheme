<?php if(!check_admin_authentication()){ redirect('home/logout'); } ?>
<div id="top"> 
		<div class="container-fluid">
			<div class="pull-left">
				<a href="#" id="brand"><?php echo SITE_NAME;?></a>
				<div class="collapse-me">
					
					<?php /*?><a href="#" class="button">
						<i class="icon-question-sign icon-white"></i>
						Support tickets
						<span class="badge badge-info">3</span>
					</a><?php */?>
                                    
                                    <?php if(check_role_permission('manage_quote')) { ?>
                   
					<a href="<?php echo site_url('quotes/finder');?>" class="button">
						<i class="icon-truck icon-white"></i>
						Add Quote
						<!--<span class="badge badge-default">5</span>-->
					</a>
                    <a href="<?php echo site_url('quotes/add_timecharge');?>" class="button">
						<i class="icon-time icon-white"></i>
						Add Timecharge
						<!--<span class="badge badge-important">21</span>-->
					</a>
                                    
                                    <?php } ?>
                                    
                                    
				</div>
			</div>
			<div class="pull-right">
				<div class="btn-group">
					<a href="#" class="button dropdown-toggle" data-toggle="dropdown"><i class="icon-white icon-user"></i><?php echo ucfirst($this->session->userdata('username'));?><span class="caret"></span></a>
					<div class="dropdown-menu pull-right">
						<div class="right-details">
							<h6>Logged in as</h6>
							<span class="name"><?php echo ucfirst($this->session->userdata('name'));?></span>
							
							<?php /*?><a href="#" class="highlighted-link">Need help?</a>
							<ul>
								
								<li>
									<a href="#">Account settings</a>
								</li>
							</ul><?php */?>
						</div>
					</div>
				</div>
				<a href="<?php echo site_url('home/logout');?>" class="button">
					<i class="icon-signout"></i>
					Logout
				</a>
			</div>
		</div>
	</div>