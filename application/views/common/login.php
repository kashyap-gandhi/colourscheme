<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
   "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<meta charset="utf8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
   <title><?php echo SITE_NAME; ?></title>
    <?php $base_url=base_url(); ?>	
   <!-- Bootstrap -->
	<link rel="stylesheet" href="<?php echo $base_url;?>css/bootstrap.min.css">
	<!-- Bootstrap responsive -->
	<link rel="stylesheet" href="<?php echo $base_url;?>css/bootstrap-responsive.min.css">
	<!-- Theme CSS -->
	<!--[if !IE]> -->
	<link rel="stylesheet" href="<?php echo $base_url;?>css/style.css">
	<!-- <![endif]-->
	<!--[if IE]>
	<link rel="stylesheet" href="css/style_ie.css">
	<![endif]-->

	<!-- jQuery -->
	<script src="<?php echo $base_url;?>js/jquery.min.js"></script>
	<!-- Bootstrap -->
	<script src="<?php echo $base_url;?>js/bootstrap.min.js"></script>

	<!-- Scrollable navigation -->
	<script src="<?php echo $base_url;?>js/jquery.nicescroll.min.js"></script>
    
	
	<script src="<?php echo $base_url;?>js/demonstration.min.js"></script>
	<!-- Theme framework -->
	<script src="<?php echo $base_url;?>js/eakroko.min.js"></script>
	<!-- Theme scripts -->
	<script src="<?php echo $base_url;?>js/application.min.js"></script>
    
   
</head>
<body class='login-body'>
   
 <div class="login-wrap">
		<h2><?php echo SITE_NAME; ?></h2>
		<div class="login">
        
        <?php if($error!='') { ?> <div class="alert alert-error">
											<button type="button" class="close" data-dismiss="alert">&times;</button>
											<strong>Warning!</strong> <?php echo $error; ?>
										</div><?php } ?>
        
			<?php if($msg) { if($msg=='logout') { ?> <div class="alert alert-success">
											<button type="button" class="close" data-dismiss="alert">&times;</button>
											User has been logout successfully.
										</div> <?php } }	?>
			
			<form action="<?php echo site_url('home/chklogin');?>" method="POST"> 
				
			
				<div class="email"><input type="text" name="username" placeholder="Username" class='input-block-level'></div>
				<div class="pw">
					<input type="password" name="password" placeholder="Password" class='input-block-level'>
				</div>
				<button type="submit" value="Sign In" class='button button-basic-darkblue btn-block'>Sign In</button>
			</form>
		</div>
		<a href="<?php echo site_url('home/forgot_password'); ?>" class='pw-link'>Forgot your <span>password</span>? <i class="icon-arrow-right"></i></a>
	</div>
   
   
</body>
</html>