
<div class="search"></div>


<ul class="mainNav" data-open-subnavs="multi">
    <li class='active'>
        <a href="<?php echo site_url('home/dashboard'); ?>"><i class="icon-home icon-white"></i><span>Dashboard</span></a>
    </li>

 <?php if(check_role_permission('manage_report')){ ?>
    <li>
        <a href=""><i class="icon-user icon-file-alt"></i><span>Reports</span><span class="label">4</span></a>
        <ul class="subnav">
            
            <?php if(check_role_permission('report_work_order')){ ?>
            <li>
                <a href="">Work Order Report</a>
            </li>
            <?php } ?>
            
            <?php if(check_role_permission('report_client_proposals')){ ?>
            <li>
                <a href="">Client Proposals</a>
            </li>
            <?php } ?>
            <?php if(check_role_permission('report_material_proposals')){ ?>
            <li>
                <a href="">Material Proposals</a>
            </li>
            <?php } ?>
            <?php if(check_role_permission('report_time_sheet')){ ?>
            <li>
                <a href="">Time Sheets</a>
            </li>
            <?php } ?>
            
        </ul>
    </li>
<?php } ?>

     <?php if(check_role_permission('manage_team')){ ?>
    <li>
        <a href=""><i class="icon-user icon-white"></i><span>Team / Employee</span><span class="label">4</span></a>
        <ul class="subnav">
            <li>
                <a href="<?php echo site_url('user/teams'); ?>">Team</a>
            </li>
            <li>
                <a href="<?php echo site_url('user/team_login'); ?>">Team Logins</a>
            </li>
            <li>
                <a href="<?php echo site_url('user/roles'); ?>">Roles</a>
            </li>
            <li>
                <a href="<?php echo site_url('user/wages'); ?>">Wage History</a>
            </li>

        </ul>
    </li>
    <?php } ?>
    
    <?php if(check_role_permission('manage_client')){ ?>
    <li>
        <a href="<?php echo site_url('clients/manage'); ?>"><i class="icon-group icon-white"></i><span>Manage Clients</span></a>
    </li>
    <?php } ?>

    <?php if(check_role_permission('manage_quote')){ ?>
    <li>
        <a href="javascript:void(0)"><i class="icon-edit icon-white"></i><span>Manage Quotes</span><span class="label">2</span></a>
        <ul class="subnav">           
            <li>
                <a href="<?php echo site_url('quotes/manage'); ?>">Quotes</a>
            </li>            
            <li>
                <a href="<?php echo site_url('quotes/managetype'); ?>">Quotes Type</a>
            </li>            
        </ul>
    </li>
    <?php } ?>
    
    
    <?php if(check_role_permission('manage_order')) { ?>
    <li>
        <a href=""><i class="icon-print icon-white"></i><span>Manage Orders</span></a>
    </li>
     <?php } ?>
    
    
     <?php if(check_role_permission('manage_product')) { ?>
    <li>
        <a href="javascript:void(0)"><i class="icon-folder-close icon-white"></i><span>Manage Products</span><span class="label">2</span></a>
        <ul class="subnav">
            <li>
                <a href="<?php echo site_url('products/manage'); ?>">Products</a>
            </li>
            <li>
                <a href="<?php echo site_url('products/managetype'); ?>">Product Quality</a>
            </li>
        </ul>
    </li>
<?php } ?>
    
    
 <?php if(check_role_permission('manage_feature')) { ?>
    <li>
        <a href="javascript:void(0)"><i class="icon-bookmark icon-white"></i><span>Manage Features</span><span class="label">2</span></a>
        <ul class="subnav">
            <li>
                <a href="<?php echo site_url('feature/footage'); ?>">Feature Footage</a>
            </li>
            <li>
                <a href="<?php echo site_url('feature/coat'); ?>">Feature Coat</a>
            </li>
        </ul>
    </li>
<?php } ?>
    
    
<?php if(check_role_permission('manage_leadsource')) { ?>
    <li>
        <a href="javascript:void(0)"><i class="icon-sitemap icon-white"></i><span>Lead Sources</span><span class="label">2</span></a>
        <ul class="subnav">
            <li>
                <a href="<?php echo site_url('leadsource/manage'); ?>">Lead Sources</a>
            </li>
            <li>
                <a href="<?php echo site_url('leadsource/managetype'); ?>">Lead Source Type</a>
            </li>
        </ul>
    </li>
<?php } ?>

<?php if(check_role_permission('manage_others')) { ?>
    <li>
        <a href="javascript:void(0)"><i class="icon-th-large icon-white"></i><span>Others</span><span class="label">6</span></a>
        <ul class="subnav">
            <?php if(check_role_permission('manage_bus_type')) { ?>
            <li>
                <a href="<?php echo site_url('others/bustype'); ?>">Bus Type</a>
            </li>
            <?php } ?>
            <?php if(check_role_permission('manage_charge_type')) { ?>
            <li>
                <a href="<?php echo site_url('others/chargetype'); ?>">Charge Type</a>
            </li>
            <?php } ?>
            <?php if(check_role_permission('manage_room_type')) { ?>
            <li>
                <a href="<?php echo site_url('others/roomtype'); ?>">Room Type</a>
            </li>
            <?php } ?>
            <?php if(check_role_permission('manage_sales_stage')) { ?>
            <li>
                <a href="<?php echo site_url('others/salesstage'); ?>">Sales Stage</a>
            </li>
            <?php } ?>
            <?php if(check_role_permission('manage_sheen')) { ?>
            <li>
                <a href="<?php echo site_url('others/sheen'); ?>">Sheen</a>
            </li>
            <?php } ?>
            <?php if(check_role_permission('manage_winloss')) { ?>
            <li>
                <a href="<?php echo site_url('others/winloss'); ?>">Win Loss</a>
            </li>
            <?php } ?>            
        </ul>
    </li>
<?php } ?>
    
    
    <?php if(check_role_permission('system_setting')) { ?>
    <li>
        <a href="javascript:void(0)"><i class="icon-cogs icon-white"></i><span>System Setting</span><span class="label">3</span></a>
        <ul class="subnav">
            <?php if(check_role_permission('site_setting')) { ?>
            <li>
                <a href="<?php echo site_url('setting/site'); ?>">Site</a>
            </li>
            <?php } ?>
            <?php if(check_role_permission('email_setting')) { ?>
            <li>
                <a href="<?php echo site_url('setting/email'); ?>">Email</a>
            </li>
            <?php } ?>
            <?php if(check_role_permission('email_templates_setting')) { ?>
            <li>
                <a href="<?php echo site_url('setting/email_template'); ?>">Email Templates</a>
            </li>
            <?php } ?>
        </ul>
    </li>
    <?php } ?>




</ul>


