<?php $site_setting=site_setting();
$quotetype_list=quotetype_list();
 ?>

<div class="page-header">
				<div class="pull-left">
					<h4><i class="icon-cogs"></i> <?php if($featurecoat_id!='') {?>Edit Feature Coat<?php } else { ?>Add Feature Coat<?php }  ?></h4>
				</div>
				<div class="pull-right">
					<ul class="bread">
						<li><a href="<?php echo site_url('feature/coat');?>">Manage Feature</a><span class="divider">/</span></li>
                        <li class="active">Feature Coat</li>
					</ul>
				</div>
			</div>
            
            
            <div class="container-fluid" id="content-area">
				
<div class="row-fluid">
                <div class="span12">
                
                 <?php if($error!=''){ ?>

		<div class="alert alert-error">
											<button type="button" class="close" data-dismiss="alert">&times;</button>
											<strong>Warning !</strong> <?php echo $error;?>
										</div>    <?php }?>
                                        
                                        
    
					
						<div class="box">
							<div class="box-head">
								<i class="icon-list-ul"></i>
								<span>Change Feature Coat details</span>
							</div>
							<div class="box-body box-body-nopadding">
                             <?php
									$attributes = array('name'=>'frm_addfeaturecoat','class'=>'form-horizontal form-bordered');
									echo form_open('feature/add_coat',$attributes);
								  ?> 
                                  
                                  
                                  
                                  <div class="control-group">
										<label for="textarea" class="control-label">Quote Type</label>
										<div class="controls">    
                                        
                  <select name="quotetype_id" id="quotetype_id" >
                  <option value="">Select</option>
				<?php if(isset($quotetype_list) && !empty($quotetype_list)) { 
						foreach($quotetype_list as $type) { ?>
                	<option value="<?php echo $type->quotetype_id;?>" <?php if($quotetype_id==$type->quotetype_id) { ?> selected="selected" <?php } ?>><?php echo ucfirst($type->quotetype); ?></option>
				<?php } } ?>
                  </select>
                  
                    </div>
				</div>
                
                
								
									<div class="control-group">
										<label for="textfield" class="control-label">Name</label>
										<div class="controls">
											<input name="rate_type" id="rate_type" type="text" value="<?php echo $rate_type; ?>" placeholder="Name" class="input-xlarge">
                                            
										</div>
									</div>
									
                       
                      
                      
                                    <div class="control-group">
										<label for="textfield" class="control-label">Rate</label>
										<div class="controls">
											<input name="rate" id="rate" type="text" value="<?php echo $rate; ?>" placeholder="Rate" class="input-xlarge">
                                            
										</div>
									</div>
                                    
                   
                          
                       
                         
                         
                               
                                    
                                    
									<div class="form-actions">
										 <?php if($featurecoat_id=='') { ?>
												<button type="submit" class="button button-basic-blue">Save</button>
											<?php } else { ?>
											<button type="submit" class="button button-basic-blue">Save changes</button>
											<?php } ?>
                                            <button type="button" class="button button-basic" onClick="window.location.href='<?php echo site_url('feature/coat');?>'">Cancel</button>
                                           
											<input type="hidden" name="featurecoat_id" id="featurecoat_id" value="<?php echo $featurecoat_id; ?>" />
											<input type="hidden" name="offset" id="offset" value="<?php echo $offset; ?>" />
										
									</div>
                                   
                                   
								</form>
							</div>
						</div>
					</div>
				</div>
				
				
				
			</div>