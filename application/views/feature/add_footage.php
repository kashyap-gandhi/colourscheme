<?php $site_setting=site_setting();
$quotetype_list=quotetype_list();
 ?>

<div class="page-header">
				<div class="pull-left">
					<h4><i class="icon-cogs"></i> <?php if($feature_id!='') {?>Edit Feature Footage<?php } else { ?>Add Feature Footage<?php }  ?></h4>
				</div>
				<div class="pull-right">
					<ul class="bread">
						<li><a href="<?php echo site_url('feature/footage');?>">Manage Feature</a><span class="divider">/</span></li>
                        <li class="active">Feature Footage</li>
					</ul>
				</div>
			</div>
            
            
            <div class="container-fluid" id="content-area">
				
<div class="row-fluid">
                <div class="span12">
                
                 <?php if($error!=''){ ?>

		<div class="alert alert-error">
											<button type="button" class="close" data-dismiss="alert">&times;</button>
											<strong>Warning !</strong> <?php echo $error;?>
										</div>    <?php }?>
                                        
                                        
    
					
						<div class="box">
							<div class="box-head">
								<i class="icon-list-ul"></i>
								<span>Change Feature Footage details</span>
							</div>
							<div class="box-body box-body-nopadding">
                             <?php
									$attributes = array('name'=>'frm_addfeaturefootage','class'=>'form-horizontal form-bordered');
									echo form_open('feature/add_footage',$attributes);
								  ?> 
                                  
                                  
                                  
                                  <div class="control-group">
										<label for="textarea" class="control-label">Quote Type</label>
										<div class="controls">    
                                        
                  <select name="quotetype_id" id="quotetype_id" >
                  <option value="">Select</option>
				<?php if(isset($quotetype_list) && !empty($quotetype_list)) { 
						foreach($quotetype_list as $type) { ?>
                	<option value="<?php echo $type->quotetype_id;?>" <?php if($quotetype_id==$type->quotetype_id) { ?> selected="selected" <?php } ?>><?php echo ucfirst($type->quotetype); ?></option>
				<?php } } ?>
                  </select>
                  
                    </div>
				</div>
                
                
								
									<div class="control-group">
										<label for="textfield" class="control-label">Name</label>
										<div class="controls">
											<input name="description" id="description" type="text" value="<?php echo $description; ?>" placeholder="Name" class="input-xlarge">
                                            
										</div>
									</div>
									
                       
                      
                      
                                    <div class="control-group">
										<label for="textfield" class="control-label">Default Footage</label>
										<div class="controls">
											<input name="default_footage" id="default_footage" type="text" value="<?php echo $default_footage; ?>" placeholder="Default Footage" class="input-xlarge">
                                            
										</div>
									</div>
                                    
                   
                          
                       
                         
                         
                               
                                    
                                    
									<div class="form-actions">
										 <?php if($feature_id=='') { ?>
												<button type="submit" class="button button-basic-blue">Save</button>
											<?php } else { ?>
											<button type="submit" class="button button-basic-blue">Save changes</button>
											<?php } ?>
                                            <button type="button" class="button button-basic" onClick="window.location.href='<?php echo site_url('feature/footage');?>'">Cancel</button>
                                           
											<input type="hidden" name="feature_id" id="feature_id" value="<?php echo $feature_id; ?>" />
											<input type="hidden" name="offset" id="offset" value="<?php echo $offset; ?>" />
										
									</div>
                                   
                                   
								</form>
							</div>
						</div>
					</div>
				</div>
				
				
				
			</div>