<?php $site_setting=site_setting();?>
<div class="page-header">
				<div class="pull-left">
					<h4><i class="icon-cogs"></i>Feature Footage</h4>
				</div>
				<div class="pull-right">
					<ul class="bread">
						<li><a href="<?php echo site_url('feature/footage');?>">Manage Feature</a><span class="divider">/</span></li>
                        <li class="active">Feature Footage</li>
					</ul>
				</div>
			</div>
            
            
            
            
            <div class="container-fluid" id="content-area">
            
            
            <div class="row-fluid">
								<div class="span12">
                                
                                
                                
                                
                                <?php if($msg!='') { ?>
<div class="alert <?php if($msg=='notfound' || $msg=='cannot') {?>alert-danger<?php } else { ?>alert-success<?php } ?>">
											<button type="button" class="close" data-dismiss="alert">&times;</button>
											<strong><?php if($msg=='notfound' || $msg=='cannot') {?>Warning<?php } else { ?>Success<?php } ?>!</strong>
<?php if($msg=='active') { ?>Feature Footage has been activated successfully. <?php } ?>
	<?php if($msg=='inactive') { ?>Feature Footage has been inactivated successfully. <?php } ?>
	<?php if($msg=='delete') { ?>Feature Footage detail has been deleted successfully. <?php } ?>	
	<?php if($msg=='insert') { ?>New Feature Footage has been added successfully. <?php } ?>	
	<?php if($msg=='update') { ?>Feature Footage detail has been updated successfully. <?php } ?>	
    <?php if($msg=='notfound') { ?>Feature Footage records not found. <?php } ?>
    <?php if($msg=='cannot') { ?>Cannot delete Feature Footage because it used in many records. <?php } ?>		
  
</div> 
<?php } ?>	
                                
                                
                                
									<div class="box">
										<div class="box-head">
											<i class="icon-table"></i>
											<span>Manage Feature Footage</span>
										</div>
                                        

									                                        

										 <form name="frm_listfeaturefootage" id="frm_listfeaturefootage" action="<?php echo site_url('feature/footage_action');?>" method="post">
<input type="hidden" name="action" id="action" />
<input type="hidden" name="offset" id="offset" value="<?php echo $offset; ?>" />	
                                        
                                        <div class="box-body box-body-nopadding">
											<div class="highlight-toolbar">
												<div class="pull-left">
													
												</div>
                                                
                                                <div class="pull-right"><div class="btn-toolbar">
													<div class="btn-group">
														<a href="<?php echo site_url('feature/add_footage');?>" class='button button-basic button-icon' rel="tooltip" title="Add"><i class="icon-plus-sign"></i></a>
                                                          <a href="javascript:void(0)" class='button button-basic button-icon' rel="tooltip" title="Delete" onclick="setaction('chk[]','delete', 'If Feature Footage is linked to any records then it will not delete record. \n\nAre you sure, you want to delete selected record(s)?', 'frm_listfeaturefootage')"><i class="icon-trash"></i></a>
												
													</div>
												</div></div>
												
											</div>
											<table class="table table-nomargin table-striped table-bordered table-hover table-pagination">
												<thead>
													<tr>
														<th><input type="checkbox" class="checkall" /></th> 
                                                        <th>Name</th>
                                                        <th>Quote Type</th>														
                                                        <th>Default Footage</th>														
														<th>Action</th>														
													</tr>
												</thead>
												<tbody>
									<?php if($result) { 
											
											foreach($result as $res) {
											
										?>
										
											<tr> 
												<td><input type="checkbox" id='chk' name="chk[]" value="<?php echo $res->feature_id;?>" /></td> 
												<td><?php echo anchor('feature/edit_footage/'.$res->feature_id.'/'.$offset,$res->description,' rel="tooltip" title="Edit" ');?></td> 	
                                                <td><?php echo ucfirst($res->quotetype); ?></td>
                                                
                                                 <td><?php echo $res->default_footage; ?></td>
										
												<td>                                               
                                                <a href="<?php echo site_url('feature/edit_footage/'.$res->feature_id.'/'.$offset);?>" class='button button-basic button-icon' rel="tooltip" title="Edit"><i class="icon-edit"></i></a>
                                               </td>
                                                
											</tr> 
											 
										 
										
										<?php   } ?>
										<?php } else { ?>
										<tr><td colspan="5" align="center" valign="middle"  style="text-align:center;">No Feature Footage has been added yet.</td></tr>
										<?php } ?>
														
													</tbody>
												</table>
												<div class="bottom-table">
													<div class="pull-left">
														
													</div>
													<div class="pull-right"><div class="pagination pagination-custom">
														<?php echo $page_link; ?>
													</div></div>
												</div>
											</div>
										
                                        </form>
                                    
                                        
                                        </div>
									</div>
								</div>
            
            
            </div>