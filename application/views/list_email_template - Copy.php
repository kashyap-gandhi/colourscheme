<div class="page-header">
				<div class="pull-left">
					<h4><i class="icon-cogs"></i> Email Templates</h4>
				</div>
				<div class="pull-right">
					<ul class="bread">
						<li>System Setting<span class="divider">/</span></li>						
						<li class='active'>Email Templates</li>
					</ul>
				</div>
			</div>
            
            
            
            
            <div class="container-fluid" id="content-area">
            
            
            <div class="row-fluid">
								<div class="span12">
                                
                                
                                
                                
                                <?php if($msg!='') { ?>
<div class="alert alert-success">
											<button type="button" class="close" data-dismiss="alert">&times;</button>
											<strong>Success !</strong>
	<?php if($msg=='active') { ?>Email Template has been activated successfully. <?php } ?>
	<?php if($msg=='inactive') { ?>Email Template has been inactivated successfully. <?php } ?>
	<?php if($msg=='delete') { ?>Email Template has been deleted successfully. <?php } ?>	
	<?php if($msg=='insert') { ?>New Email Template has been added successfully. <?php } ?>	
	<?php if($msg=='update') { ?>Email Template has been updated successfully. <?php } ?>		
</div> 
<?php } ?>	
                                
                                
                                
									<div class="box">
										<div class="box-head">
											<i class="icon-table"></i>
											<span>Email Templates</span>
										</div>
										<div class="box-body box-body-nopadding">
											<div class="highlight-toolbar">
												<div class="pull-left">
													
												</div>
                                                
                                                <div class="pull-right"><div class="btn-toolbar">
													<div class="btn-group">
														<a href="#" class='button button-basic button-icon' rel="tooltip" title="Archive"><i class="icon-inbox"></i></a>
														<a href="#" class='button button-basic button-icon' rel="tooltip" title="Mark as spam"><i class="icon-exclamation-sign"></i></a>
														<a href="#" class='button button-basic button-icon' rel="tooltip" title="Delete"><i class="icon-trash"></i></a>
													</div>
												</div></div>
												
											</div>
											<table class="table table-nomargin table-striped table-bordered table-hover table-pagination">
												<thead>
													<tr>
														<th>Name</th>
														<th>Subject</th>
														<th>From Email</th>
														<th>Action</th>														
													</tr>
												</thead>
												<tbody>
													<tr>
														<td>Trident</td>
														<td>Internet
															Explorer 4.0</td>
															<td>Win 95+</td>
															<td>4</td>
															<td>X</td>
														</tr>
														<tr>
															<td>Presto</td>
															<td>Nokia N800</td>
															<td>N800</td>
															<td>-</td>
															<td>A</td>
														</tr>
														<tr>
															<td>Misc</td>
															<td>NetFront 3.4</td>
															<td>Embedded devices</td>
															<td>-</td>
															<td>A</td>
														</tr>
														<tr>
															<td>Misc</td>
															<td>Dillo 0.8</td>
															<td>Embedded devices</td>
															<td>-</td>
															<td>X</td>
														</tr>
														<tr>
															<td>Misc</td>
															<td>Links</td>
															<td>Text only</td>
															<td>-</td>
															<td>X</td>
														</tr>
														<tr>
															<td>Misc</td>
															<td>Lynx</td>
															<td>Text only</td>
															<td>-</td>
															<td>X</td>
														</tr>
														<tr>
															<td>Misc</td>
															<td>IE Mobile</td>
															<td>Windows Mobile 6</td>
															<td>-</td>
															<td>C</td>
														</tr>
														<tr>
															<td>Misc</td>
															<td>PSP browser</td>
															<td>PSP</td>
															<td>-</td>
															<td>C</td>
														</tr>
														<tr>
															<td>Other browsers</td>
															<td>All others</td>
															<td>-</td>
															<td>-</td>
															<td>U</td>
														</tr>
													</tbody>
												</table>
												<div class="bottom-table">
													<div class="pull-left">
														<a href="#" class="button button-basic">Another button</a>
													</div>
													<div class="pull-right"><div class="pagination pagination-custom">
														<ul>
															<li><a href="#"><i class="icon-double-angle-left"></i></a></li>
															<li><a href="#">1</a></li>
															<li class='active'><a href="#">2</a></li>
															<li><a href="#">3</a></li>
															<li><a href="#">4</a></li>
															<li><a href="#">5</a></li>
															<li><a href="#"><i class="icon-double-angle-right"></i></a></li>
														</ul>
													</div></div>
												</div>
											</div>
										</div>
									</div>
								</div>
            
            
            </div>