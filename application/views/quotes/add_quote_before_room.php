<?php $site_setting=site_setting();
 ?>
 	<!-- datepicker plugin -->
	<link rel="stylesheet" href="<?php echo base_url();?>css/bootstrap-datetimepicker.min.css">

    
	<!-- datepicker plugin -->
	<script src="<?php echo base_url();?>js/bootstrap-datetimepicker.min.js"></script>

 

<div class="page-header">
				<div class="pull-left">
					<h4><i class="icon-cogs"></i> <?php if($quote_id!='') {?>Edit Quote<?php } else { ?>Add Quote<?php }  ?></h4>
				</div>
				<div class="pull-right">
					<ul class="bread">
						<li><a href="<?php echo site_url('quotes/manage');?>">Manage Quotes</a><span class="divider">/</span></li>
                        <li class="active">Quotes</li>
					</ul>
				</div>
			</div>
            
            
            <div class="container-fluid" id="content-area">
				
<div class="row-fluid">
                <div class="span12">
                
                 <?php if($error!=''){ ?>

                    <div class="alert alert-error">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                        <strong>Warning !</strong> <?php echo $error;?>
                    </div>    
                    <?php }?>
                                        
                                        
       <?php if($msg!='') { ?>
<div class="alert <?php if($msg=='notfound' || $msg=='cannot') {?>alert-danger<?php } else { ?>alert-success<?php } ?>">
											<button type="button" class="close" data-dismiss="alert">&times;</button>
											<strong><?php if($msg=='notfound' || $msg=='cannot') {?>Warning<?php } else { ?>Success<?php } ?>!</strong>

	<?php if($msg=='delete') { ?>Quote has been deleted successfully. <?php } ?>	
	<?php if($msg=='insert') { ?>New Quote has been added successfully. <?php } ?>	
	<?php if($msg=='update') { ?>Quote detail has been updated successfully. <?php } ?>	
    <?php if($msg=='notfound') { ?>Quote records not found. <?php } ?>
    <?php if($msg=='cannot') { ?>Cannot delete Quote because it used in many records. <?php } ?>		
  
</div> 
<?php } ?>	                
    
					
						<div class="box">
							<div class="box-head">
								<i class="icon-list-ul"></i>
								<span>Change Quote details</span>
                                
                         <?php
						   $data['quote_details']=$quote_details;						 
						   $this->load->view('quotes/quote_title',$data); 
						   ?>
                                
							</div>
							<div class="box-body box-body-nopadding">
                             <?php
									$attributes = array('name'=>'frm_addquote','class'=>'form-horizontal form-bordered form-wizard');
									echo form_open('quotes/add_quote/'.$quote_id,$attributes);
								  ?> 
                                  
								
                               
                              
                                    	<?php $this->load->view('quotes/quote_buttons',$data);  ?>
                                    
                                    
                                    <div class="step" id="firstStep">
										
                                        
                                          <!-----steps--->   
                                         <?php 
										   $data['step_index']=1;
										 $this->load->view('quotes/quote_steps',$data);  ?>
                                        <!-----steps--->
                                        
                                        
                                        
                                        
                                         <div class="control-group">
                                            <label for="textfield" class="control-label">name</label>
                                            <div class="controls controls-row" style="height: 15px;">
                                                <label for="first_name" class="span6">Company</label>
                                                <label for="QuoteContact" class="span6">Contact</label>
                                               
                                            </div>
                                            <div class="controls controls-row" style="height: 40px;">
                                                <input type="text" name="first_name" id="first_name" value="<?php echo $quote_details->first_name; ?>" placeholder="Company" class="span6" readonly="readonly">
                                                <input type="text" name="QuoteContact" id="QuoteContact" value="<?php echo $QuoteContact; ?>" placeholder="Contact" class="span6">
                                               
                                            </div>
                                        </div>
                                        
                                    <div class="control-group">&nbsp;</div>
                                        
										<div class="control-group">
                                            <label for="textfield" class="control-label">contact info</label>
                                            <div class="controls controls-row" style="height: 15px;">
                                                  <label for="QuoteAddress" class="span4">Location</label>                                            
                                                <label for="QuoteContactPhone" class="span4">Phone</label>    
                                               
                                                <label for="short_description" class="span4">Description</label>                                            
                                            </div>
                                            <div class="controls controls-row" style="height: 100px;">
                                               
                                               <?php 
											 	$location=$QuoteAddress;	
												  
											   if($QuoteAddress==''){
											   	$client_location='';
											    if($quote_details->Address!='') { $client_location.=$quote_details->Address; }
												if($quote_details->City!='') { $client_location.=','.$quote_details->City; }
												if($quote_details->Province!='') { $client_location.=','.$quote_details->Province; }
												if($quote_details->postal_code!='') { $client_location.=' - '.$quote_details->postal_code; }
												$location=$client_location;
										
												} 
											?>
                                               
                                               
                                                <input type="text" name="QuoteAddress" id="QuoteAddress" value="<?php echo $location;?>" placeholder="Location" class="span4">
                                                <input type="text" name="QuoteContactPhone" id="QuoteContactPhone" value="<?php echo $QuoteContactPhone; ?>" placeholder="Phone" class="span4">                                                <textarea rows="5" name="short_description" id="short_description" class="input-block-level span4" placeholder="Description"><?php echo $short_description; ?></textarea>
                                            </div>
                                        </div>
                                        
                                        <div class="control-group">&nbsp;</div>

                                       <div class="control-group">
                                            <label for="textfield" class="control-label">Lead</label>
                                            <div class="controls controls-row" style="height: 15px;">
                                                <label for="leadsource_id" class="span6">Source</label>
                                                <label for="ReferralName" class="span6">Referral</label>
                                               
                                            </div>
                                            <div class="controls controls-row" style="height: 40px;">
                                            <?php $leadsource_list=leadsource_list();?>
                                            <div class="span6">
                                            <select class="chosen-select" name="leadsource_id" id="leadsource_id">
                                                <option value="">---None---</option>
                                                <?php if(!empty($leadsource_list)){ 
                                              foreach($leadsource_list as $source) {?>
                                              <option value="<?php echo $source->leadsource_id;?>" <?php if($leadsource_id==$source->leadsource_id) {?> selected="selected"<?php } ?>><?php echo ucfirst($source->leadsource);?></option>
                                              <?php } } ?>
                                            </select>
                                            </div>
                                               
                                                <input type="text" name="ReferralName" id="ReferralName" value="<?php echo $ReferralName; ?>" placeholder="Referral Name" class="span6">
                                               
                                            </div>
                                        </div>
                                        
                                        <div class="control-group">&nbsp;</div>
										
										
										<div class="control-group">
                                            <label for="textfield" class="control-label">Rate / Date</label>
                                            <div class="controls controls-row" style="height: 15px;">
                                                <label for="first_name" class="span4">Paint</label>
                                                <label for="rate" class="span4">Job Rate(<?php echo $site_setting->currency_symbol;?>)</label>
                                                 <label for="date" class="span4">Quote Date</label>
                                                 
                                               
                                            </div>
                                            <div class="controls controls-row" style="height: 40px;">
                                            <div class="span4">
                                  <select name="PaintQuality" id="PaintQuality" class="chosen-select">
									 <?php if(!empty($quote_paint)){ 
                                      foreach($quote_paint as $paint) {?>
                                      <option value="<?php echo $paint->productcost_id;?>" <?php if($PaintQuality==$paint->productcost_id){?> selected="selected" <?php } ?>><?php echo ucfirst(trim($paint->description)).'&nbsp;&nbsp;|&nbsp;&nbsp;'.ucfirst(trim($paint->paintbrand));?></option>
                                      <?php } } ?>
                                 </select>
                                 </div>
                                            
                                            
                                            
                                            <select name="rate" id="rate" class="span4">
										    <?php for($i=58;$i>=40;$i--){ ?>
                                            <option value="<?php echo $i; ?>" <?php if($rate==$i){?> selected="selected" <?php } ?>><?php echo $i; ?></option>
                                            <?php } ?>
                                            </select>
                                               
                                          
                                            
                                            <div  class="input-append date datetimepicker span4">
                                            <input name="date" id="date" type="text" value="<?php echo date('d-m-Y H:i:s',strtotime($date));?>" readonly="readonly" placeholder="Quote Date" class="input-large" />
                                            <span class="add-on">
                                              <i data-time-icon="icon-time" data-date-icon="icon-calendar">
                                              </i>
                                            </span>
                                          </div>
                                            
                                            
                                          	
                                            
                                            
                                               
                                            </div>
                                        </div>

                                        
                                         <div class="control-group">&nbsp;</div>
                                        
                                        <div class="control-group">
                                            <label for="textfield" class="control-label">Sales</label>
                                            <div class="controls controls-row" style="height: 15px;">
                                                <label for="first_name" class="span3">Sales Lead</label>
                                                <label for="last_name" class="span3">Stage</label>
                                                 <label for="last_name" class="span3">Probability</label>
                                                 <label for="last_name" class="span3">Win / Loss</label>
                                                 
                                               
                                            </div>
                                            <div class="controls controls-row" style="height: 40px;">
                                            <?php $team_list=team_list(); ?>
                                            <div class="span3"><select name="team_id" id="team_id" class="chosen-select">
                                             <?php if(!empty($team_list)){ 
										  foreach($team_list as $team) {?>
                                          <option value="<?php echo $team->team_id;?>" <?php if($team_id==$team->team_id){?> selected="selected" <?php } ?>><?php echo ucfirst($team->name);?></option>
                                          <?php } } ?>
                                            </select></div>
                                            
                                            
                                            
                                             <?php $salesstage_list=salesstage_list(); ?>                                              
                                            <select class="span3" name="salesstage_id" id="salesstage_id">
                                            <?php if(!empty($salesstage_list)){ 
										  foreach($salesstage_list as $stage) {?>
                                          <option value="<?php echo $stage->salesstage_id;?>" <?php if($salesstage_id==$stage->salesstage_id){?> selected="selected" <?php } ?>><?php echo ucfirst($stage->sales_stage);?></option>
                                          <?php } } ?>
                                            </select>
                                            
                                            
                                            
                                            <select class="span3" name="Probability" id="Probability">
                                            <?php if(!empty($all_probability)) { foreach($all_probability as $key=>$prob) { ?>
                                            <option value="<?php echo $prob;?>" <?php if($Probability==$prob){?> selected="selected" <?php } ?>><?php echo $prob;?></option>
                                            <?php }} ?>
                                            </select>
                                            
											
											
											<?php $winloss_list=winloss_list();?>
                                            <select class="span3" name="wonreason_id" id="wonreason_id">
                                              <?php if(!empty($winloss_list)){ 
										  foreach($winloss_list as $wonloss) {?>
                                          <option value="<?php echo $wonloss->wonreason_id;?>" <?php if($wonreason_id==$wonloss->wonreason_id){?> selected="selected" <?php } ?>><?php echo ucfirst($wonloss->reason);?></option>
                                          <?php } } ?>
                                          </select>
                                               
                                            </div>
                                        </div>
                                        
                                 <div class="control-group">&nbsp;</div>
                                 
                                 
                                 <div class="control-group" style="height:50px;">
                                 <label for="textfield" class="control-label">Quote Details</label>
                                 </div>
                                 
                                 <div class="form-actions">
										
                                    <button type="button" class="button button-basic-green addroom">Add Room</button>
                                   
                                    
                                </div>
                                        
                               <style>
							   .ht0 { height: 0px; }
							   .ht15 { height: 15px; }
							   .ht40 { height: 40px; }
							   .ht65 { height: 65px; }
							   .pad10 { padding: 10px; }
							   .pad4 { padding: 4px; }
							   .roommain { border: 2px solid #ADADAD; margin-bottom:2px; }
							   .borT { border-top: 1px solid #ccc; } 
							   .borB { border-bottom: 1px solid #ccc; }
							   #maindiv .controls { margin:0px !important; }
							   </style>         
                                        <!----------room part--------->
                                        
                                        
                                <div class="control-group">
                                 
                                 <?php $roomcnt=1; ?>
                                 
                                 <div id="maindiv">
                                 
                                 
                                 
                                 
                                	<!--rroom type-->
                                    <div id="room<?php echo $roomcnt; ?>" class="roommain">
                                
                                    <div class="controls controls-row ht40">
                                    <div class="span4">Room Type <?php echo $roomcnt; ?></div>
                                    <div class="span6">Additional Room Description</div>
                                    <div class="span2"><button type="button" data-cnt="<?php echo $roomcnt; ?>" class="button button-basic-blue addroomfeature">Add Feature</button></div>
                                    </div>
                                  
                                    <div class="controls controls-row ht40">
                                    <select class="span4" name="roomtype_id<?php echo $roomcnt; ?>" id="roomtype_id<?php echo $roomcnt; ?>"><option>Dock</option></select>
                                    <input type="text" name="additionalroomdescription<?php echo $roomcnt; ?>" id="additionalroomdescription<?php echo $roomcnt; ?>" placeholder="Additional Room Description" class="span6">
                                    <a href="javascript:void(0)" title="Delete" data-id="" data-cnt="<?php echo $roomcnt; ?>" class="span2 pad10 removeroom"><i class="icon-remove"></i></a>
                                    </div>
                                    <!--rroom type-->    
                                
                                	<!--room feature 1--->
                                	<div id="roomfeature<?php echo $roomcnt; ?>" class="borT">
                                    <?php $roomfeaturecnt=1; ?>
                                    
                                    
                                    <div class="controls controls-row borB">                                    
                                     	<div class="span1">Feature</div>
                                        <div class="span1">Lenght</div>
                                        <div class="span1">Width</div>
                                        <div class="span1">Height</div>
                                        <div class="span1">Qty.</div>
                                        <div class="span1">UpG</div>
                                        <div class="span1">Rate</div>
                                        <div class="span1">Coats</div>
                                        <div class="span1">Sheen</div>
                                        <div class="span1">Hrs.</div>
                                        <div class="span1">Sq. Ft.</div>
                                        <div class="span1">Optional/<br />Action</div>
                                    </div>
                                    
<div id="feature<?php echo $roomfeaturecnt; ?>" class="controls controls-row borB ht65">
<select name="feature_id<?php echo $roomfeaturecnt; ?>" id="feature_id<?php echo $roomfeaturecnt; ?>" class="span1"><option>Features</option></select>
<select name="Length<?php echo $roomfeaturecnt; ?>" id="Length<?php echo $roomfeaturecnt; ?>" class="span1"><option>1</option></select>
<select name="Width<?php echo $roomfeaturecnt; ?>" id="Width<?php echo $roomfeaturecnt; ?>" class="span1"><option>2</option></select>
<select name="Height<?php echo $roomfeaturecnt; ?>" id="Height<?php echo $roomfeaturecnt; ?>" class="span1"><option>3</option></select>
<select name="Quantity<?php echo $roomfeaturecnt; ?>" id="Quantity<?php echo $roomfeaturecnt; ?>" class="span1"><option>0</option></select>
<select name="addSqft<?php echo $roomfeaturecnt; ?>" id="addSqft<?php echo $roomfeaturecnt; ?>" class="span1">

<?php for($ui=10;$ui<=25;$ui++) {?>
<option><?php echo $ui; ?></option>
<?php } ?>

</select>
<select name="rate<?php echo $roomfeaturecnt; ?>" id="rate<?php echo $roomfeaturecnt; ?>" class="span1"><option>0.25</option></select>
<select name="Multiplyer<?php echo $roomfeaturecnt; ?>" id="Multiplyer<?php echo $roomfeaturecnt; ?>" class="span1"><option>1</option></select>
<select name="sheen_id<?php echo $roomfeaturecnt; ?>" id="sheen_id<?php echo $roomfeaturecnt; ?>" class="span1"><option>N/A</option></select>
<input name="sqfootage<?php echo $roomfeaturecnt; ?>" id="sqfootage<?php echo $roomfeaturecnt; ?>" class="span1" value="0.0" type="text" readonly="readonly" />
<input name="totTime<?php echo $roomfeaturecnt; ?>" id="totTime<?php echo $roomfeaturecnt; ?>" class="span1" value="0.0" type="text" readonly="readonly" />
<div class="span1">
<input type="checkbox" name="<?php echo $roomfeaturecnt; ?>" id="<?php echo $roomfeaturecnt; ?>" />
<a href="javascript:void(0)" title="Delete" data-id="" data-fcnt="<?php echo $roomfeaturecnt; ?>" data-cnt="<?php echo $roomcnt; ?>" class="pad10 removefeature"><i class="icon-remove"></i></a> </div>         <br /><br />
<input type="text" name="description<?php echo $roomfeaturecnt; ?>" id="description<?php echo $roomfeaturecnt; ?>" placeholder="Additional Description" class="span12">
</div>
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    </div><!--room feature 1--->
                             <input type="hidden" name="roomfeaturecnt<?php echo $roomcnt; ?>" id="roomfeaturecnt<?php echo $roomcnt; ?>" value="<?php echo $roomfeaturecnt; ?>" />
                                    
                                    
                                    <!--room commited 1--->
                                    
                                    <div class="controls controls-row ht0">&nbsp;</div>
                                    <div class="controls controls-row borB ht15">                                    
                                     	<div class="span12">Committed Features</div>
                                    </div>
                                    
                                    <div class="controls controls-row borB ht15">                                    
                                     	<div class="span3">Prep</div>
                                        <div class="span3">S & C</div>
                                        <div class="span3">Closets</div>
                                        <div class="span3">Misc</div>
                                    </div>
                                    
                                    
                                    
                                    <div class="controls controls-row ht40">
                                        <select name="prep<?php echo $roomcnt; ?>" id="prep<?php echo $roomcnt; ?>" class="span3"><option>1</option></select>
                                        <select name="setclean<?php echo $roomcnt; ?>" id="setclean<?php echo $roomcnt; ?>" class="span3"><option>1</option></select>
                                        <select name="closets<?php echo $roomcnt; ?>" id="closets<?php echo $roomcnt; ?>" class="span3"><option>2</option></select>
                                        <select name="misc<?php echo $roomcnt; ?>" id="misc<?php echo $roomcnt; ?>" class="span3"><option>3</option></select>
                                    </div>
                                    
                                    <div class="controls controls-row ht0">&nbsp;</div>
                                    
                                     <div class="controls controls-row borB ht15">                                    
                                     	<div class="span12">Optional Features</div>
                                    </div>
                                    
                                    <div class="controls controls-row borB ht15">                                    
                                     	<div class="span3">Prep</div>
                                        <div class="span3">S & C</div>
                                        <div class="span3">Closets</div>
                                        <div class="span3">Misc</div>
                                    </div>
                                    
                                    <div class="controls controls-row ht40">
                                        <select name="optprep<?php echo $roomcnt; ?>" id="optprep<?php echo $roomcnt; ?>" class="span3"><option>1</option></select>
                                        <select name="optsetclean<?php echo $roomcnt; ?>" id="optsetclean<?php echo $roomcnt; ?>" class="span3"><option>1</option></select>
                                        <select name="optclosets<?php echo $roomcnt; ?>" id="optclosets<?php echo $roomcnt; ?>" class="span3"><option>2</option></select>
                                        <select name="optmisc<?php echo $roomcnt; ?>" id="optmisc<?php echo $roomcnt; ?>" class="span3"><option>3</option></select>
                                    </div>
                                    
                                    <!--room commited 1--->
                                    
                                    
                                    
                                	</div><!---room 1-->
                                
                                
                                
                                
                                
                                
                                </div><!--maindiv-->
                                
                                
                                </div> 
                                
                                
                                <input type="hidden" id="roomcnt" name="roomcnt" value="<?php echo $roomcnt; ?>" />
                                       
                                <!----------room part--------->
                                        
                                        
                                        
                                        
                                        
                                        
									</div><!--step 1 div-->
                                    
									
                                    
                                    
									<div class="form-actions">
										
												<button type="submit" class="button button-basic-blue">Save & Next</button>
										
                                            <button type="button" class="button button-basic" onClick="window.location.href='<?php echo site_url('quotes/managetype');?>'">Cancel</button>
                                           
											<input type="hidden" name="quote_id" id="quote_id" value="<?php echo $quote_id; ?>" />
											<input type="hidden" name="offset" id="offset" value="<?php echo $offset; ?>" />
										
									</div>
                                   
                                   
								</form>
							</div>
						</div>
					</div>
				</div>
				
				
				
			</div>
            
            
            
<script type="text/javascript">
  $(function() {
    $('.datetimepicker').datetimepicker({
		format: 'dd-MM-yyyy hh:mm:ss'
	});
		
		////===add room===
		$(".addroom").on("click",function(){
			
			var roomcnt = $("#roomcnt").val();	
			var newroomcnt = parseInt(roomcnt) + parseInt(1);		
			var roomhtml = addroom(newroomcnt);
			
			$("#maindiv").append(roomhtml);
			$("#roomcnt").val(newroomcnt);	
			
			$("div#room"+newroomcnt+" .addroomfeature").bind("click",function(){				
				featurebind(newroomcnt);				
			});
			
			$("div#room"+newroomcnt+" .removeroom").bind("click",function(){
				var roomcnt = $(this).attr('data-cnt');			
				var roomdbid = $(this).attr('data-id');						
				deleteroom(newroomcnt,roomdbid);			
			});
			
			var roomfeaturecnt=1;
			$("div#room"+newroomcnt+" div#roomfeature"+newroomcnt+" div#feature"+roomfeaturecnt+" .removefeature").bind("click",function(){			
				var roomcnt = $(this).attr('data-cnt');
				var roomfeaturecnt = $(this).attr('data-fcnt');			
				var roomfeaturedbid = $(this).attr('data-id');				
				deleteroomfeature(roomcnt,roomfeaturecnt,roomfeaturedbid);			
			});
			
		});
		
		//====add room feature====
		$(".addroomfeature").on("click",function(){			
			var roomcnt = $(this).attr('data-cnt');			
			featurebind(roomcnt);	
		});
		
		
		///===remove roome===
		$(".removeroom").on("click",function(){			
			var roomcnt = $(this).attr('data-cnt');			
			var roomdbid = $(this).attr('data-id');				
			deleteroom(roomcnt,roomdbid);			
		});
		
		///===remove feature====
		$(".removefeature").on("click",function(){			
			var roomcnt = $(this).attr('data-cnt');
			var roomfeaturecnt = $(this).attr('data-fcnt');			
			var roomfeaturedbid = $(this).attr('data-id');				
			deleteroomfeature(roomcnt,roomfeaturecnt,roomfeaturedbid);			
		});
		
		
  });
  
  function deleteroom(roomcnt,roomdbid){
		if(roomdbid>0) {
		
		} 
		
		$("div#room"+roomcnt).remove();
  }
  
  function deleteroomfeature(roomcnt,roomfeaturecnt,roomfeaturedbid){
  		
		if(roomfeaturedbid>0) {
		
		} 
		
		$("div#room"+roomcnt+" div#roomfeature"+roomcnt+" div#feature"+roomfeaturecnt).remove();
  }
  
  function featurebind(roomcnt){
		var roomfeaturecnt = $("#roomfeaturecnt"+roomcnt).val();
		
		var newroomfeaturecnt = parseInt(roomfeaturecnt) + parseInt(1);	
			
		var featurehtml = addroomfeature(roomcnt,newroomfeaturecnt);
		
		$("div#room"+roomcnt+" div#roomfeature"+roomcnt).append(featurehtml);
		$("div#room"+roomcnt+" input#roomfeaturecnt"+roomcnt).val(newroomfeaturecnt);	
		
		
		$("div#room"+roomcnt+" div#roomfeature"+roomcnt+" div#feature"+newroomfeaturecnt+" .removefeature").bind("click",function(){			
			var roomcnt = $(this).attr('data-cnt');
			var roomfeaturecnt = $(this).attr('data-fcnt');			
			var roomfeaturedbid = $(this).attr('data-id');				
			deleteroomfeature(roomcnt,roomfeaturecnt,roomfeaturedbid);			
		});
			
  }
  
  function addroom(roomcnt){
  	
	var html="";
	
	var roomfeaturecnt=1;
	
	html +='<div id="room'+roomcnt+'" class="roommain">';
	
	html +='<div class="controls controls-row ht40"><div class="span4">Room Type '+roomcnt+'</div><div class="span6">Additional Room Description</div><div class="span2"><button type="button" data-cnt="'+roomcnt+'" class="button button-basic-blue addroomfeature">Add Feature</button></div></div>';
	
	html +='<div class="controls controls-row ht40"><select class="span4" name="roomtype_id'+roomcnt+'" id="roomtype_id'+roomcnt+'">';
	
	<?php if(!empty($all_room_type)) { foreach($all_room_type as $roomtype) { ?>
	html +='<option value="<?php echo $roomtype->roomtype_id;?>"><?php echo ucfirst($roomtype->description);?></option>';
	<?php }} ?>
	
	html +='</select><input type="text" name="additionalroomdescription'+roomcnt+'" id="additionalroomdescription'+roomcnt+'" placeholder="Additional Room Description" class="span6"><a href="javascript:void(0)" title="Delete" data-id="" data-cnt="'+roomcnt+'" class="span2 pad10 removeroom"><i class="icon-remove"></i></a></div>';
	
	html +='<div id="roomfeature'+roomcnt+'" class="borT">';
	
	html +='<div class="controls controls-row borB"><div class="span1">Feature</div><div class="span1">Length</div><div class="span1">Width</div><div class="span1">Height</div><div class="span1">Qty.</div><div class="span1">UpG</div><div class="span1">Rate</div><div class="span1">Coats</div><div class="span1">Sheen</div><div class="span1">Hrs.</div><div class="span1">Sq. Ft.</div><div class="span1">&nbsp;</div></div>';
	
	html +=addroomfeature(roomcnt,roomfeaturecnt);
	
	html +='</div>';
	html +='<input type="hidden" name="roomfeaturecnt'+roomcnt+'" id="roomfeaturecnt'+roomcnt+'" value="'+roomfeaturecnt+'" />';
	
	
	html +='<div class="controls controls-row ht0">&nbsp;</div>';
	html +='<div class="controls controls-row borB ht15"><div class="span12">Committed Features</div></div>';
	html +='<div class="controls controls-row borB ht15"><div class="span3">Prep</div><div class="span3">S &amp; C</div><div class="span3">Closets</div><div class="span3">Misc</div></div>';
	
	html +='<div class="controls controls-row ht40">';
	html +='<select name="prep'+roomcnt+'" id="prep'+roomcnt+'" class="span3">';
	
	<?php if(!empty($all_prep)) { foreach($all_prep as $key=>$prep) { ?>
	html +='<option value="<?php echo $prep;?>"><?php echo $prep;?></option>';
	<?php }} ?>
	
	html +='</select><select name="setclean'+roomcnt+'" id="setclean'+roomcnt+'" class="span3">';
	
	<?php if(!empty($all_sc)) { foreach($all_sc as $key=>$sc) { ?>
	html +='<option value="<?php echo $sc;?>"><?php echo $sc;?></option>';
	<?php }} ?>
	
	html +='</select><select name="closets'+roomcnt+'" id="closets'+roomcnt+'" class="span3">';
	
	<?php if(!empty($all_clostes)) { foreach($all_clostes as $key=>$clostes) { ?>
	html +='<option value="<?php echo $clostes;?>"><?php echo $clostes;?></option>';
	<?php }} ?>
	
	html +='</select><select name="misc'+roomcnt+'" id="misc'+roomcnt+'" class="span3">';
	
	<?php if(!empty($all_misc)) { foreach($all_misc as $key=>$misc) { ?>
	html +='<option value="<?php echo $misc;?>"><?php echo $misc;?></option>';
	<?php }} ?>
	
	html +='</select>';
	html +='</div>';
	
	
	html +='<div class="controls controls-row ht0">&nbsp;</div>';
	html +='<div class="controls controls-row borB ht15"><div class="span12">Optional Features</div></div>';
	html +='<div class="controls controls-row borB ht15"><div class="span3">Prep</div><div class="span3">S &amp; C</div><div class="span3">Closets</div><div class="span3">Misc</div></div>';
	
	
	html +='<div class="controls controls-row ht40">';
	html +='<select name="optprep'+roomcnt+'" id="optprep'+roomcnt+'" class="span3">';
	
	<?php if(!empty($all_prep)) { foreach($all_prep as $key=>$prep) { ?>
	html +='<option value="<?php echo $prep;?>"><?php echo $prep;?></option>';
	<?php }} ?>
	
	html +='</select><select name="optsetclean'+roomcnt+'" id="optsetclean'+roomcnt+'" class="span3">';
	
	<?php if(!empty($all_sc)) { foreach($all_sc as $key=>$sc) { ?>
	html +='<option value="<?php echo $sc;?>"><?php echo $sc;?></option>';
	<?php }} ?>
	
	html +='</select><select name="optclosets'+roomcnt+'" id="optclosets'+roomcnt+'" class="span3">';
	
	<?php if(!empty($all_clostes)) { foreach($all_clostes as $key=>$clostes) { ?>
	html +='<option value="<?php echo $clostes;?>"><?php echo $clostes;?></option>';
	<?php }} ?>
	
	html +='</select><select name="optmisc'+roomcnt+'" id="optmisc'+roomcnt+'" class="span3">';
	
	<?php if(!empty($all_misc)) { foreach($all_misc as $key=>$misc) { ?>
	html +='<option value="<?php echo $misc;?>"><?php echo $misc;?></option>';
	<?php }} ?>
	
	html +='</select>';
	html +='</div>';
	
	html +='</div>';
	
	return html;
  
  }
  
  function addroomfeature(roomcnt,roomfeaturecnt){
  	var html="";
	
	html +='<div id="feature'+roomfeaturecnt+'" class="controls controls-row borB ht65">';
	
	html +='<select name="feature_id'+roomfeaturecnt+'" id="feature_id'+roomfeaturecnt+'" class="span1">';
	
	<?php if(!empty($all_feature_type)) { foreach($all_feature_type as $feature) { ?>
	html +='<option value="<?php echo $feature->feature_id;?>"><?php echo ucfirst($feature->description);?></option>';
	<?php }} ?>
	
	html +='</select><select name="Length'+roomfeaturecnt+'" id="Length'+roomfeaturecnt+'" class="span1">';
	
	<?php for($i=1;$i<=50;$i++){ ?>
	html +='<option value="<?php echo $i;?>"><?php echo $i;?></option>';
	<?php } ?>
	
	html +='</select><select name="Width'+roomfeaturecnt+'" id="Width'+roomfeaturecnt+'" class="span1">';
	
	<?php for($i=1;$i<=50;$i++){ ?>
	html +='<option value="<?php echo $i;?>"><?php echo $i;?></option>';
	<?php } ?>
	
	html +='</select><select name="Height'+roomfeaturecnt+'" id="Height'+roomfeaturecnt+'" class="span1">';
	
	<?php for($i=1;$i<=50;$i++){ ?>
	html +='<option value="<?php echo $i;?>"><?php echo $i;?></option>';
	<?php } ?>
	
	html +='</select><select name="Quantity'+roomfeaturecnt+'" id="Quantity'+roomfeaturecnt+'" class="span1">';
	
	<?php for($i=1;$i<=50;$i++){ ?>
	html +='<option value="<?php echo $i;?>"><?php echo $i;?></option>';
	<?php } ?>
	
	html +='</select><select name="addSqft'+roomfeaturecnt+'" id="addSqft'+roomfeaturecnt+'" class="span1">';
	
	<?php for($i=10;$i<=25;$i++){ ?>
	html +='<option value="<?php echo $i;?>"><?php echo $i;?></option>';
	<?php } ?>
	
	html +='</select><select name="rate'+roomfeaturecnt+'" id="rate'+roomfeaturecnt+'" class="span1">';
	
	<?php if(!empty($all_feature_coat)) { foreach($all_feature_coat as $fcoat) { ?>
	html +='<option value="<?php echo $fcoat->rate;?>"><?php echo $fcoat->rate."&nbsp;&nbsp;|&nbsp;&nbsp;".ucfirst($fcoat->rate_type);?></option>';
	<?php }} ?>
	
	html +='</select><select name="Multiplyer'+roomfeaturecnt+'" id="Multiplyer'+roomfeaturecnt+'" class="span1">';
	
	html +='<option value="1">1</option>';
	html +='<option value="2">2</option>';
	html +='<option value="3">3</option>';
	html +='<option value="4">4</option>';
	
	html +='</select><select name="sheen_id'+roomfeaturecnt+'" id="sheen_id'+roomfeaturecnt+'" class="span1">';
	
	<?php if(!empty($all_sheen)) { foreach($all_sheen as $sheen) { ?>
	html +='<option value="<?php echo $sheen->sheen_id;?>"><?php echo ucfirst($sheen->sheen_description);?></option>';
	<?php }} ?>
	
	html +='</select>';
	
	html +='<input name="sqfootage'+roomfeaturecnt+'" id="sqfootage'+roomfeaturecnt+'" class="span1" value="0.0" type="text" readonly="readonly" />';
	html +='<input name="totTime'+roomfeaturecnt+'" id="totTime'+roomfeaturecnt+'" class="span1" value="0.0" type="text" readonly="readonly" />';
	
	html+='<div class="span1">';
	
	html+='<input type="checkbox" name="'+roomcnt+'" id="'+roomcnt+'" />';
	
	html +='<a href="javascript:void(0)" title="Delete" data-id="" data-fcnt="'+roomfeaturecnt+'" data-cnt="'+roomcnt+'" class="pad10 removefeature"><i class="icon-remove"></i></a>';
	
	html+='</div><br /><br/>';
	
	html +='<input type="text" name="description'+roomfeaturecnt+'" id="description'+roomfeaturecnt+'" placeholder="Additional Description" class="span12" />';
	html +='</div>';
	
	return html;
  }
</script>   