<?php
$site_setting = site_setting();
$site_pst_tax = $site_setting->past_tax;
?>
<!-- datepicker plugin -->
<link rel="stylesheet" href="<?php echo base_url(); ?>css/bootstrap-datetimepicker.min.css">


<!-- datepicker plugin -->
<script src="<?php echo base_url(); ?>js/bootstrap-datetimepicker.min.js"></script>



<div class="page-header">
    <div class="pull-left">
        <h4><i class="icon-cogs"></i> <?php if ($quote_id != '') { ?>Edit Quote<?php } else { ?>Add Quote<?php } ?></h4>
    </div>
    <div class="pull-right">
        <ul class="bread">
            <li><a href="<?php echo site_url('quotes/manage'); ?>">Manage Quotes</a><span class="divider">/</span></li>
            <li class="active">Quotes</li>
        </ul>
    </div>
</div>


<div class="container-fluid" id="content-area">

    <div class="row-fluid">
        <div class="span12">

<?php if ($error != '') { ?>

                <div class="alert alert-error">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    <strong>Warning !</strong> <?php echo $error; ?>
                </div>    <?php } ?>

<?php if ($msg != '') { ?>
                <div class="alert <?php if ($msg == 'notfound' || $msg == 'cannot') { ?>alert-danger<?php } else { ?>alert-success<?php } ?>">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    <strong><?php if ($msg == 'notfound' || $msg == 'cannot') { ?>Warning<?php } else { ?>Success<?php } ?>!</strong>

                    <?php if ($msg == 'delete') { ?>Quote Materials has been deleted successfully. <?php } ?>	
                    <?php if ($msg == 'insert') { ?>New Quote Materials has been added successfully. <?php } ?>	
                    <?php if ($msg == 'update') { ?>Quote Materials detail has been updated successfully. <?php } ?>	
                    <?php if ($msg == 'notfound') { ?>Quote Materials records not found. <?php } ?>
    <?php if ($msg == 'cannot') { ?>Cannot delete Quote Materials because it used in many records. <?php } ?>		

                </div> 
<?php } ?>	                



            <div class="box">
                <div class="box-head">
                    <i class="icon-list-ul"></i>
                    <span>Change Quote details</span>

                    <?php
                    $data['quote_details'] = $quote_details;
                    $this->load->view('quotes/quote_title', $data);
                    ?>
                </div>
                <div class="box-body box-body-nopadding">
                    <?php
                    $attributes = array('name' => 'frm_addquote', 'class' => 'form-horizontal form-bordered form-wizard');
                    echo form_open('quotes/step2/' . $quote_id, $attributes);
                    ?> 


<?php $this->load->view('quotes/quote_buttons', $data); ?>

                   <div class="step" id="firstStep">


                        <!-----steps--->   
                        <?php
                        $data['step_index'] = 2;
                        $this->load->view('quotes/quote_steps', $data);
                        ?>
                        <!-----steps--->







                        <br /><br />

                        <div class="row-fluid">

                            <!---commited features--->
                            <div class="span6">

                                <div align="center" style="font-weight:bold;">Committed - Materials Required</div>


                                <div class="controls controls-row borB marAll padT20">                                    
                                    <div class="span3">Feature</div>
                                    <div class="span3">Sheen</div>
                                    <div class="span3">SqFt</div>
                                    <div class="span3">Gallons</div>
                                </div>


                                <?php
                                $spreadqty = 0;
//echo $quote_details->PaintQuality;
                                if ($quote_details->PaintQuality != '') {

                                    $product_detail = product_by_productcost_id($quote_details->PaintQuality);

  //                                  print_r($product_detail); 
                                    if (!empty($product_detail)) {

                                        $spreadqty = $product_detail->spreadqty;
                                    }
                                }
//echo $spreadqty; die;


                                if (!empty($commit_material)) {
                                    foreach ($commit_material as $comt) {

                                        $gallon_total=0;
                                        if($spreadqty!=0) {
                                            
                                          //  echo $comt->total_sqft."== / ==".$spreadqty;
                                          $gallon_total=  $comt->total_sqft / $spreadqty;
                                          $gallon_total=round_up($gallon_total,0);
                                        }
                                        ?>

                                        <div class="controls controls-row marAll padT10">   
                                            <input class="span3" value="<?php if ($comt->description != '') {
                                    echo $comt->description;
                                } ?>" type="text" readonly="readonly" />
                                            <input class="span3" value="<?php if ($comt->sheen_description != '') {
                                    echo $comt->sheen_description;
                                } else {
                                    echo "N/A";
                                } ?>" type="text" readonly="readonly" />
                                            <input class="span3" value="<?php echo $comt->total_sqft; ?>" type="text" readonly="readonly" />
                                            <input class="span3" value="<?php echo $gallon_total; ?>" type="text" readonly="readonly" />
                                        </div>


    <?php }
} ?>



                            </div>
                            <!---commited features--->


                            <!---optional features--->
                            <div class="span6">

                                <div align="center" style="font-weight:bold;">Optional - Materials</div>

                                <div class="controls controls-row borB marAll2 padT20">                                    
                                    <div class="span3">Feature</div>
                                    <div class="span3">Sheen</div>
                                    <div class="span3">SqFt</div>
                                    <div class="span3">Gallons</div>
                                </div>

<?php if (!empty($optional_material)) {
    foreach ($optional_material as $opmt) { 
        
        $gallon_total=0;
                                        if($spreadqty!=0) {
                                            
                                                                                       
                                          // echo $opmt->total_sqftc."=== / ===".$spreadqty;

                                          $gallon_total=  $opmt->total_sqftc / $spreadqty;
                                          $gallon_total=round_up($gallon_total,0);
                                        }
                                        
                                        ?>
                                        <div class="controls controls-row marAll2 padT10">   
                                            <input class="span3" value="<?php if ($opmt->description != '') {
            echo $opmt->description;
        } ?>" type="text" readonly="readonly" />
                                            <input class="span3" value="<?php if ($opmt->sheen_description != '') {
            echo $opmt->sheen_description;
        } else {
            echo "N/A";
        } ?>" type="text" readonly="readonly" />
                                            <input class="span3" value="<?php echo $opmt->total_sqftc; ?>" type="text" readonly="readonly" />
                                            <input class="span3" value="<?php echo $gallon_total; ?>" type="text" readonly="readonly" />
                                        </div>
    <?php }
} ?>



                            </div>
                            <!---optional features--->


                        </div>


                        <br /><br /><br />



                        <div class="control-group borT">
                            <label for="textfield" class="control-label">Order Materials</label>
                        </div>

                        <div class="form-actions">

                            <button type="button" class="button button-basic-green addmaterial">Add Material</button>


                        </div>

                        <style>
                            .marAll{ margin:0px 0px 0px 8px !important; }
                            .marAll2{ margin:0px 8px 0px 0px !important; }
                            .marL0 { margin:0px !important; }
                            .ht0 { height: 0px; }
                            .ht15 { height: 15px; }
                            .ht40 { height: 40px; }
                            .ht65 { height: 65px; }
                            .padT10 { padding-top:10px; }
                            .padT20 { padding-top:20px; }
                            .pad10 { padding: 10px; }
                            .roommain { border: 2px solid #ADADAD; margin-bottom:2px; }
                            .borT { border-top: 1px solid #ccc; } 
                            .borB { border-bottom: 1px solid #ccc; }
                            #maindiv .controls { margin:0px !important; }
                        </style>         
                        <!----------room part--------->


                        <div class="control-group">





                            <!--rroom type-->
                            <div id="maindiv" class="roommain">


                                <div class="controls controls-row borB materialheader">                                    
                                    <div class="span3">Product</div>
                                    <div class="span2">Quote</div>
                                    <div class="span1">Qty.</div>
<!--                                    <div class="span1">Opt. Qty.</div>-->
                                    <div class="span2">Sheen</div>
                                    <div class="span2">Colour</div>                                        
<!--                                    <div class="span1">PST Tax(%)</div>-->
                                                                          
                                    <div class="span1">Cost(<?php echo $site_setting->currency_symbol; ?>)</div>
<!--                                    <div class="span1">Opt. Cost(<?php //echo $site_setting->currency_symbol; ?>)</div>-->

                                    <div class="span1">&nbsp;</div>
                                </div>


                                <!--order material 1--->

<?php
$ordercnt = 1;

if (!empty($order_details)) {

    foreach ($order_details as $order) {
        ?>

                                        <div id="order<?php echo $ordercnt; ?>" class="borT"> 

                                            <div class="controls controls-row borB" <?php if($order->opttype == 1) { ?> style="background: #E3FFD9;" <?php } elseif($order->opttype == 2) { ?> style="background: #FDDEC1;" <?php } elseif($order->opttype == 3) { ?> style="background: #DFF6FF;" <?php } elseif($order->opttype == 4) { ?> style="background: #FCDFDF;" <?php } ?>>  
                                                Material <?php echo $ordercnt; ?>
                                            </div>                



                                            <div class="controls controls-row borB ht65" <?php if($order->opttype == 1) { ?> style="background: #E3FFD9;" <?php } elseif($order->opttype == 2) { ?> style="background: #FDDEC1;" <?php } elseif($order->opttype == 3) { ?> style="background: #DFF6FF;" <?php } elseif($order->opttype == 4) { ?> style="background: #FCDFDF;" <?php } ?>>
                                                <select name="productcost_id<?php echo $ordercnt; ?>" id="productcost_id<?php echo $ordercnt; ?>" class="span3 prodsel">
                                                    <option value="">Select Product</option>
                                                    <?php if (!empty($all_product_list)) {
                                                        foreach ($all_product_list as $prod) { ?>
                                                            <option value="<?php echo $prod->productcost_id; ?>" data-cost="<?php echo $prod->cost; ?>" data-markupcost="<?php echo $prod->markup_cost; ?>" data-psttax="<?php echo $prod->pst_tax; ?>" <?php if ($order->productcost_id == $prod->productcost_id) { ?> selected="selected" <?php } ?>><?php echo $prod->cost . '&nbsp;&nbsp;|&nbsp;&nbsp;' . ucfirst(trim($prod->description)) . '&nbsp;&nbsp;|&nbsp;&nbsp;' . ucfirst(trim($prod->paintbrand)); ?></option>
                                                        <?php }
                                                    } ?>
                                                </select>
                                                
                                          <select name="opttype<?php echo $ordercnt; ?>" id="opttype<?php echo $ordercnt; ?>" class="span2 selopttype">
    <option value="0" <?php if ($order->opttype == 0) { ?> selected="selected" <?php } ?>>Base</option><option value="1"  <?php if ($order->opttype == 1) { ?> selected="selected" <?php } ?>>Opt 1</option><option value="2"  <?php if ($order->opttype == 2) { ?> selected="selected" <?php } ?>>Opt 2</option><option value="3"  <?php if ($order->opttype == 3) { ?> selected="selected" <?php } ?>>Opt 3</option><option value="4"  <?php if ($order->opttype == 4) { ?> selected="selected" <?php } ?>>Opt 4</option></select>
                                                
                                                
                                                <select name="qty<?php echo $ordercnt; ?>" id="qty<?php echo $ordercnt; ?>" class="span1 selqty">
        <?php for ($i = 1; $i <= 50; $i++) { ?>
                                                        <option value="<?php echo $i; ?>" <?php if ($order->qty == $i) { ?> selected="selected" <?php } ?>><?php echo $i; ?></option>
        <?php } ?>
                                                </select>
                                               <?php /* <select name="ncqty<?php echo $ordercnt; ?>" id="ncqty<?php echo $ordercnt; ?>" class="span1 selnqty">
                                                    <?php for ($i = 0; $i <= 50; $i++) { ?>
                                                        <option value="<?php echo $i; ?>" <?php if ($order->ncqty == $i) { ?> selected="selected" <?php } ?>><?php echo $i; ?></option>
                                                    <?php } ?>
                                                </select> */ ?>
                                                <select name="sheen_id<?php echo $ordercnt; ?>" id="sheen_id<?php echo $ordercnt; ?>" class="span2">
        <?php if (!empty($all_sheen)) {
            foreach ($all_sheen as $sheen) { ?>
                                                            <option value="<?php echo $sheen->sheen_id; ?>" <?php if ($order->sheen_id == $sheen->sheen_id) { ?> selected="selected" <?php } ?>><?php echo ucfirst($sheen->sheen_description); ?></option>
            <?php }
        } ?>
                                                </select>
                                                <input name="colour<?php echo $ordercnt; ?>" id="colour<?php echo $ordercnt; ?>" class="span2" value="<?php echo $order->colour; ?>" type="text" />

                                                <input name="pst_tax<?php echo $ordercnt; ?>" id="pst_tax<?php echo $ordercnt; ?>" class="span1 selpst" value="<?php echo $order->pst_tax; ?>" type="hidden" readonly="readonly" />

                                               


                                                <input name="productcostsum<?php echo $ordercnt; ?>" id="productcostsum<?php echo $ordercnt; ?>" class="span1" value="<?php echo $order->productcostsum; ?>" type="text" readonly="readonly" />
                                                <input name="optionalcostsum<?php echo $ordercnt; ?>" id="optionalcostsum<?php echo $ordercnt; ?>" class="span1" value="<?php echo $order->optionalcostsum; ?>" type="hidden" />
                                                <a href="javascript:void(0)" data-id="<?php echo $order->orderdetail_id; ?>" data-cnt="<?php echo $ordercnt; ?>" class="span1 pad10 removeorder"><i class="icon-remove"></i></a>          <br />
                                                <input type="text" name="notes<?php echo $ordercnt; ?>" id="notes<?php echo $ordercnt; ?>" placeholder="Order Notes"  value="<?php echo $order->notes; ?>" class="span12">
                                                
                                                <input type="hidden" name="ncqty<?php echo $ordercnt; ?>" id="ncqty<?php echo $ordercnt; ?>" value="<?php echo $order->ncqty; ?>" />
                                               
                                                
                                                <input type="hidden" name="orderdetail_id<?php echo $ordercnt; ?>" id="orderdetail_id<?php echo $ordercnt; ?>" value="<?php echo $order->orderdetail_id; ?>" />
                                                 <input type="hidden" name="markup_cost<?php echo $ordercnt; ?>" id="markup_cost<?php echo $ordercnt; ?>" value="<?php echo $order->markup_cost; ?>" />
                                            </div>



                                        </div><!--order material 1--->

        <?php $ordercnt++;
    }
} ?>



                            </div><!---room 1--><!--maindiv-->




                        </div> 


                        <input type="hidden" id="ordercnt" name="ordercnt" value="<?php echo $ordercnt; ?>" />

                        <!----------room part--------->






                    </div><!--step 1 div-->




                    <div class="form-actions">

                        <button type="submit" class="button button-basic-blue savebtnquote">Save</button>

                        <button type="button" class="button button-basic" onClick="window.location.href='<?php echo site_url('quotes/managetype'); ?>'">Cancel</button>

                        <input type="hidden" name="quote_id" id="quote_id" value="<?php echo $quote_id; ?>" />


                    </div>


                    </form>
                </div>
            </div>
        </div>
    </div>



</div>



<script type="text/javascript">
    $(function() {
   
        ////===add material===
        $(".addmaterial").on("click",function(){
			
            var ordercnt = $("#ordercnt").val();	
            var newordercnt = parseInt(ordercnt) + parseInt(1);		
            var materialhtml = addmaterial(ordercnt);
			
            //$("#maindiv").prepend(materialhtml);
            
            $("#maindiv div.materialheader").after(materialhtml);
            
            
             
            
            $("div#order"+ordercnt).slideDown('slow');
            var speed =  1000;
                $('html,body').animate({
                    scrollTop: $("div#order"+ordercnt).offset().top - 150
                }, speed);
                
            
            
            
            $("#ordercnt").val(newordercnt);	
			
			
            $("div#order"+ordercnt+" .removeorder").bind("click",function(){
                var ordercnt = $(this).attr('data-cnt');			
                var orderdbid = $(this).attr('data-id');						
                deleteordermaterial(ordercnt,orderdbid);			
            });
			
			
        });
		
		
		
        ///===remove roome===
        $(".removeorder").on("click",function(){			
            var ordercnt = $(this).attr('data-cnt');			
            var orderdbid = $(this).attr('data-id');				
            deleteordermaterial(ordercnt,orderdbid);			
        });
		
		
        //===product cost assign
		
        $(".prodsel,.selqty,.selpst,.selmarkup,.selopttype").live("change",function(){
            var cnt=$(this).attr('id').match(/\d+/);		
            dofinalcost(cnt);
        });
		
        $(".selnqty").live("change",function(){
            var cnt=$(this).attr('id').match(/\d+/);
            dofinalcostopt(cnt);
        });
		
		
		
		
    });
  
    function dofinalcost(cnt){
  		
		
		
        var cost=$("#productcost_id"+cnt+" option:selected" ).attr('data-cost');
        var qty=$("#qty"+cnt).val();
	

        var psttax_tax=$("#productcost_id"+cnt+" option:selected" ).attr('data-psttax');
        $("#pst_tax"+cnt).val(psttax_tax);
        
        var pst_tax=$("#pst_tax"+cnt).val();
        
        //var markup_cost=$("#markup_cost"+cnt+" option:selected" ).val();
        var markupcost_cost=$("#productcost_id"+cnt+" option:selected" ).attr('data-markupcost');
        $("#markup_cost"+cnt).val(markupcost_cost);
        
        var markup_cost=$("#markup_cost"+cnt).val();
		
        if(cost>0){
            var step1= (cost * qty);
			
            var step2=0;			
            if(pst_tax>0){ 
                step2=(step1*pst_tax)/100;
            }
            var step3= step1+step2;
			
            var step4=0;
            if(markup_cost>0){
                step4= (step3*markup_cost)/100;
            }
			
            var step5= step3+step4;
			
            //alert(cost+"=="+qty+"=="+step1+"=="+pst_tax+"=="+step2+"=="+step3+"=="+markup_cost+"=="+step4+"=="+step5);
			
            //{(base price X times) + PSTtaxes} x markup% = markup cost.
			
            var final_cost=step5;
            $("#productcostsum"+cnt).val(final_cost);
            
            ///==check base quote
            var opttype=$("#opttype"+cnt+" option:selected" ).val();
            if(opttype>0){
                $("#ncqty"+cnt).val(qty);
                $("#optionalcostsum"+cnt).val(final_cost);                
            } else {
                $("#ncqty"+cnt).val(0);
                $("#optionalcostsum"+cnt).val(0);                
            }
            
            
        }
    }
  
    function dofinalcostopt(cnt){
  		
		
		
        var cost=$("#productcost_id"+cnt+" option:selected" ).attr('data-cost');
        var qty=$("#ncqty"+cnt).val();
		
        var psttax_tax=$("#productcost_id"+cnt+" option:selected" ).attr('data-psttax');
        $("#pst_tax"+cnt).val(psttax_tax);        
                
        var pst_tax=$("#pst_tax"+cnt).val();
        
        //var markup_cost=$("#markup_cost"+cnt+" option:selected" ).val();
        var markupcost_cost=$("#productcost_id"+cnt+" option:selected" ).attr('data-markupcost');
        $("#markup_cost"+cnt).val(markupcost_cost);
        
        var markup_cost=$("#markup_cost"+cnt).val();
		
        if(cost>0){
            var step1= (cost * qty);
			
            var step2=0;			
            if(pst_tax>0){ 
                step2=(step1*pst_tax)/100;
            }
            var step3= step1+step2;
			
            var step4=0;
            if(markup_cost>0){
                step4= (step3*markup_cost)/100;
            }
			
            var step5= step3+step4;
		
            //alert(cost+"=="+qty+"=="+step1+"=="+pst_tax+"=="+step2+"=="+step3+"=="+markup_cost+"=="+step4+"=="+step5);
			
            //{(base price X times) + PSTtaxes} x markup% = markup cost.
			
            var final_cost=step5;
            $("#optionalcostsum"+cnt).val(final_cost);
        }
	
    }
  
    function deleteordermaterial(ordercnt,orderdbid){
        if(confirm("Are you sure, you want to delete selected order material?"))
        {
            if(orderdbid>0) {
			
            } 
			
            $("div#order"+ordercnt).remove();
        }
    }
  
  
    function addmaterial(ordercnt){
  	
        var html="";
	
        html +='<div id="order'+ordercnt+'" class="borT" style="display:none;">';
	
        html +='<div class="controls controls-row borB">Material '+ordercnt+'</div>';
	 
        html +='<div class="controls controls-row borB ht65">';
	
	
        html +='<select name="productcost_id'+ordercnt+'" id="productcost_id'+ordercnt+'" class="span3 prodsel ui-wizard-content ui-helper-reset ui-state-default">';
	
        html +='<option value="">Select Product</option>';
<?php if (!empty($all_product_list)) {
    foreach ($all_product_list as $prod) { ?>
                        html +='<option value="<?php echo $prod->productcost_id; ?>"  data-cost="<?php echo $prod->cost; ?>" data-markupcost="<?php echo $prod->markup_cost; ?>"  data-psttax="<?php echo $prod->pst_tax; ?>"><?php echo $prod->cost . '&nbsp;&nbsp;|&nbsp;&nbsp;' . ucfirst(trim($prod->description)) . '&nbsp;&nbsp;|&nbsp;&nbsp;' . ucfirst(trim($prod->paintbrand)); ?></option>';
    <?php }
} ?>
	
                html +='</select>';
            
            
            
             html +='<select name="opttype'+ordercnt+'" id="opttype'+ordercnt+'" class="span2 selopttype ui-wizard-content ui-helper-reset ui-state-default"><option value="0" selected>Base</option><option value="1">Opt 1</option><option value="2">Opt 2</option><option value="3">Opt 3</option><option value="4">Opt 4</option></select>';
    
    
        
    html +='<select name="qty'+ordercnt+'" id="qty'+ordercnt+'" class="span1 selqty ui-wizard-content ui-helper-reset ui-state-default">';
	
<?php for ($i = 1; $i <= 50; $i++) { ?>
                    html +='<option value="<?php echo $i; ?>"><?php echo $i; ?></option>';
<?php } ?>
	
                html +='</select>';
            
        /*html +='<select name="ncqty'+ordercnt+'" id="ncqty'+ordercnt+'" class="span1 selnqty ui-wizard-content ui-helper-reset ui-state-default">';
	
<?php /*for ($i = 0; $i <= 50; $i++) { ?>
                    html +='<option value="<?php echo $i; ?>"><?php echo $i; ?></option>';
<?php }*/ ?>
	
                html +='</select>';*/
            
        html+='<select name="sheen_id'+ordercnt+'" id="sheen_id'+ordercnt+'" class="span2 ui-wizard-content ui-helper-reset ui-state-default">';
	
<?php if (!empty($all_sheen)) {
    foreach ($all_sheen as $sheen) { ?>
                        html +='<option value="<?php echo $sheen->sheen_id; ?>"><?php echo ucfirst($sheen->sheen_description); ?></option>';
    <?php }
} ?>
	
                html +='</select>';
	
                html +='<input name="colour'+ordercnt+'" id="colour'+ordercnt+'" class="span2 ui-wizard-content ui-helper-reset ui-state-default" value="" type="text" />';
	
	
                html +='<input name="pst_tax'+ordercnt+'" id="pst_tax'+ordercnt+'" class="span1 ui-wizard-content ui-helper-reset ui-state-default selpst" value="<?php echo $site_pst_tax; ?>" type="hidden" readonly="readonly" />';
	
                /*html +='<select name="markup_cost'+ordercnt+'" id="markup_cost'+ordercnt+'" class="span1 selmarkup">';
	
<?php /*if (!empty($all_markup_cost)) {
    foreach ($all_markup_cost as $key => $mcost) { ?>
                        html +='<option value="<?php echo $mcost; ?>"><?php echo $mcost; ?></option>';
    <?php }
}*/ ?>

                html +='</select>';*/
	
	
	
                html +='<input name="productcostsum'+ordercnt+'" id="productcostsum'+ordercnt+'" class="span1 ui-wizard-content ui-helper-reset ui-state-default" value="0.0" type="text" readonly="readonly" />';
                html +='<input name="optionalcostsum'+ordercnt+'" id="optionalcostsum'+ordercnt+'" class="span1 ui-wizard-content ui-helper-reset ui-state-default" value="0.0" type="hidden" />';
	
                html +='<a href="javascript:void(0)" data-id="" data-cnt="'+ordercnt+'" class="span1 pad10 removeorder"><i class="icon-remove"></i></a><br />';
	
                html +='<input type="text" name="notes'+ordercnt+'" id="notes'+ordercnt+'" placeholder="Order Notes" class="span12 ui-wizard-content ui-helper-reset ui-state-default">';
	
        
        
        html+='<input type="hidden" name="ncqty'+ordercnt+'" id="ncqty'+ordercnt+'" value="0" />';
        
        
                html +='<input type="hidden" name="orderdetail_id'+ordercnt+'" id="orderdetail_id'+ordercnt+'" value="" />';
                
                
                html +='<input type="hidden" name="markup_cost'+ordercnt+'" id="markup_cost'+ordercnt+'" value="0" />';
                
                 
                html +='</div>';                                    
                html +='</div>';
	
                return html;
  
            }
  
  

var clicked = false;

$(document).ready(function(){
//    $("a[href]").click(function(){
//        clicked = true;
//    });
//
//    $(document).bind('keypress keydown keyup', function(e) {
//        if(e.which === 116) {
//            clicked = true;
//        }
//        if(e.which === 82 && e.ctrlKey) {
//            clicked = true;
//        }
//    });

    $(".savebtnquote").click(function(){
        clicked = true;
    });
    
});

window.addEventListener("beforeunload", function (e) {
    if(!clicked) {
        
         var confirmationMessage = 'Please click on stay on page to save the data you have entered and then continue.';    

        (e || window.event).returnValue = confirmationMessage; //Gecko + IE
        return confirmationMessage; //Gecko + Webkit, Safari, Chrome etc.
    }
});
//window.onbeforeunload = function(e){
//    if(!clicked) {
//        
//          if (typeof event == 'undefined') {
//            event = window.event;
//          }
//          if (event) {
//            event.returnValue = 'You have unsaved stuff.';
//          }
//          return message;
//        
//        //return 'You have unsaved stuff.';
//    }
//};

</script>   
<style>
    #firstStep{
        opacity:1 !important;
    }
</style>