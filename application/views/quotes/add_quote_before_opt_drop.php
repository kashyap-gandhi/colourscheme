<?php $site_setting = site_setting(); ?>
<!-- datepicker plugin -->
<link rel="stylesheet" href="<?php echo base_url(); ?>css/bootstrap-datetimepicker.min.css">


<!-- datepicker plugin -->
<script src="<?php echo base_url(); ?>js/bootstrap-datetimepicker.min.js"></script>



<div class="page-header">
    <div class="pull-left">
        <h4><i class="icon-cogs"></i> <?php if ($quote_id != '') { ?>Edit Quote<?php } else { ?>Add Quote<?php } ?></h4>
    </div>
    <div class="pull-right">
        <ul class="bread">
            <li><a href="<?php echo site_url('quotes/manage'); ?>">Manage Quotes</a><span class="divider">/</span></li>
            <li class="active">Quotes</li>
        </ul>
    </div>
</div>


<div class="container-fluid" id="content-area">

    <div class="row-fluid">
        <div class="span12">

            <?php if ($error != '') { ?>

                <div class="alert alert-error">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    <strong>Warning !</strong> <?php echo $error; ?>
                </div>    
            <?php } ?>


            <?php if ($msg != '') { ?>
                <div class="alert <?php if ($msg == 'notfound' || $msg == 'cannot') { ?>alert-danger<?php } else { ?>alert-success<?php } ?>">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    <strong><?php if ($msg == 'notfound' || $msg == 'cannot') { ?>Warning<?php } else { ?>Success<?php } ?>!</strong>

                    <?php if ($msg == 'delete') { ?>Quote has been deleted successfully. <?php } ?>	
                    <?php if ($msg == 'insert') { ?>New Quote has been added successfully. <?php } ?>	
                    <?php if ($msg == 'update') { ?>Quote detail has been updated successfully. <?php } ?>	
                    <?php if ($msg == 'notfound') { ?>Quote records not found. <?php } ?>
                    <?php if ($msg == 'cannot') { ?>Cannot delete Quote because it used in many records. <?php } ?>		

                </div> 
            <?php } ?>	                


            <div class="box">
                <div class="box-head">
                    <i class="icon-list-ul"></i>
                    <span>Change Quote details</span>

                    <?php
                    $data['quote_details'] = $quote_details;
                    $this->load->view('quotes/quote_title', $data);
                    ?>

                </div>
                <div class="box-body box-body-nopadding">
                    <?php
                    $attributes = array('name' => 'frm_addquote', 'class' => 'form-horizontal form-bordered form-wizard');
                    echo form_open('quotes/add_quote/' . $quote_id, $attributes);
                    ?> 




                    <?php $this->load->view('quotes/quote_buttons', $data); ?>


                    <div class="step" id="firstStep">


                        <!-----steps--->   
                        <?php
                        $data['step_index'] = 1;
                        $this->load->view('quotes/quote_steps', $data);
                        ?>
                        <!-----steps--->




                        <div class="control-group">
                            <label for="textfield" class="control-label">Company Info</label>
                            <div class="controls controls-row" style="height: 15px;">
                                <label for="first_name" class="span6">Company</label>
                                <label for="QuoteContact" class="span6">Contact</label>

                            </div>
                            <div class="controls controls-row" style="height: 40px;">
                                <input type="text" name="first_name" id="first_name" value="<?php echo $quote_details->first_name.' '.$quote_details->last_name; ?>" placeholder="Company" class="span6" readonly="readonly">
                                <input type="text" name="QuoteContact" id="QuoteContact" value="<?php echo $QuoteContact; ?>" placeholder="Contact" class="span6">

                            </div>
                            
                            <div class="controls controls-row" style="height: 15px;">
                                <label for="QuoteAddress" class="span6">Location</label>                                            
                                <label for="QuoteContactPhone" class="span6">Phone</label>   
                                      
                            </div>
                            
                            <div class="controls controls-row" style="height: 40px;">

                                <?php
                                $location = $QuoteAddress;

                                if ($QuoteAddress == '') {
                                    $client_location = '';
                                    if ($quote_details->Address != '') {
                                        $client_location.=$quote_details->Address;
                                    }
                                    if ($quote_details->City != '') {
                                        $client_location.=',' . $quote_details->City;
                                    }
                                    if ($quote_details->Province != '') {
                                        $client_location.=',' . $quote_details->Province;
                                    }
                                    if ($quote_details->postal_code != '') {
                                        $client_location.=' - ' . $quote_details->postal_code;
                                    }
                                    $location = $client_location;
                                }
                                ?>


                                <input type="text" name="QuoteAddress" id="QuoteAddress" value="<?php echo $location; ?>" placeholder="Location" class="span6">
                                <input type="text" name="QuoteContactPhone" id="QuoteContactPhone" value="<?php echo $QuoteContactPhone; ?>" placeholder="Phone" class="span6">                                               
                            </div>
                            
                            <!--end-->
                            
                            
                            
                        </div>

                        <div class="control-group">&nbsp;</div>

                        <div class="control-group">
                            <label for="textfield" class="control-label">Description</label>
                            
                            <div class="controls controls-row" style="height: 100px;">

                                                               <textarea rows="5" name="short_description" id="short_description" class="input-block-level span12" placeholder="Description"><?php echo $short_description; ?></textarea>
                            </div>
                        </div>

                        <div class="control-group">&nbsp;</div>

                        <div class="control-group">
                            <label for="textfield" class="control-label">Lead</label>
                            <div class="controls controls-row" style="height: 15px;">
                                <label for="leadsource_id" class="span6">Source</label>
                                <label for="ReferralName" class="span6">Referral</label>

                            </div>
                            <div class="controls controls-row" style="height: 40px;">
                                        <?php $leadsource_list = leadsource_list(); ?>
                                <div class="span6">
                                    <select class="chosen-select" name="leadsource_id" id="leadsource_id">
                                        <option value="">---None---</option>
<?php if (!empty($leadsource_list)) {
    foreach ($leadsource_list as $source) { ?>
                                                <option value="<?php echo $source->leadsource_id; ?>" <?php if ($leadsource_id == $source->leadsource_id) { ?> selected="selected"<?php } ?>><?php echo ucfirst($source->leadsource); ?></option>
    <?php }
} ?>
                                    </select>
                                </div>

                                <input type="text" name="ReferralName" id="ReferralName" value="<?php echo $ReferralName; ?>" placeholder="Referral Name" class="span6">

                            </div>
                        </div>

                        <div class="control-group">&nbsp;</div>


                        <div class="control-group">
                            <label for="textfield" class="control-label">Rate / Date</label>
                            <div class="controls controls-row" style="height: 15px;">
                                <label for="first_name" class="span4">Paint</label>
                                <label for="rate" class="span4">Job Rate(<?php echo $site_setting->currency_symbol; ?>)</label>
                                <label for="date" class="span4">Quote Date</label>


                            </div>
                            <div class="controls controls-row" style="height: 40px;">
                                <div class="span4">
                                    <select name="PaintQuality" id="PaintQuality" class="chosen-select">
                                        <option value="">---Select---</option>
<?php if (!empty($quote_paint)) {
    foreach ($quote_paint as $paint) { ?>
                                                <option value="<?php echo $paint->productcost_id; ?>" <?php if ($PaintQuality == $paint->productcost_id) { ?> selected="selected" <?php } ?>><?php echo ucfirst(trim($paint->description)) . '&nbsp;&nbsp;|&nbsp;&nbsp;' . ucfirst(trim($paint->paintbrand)); ?></option>
                                        <?php }
                                    } ?>
                                    </select>
                                </div>



                                <select name="rate" id="rate" class="span4">
                                     <option value="">Select</option>
<?php for ($i = 75; $i >= 46; $i--) { ?>
                                        <option value="<?php echo $i; ?>" <?php if ($rate == $i) { ?> selected="selected" <?php } ?>><?php echo $i; ?></option>
<?php } ?>
                                </select>



                                <div  class="input-append date datetimepicker span4">
                                    <input name="date" id="date" type="text" value="<?php echo date('d-m-Y H:i:s', strtotime($date)); ?>" readonly="readonly" placeholder="Quote Date" class="input-large" />
                                    <span class="add-on">
                                        <i data-time-icon="icon-time" data-date-icon="icon-calendar">
                                        </i>
                                    </span>
                                </div>






                            </div>
                        </div>


                        <div class="control-group">&nbsp;</div>

                        <div class="control-group">
                            <label for="textfield" class="control-label">Sales</label>
                            <div class="controls controls-row" style="height: 15px;">
                                <label for="first_name" class="span3">Sales Lead</label>
                                <label for="last_name" class="span3">Stage</label>
                                <label for="last_name" class="span3">Probability</label>
                                <label for="last_name" class="span3">Win / Loss</label>


                            </div>
                            <div class="controls controls-row" style="height: 40px;">
                                <?php $team_list = team_list_by_role_id(1); ?>
                                <div class="span3"><select name="team_id" id="team_id" class="chosen-select">
                                         <option value="">---Select---</option>
                                    <?php if (!empty($team_list)) {
                                        foreach ($team_list as $team) { ?>
                                                <option value="<?php echo $team->team_id; ?>" <?php if ($team_id == $team->team_id) { ?> selected="selected" <?php } ?>><?php echo ucfirst($team->name); ?></option>
                                        <?php }
                                    } ?>
                                    </select></div>



                                    <?php $salesstage_list = salesstage_list(); ?>                                              
                                <select class="span3" name="salesstage_id" id="salesstage_id">
                                    <option value="">Select</option>
                                    <?php if (!empty($salesstage_list)) {
                                        foreach ($salesstage_list as $stage) { ?>
                                            <option value="<?php echo $stage->salesstage_id; ?>" <?php if ($salesstage_id == $stage->salesstage_id) { ?> selected="selected" <?php } ?>><?php echo ucfirst($stage->sales_stage); ?></option>
    <?php }
} ?>
                                </select>



                                <select class="span3" name="Probability" id="Probability">
                                     <option value="">Select</option>
                                    <?php if (!empty($all_probability)) {
                                        foreach ($all_probability as $key => $prob) { ?>
                                            <option value="<?php echo $prob; ?>" <?php if ($Probability == $prob) { ?> selected="selected" <?php } ?>><?php echo $prob; ?></option>
    <?php }
} ?>
                                </select>



<?php $winloss_list = winloss_list(); ?>
                                <select class="span3" name="wonreason_id" id="wonreason_id">
                                     <option value="">Select</option>
<?php if (!empty($winloss_list)) {
    foreach ($winloss_list as $wonloss) { ?>
                                            <option value="<?php echo $wonloss->wonreason_id; ?>" <?php if ($wonreason_id == $wonloss->wonreason_id) { ?> selected="selected" <?php } ?>><?php echo ucfirst($wonloss->reason); ?></option>
    <?php }
} ?>
                                </select>

                            </div>
                        </div>

                        <div class="control-group">&nbsp;</div>


                        <div class="control-group" style="height:50px;">
                            <label for="textfield" class="control-label">Quote Details</label>
                        </div>

                        <div class="form-actions">

                            <button type="button" class="button button-basic-green addroom">Add Room</button>


                        </div>

                        <style>
                            .ht0 { height: 0px; }
                            .ht15 { height: 15px; }
                            .ht40 { height: 40px; }
                            .ht65 { height: 65px; }
                            .pad10 { padding: 10px; }
                            .pad4 { padding: 4px; }
                            .roommain { border: 2px solid #ADADAD; margin-bottom:2px; }
                            .borT { border-top: 1px solid #ccc; } 
                            .borB { border-bottom: 1px solid #ccc; }
                            #maindiv .controls { margin:0px !important; }
                            .featuremain{
                                
                            }
                            .featuremain .featureheader{
                                
                            }
                            .featuremain .featureheader .span1:first-child{
                                margin-left: 0px !important;
                            }
                            
                            .featuremain .featureheader .span1{
                                margin-left: 1.23% !important;
                                 width: 5.23%;
                            }

                            .featuremain .featurecontent{
                                
                            }
                            .featuremain .featurecontent .span1:first-child{
                                margin-left: 0px !important;
                            }
                            .featuremain .featurecontent .span1{
                                 margin-left: 1.23% !important;
                                 width: 5.23%;
                            }
                            
                            .featuremain .featureheader .cstfeature, .featuremain .featurecontent .cstfeature{
                                width:18% !important;
                            }
                            .featuremain .featureheader .cstsheen, .featuremain .featurecontent .cstsheen{
                                width:14% !important;
                            }
                            
                             
                        </style>         
                        <!----------room part--------->


                        <div class="control-group">

<?php $roomcnt = 0; ?>

                            <div id="maindiv">


<?php
if (!empty($room_details)) {

    $roomcnt = 1;

    foreach ($room_details as $rooms) {
        ?>

                                        <!--rroom type-->
                                        <div id="room<?php echo $roomcnt; ?>" class="roommain">

                                            <div class="controls controls-row ht40">
                                                <div class="span4">Room Type <?php echo $roomcnt; ?></div>
                                                <div class="span6">Additional Room Description</div>
                                                <div class="span2"><button type="button" data-cnt="<?php echo $roomcnt; ?>" class="button button-basic-blue addroomfeature">Add Feature</button></div>
                                            </div>

                                            <div class="controls controls-row ht40">
                                                <select class="span4" name="roomtype_id<?php echo $roomcnt; ?>" id="roomtype_id<?php echo $roomcnt; ?>">
        <?php if (!empty($all_room_type)) {
            foreach ($all_room_type as $roomtype) { ?>
                                                            <option value="<?php echo $roomtype->roomtype_id; ?>" <?php if ($rooms->roomtype_id == $roomtype->roomtype_id) { ?> selected="selected" <?php } ?>><?php echo ucfirst($roomtype->description); ?></option>
            <?php }
        } ?>
                                                </select>

                                                <input type="text" value="<?php echo $rooms->additionalroomdescription; ?>" name="additionalroomdescription<?php echo $roomcnt; ?>" id="additionalroomdescription<?php echo $roomcnt; ?>" placeholder="Additional Room Description" class="span6">
                                                <a href="javascript:void(0)" title="Delete Room" data-id="<?php echo $rooms->quotedetail_id; ?>" data-cnt="<?php echo $roomcnt; ?>" class="span2 pad10 removeroom"><i class="icon-remove"></i> Delete Room</a>
                                            </div>
                                            <!--rroom type-->    

                                            <!--room feature 1--->
                                            <div id="roomfeature<?php echo $roomcnt; ?>" class="borT featuremain">


                                                <!--feature title-->

                                                <div class="controls controls-row borB featureheader">                                    
                                                    <div class="span1 cstfeature">Feature</div>
                                                    <div class="span1 cstlen">Length</div>
                                                    <div class="span1 cstwid">Width</div>
                                                    <div class="span1 csthei">Height</div>
                                                    <div class="span1 cstqty">Qty.</div>
                                                    <div class="span1 cstaddsqft">UpG</div>
                                                    <div class="span1 prodcost">Rate</div>
                                                    <div class="span1 cstmulti">Coats</div>
                                                    <div class="span1 cstsheen">Sheen</div>
                                                    <div class="span1 cstlhrs">Hrs.</div>
                                                    <div class="span1 cstlsqft">Sq. Ft.</div>
                                                    <div class="span1 cstlact">Optional/<br />Action</div>
                                                </div>

                                                <!--feature title-->


        <?php $roomfeaturecnt = 0;

        $totalhrs = 0; ?>

                                                        <?php
                                                        $room_features_detail = $this->quotes_model->room_feature_details($rooms->quotedetail_id);

                                                        if (!empty($room_features_detail)) {

                                                            $roomfeaturecnt = 1;

                                                            foreach ($room_features_detail as $room_features) {

                                                                $all_feature_upg = $this->quotes_model->feature_upg($room_features->feature_id);
                                                                $all_feature_coat = $this->quotes_model->feature_rate($room_features->feature_id);
                                                                ?>

                                                        <div id="feature<?php echo $roomfeaturecnt; ?>" class="controls controls-row borB ht65 featurecontent">
                                                            <select name="feature_id<?php echo $roomfeaturecnt; ?>-<?php echo $roomcnt; ?>" id="feature_id<?php echo $roomfeaturecnt; ?>-<?php echo $roomcnt; ?>" class="span1 cstfeature">
                                                                <option value="0">Select</option>
                                                                <?php if (!empty($all_feature_type)) {
                                                                    foreach ($all_feature_type as $feature) { ?>
                                                                        <option value="<?php echo $feature->feature_id; ?>" <?php if ($room_features->feature_id == $feature->feature_id) { ?> selected="selected" <?php } ?>><?php echo ucfirst($feature->description); ?></option>
                    <?php }
                } ?>
                                                            </select>
                                                            
                                                            <?php 
                                                            
                                                            $length_disable='';
                                                            if($room_features->Length==0){
                                                                
                                                                $length_disable=' disabled ';
                                                                
                                                                }
                                                                
                                                                
                                                            ?>
                                                            <select name="Length<?php echo $roomfeaturecnt; ?>-<?php echo $roomcnt; ?>" id="Length<?php echo $roomfeaturecnt; ?>-<?php echo $roomcnt; ?>" class="span1 cstlen">
                                                                <?php if($room_features->Length==-1){ ?>
                                                                    <option value="-1">Select</option>
                                                                <?php } ?>
                                                                
                                                                
                                                                <option value="0" <?php echo $length_disable; ?>>N/A</option>
                                                                <?php for ($i = 1; $i <= 50; $i++) { ?>
                                                                    <option value="<?php echo $i; ?>" <?php if ($room_features->Length == $i) { ?> selected="selected" <?php } ?>  <?php echo $length_disable; ?>><?php echo $i; ?></option>
                <?php } ?>
                                                            </select>
                                                            
                                                             <?php 
                                                            
                                                            $width_disable='';
                                                            if($room_features->Width==0){
                                                                
                                                                $width_disable=' disabled ';
                                                                
                                                                }
                                                            ?>
                                                            
                                                            <select name="Width<?php echo $roomfeaturecnt; ?>-<?php echo $roomcnt; ?>" id="Width<?php echo $roomfeaturecnt; ?>-<?php echo $roomcnt; ?>" class="span1 cstwid">
                                                                 <?php if($room_features->Width==-1){ ?>
                                                                    <option value="-1">Select</option>
                                                                <?php } ?>
                                                                <option value="0"  <?php echo $width_disable; ?>>N/A</option>
                                                                <?php for ($i = 1; $i <= 50; $i++) { ?>
                                                                    <option value="<?php echo $i; ?>" <?php if ($room_features->Width == $i) { ?> selected="selected" <?php } ?> <?php echo $width_disable; ?>><?php echo $i; ?></option>
                <?php } ?>
                                                            </select>
                                                            
                                                            <?php 
                                                            
                                                            $height_disable='';
                                                            if($room_features->Height==0){
                                                                
                                                                $height_disable=' disabled ';
                                                                
                                                                }
                                                            ?>
                                                            
                                                            <select name="Height<?php echo $roomfeaturecnt; ?>-<?php echo $roomcnt; ?>" id="Height<?php echo $roomfeaturecnt; ?>-<?php echo $roomcnt; ?>" class="span1 csthei">
                                                                 <?php if($room_features->Height==-1){ ?>
                                                                    <option value="-1">Select</option>
                                                                <?php } ?>
                                                                <option value="0" <?php echo $height_disable; ?>>N/A</option>
                                                                <?php for ($i = 1; $i <= 50; $i++) { ?>
                                                                    <option value="<?php echo $i; ?>" <?php if ($room_features->Height == $i) { ?> selected="selected" <?php } ?> <?php echo $height_disable; ?>><?php echo $i; ?></option>
                <?php } ?>
                                                            </select>
                                                            <select name="Quantity<?php echo $roomfeaturecnt; ?>-<?php echo $roomcnt; ?>" id="Quantity<?php echo $roomfeaturecnt; ?>-<?php echo $roomcnt; ?>" class="span1 cstqty">
                                                                <option value="0">N/A</option>
                                                                <?php for ($i = 1; $i <= 50; $i++) { ?>
                                                                    <option value="<?php echo $i; ?>" <?php if ($room_features->Quantity == $i) { ?> selected="selected" <?php } ?>><?php echo $i; ?></option>
                <?php } ?>
                                                            </select>
                                                            <select name="addSqft<?php echo $roomfeaturecnt; ?>-<?php echo $roomcnt; ?>" id="addSqft<?php echo $roomfeaturecnt; ?>-<?php echo $roomcnt; ?>" class="span1 cstaddsqft">
                                                                <option value="0">N/A</option>

                                                                <?php if (!empty($all_feature_upg)) {

                                                                    foreach ($all_feature_upg as $fcoat) { ?>

                                                                        <option value="<?php echo $fcoat->addSqft; ?>" <?php if ($room_features->addSqft == $fcoat->addSqft) { ?> selected="selected" <?php } ?>><?php echo $fcoat->addSqft; ?></option>

                                                                    <?php }
                                                                } else {
                                                                    for ($ui = 10; $ui <= 25; $ui++) { ?>
                                                                        <option value="<?php echo $ui; ?>" <?php if ($room_features->addSqft == $ui) { ?> selected="selected" <?php } ?>><?php echo $ui; ?></option>
                    <?php }
                } ?>
                                                            </select>
                                                            <select name="coat_type<?php echo $roomfeaturecnt; ?>-<?php echo $roomcnt; ?>" id="coat_type<?php echo $roomfeaturecnt; ?>-<?php echo $roomcnt; ?>" class="span1 prodcost">
                                                                <option value="0">N/A</option>
                <?php if (!empty($all_feature_coat)) {
                    foreach ($all_feature_coat as $fcoat) { ?>
                                                                        <option value="<?php echo $fcoat->feature_configure_id; ?>" data-rate="<?php echo $fcoat->rate; ?>" <?php if ($room_features->coat_type == $fcoat->feature_configure_id) { ?> selected="selected" <?php } ?>><?php echo $fcoat->rate; /* "&nbsp;&nbsp;|&nbsp;&nbsp;".ucfirst($fcoat->rate_type); */ ?></option>
                    <?php }
                } ?>
                                                            </select>
                                                            <select name="Multiplyer<?php echo $roomfeaturecnt; ?>-<?php echo $roomcnt; ?>" id="Multiplyer<?php echo $roomfeaturecnt; ?>-<?php echo $roomcnt; ?>" class="span1 cstmulti">
                                                                <option value="0">N/A</option>
                <?php if (!empty($all_multiplyer)) {
                    
                    if($room_features->Multiplyer==''){
                        $sel_multi=2;
                    } else {
                        $sel_multi=$room_features->Multiplyer;
                    }
                    
                    foreach ($all_multiplyer as $key => $multiplyers) { ?>
                                  <option value="<?php echo $multiplyers; ?>" <?php if ($sel_multi == $multiplyers) { ?> selected="selected" <?php } ?>><?php echo $multiplyers; ?></option>
                    <?php }
                } ?>
                                                            </select>
                                                            <select name="sheen_id<?php echo $roomfeaturecnt; ?>-<?php echo $roomcnt; ?>" id="sheen_id<?php echo $roomfeaturecnt; ?>-<?php echo $roomcnt; ?>" class="span1 cstsheen">
                                                                <option value="0">N/A</option>
                                                        <?php if (!empty($all_sheen)) {
                                                            foreach ($all_sheen as $sheen) { ?>
                                                                        <option value="<?php echo $sheen->sheen_id; ?>" <?php if ($room_features->sheen_id == $sheen->sheen_id) { ?> selected="selected" <?php } ?>><?php echo ucfirst($sheen->sheen_description); ?></option>
                                                            <?php }
                                                        } ?>
                                                            </select>

                                                            <label class="span1 cstlhrs" id="Hrs<?php echo $roomfeaturecnt; ?>-<?php echo $roomcnt; ?>"><?php echo $room_features->totTime; ?></label>
                                                            <label class="span1 cstlsqft" id="SqFt<?php echo $roomfeaturecnt; ?>-<?php echo $roomcnt; ?>"><?php echo $room_features->sqfootage; ?></label>

                                                            <div class="span1 cstlact">
                                                                <input name="rate<?php echo $roomfeaturecnt; ?>-<?php echo $roomcnt; ?>" id="rate<?php echo $roomfeaturecnt; ?>-<?php echo $roomcnt; ?>"  value="<?php echo $room_features->rate; ?>" type="hidden" readonly="readonly" />
                                                                <input class="cstsqfootage" name="sqfootage<?php echo $roomfeaturecnt; ?>-<?php echo $roomcnt; ?>" id="sqfootage<?php echo $roomfeaturecnt; ?>-<?php echo $roomcnt; ?>"  value="<?php echo $room_features->sqfootage; ?>" type="hidden" readonly="readonly" />
                                                                <input class="csttotTime" name="totTime<?php echo $roomfeaturecnt; ?>-<?php echo $roomcnt; ?>" id="totTime<?php echo $roomfeaturecnt; ?>-<?php echo $roomcnt; ?>"  value="<?php echo $room_features->totTime; ?>" type="hidden" readonly="readonly" />
                                                                <input class="cstsqfootagec" name="sqfootagec<?php echo $roomfeaturecnt; ?>-<?php echo $roomcnt; ?>" id="sqfootagec<?php echo $roomfeaturecnt; ?>-<?php echo $roomcnt; ?>"  value="<?php echo $room_features->sqfootagec; ?>" type="hidden" readonly="readonly" />
                                                                <input class="csttotTimec" name="totTimec<?php echo $roomfeaturecnt; ?>-<?php echo $roomcnt; ?>" id="totTimec<?php echo $roomfeaturecnt; ?>-<?php echo $roomcnt; ?>" value="<?php echo $room_features->totTimeC; ?>" type="hidden" readonly="readonly" />

                                                                <input type="checkbox" value="1" name="commit<?php echo $roomfeaturecnt; ?>-<?php echo $roomcnt; ?>" id="commit<?php echo $roomfeaturecnt; ?>-<?php echo $roomcnt; ?>" <?php if ($room_features->commit == 1) { ?> checked="checked" <?php } ?> class="cstcommit" />
                                                                <a href="javascript:void(0)" title="Delete Feature" data-id="<?php echo $room_features->roomdetail_id; ?>" data-fcnt="<?php echo $roomfeaturecnt; ?>" data-cnt="<?php echo $roomcnt; ?>" class="pad10 removefeature"><i class="icon-remove"></i></a> 
                                                            </div>         <br /><br />
                                                            <input type="text" name="description<?php echo $roomfeaturecnt; ?>-<?php echo $roomcnt; ?>" id="description<?php echo $roomfeaturecnt; ?>-<?php echo $roomcnt; ?>" value="<?php echo $room_features->description; ?>" placeholder="Additional Description" class="span12">



                                                            <input name="roomdetail_id<?php echo $roomfeaturecnt; ?>-<?php echo $roomcnt; ?>" id="roomdetail_id<?php echo $roomfeaturecnt; ?>-<?php echo $roomcnt; ?>" value="<?php echo $room_features->roomdetail_id; ?>" type="hidden" readonly="readonly" />

                                                        </div><!---feature div-->


                <?php
                $totalhrs = $totalhrs + $room_features->totTime;

                $roomfeaturecnt++;
            }
        }
        ?>      

                                            </div><!--room feature 1--->






                                            <input type="hidden" name="roomfeaturecnt<?php echo $roomcnt; ?>" id="roomfeaturecnt<?php echo $roomcnt; ?>" value="<?php echo $roomfeaturecnt; ?>" />


                                            <!--room commited 1--->

                                            <div class="controls controls-row ht0">&nbsp;</div>
                                            <div class="controls controls-row borB ht15">                                    
                                                <div class="span12"><b>Committed Features</b></div>
                                            </div>

                                            <div class="controls controls-row borB ht15">                                    
                                                <div class="span3">Prep</div>
                                                <div class="span3">S & C</div>
                                                <!--<div class="span3">Closets</div>
                                                <div class="span3">Misc</div>-->
                                            </div>



                                            <div class="controls controls-row ht40">
                                                
                               <input name="prep<?php echo $roomcnt; ?>" id="prep<?php echo $roomcnt; ?>" class="span3 cstprep" value="<?php if($rooms->prep==''){ echo "1.0"; } else { echo $rooms->prep; } ?>" />                  
                                                
                                                <?php /*<select name="prep<?php echo $roomcnt; ?>" id="prep<?php echo $roomcnt; ?>" class="span3">
        <?php if (!empty($all_prep)) {
            foreach ($all_prep as $key => $prep) { ?>
                                                            <option value="<?php echo $prep; ?>" <?php if ($rooms->prep == $prep) { ?> selected="selected" <?php } ?>><?php echo $prep; ?></option>
            <?php }
        } ?>
                                                </select>*/ ?>

        <?php /* <select name="setclean<?php echo $roomcnt; ?>" id="setclean<?php echo $roomcnt; ?>" class="span3">
          <?php if(!empty($all_sc)) { foreach($all_sc as $key=>$sc) { ?>
          <option value="<?php echo $sc;?>" <?php if($rooms->setclean==$sc){ ?> selected="selected" <?php } ?>><?php echo $sc;?></option>
          <?php }} ?>
          </select> */ ?>

                                                <input type="text" name="setclean<?php echo $roomcnt; ?>" id="setclean<?php echo $roomcnt; ?>" value="<?php if($rooms->setclean=='') { echo "1.25"; } else { echo $rooms->setclean; } ?>" class="span3 cstsetclean" />

                                                <?php /* ?><select name="closets<?php echo $roomcnt; ?>" id="closets<?php echo $roomcnt; ?>" class="span3">
                                                  <?php if(!empty($all_clostes)) { foreach($all_clostes as $key=>$clostes) { ?>
                                                  <option value="<?php echo $clostes;?>" <?php if($rooms->closets==$clostes){ ?> selected="selected" <?php } ?>><?php echo $clostes;?></option>
                                                  <?php }} ?>
                                                  </select>
                                                  <select name="misc<?php echo $roomcnt; ?>" id="misc<?php echo $roomcnt; ?>" class="span3">
                                                  <?php if(!empty($all_misc)) { foreach($all_misc as $key=>$misc) { ?>
                                                  <option value="<?php echo $misc;?>" <?php if($rooms->misc==$misc){ ?> selected="selected" <?php } ?>><?php echo $misc;?></option>
                                                  <?php }} ?>
                                                  </select>
                                                  <?php */ ?>
                                            </div>

                                            
                                            
                                            <!--<div class="controls controls-row ht0">&nbsp;</div>

                                            <div class="controls controls-row borB ht15">                                    
                                                <div class="span12"><b>Optional Features</b></div>
                                            </div>

                                            <div class="controls controls-row borB ht15">                                    
                                                <div class="span3">Prep</div>
                                                <div class="span3">S & C</div>
                                                <div class="span3">Closets</div>
                                                <div class="span3">Misc</div>
                                            </div>-->
                                            
                                            <input type="hidden" name="optprep<?php echo $roomcnt; ?>" id="optprep<?php echo $roomcnt; ?>" value="<?php  if($rooms->optprep==''){ echo "1.0"; } else { echo $rooms->optprep; } ?>" class="span3" />
                                            <input type="hidden" name="optsetclean<?php echo $roomcnt; ?>" id="optsetclean<?php echo $roomcnt; ?>" value="<?php  if($rooms->optsetclean==''){ echo "1.25"; } else { echo $rooms->optsetclean; } ?>" class="span3" />
                                            
                                             <!--<div class="controls controls-row borB ht40">
                                                <select name="optprep<?php //echo $roomcnt; ?>" id="optprep<?php //echo $roomcnt; ?>" class="span3">
        <?php /*if (!empty($all_prep)) {
            foreach ($all_prep as $key => $prep) { ?>
                                                            <option value="<?php echo $prep; ?>" <?php if ($rooms->optprep == $prep) { ?> selected="selected" <?php } ?>><?php echo $prep; ?></option>
            <?php }
        }*/ ?>
                                                </select>

        <?php /* <select name="optsetclean<?php echo $roomcnt; ?>" id="optsetclean<?php echo $roomcnt; ?>" class="span3">
          <?php if(!empty($all_sc)) { foreach($all_sc as $key=>$sc) { ?>
          <option value="<?php echo $sc;?>" <?php if($rooms->optsetclean==$sc){ ?> selected="selected" <?php } ?>><?php echo $sc;?></option>
          <?php }} ?>
          </select> */ ?>

                                                <input type="text" name="optsetclean<?php //echo $roomcnt; ?>" id="optsetclean<?php //echo $roomcnt; ?>" value="<?php //echo $rooms->optsetclean; ?>" class="span3" />


                                        <?php /* ?><select name="optclosets<?php echo $roomcnt; ?>" id="optclosets<?php echo $roomcnt; ?>" class="span3">
                                          <?php if(!empty($all_clostes)) { foreach($all_clostes as $key=>$clostes) { ?>
                                          <option value="<?php echo $clostes;?>" <?php if($rooms->optclosets==$clostes){ ?> selected="selected" <?php } ?>><?php echo $clostes;?></option>
                                          <?php }} ?>
                                          </select>
                                          <select name="optmisc<?php echo $roomcnt; ?>" id="optmisc<?php echo $roomcnt; ?>" class="span3">
                                          <?php if(!empty($all_misc)) { foreach($all_misc as $key=>$misc) { ?>
                                          <option value="<?php echo $misc;?>" <?php if($rooms->optmisc==$misc){ ?> selected="selected" <?php } ?>><?php echo $misc;?></option>
                                          <?php }} ?>
                                          </select>
                                          <?php */ ?>
                                            </div>-->




                                            <div class="controls controls-row borB ht15">         
                                                <div class="span9">&nbsp;</div>                           
                                                <div class="span3">Total Hours</div>                                     
                                            </div>
                                            <div class="controls controls-row ht40">
                                                <div class="span9">&nbsp;</div>
                                                <div class="span3" id="totalhours<?php echo $roomcnt; ?>"><?php echo $totalhrs; ?></div>     
                                            </div>

                                            <!--room commited 1--->


                                            <input type="hidden" name="quotedetail_id<?php echo $roomcnt; ?>" id="quotedetail_id<?php echo $roomcnt; ?>" value="<?php echo $rooms->quotedetail_id; ?>" />

                                        </div><!---room 1-->


        <?php
        $roomcnt++;
    }
    ?>
                                    <!---foreach-->


<?php } ?>





                            </div><!--maindiv-->


                        </div> 


                        <input type="hidden" id="roomcnt" name="roomcnt" value="<?php if($roomcnt>0){ echo $roomcnt-1; } else { echo $roomcnt; } ?>" />

                        <!----------room part--------->






                    </div><!--step 1 div-->




                    <div class="form-actions">

                        <button type="submit" class="button button-basic-blue">Save</button>

                        <button type="button" class="button button-basic" onClick="window.location.href='<?php echo site_url('quotes/managetype'); ?>'">Cancel</button>

                        <input type="hidden" name="quote_id" id="quote_id" value="<?php echo $quote_id; ?>" />
                        <input type="hidden" name="offset" id="offset" value="<?php echo $offset; ?>" />

                    </div>


                    </form>
                </div>
            </div>
        </div>
    </div>



</div>



<script type="text/javascript">
    $(function() {
        $('.datetimepicker').datetimepicker({
            format: 'dd-MM-yyyy hh:mm:ss'
        });
		
        ////===add room===
        $(".addroom").on("click",function(){
			
            var roomcnt = $("#roomcnt").val();	
            var newroomcnt = parseInt(roomcnt) + parseInt(1);		
            var roomhtml = addroom(newroomcnt);
			
            //$("#maindiv").append(roomhtml);
            $("#maindiv").prepend(roomhtml).hide().slideDown();
            
                           
                
                var speed =  1000;
                $('html,body').animate({
                    scrollTop: $("div#room"+newroomcnt).offset().top - 150
                }, speed);
                
            
            $("#roomcnt").val(newroomcnt);	
			
            $("div#room"+newroomcnt+" .addroomfeature").bind("click",function(){				
                featurebind(newroomcnt);				
            });
			
            $("div#room"+newroomcnt+" .removeroom").bind("click",function(){
                var roomcnt = $(this).attr('data-cnt');			
                var roomdbid = $(this).attr('data-id');						
                deleteroom(newroomcnt,roomdbid);			
            });
			
            var roomfeaturecnt=1;
            $("div#room"+newroomcnt+" div#roomfeature"+newroomcnt+" div#feature"+roomfeaturecnt+" .removefeature").bind("click",function(){			
                var roomcnt = $(this).attr('data-cnt');
                var roomfeaturecnt = $(this).attr('data-fcnt');			
                var roomfeaturedbid = $(this).attr('data-id');				
                deleteroomfeature(roomcnt,roomfeaturecnt,roomfeaturedbid);			
            });
			
        });
		
        //====add room feature====
        $(".addroomfeature").on("click",function(){			
            var roomcnt = $(this).attr('data-cnt');			
            featurebind(roomcnt);	
        });
		
		
        ///===remove roome===
        $(".removeroom").on("click",function(){			
			
		
            var roomcnt = $(this).attr('data-cnt');			
            var roomdbid = $(this).attr('data-id');				
            deleteroom(roomcnt,roomdbid);
				
					
			
			
        });
		
        ///===remove feature====
        $(".removefeature").on("click",function(){			
			
			
            var roomcnt = $(this).attr('data-cnt');
            var roomfeaturecnt = $(this).attr('data-fcnt');			
            var roomfeaturedbid = $(this).attr('data-id');				
            deleteroomfeature(roomcnt,roomfeaturecnt,roomfeaturedbid);		
			
        });
		
		
        ///====apply on change events
		
        $(".cstfeature").live("change",function(){			
            var fselid=$(this).attr('id');					
            var id=fselid.replace('feature_id','');			
            var fid= $("#"+fselid+" option:selected").val();			
            featurechange(id,fid);		
		
        });
		
		
		
		
        $(".prodcost").live("change",function(){
            var curid=$(this).attr('id');			
            var id=curid.replace('coat_type','');
            var configid= $("#"+curid+" option:selected").val();
            ratechange(id,configid);			
        });
		
		
        $(".cstlen").live("change",function(){
            var curid=$(this).attr('id');			
            var id=curid.replace('Length','');
            finalcalculation(id);
        });
        $(".cstwid").live("change",function(){
            var curid=$(this).attr('id');			
            var id=curid.replace('Width','');
            finalcalculation(id);
        });
        $(".csthei").live("change",function(){
            var curid=$(this).attr('id');			
            var id=curid.replace('Height','');
            finalcalculation(id);
        });
        $(".cstqty").live("change",function(){
            var curid=$(this).attr('id');			
            var id=curid.replace('Quantity','');
            finalcalculation(id);
        });
        $(".cstaddsqft").live("change",function(){
            var curid=$(this).attr('id');			
            var id=curid.replace('addSqft','');
            finalcalculation(id);
        });		
        $(".cstmulti").live("change",function(){
            var curid=$(this).attr('id');			
            var id=curid.replace('Multiplyer','');
            finalcalculation(id);
        });
        $(".cstsheen").live("change",function(){
            var curid=$(this).attr('id');			
            var id=curid.replace('sheen_id','');
            finalcalculation(id);
        });
        $(".cstcommit").live("change",function(){
            var curid=$(this).attr('id');			
            var id=curid.replace('commit','');
            finalcalculation(id);
        });
        $(".cstprep").live("change",function(){
            var curid=$(this).attr('id');			
            $("#opt"+curid).val($(this).val());
        });
        $(".cstsetclean").live("change",function(){
            var curid=$(this).attr('id');			
            $("#opt"+curid).val($(this).val());
        });
		
		
    });
  
  
    function featurechange(id,fid){
        var res = $.ajax({						
            type: 'POST',
            url: '<?php echo site_url('quotes/feature_config/'); ?>',
            data: {feature_id:fid},
            dataType: 'json', 
            cache: false,
            async: false                     
        }).responseText;							
		 
	
		 
        var rcom_res = jQuery.parseJSON(res);

        if(rcom_res.status=='success')
        {
				
            var configid=rcom_res.config_id;
				
            $("#coat_type"+id).html(rcom_res.html);
				
				
            ///===default apply first rate setting
            $("#coat_type"+id+" option[value='"+configid+"']").attr('selected', 'selected');
            ratechange(id,configid);			
				
				
				 
			
        }
        else {
            alert("Unable to find configuration for selected feature.");
            return false;
        }
				
    }
  
    function ratechange(id,configid){
  		
        var res = $.ajax({						
            type: 'POST',
            url: '<?php echo site_url('quotes/rate_config/'); ?>',
            data: {config_id:configid},
            dataType: 'json', 
            cache: false,
            async: false                     
        }).responseText;							
		 
	
		 	
		 
        var rcom_res = jQuery.parseJSON(res);

        if(rcom_res.status=='success')
        {
				
				
            ///===apply hidden rate
            var rate= $("#coat_type"+id+" option:selected").attr('data-rate');				
            $("#rate"+id).val(rate);
				
            
            if(rcom_res.html.length>0){
                $("#Length"+id).prepend("<option value='-1' selected='selected'>Select</option>");
                $("#Length"+id+" option").prop('disabled', false);
            } else {
                $("#Length"+id+" option[value='"+rcom_res.html.length+"']").attr('selected', 'selected');
                $("#Length"+id+" option").prop('disabled', true);
            }
            
             
            if(rcom_res.html.width>0){
                $("#Width"+id).prepend("<option value='-1' selected='selected'>Select</option>");
                $("#Width"+id+" option").prop('disabled', false);
            } else {
                $("#Width"+id+" option[value='"+rcom_res.html.width+"']").attr('selected', 'selected');
                $("#Width"+id+" option").prop('disabled', true);
            }
            
            
            if(rcom_res.html.height>0){
                $("#Height"+id).prepend("<option value='-1' selected='selected'>Select</option>");
                $("#Height"+id+" option").prop('disabled', false);
            } else {
                $("#Height"+id+" option[value='"+rcom_res.html.height+"']").attr('selected', 'selected');
                $("#Height"+id+" option").prop('disabled', true);
            }
            
            
            $("#Quantity"+id+" option[value='"+rcom_res.html.quantity+"']").attr('selected', 'selected');
            $("#addSqft"+id+" option[value='"+rcom_res.html.addSqft+"']").attr('selected', 'selected');
            $("#Multiplyer"+id+" option[value='"+rcom_res.html.multiplyer+"']").attr('selected', 'selected');
            $("#sheen_id"+id+" option[value='"+rcom_res.html.sheen_id+"']").attr('selected', 'selected');
					
            finalcalculation(id);
				
			
        }
        else {
            alert("Unable to find configuration for selected rate.");
            return false;
        }
    }
  
  
    function finalcalculation(id){

		
        var feature = $("#feature_id"+id+" option:selected").val();			  	
        var length = $("#Length"+id+" option:selected").val();			
        var width = $("#Width"+id+" option:selected").val();			
        var height = $("#Height"+id+" option:selected").val();			
        var quantity = $("#Quantity"+id+" option:selected").val();			
        var addsqft = $("#addSqft"+id+" option:selected").val();			
        var multiplyer = $("#Multiplyer"+id+" option:selected").val();			
        var sheen = $("#sheen_id"+id+" option:selected").val();			
        var coat_type = $("#coat_type"+id+" option:selected").val();				
        var rate = $("#rate"+id).val();
		
        var commit = $("#commit"+id).is(':checked');
		
        //alert("length="+length+"==width="+width+"==height="+height+"==quantity="+quantity+"==addsqft="+addsqft+"==multiplyer="+multiplyer+"==sheen="+sheen+"==coat_type="+coat_type+"==rate="+rate+"==commit="+commit);
		
        var sqfootage=0,totTime=0,sqfootagec=0,totTimec=0;
		
		
        var res = $.ajax({						
            type: 'POST',
            url: '<?php echo site_url('quotes/calculatesqfttime/'); ?>',
            data: {feature_id:feature,length:length,width:width,height:height,quantity:quantity,addsqft:addsqft,multiplyer:multiplyer,sheen:sheen,coat_type:coat_type,rate:rate,commit:commit},
            dataType: 'json', 
            cache: false,
            async: false                     
        }).responseText;							
		 
	
		 	
		 
        var rcom_res = jQuery.parseJSON(res);

        if(rcom_res.status=='success')
        {
            sqfootage=rcom_res.sqfootage;
            totTime=rcom_res.totTime;
            sqfootagec=rcom_res.sqfootagec;
            totTimec=rcom_res.totTimec;
				
        } else {
			
        }
		
		
			
        if(commit==true) {
            //===if true or 1 then enter only in 2 fields
        } else {
            //===if false or 0 then enter in all 4 fields
        }
		
        //alert(sqfootage+"==="+totTime+"==="+sqfootagec+"==="+totTimec);
        $("#sqfootage"+id).val(sqfootage);
        $("#totTime"+id).val(totTime);
        $("#sqfootagec"+id).val(sqfootagec);
        $("#totTimec"+id).val(totTimec);
		
        $("#SqFt"+id).html(sqfootage);
        $("#Hrs"+id).html(totTime);
		
                
        var split_id=id.split('-');
        var roomcnt = split_id[1];
        var roomfeaturecnt = split_id[0];
        totalroomhours(roomcnt);
		

    }
  
    function totalroomhours(roomcnt){
        var totalhrs=0;
       
        $("#room"+roomcnt+" input.csttotTime").each(function(index){
        
            totalhrs=parseFloat(totalhrs)+parseFloat($(this).val());
        });
    
        totalhrs=totalhrs.toFixed(2);
    
        $("#totalhours"+roomcnt).html(totalhrs);
    }
  
    function deleteroom(roomcnt,roomdbid){
		
        if(confirm("Are you sure, you want to delete selected room? \n\nIf you delete selected room then feature related to room also delete."))
        {
            if(roomdbid>0) {
				
            } 
				
            $("div#room"+roomcnt).remove();
        }
    }
  
    function deleteroomfeature(roomcnt,roomfeaturecnt,roomfeaturedbid){
        if(confirm("Are you sure, you want to delete selected feature?"))
        {
            if(roomfeaturedbid>0) {
			
            } 
			
            $("div#room"+roomcnt+" div#roomfeature"+roomcnt+" div#feature"+roomfeaturecnt).remove();
        }
    }
  
    function featurebind(roomcnt){
        var roomfeaturecnt = $("#roomfeaturecnt"+roomcnt).val();
		
        var newroomfeaturecnt = parseInt(roomfeaturecnt) + parseInt(1);	
			
        var featurehtml = addroomfeature(roomcnt,newroomfeaturecnt);
		
        $("div#room"+roomcnt+" div#roomfeature"+roomcnt).append(featurehtml);
        $("div#room"+roomcnt+" div#roomfeature"+roomcnt+" div#feature"+newroomfeaturecnt).slideDown('slow');
        $("div#room"+roomcnt+" input#roomfeaturecnt"+roomcnt).val(newroomfeaturecnt);	
		
		
        $("div#room"+roomcnt+" div#roomfeature"+roomcnt+" div#feature"+newroomfeaturecnt+" .removefeature").bind("click",function(){			
            var roomcnt = $(this).attr('data-cnt');
            var roomfeaturecnt = $(this).attr('data-fcnt');			
            var roomfeaturedbid = $(this).attr('data-id');				
            deleteroomfeature(roomcnt,roomfeaturecnt,roomfeaturedbid);			
        });
			
    }
  
    function addroom(roomcnt){
  	
        var html="";
	
        var roomfeaturecnt=1;
	
        html +='<div id="room'+roomcnt+'" class="roommain">';
	
        html +='<div class="controls controls-row ht40"><div class="span4">Room Type '+roomcnt+'</div><div class="span6">Additional Room Description</div><div class="span2"><button type="button" data-cnt="'+roomcnt+'" class="button button-basic-blue addroomfeature">Add Feature</button></div></div>';
	
        html +='<div class="controls controls-row ht40"><select class="span4 ui-wizard-content ui-helper-reset ui-state-default" name="roomtype_id'+roomcnt+'" id="roomtype_id'+roomcnt+'">';
	
<?php if (!empty($all_room_type)) {
    foreach ($all_room_type as $roomtype) { ?>
                        html +='<option value="<?php echo $roomtype->roomtype_id; ?>"><?php echo ucfirst($roomtype->description); ?></option>';
    <?php }
} ?>
	
                html +='</select><input type="text" name="additionalroomdescription'+roomcnt+'" id="additionalroomdescription'+roomcnt+'" placeholder="Additional Room Description" class="span6 ui-wizard-content ui-helper-reset ui-state-default"><a href="javascript:void(0)" title="Delete Room" data-id="" data-cnt="'+roomcnt+'" class="span2 pad10 removeroom"><i class="icon-remove"></i> Delete Room</a></div>';
	
                html +='<div id="roomfeature'+roomcnt+'" class="borT featuremain">';
	
                html +='<div class="controls controls-row borB featureheader"><div class="span1 cstfeature">Feature</div><div class="span1 cstlen">Length</div><div class="span1 cstwid">Width</div><div class="span1 csthei">Height</div><div class="span1 cstqty">Qty.</div><div class="span1 cstaddsqft">UpG</div><div class="span1 prodcost">Rate</div><div class="span1 cstmulti">Coats</div><div class="span1 cstsheen">Sheen</div><div class="span1 cstlhrs">Hrs.</div><div class="span1 cstlsqft">Sq. Ft.</div><div class="span1 cstlact">Optional/<br />Action</div></div>';
	
                html +=addroomfeature(roomcnt,roomfeaturecnt);
	
                html +='</div>';
                html +='<input type="hidden" name="roomfeaturecnt'+roomcnt+'" id="roomfeaturecnt'+roomcnt+'" value="'+roomfeaturecnt+'" />';
	
	
                html +='<div class="controls controls-row ht0">&nbsp;</div>';
                html +='<div class="controls controls-row borB ht15"><div class="span12"><b>Committed Features</b></div></div>';
                html +='<div class="controls controls-row borB ht15"><div class="span3">Prep</div><div class="span3">S &amp; C</div>';
	
                //html +='<div class="span3">Closets</div><div class="span3">Misc</div>';
	
                html +='</div>';
	
                html +='<div class="controls controls-row ht40">';
                
                
                html +='<input name="prep'+roomcnt+'" id="prep'+roomcnt+'" class="span3 cstprep ui-wizard-content ui-helper-reset ui-state-default" value="1.0" />';
                
                /*html +='<select name="prep'+roomcnt+'" id="prep'+roomcnt+'" class="span3">';
	
<?php /*if (!empty($all_prep)) {
    foreach ($all_prep as $key => $prep) { ?>
                        html +='<option value="<?php echo $prep; ?>"><?php echo $prep; ?></option>';
    <?php }
}*/ ?>
	
                html +='</select>';*/
	
                /*html +='<select name="setclean'+roomcnt+'" id="setclean'+roomcnt+'" class="span3">';
	
<?php //if(!empty($all_sc)) { foreach($all_sc as $key=>$sc) {  ?>
        html +='<option value="<?php //echo $sc; ?>"><?php //echo $sc; ?></option>';
<?php //}}  ?>
	
        html +='</select>';*/
	
	
                html +='<input type="text" name="setclean'+roomcnt+'" id="setclean'+roomcnt+'" class="span3 cstsetclean ui-wizard-content ui-helper-reset ui-state-default" value="1.25" />';
	
	
                /*html +='<select name="closets'+roomcnt+'" id="closets'+roomcnt+'" class="span3">';
	
<?php //if(!empty($all_clostes)) { foreach($all_clostes as $key=>$clostes) {  ?>
        html +='<option value="<?php //echo $clostes; ?>"><?php //echo $clostes; ?></option>';
<?php //}}  ?>
	
        html +='</select><select name="misc'+roomcnt+'" id="misc'+roomcnt+'" class="span3">';
	
<?php //if(!empty($all_misc)) { foreach($all_misc as $key=>$misc) {  ?>
        html +='<option value="<?php //echo $misc; ?>"><?php //echo $misc; ?></option>';
<?php //}}  ?>
	
        html +='</select>';*/
	
	
                html +='</div>';
                
                
                
                html +='<input type="hidden" name="optprep'+roomcnt+'" id="optprep'+roomcnt+'" value="1.0" />';              
                html +='<input type="hidden" name="optsetclean'+roomcnt+'" id="optsetclean'+roomcnt+'" value="1.25"  />';
                
	
                /*html +='<div class="controls controls-row ht0">&nbsp;</div>';
                html +='<div class="controls controls-row borB ht15"><div class="span12"><b>Optional Features</b></div></div>';
                html +='<div class="controls controls-row borB ht15"><div class="span3">Prep</div><div class="span3">S &amp; C</div>';
	
                //html +='<div class="span3">Closets</div><div class="span3">Misc</div>';
	
                html +='</div>';
	
	
                html +='<div class="controls controls-row borB ht40">';
                html +='<select name="optprep'+roomcnt+'" id="optprep'+roomcnt+'" class="span3">';
	
<?php /*if (!empty($all_prep)) {
    foreach ($all_prep as $key => $prep) { ?>
                        html +='<option value="<?php echo $prep; ?>"><?php echo $prep; ?></option>';
    <?php }
}*/ ?>
	
                html +='</select>';
	
	
                /*html +='<select name="optsetclean'+roomcnt+'" id="optsetclean'+roomcnt+'" class="span3">';
	
<?php //if(!empty($all_sc)) { foreach($all_sc as $key=>$sc) {  ?>
        html +='<option value="<?php //echo $sc; ?>"><?php //echo $sc; ?></option>';
<?php //}}  ?>
	
        html +='</select>';*/
	
                //html +='<input type="text" name="optsetclean'+roomcnt+'" id="optsetclean'+roomcnt+'" class="span3" />';
	
                /*html +='<select name="optclosets'+roomcnt+'" id="optclosets'+roomcnt+'" class="span3">';
	
<?php //if(!empty($all_clostes)) { foreach($all_clostes as $key=>$clostes) {  ?>
        html +='<option value="<?php //echo $clostes; ?>"><?php //echo $clostes; ?></option>';
<?php //}}  ?>
	
        html +='</select><select name="optmisc'+roomcnt+'" id="optmisc'+roomcnt+'" class="span3">';
	
<?php //if(!empty($all_misc)) { foreach($all_misc as $key=>$misc) {  ?>
        html +='<option value="<?php //echo $misc; ?>"><?php //echo $misc; ?></option>';
<?php //}}  ?>
	
        html +='</select>';
	
                html +='</div>';*/
        
        
        
        
        
	
                html +='<div class="controls controls-row borB ht15"><div class="span9">&nbsp;</div><div class="span3">Total Hours</div></div>';
	
                html +='<div class="controls controls-row ht40">';
	
                html +='<div class="span9">&nbsp;</div>';
	
                html +='<div class="span3" id="totalhours'+roomcnt+'">0.00</div>';
	
                html +='</div>';
									 
	
                html +='<input type="hidden" name="quotedetail_id'+roomcnt+'" id="quotedetail_id'+roomcnt+'" value="" />';
	
                html +='</div>';
	
                return html;
  
            }
  
            function addroomfeature(roomcnt,roomfeaturecnt){
                var html="";
	
                html +='<div id="feature'+roomfeaturecnt+'" class="controls controls-row borB ht65 featurecontent" style="display:none;">';
	
                html +='<select name="feature_id'+roomfeaturecnt+'-'+roomcnt+'" id="feature_id'+roomfeaturecnt+'-'+roomcnt+'" class="span1 cstfeature ui-wizard-content ui-helper-reset ui-state-default">';
	
                html +='<option value="0">Select</option>';
<?php if (!empty($all_feature_type)) {
    foreach ($all_feature_type as $feature) { ?>
                        html +='<option value="<?php echo $feature->feature_id; ?>"><?php echo ucfirst($feature->description); ?></option>';
    <?php }
} ?>
	
                html +='</select><select name="Length'+roomfeaturecnt+'-'+roomcnt+'" id="Length'+roomfeaturecnt+'-'+roomcnt+'" class="span1 cstlen ui-wizard-content ui-helper-reset ui-state-default">';
	
                html +='<option value="0">N/A</option>';	
<?php for ($i = 1; $i <= 50; $i++) { ?>
                    html +='<option value="<?php echo $i; ?>"><?php echo $i; ?></option>';
<?php } ?>
	
                html +='</select><select name="Width'+roomfeaturecnt+'-'+roomcnt+'" id="Width'+roomfeaturecnt+'-'+roomcnt+'" class="span1 cstwid ui-wizard-content ui-helper-reset ui-state-default">';
	
                html +='<option value="0">N/A</option>';
<?php for ($i = 1; $i <= 50; $i++) { ?>
                    html +='<option value="<?php echo $i; ?>"><?php echo $i; ?></option>';
<?php } ?>
	
                html +='</select><select name="Height'+roomfeaturecnt+'-'+roomcnt+'" id="Height'+roomfeaturecnt+'-'+roomcnt+'" class="span1 csthei ui-wizard-content ui-helper-reset ui-state-default">';
	
                html +='<option value="0">N/A</option>';
<?php for ($i = 1; $i <= 50; $i++) { ?>
                    html +='<option value="<?php echo $i; ?>"><?php echo $i; ?></option>';
<?php } ?>
	
                html +='</select><select name="Quantity'+roomfeaturecnt+'-'+roomcnt+'" id="Quantity'+roomfeaturecnt+'-'+roomcnt+'" class="span1 cstqty ui-wizard-content ui-helper-reset ui-state-default">';
                html +='<option value="0">N/A</option>';
<?php for ($i = 1; $i <= 50; $i++) { ?>
                    html +='<option value="<?php echo $i; ?>"><?php echo $i; ?></option>';
<?php } ?>
	
                html +='</select><select name="addSqft'+roomfeaturecnt+'-'+roomcnt+'" id="addSqft'+roomfeaturecnt+'-'+roomcnt+'" class="span1 cstaddsqft ui-wizard-content ui-helper-reset ui-state-default">';
	
                html +='<option value="0">N/A</option>';	
<?php for ($i = 10; $i <= 25; $i++) { ?>
                    html +='<option value="<?php echo $i; ?>"><?php echo $i; ?></option>';
<?php } ?>
	
                html +='</select><select name="coat_type'+roomfeaturecnt+'-'+roomcnt+'" id="coat_type'+roomfeaturecnt+'-'+roomcnt+'" class="span1 prodcost ui-wizard-content ui-helper-reset ui-state-default">';
	
                html +='<option value="0">N/A</option>';
	
<?php /* if(!empty($all_feature_coat)) { foreach($all_feature_coat as $fcoat) { ?>
  html +='<option value="<?php echo $fcoat->featurecoat_id;?>"  data-rate="<?php echo $fcoat->rate;?>"><?php echo $fcoat->rate."&nbsp;&nbsp;|&nbsp;&nbsp;".ucfirst($fcoat->rate_type);?></option>';
  <?php }} */ ?>
	
                html +='</select><select name="Multiplyer'+roomfeaturecnt+'-'+roomcnt+'" id="Multiplyer'+roomfeaturecnt+'-'+roomcnt+'" class="span1 cstmulti ui-wizard-content ui-helper-reset ui-state-default">';
                html +='<option value="0">N/A</option>';
<?php if (!empty($all_multiplyer)) {
    $sel_mul=2;
    foreach ($all_multiplyer as $key => $multiplyers) { ?>
                        html +='<option value="<?php echo $multiplyers; ?>"  <?php if($sel_mul==$multiplyers){?> selected="selected" <?php } ?> ><?php echo $multiplyers; ?></option>';
    <?php }
} ?>
	
                html +='</select><select name="sheen_id'+roomfeaturecnt+'-'+roomcnt+'" id="sheen_id'+roomfeaturecnt+'-'+roomcnt+'" class="span1 cstsheen ui-wizard-content ui-helper-reset ui-state-default">';
	
                html +='<option value="0">N/A</option>';
	
<?php if (!empty($all_sheen)) {
    foreach ($all_sheen as $sheen) { ?>
                        html +='<option value="<?php echo $sheen->sheen_id; ?>"><?php echo ucfirst($sheen->sheen_description); ?></option>';
    <?php }
} ?>
	
                html +='</select>';
	
	
                html +='<label class="span1 cstlhrs" id="Hrs'+roomfeaturecnt+'-'+roomcnt+'">0.0</label>';
                html +='<label class="span1 cstlsqft" id="SqFt'+roomfeaturecnt+'-'+roomcnt+'">0.0</label>';
	
	
                html+='<div class="span1 cstlact">';
	
                html +='<input name="rate'+roomfeaturecnt+'-'+roomcnt+'" id="rate'+roomfeaturecnt+'-'+roomcnt+'"  value="0.0" type="hidden" readonly="readonly" />';
	
                html +='<input class="cstsqfootage" name="sqfootage'+roomfeaturecnt+'-'+roomcnt+'" id="sqfootage'+roomfeaturecnt+'-'+roomcnt+'" value="0.0" type="hidden" readonly="readonly" />';
                html +='<input class="csttotTime" name="totTime'+roomfeaturecnt+'-'+roomcnt+'" id="totTime'+roomfeaturecnt+'-'+roomcnt+'"  value="0.0" type="hidden" readonly="readonly" />';
	
                html +='<input class="cstsqfootagec" name="sqfootagec'+roomfeaturecnt+'-'+roomcnt+'" id="sqfootagec'+roomfeaturecnt+'-'+roomcnt+'" value="0.0" type="hidden" readonly="readonly" />';
                html +='<input class="csttotTimec" name="totTimec'+roomfeaturecnt+'-'+roomcnt+'" id="totTimec'+roomfeaturecnt+'-'+roomcnt+'" value="0.0" type="hidden" readonly="readonly" />';
	
                html+='<input type="checkbox" value="1" name="commit'+roomfeaturecnt+'-'+roomcnt+'" id="commit'+roomfeaturecnt+'-'+roomcnt+'" class="cstcommit ui-wizard-content ui-helper-reset ui-state-default" />';
	
                html +='<a href="javascript:void(0)" title="Delete Feature" data-id="" data-fcnt="'+roomfeaturecnt+'" data-cnt="'+roomcnt+'" class="pad10 removefeature"><i class="icon-remove"></i></a>';
	
                html+='</div><br /><br/>';
	
                html +='<input type="text" name="description'+roomfeaturecnt+'-'+roomcnt+'" id="description'+roomfeaturecnt+'-'+roomcnt+'" placeholder="Additional Description" class="span12 ui-wizard-content ui-helper-reset ui-state-default" />';
	
                html +='<input name="roomdetail_id'+roomfeaturecnt+'-'+roomcnt+'" id="roomdetail_id'+roomfeaturecnt+'-'+roomcnt+'" value="" type="hidden" readonly="readonly" />';
	
                html +='</div>';
	
                return html;
            }
</script>