<?php $site_setting = site_setting(); ?>
<div class="page-header">
    <div class="pull-left">
        <h4><i class="icon-cogs"></i>Quotes</h4>
    </div>
    <div class="pull-right">
        <ul class="bread">
            <li><a href="<?php echo site_url('quotes/manage'); ?>">Manage Quotes</a><span class="divider">/</span></li>
            <li class="active">Quotes</li>
        </ul>
    </div>
</div>




<div class="container-fluid" id="content-area">


    <div class="row-fluid">
        <div class="span12">




            <?php if ($msg != '') { ?>
                <div class="alert <?php if ($msg == 'notfound' || $msg == 'cannot') { ?>alert-danger<?php } else { ?>alert-success<?php } ?>">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    <strong><?php if ($msg == 'notfound' || $msg == 'cannot') { ?>Warning<?php } else { ?>Success<?php } ?>!</strong>
                    <?php if ($msg == 'active') { ?>Quote has been activated successfully. <?php } ?>
                    <?php if ($msg == 'inactive') { ?>Quote has been inactivated successfully. <?php } ?>
                    <?php if ($msg == 'delete') { ?>Quote has been deleted successfully. <?php } ?>	
                    <?php if ($msg == 'insert') { ?>New Quote has been added successfully. <?php } ?>	
                    <?php if ($msg == 'update') { ?>Quote detail has been updated successfully. <?php } ?>	
                    <?php if ($msg == 'notfound') { ?>Quote records not found. <?php } ?>
                    <?php if ($msg == 'cannot') { ?>Cannot delete Quote because it used in many records. <?php } ?>		

                </div> 
            <?php } ?>	


            <script>
        
        function searchme(){
                
                var search_value=$.trim($("input#search_value").val());
                var search_by=$.trim($("input#search_by").val());
                
                
                 
                if(search_by!='' && search_value!='')
		{
                    
                    window.location.href='<?php echo site_url('quotes/search');?>/'+search_by+'/'+search_value;
		}
		else
		{
                    window.location.href='<?php echo site_url('quotes/manage');?>/';
                }
		
        }
        
        </script>
            

            <div class="box">
                <div class="box-head">
                    <i class="icon-table"></i>
                    <span>Manage Quotes</span>
                </div>




                    <div class="box-body box-body-nopadding">
                        <div class="highlight-toolbar">
                            <div class="pull-left">
                                
                                <input type="hidden" name="search_by" id="search_by" value="name" />
                                
                                <input type="text" value=" <?php if($keyword!=''){ echo $keyword; } ?>" name="search_value" id="search_value" style="margin: 5px;" />
                                <input type="button" name="search_btn" value="Search" class="button button-basic" onclick="searchme();" />
                                
                                
                                
                                
                            </div>
                            
                            
                <form name="frm_listquotes" id="frm_listquotes" action="<?php echo site_url('quotes/quote_action'); ?>" method="post">
                    <input type="hidden" name="action" id="action" />
                    <input type="hidden" name="offset" id="offset" value="<?php echo $offset; ?>" />	


                            <div class="pull-right"><div class="btn-toolbar">
                                    <div class="btn-group">
                                        <a href="<?php echo site_url('quotes/finder'); ?>" class='button button-basic button-icon' rel="tooltip" title="Add"><i class="icon-plus-sign"></i></a>
                                        <a href="javascript:void(0)" class='button button-basic button-icon' rel="tooltip" title="Delete" onclick="setaction('chk[]','delete', 'If Quote is linked to any records then it will not delete record. \n\nAre you sure, you want to delete selected record(s)?', 'frm_listquotes')"><i class="icon-trash"></i></a>

                                    </div>
                                </div></div>

                        </div>
                        <table class="table table-nomargin table-striped table-bordered table-hover table-pagination">
                            <thead>
                                <tr>
                                    <th><input type="checkbox" class="checkall" /></th> 
                                    <th>Client</th>
                                    <th>Quote ID</th>
                                    <th>Quote Type</th>
                                    <th>Type</th>
                                    <th>Sales Lead</th>
                                    <th>Won</th>
                                    <th>Date</th>
                                    <th>Action</th>														
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if ($result) {

                                    foreach ($result as $res) {
                                        ?>

                                        <tr> 
                                            <td><input type="checkbox" id='chk' name="chk[]" value="<?php echo $res->quote_id; ?>" /></td> 
                                            <td><?php echo anchor('quotes/add_quote/' . $res->quote_id, ucfirst($res->first_name . ' ' . $res->last_name), ' rel="tooltip" title="Edit" '); ?></td> 

                                            <td><?php echo $res->quote_unique_id; ?></td>
                                            <td><?php if ($res->quotetype_id > 0) {
                                            $quote_type = quotetype_by_id($res->quotetype_id);
                                            if (!empty($quote_type)) {
                                                echo ucfirst($quote_type->quotetype);
                                            }
                                        } ?></td>
                                            <td><?php if ($res->bustype_id > 0) {
                                            $bustype_type = bustype_by_id($res->bustype_id);
                                            if (!empty($bustype_type)) {
                                                echo ucfirst($bustype_type->description);
                                            }
                                        } ?></td>

                                            <td><?php if ($res->team_id > 0) {
                                            $team_detail = team_by_id($res->team_id);
                                            if (!empty($team_detail)) {
                                                echo ucfirst($team_detail->name);
                                            }
                                        } ?></td>


                                            <td><?php if ($res->wonreason_id > 0) {
                                    $wonreason_detail = winloss_by_id($res->wonreason_id);
                                    if (!empty($wonreason_detail)) {
                                        echo ucfirst($wonreason_detail->reason);
                                    }
                                } ?></td>

                                            <td><?php if ($res->date != '') {
                                    echo date($site_setting->date_time_format, strtotime($res->date));
                                } ?></td> 

                                            <td>                                               
                                                <a href="<?php echo site_url('quotes/add_quote/' . $res->quote_id); ?>" class='button button-basic button-icon' rel="tooltip" title="Edit"><i class="icon-edit"></i></a>
                                            </td>

                                        </tr> 



    <?php } ?>
<?php } else { ?>
                                    <tr><td colspan="10" align="center" valign="middle"  style="text-align:center;">No Quote Type has been added yet.</td></tr>
<?php } ?>

                            </tbody>
                        </table>
                        <div class="bottom-table">
                            <div class="pull-left">

                            </div>
                            <div class="pull-right"><div class="pagination pagination-custom">
<?php echo $page_link; ?>
                                </div></div>
                        </div>
                    </div>

                </form>


            </div>
        </div>
    </div>


</div>