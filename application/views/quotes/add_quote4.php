<?php $site_setting=site_setting();
 ?>
 	<!-- datepicker plugin -->
	<link rel="stylesheet" href="<?php echo base_url();?>css/bootstrap-datetimepicker.min.css">

    
	<!-- datepicker plugin -->
	<script src="<?php echo base_url();?>js/bootstrap-datetimepicker.min.js"></script>

 

<div class="page-header">
				<div class="pull-left">
					<h4><i class="icon-cogs"></i> <?php if($quote_id!='') {?>Edit Quote<?php } else { ?>Add Quote<?php }  ?></h4>
				</div>
				<div class="pull-right">
					<ul class="bread">
						<li><a href="<?php echo site_url('quotes/manage');?>">Manage Quotes</a><span class="divider">/</span></li>
                        <li class="active">Quotes</li>
					</ul>
				</div>
			</div>
            
            
            <div class="container-fluid" id="content-area">
				
<div class="row-fluid">
                <div class="span12">
                
                 <?php if($error!=''){ ?>

		<div class="alert alert-error">
											<button type="button" class="close" data-dismiss="alert">&times;</button>
											<strong>Warning !</strong> <?php echo $error;?>
										</div>    <?php }?>
                                        
                                        
      <?php if($msg!='') { ?>
<div class="alert <?php if($msg=='notfound' || $msg=='cannot') {?>alert-danger<?php } else { ?>alert-success<?php } ?>">
											<button type="button" class="close" data-dismiss="alert">&times;</button>
											<strong><?php if($msg=='notfound' || $msg=='cannot') {?>Warning<?php } else { ?>Success<?php } ?>!</strong>

	<?php if($msg=='delete') { ?>Quote has been deleted successfully. <?php } ?>	
	<?php if($msg=='insert') { ?>New Quote has been added successfully. <?php } ?>	
	<?php if($msg=='update') { ?>Quote detail has been updated successfully. <?php } ?>	
    <?php if($msg=='notfound') { ?>Quote records not found. <?php } ?>
    <?php if($msg=='cannot') { ?>Cannot delete Quote because it used in many records. <?php } ?>		
  
</div> 
<?php } ?>	              
					
						<div class="box">
							<div class="box-head">
								<i class="icon-list-ul"></i>
								<span>Change Quote details</span>
                                
                                <?php
						   $data['quote_details']=$quote_details;						 
						   $this->load->view('quotes/quote_title',$data); 
						   ?>
							</div>
							<div class="box-body box-body-nopadding">
                             <?php
									$attributes = array('name'=>'frm_addquote','class'=>'form-horizontal form-bordered form-wizard');
									echo form_open('quotes/step4/'. $quote_id,$attributes);
								  ?> 
                                  
								
                             	<?php $this->load->view('quotes/quote_buttons',$data);  ?>
                                    
                                  <div class="step" id="firstStep" style="opacity:1 !important;">
										
                                        
                                          <!-----steps--->   
                                         <?php 
										   $data['step_index']=4;
										 $this->load->view('quotes/quote_steps',$data);  ?>
                                        <!-----steps--->
                                        
                                        
                                        
                                        
                                        
                                     <style>
							   .marAll{ margin:0px 0px 0px 8px !important; }
							   .marAll2{ margin:0px 8px 0px 0px !important; }
							   .marL0 { margin:0px !important; }
							   .ht0 { height: 0px; }
							   .ht15 { height: 15px; }
							   .ht40 { height: 40px; }
							   .ht65 { height: 65px; }
							   .padT10 { padding-top:10px; }
							   .padT20 { padding-top:20px; }
							   .padB20 { padding-bottom:20px; }
							   .pad10 { padding: 10px; }
							   .roommain { border: 2px solid #ADADAD; margin-bottom:2px; }
							   .borT { border-top: 1px solid #ccc; } 
							   .borB { border-bottom: 1px solid #ccc; }
							   #maindiv .controls { margin:0px !important; }
                                                           .bordertable { /*background: #ccc;*/ border-spacing:1px; /*border-collapse: inherit;*/   border: 1px solid #FFFFFF; }
                                                           .bordertable tr td{ /*background: #F4F4F4;*/ }
							   </style>             
                                        
                                   <br /><br />

                                   <?php $jobrate=$quote_details->rate; ?>
                                   
                                     <?php
                                $total_customer_hours = $total_commit_hours;
                                
                                $total_customer_material=$total_commit_material;
                                
                                
                                
                                
                                
                                if($quote_details->confirm_opt1==1){
                                    $total_customer_hours = $total_customer_hours + $total_optional_hours_1;
                                    $total_customer_material = $total_customer_material + $total_optional_material_1;
                                }
                                if($quote_details->confirm_opt2==1){
                                    $total_customer_hours = $total_customer_hours + $total_optional_hours_2;
                                    $total_customer_material = $total_customer_material + $total_optional_material_2;
                                }
                                if($quote_details->confirm_opt3==1){
                                    $total_customer_hours = $total_customer_hours + $total_optional_hours_3;
                                    $total_customer_material = $total_customer_material + $total_optional_material_3;
                                }
                                if($quote_details->confirm_opt4==1){
                                    $total_customer_hours = $total_customer_hours + $total_optional_hours_4;
                                    $total_customer_material = $total_customer_material + $total_optional_material_4;
                                }
                                

                                $total_customer_labour_costs = $total_customer_hours * $jobrate;
                                ?>
                                   
                                   
                                    <div class="row-fluid">
                                    
                                    <!---commited features--->
                                    <div class="span6">
                                    
                                    <div align="center" class="padB20" style="font-weight:bold;">Customer Quote</div>
                                    
                                    
                                    <!---upper top--->
                                    <div class="controls controls-row marAll padT10 borT">   
                                        <label class="span6">Total Hours :</label>
                                       <input type="text" value="<?php echo $total_customer_hours; ?>" name="total_customer_hours" id="total_customer_hours" placeholder="0" class="span6" readonly="readonly">
                                    </div>
                                   <div class="controls controls-row marAll padT10">          
                                        <label class="span6">Job Rate(<?php echo $site_setting->currency_symbol;?>) :</label>
                                        <input type="text" value="<?php echo $jobrate; ?>" name="rate" id="rate" placeholder="0" class="span6" readonly="readonly">
                                    </div>
                                    
                                    <div class="controls controls-row marAll padT10">          
                                        <label class="span6">Labour Costs(<?php echo $site_setting->currency_symbol;?>) :</label>
                                        <input type="text" value="<?php echo $total_customer_labour_costs; ?>" name="total_customer_labour_costs" id="total_customer_labour_costs" placeholder="0" class="span6" readonly="readonly">
                                    </div>
                                    
                                    <div class="controls controls-row marAll padT10">          
                                        <label class="span6">Quoted Materials(<?php echo $site_setting->currency_symbol;?>) :</label>
                                        <input type="text" value="<?php echo $total_customer_material; ?>" name="total_customer_material" id="total_customer_material" placeholder="0" class="span6" readonly="readonly">
                                    </div>
                                     <!---upper top--->
                                    
                                    
                                    <!---other service-->
                                    
                                    
                                     
                                    <div class="controls controls-row marAll padT10 borT">   
                                   <select class="span6" name="" id="" disabled="disabled">
                                  <option value="">Select Service 1</option>
                                   <?php if(!empty($other_service)) { 
								   	foreach($other_service as $key=>$service) {?>
                                   	<option value="<?php echo $key;?>" <?php if ($quote_details->AdditionalCostDesc1 == $key) {
                                            echo "selected";
                                        } ?>><?php echo ucfirst($service);?></option>
                                    <?php } } ?>
                                   </select>
                                        
                                        
                                        
                                        
                                        
                                         
                                         
                                           <div class="span6 input-append input-prepend">
                                               <span class="add-on"><?php echo $site_setting->currency_symbol;?></span>
                                               <input class="input-small" id="AdditionalCostAmt1" name="AdditionalCostAmt1" value="<?php echo $quote_details->AdditionalCostAmt1; ?>" type="text" readonly="" />
                                            <div class="btn-group">
                                                    <button class="button button-basic dropdown-toggle" data-toggle="dropdown">
                                                            Select
                                                            <span class="caret"></span>
                                                    </button>
                                                <ul class="dropdown-menu cstdropmenu">
                                                        
                                                         <?php if(!empty($other_service_pay)) { 
								   	foreach($other_service_pay as $key=>$pay) {?>
                                                    <li class="disabled"><a href="javascript:void(0)" data-field="AdditionalCostAmt1" data-val="<?php echo $pay; ?>"><?php echo $pay; ?></a></li>
                                                        
                                                     
                                                            <?php } } ?>
                                                        
                                                    </ul>
                                            </div>
                                    </div>
                                            
                                        
                                        
                                        
                                        
                                        
                                        
                                        
                                        
                                    </div>
                                    
                                    <div class="controls controls-row marAll padT10">   
                                   <select class="span6" name="" id="" disabled="disabled">
                                   <option value="">Select Service 2</option>
                                   <?php if(!empty($other_service)) { 
								   	foreach($other_service as $key=>$service) {?>
                                   	<option value="<?php echo $key;?>" <?php if ($quote_details->AdditionalCostDesc2 == $key) {
                                            echo "selected";
                                        } ?>><?php echo ucfirst($service);?></option>
                                    <?php } } ?>
                                   </select>
                                
                                        
                                        
                                        
                                        
                                        
                                         
                                            
                                         
                                           <div class="span6 input-append input-prepend">
                                               <span class="add-on"><?php echo $site_setting->currency_symbol;?></span>
                                               <input class="input-small" id="AdditionalCostAmt2" name="AdditionalCostAmt2" value="<?php echo $quote_details->AdditionalCostAmt2; ?>" type="text" readonly="" />
                                            <div class="btn-group">
                                                    <button class="button button-basic dropdown-toggle" data-toggle="dropdown">
                                                            Select
                                                            <span class="caret"></span>
                                                    </button>
                                                <ul class="dropdown-menu cstdropmenu">
                                                        
                                                         <?php if(!empty($other_service_pay)) { 
								   	foreach($other_service_pay as $key=>$pay) {?>
                                                    <li class="disabled"><a href="javascript:void(0)" data-field="AdditionalCostAmt2" data-val="<?php echo $pay; ?>"><?php echo $pay; ?></a></li>
                                                        
                                                     
                                                            <?php } } ?>
                                                        
                                                    </ul>
                                            </div>
                                    </div>
                                            
                                        
                                        
                                        
                                        
                                        
                                        
                                        
                                        
                                        
                                    </div>
                                    
                                    <div class="controls controls-row marAll padT10">   
                                   <select class="span6" name="" id="" disabled="disabled">
                                   <option value="">Select Service 3</option>
                                   <?php if(!empty($other_service)) { 
								   	foreach($other_service as $key=>$service) {?>
                                   	<option value="<?php echo $key;?>" <?php if ($quote_details->AdditionalCostDesc3 == $key) {
                                            echo "selected";
                                        } ?>><?php echo ucfirst($service);?></option>
                                    <?php } } ?>
                                   </select>
                                 
                                        
                                        
                                        
                                            
                                         
                                           <div class="span6 input-append input-prepend">
                                               <span class="add-on"><?php echo $site_setting->currency_symbol;?></span>
                                               <input class="input-small" id="AdditionalCostAmt3" name="AdditionalCostAmt3" value="<?php echo $quote_details->AdditionalCostAmt3; ?>" type="text" readonly="" />
                                            <div class="btn-group">
                                                    <button class="button button-basic dropdown-toggle" data-toggle="dropdown">
                                                            Select
                                                            <span class="caret"></span>
                                                    </button>
                                                <ul class="dropdown-menu cstdropmenu">
                                                        
                                                         <?php if(!empty($other_service_pay)) { 
								   	foreach($other_service_pay as $key=>$pay) {?>
                                                    <li class="disabled"><a href="javascript:void(0)" data-field="AdditionalCostAmt3" data-val="<?php echo $pay; ?>"><?php echo $pay; ?></a></li>
                                                        
                                                     
                                                            <?php } } ?>
                                                        
                                                    </ul>
                                            </div>
                                    </div>
                                            
                                        
                                        
                                        
                                        
                                        
                                        
                                        
                                    </div>
                                    
                                    <div class="controls controls-row marAll padT10 ">   
                                   <select class="span6" name="" id="" disabled="disabled">
                                   <option value="">Select Service 4</option>
                                   <?php if(!empty($other_service)) { 
								   	foreach($other_service as $key=>$service) {?>
                                   	<option value="<?php echo $key;?>" <?php if ($quote_details->AdditionalCostDesc4 == $key) {
                                            echo "selected";
                                        } ?>><?php echo ucfirst($service);?></option>
                                    <?php } } ?>
                                   </select>
                                 
                                        
                                   
                                            
                                         
                                           <div class="span6 input-append input-prepend">
                                               <span class="add-on"><?php echo $site_setting->currency_symbol;?></span>
                                               <input class="input-small" id="AdditionalCostAmt4" name="AdditionalCostAmt4" value="<?php echo $quote_details->AdditionalCostAmt4; ?>" type="text" readonly="" />
                                            <div class="btn-group">
                                                    <button class="button button-basic dropdown-toggle" data-toggle="dropdown">
                                                            Select
                                                            <span class="caret"></span>
                                                    </button>
                                                <ul class="dropdown-menu cstdropmenu">
                                                        
                                                         <?php if(!empty($other_service_pay)) { 
								   	foreach($other_service_pay as $key=>$pay) {?>
                                                    <li class="disabled"><a href="javascript:void(0)" data-field="AdditionalCostAmt4" data-val="<?php echo $pay; ?>"><?php echo $pay; ?></a></li>
                                                        
                                                     
                                                            <?php } } ?>
                                                        
                                                    </ul>
                                            </div>
                                    </div>
                                            
                                        
                                        
                                        
                                        
                                        
                                    </div>
                                    
                                    
                                        <div class="controls controls-row marAll padT10 padB20 borB">   
                                   <select class="span6" name="" id="" disabled="disabled">
                                   <option value="">Select Service 5</option>
                                   <?php if(!empty($other_service)) { 
								   	foreach($other_service as $key=>$service) {?>
                                   	<option value="<?php echo $key;?>" <?php if ($quote_details->AdditionalCostDesc5 == $key) {
                                            echo "selected";
                                        } ?>><?php echo ucfirst($service);?></option>
                                    <?php } } ?>
                                   </select>
                                            
                                            
                                            
                                            
                                            
                                            
                                         
                                           <div class="span6 input-append input-prepend">
                                               <span class="add-on"><?php echo $site_setting->currency_symbol;?></span>
                                               <input class="input-small" id="AdditionalCostAmt5" name="AdditionalCostAmt5" value="<?php echo $quote_details->AdditionalCostAmt5; ?>" type="text" readonly="" />
                                            <div class="btn-group">
                                                    <button class="button button-basic dropdown-toggle" data-toggle="dropdown">
                                                            Select
                                                            <span class="caret"></span>
                                                    </button>
                                                <ul class="dropdown-menu cstdropmenu">
                                                        
                                                         <?php if(!empty($other_service_pay)) { 
								   	foreach($other_service_pay as $key=>$pay) {?>
                                                    <li class="disabled"><a href="javascript:void(0)" data-field="AdditionalCostAmt5" data-val="<?php echo $pay; ?>"><?php echo $pay; ?></a></li>
                                                        
                                                     
                                                            <?php } } ?>
                                                        
                                                    </ul>
                                            </div>
                                    </div>
                                            
                                            
                                            
                                    </div>
                                    
                                    
                                    
                                    
                                    <!--optional services--->
                                                                        
                                  <?php if($quote_details->confirm_opt1==1 || $quote_details->confirm_opt2==1 || $quote_details->confirm_opt3==1 || $quote_details->confirm_opt4==1){ ?>  <div class="controls controls-row marAll padT10 padB20 borB" style="max-height: 150px; overflow-y: scroll;">   
                                   
                                    <?php if($quote_details->confirm_opt1==1){ 
                                       
                                        if($quote_details->OptionalCostAmt11>0 || $quote_details->OptionalCostAmt12>0 || $quote_details->OptionalCostAmt13>0 || $quote_details->OptionalCostAmt14>0 || $quote_details->OptionalCostAmt15>0) {
                                       
                                       ?>
                                   <div style="background: #E3FFD9;">
                                       
                                       <div align="center" class="padB20 padT20 opttitle" style="font-weight:bold;">Optional 1 - Services </div>
                                       
                                       <table class="bordertable"  border="1" cellpadding="5" cellspacing="1" width="100%">
                                    
                                           <?php if($quote_details->OptionalCostAmt11>0) { ?><tr><td><?php echo $quote_details->OptionalCostDesc11; ?></td><td width="30%"><?php echo $site_setting->currency_symbol;?><?php echo $quote_details->OptionalCostAmt11; ?></td></tr> <?php } ?>
                                           
                                           
                                           <?php if($quote_details->OptionalCostAmt12>0) { ?><tr><td><?php echo $quote_details->OptionalCostDesc12; ?></td><td width="30%"><?php echo $site_setting->currency_symbol;?><?php echo $quote_details->OptionalCostAmt12; ?></td></tr> <?php } ?>
                                           
                                           <?php if($quote_details->OptionalCostAmt13>0) { ?><tr><td><?php echo $quote_details->OptionalCostDesc13; ?></td><td width="30%"><?php echo $site_setting->currency_symbol;?><?php echo $quote_details->OptionalCostAmt13; ?></td></tr> <?php } ?>
                                           
                                           <?php if($quote_details->OptionalCostAmt14>0) { ?><tr><td><?php echo $quote_details->OptionalCostDesc14; ?></td><td width="30%"><?php echo $site_setting->currency_symbol;?><?php echo $quote_details->OptionalCostAmt14; ?></td></tr> <?php } ?>
                                           
                                           <?php if($quote_details->OptionalCostAmt15>0) { ?><tr><td><?php echo $quote_details->OptionalCostDesc15; ?></td><td width="30%"><?php echo $site_setting->currency_symbol;?><?php echo $quote_details->OptionalCostAmt15; ?></td></tr> <?php } ?>
                                           
                                       
                                       </table>
                                       
                                       
                                    
                                   </div>    
                                   <?php }} ?>
                                  
                                  
                                   
                                   
                                   
                                   
                                   
                                   <?php if($quote_details->confirm_opt2==1){ 
                                       
                                        if($quote_details->OptionalCostAmt21>0 || $quote_details->OptionalCostAmt22>0 || $quote_details->OptionalCostAmt23>0 || $quote_details->OptionalCostAmt24>0 || $quote_details->OptionalCostAmt25>0) {
                                       
                                       ?>
                                   <div style="background: #FDDEC1;">       
                                       
                                       <div align="center" class="padB20 padT20 opttitle" style="font-weight:bold;">Optional 2 - Services </div>
                                       
                                       <table class="bordertable"  border="1" cellpadding="5" cellspacing="1" width="100%">
                                    
                                           <?php if($quote_details->OptionalCostAmt21>0) { ?><tr><td><?php echo $quote_details->OptionalCostDesc21; ?></td><td width="30%"><?php echo $site_setting->currency_symbol;?><?php echo $quote_details->OptionalCostAmt21; ?></td></tr> <?php } ?>
                                           
                                           
                                           <?php if($quote_details->OptionalCostAmt22>0) { ?><tr><td><?php echo $quote_details->OptionalCostDesc22; ?></td><td width="30%"><?php echo $site_setting->currency_symbol;?><?php echo $quote_details->OptionalCostAmt22; ?></td></tr> <?php } ?>
                                           
                                           <?php if($quote_details->OptionalCostAmt23>0) { ?><tr><td><?php echo $quote_details->OptionalCostDesc23; ?></td><td width="30%"><?php echo $site_setting->currency_symbol;?><?php echo $quote_details->OptionalCostAmt23; ?></td></tr> <?php } ?>
                                           
                                           <?php if($quote_details->OptionalCostAmt24>0) { ?><tr><td><?php echo $quote_details->OptionalCostDesc24; ?></td><td width="30%"><?php echo $site_setting->currency_symbol;?><?php echo $quote_details->OptionalCostAmt24; ?></td></tr> <?php } ?>
                                           
                                           <?php if($quote_details->OptionalCostAmt25>0) { ?><tr><td><?php echo $quote_details->OptionalCostDesc25; ?></td><td width="30%"><?php echo $site_setting->currency_symbol;?><?php echo $quote_details->OptionalCostAmt25; ?></td></tr> <?php } ?>
                                           
                                       
                                       </table>
                                       
                                       
                                    
                                   </div>    
                                   <?php }} ?>
                                  
                                   
                                     <?php if($quote_details->confirm_opt3==1){ 
                                       
                                        if($quote_details->OptionalCostAmt31>0 || $quote_details->OptionalCostAmt32>0 || $quote_details->OptionalCostAmt33>0 || $quote_details->OptionalCostAmt34>0 || $quote_details->OptionalCostAmt35>0) {
                                       
                                       ?>
                                   <div style="background: #DFF6FF;">
                                       
                                       <div align="center" class="padB20 padT20 opttitle" style="font-weight:bold;">Optional 3 - Services </div>
                                       
                                       <table class="bordertable"  border="1" cellpadding="5" cellspacing="1" width="100%">
                                    
                                           <?php if($quote_details->OptionalCostAmt31>0) { ?><tr><td><?php echo $quote_details->OptionalCostDesc31; ?></td><td width="30%"><?php echo $site_setting->currency_symbol;?><?php echo $quote_details->OptionalCostAmt31; ?></td></tr> <?php } ?>
                                           
                                           
                                           <?php if($quote_details->OptionalCostAmt32>0) { ?><tr><td><?php echo $quote_details->OptionalCostDesc32; ?></td><td width="30%"><?php echo $site_setting->currency_symbol;?><?php echo $quote_details->OptionalCostAmt32; ?></td></tr> <?php } ?>
                                           
                                           <?php if($quote_details->OptionalCostAmt33>0) { ?><tr><td><?php echo $quote_details->OptionalCostDesc33; ?></td><td width="30%"><?php echo $site_setting->currency_symbol;?><?php echo $quote_details->OptionalCostAmt33; ?></td></tr> <?php } ?>
                                           
                                           <?php if($quote_details->OptionalCostAmt34>0) { ?><tr><td><?php echo $quote_details->OptionalCostDesc34; ?></td><td width="30%"><?php echo $site_setting->currency_symbol;?><?php echo $quote_details->OptionalCostAmt34; ?></td></tr> <?php } ?>
                                           
                                           <?php if($quote_details->OptionalCostAmt35>0) { ?><tr><td><?php echo $quote_details->OptionalCostDesc35; ?></td><td width="30%"><?php echo $site_setting->currency_symbol;?><?php echo $quote_details->OptionalCostAmt35; ?></td></tr> <?php } ?>
                                           
                                       
                                       </table>
                                       
                                       
                                    
                                   </div>    
                                   <?php }} ?>
                                  
                                 
                                   
                                   
                                     <?php if($quote_details->confirm_opt4==1){ 
                                       
                                        if($quote_details->OptionalCostAmt41>0 || $quote_details->OptionalCostAmt42>0 || $quote_details->OptionalCostAmt43>0 || $quote_details->OptionalCostAmt44>0 || $quote_details->OptionalCostAmt45>0) {
                                       
                                       ?>
                                   <div style="background: #FCDFDF;">
                                       
                                       <div align="center" class="padB20 padT20 opttitle" style="font-weight:bold;">Optional 4 - Services </div>
                                       
                                       <table class="bordertable"  border="1" cellpadding="5" cellspacing="1" width="100%">
                                    
                                           <?php if($quote_details->OptionalCostAmt41>0) { ?><tr><td><?php echo $quote_details->OptionalCostDesc41; ?></td><td width="30%"><?php echo $site_setting->currency_symbol;?><?php echo $quote_details->OptionalCostAmt41; ?></td></tr> <?php } ?>
                                           
                                           
                                           <?php if($quote_details->OptionalCostAmt42>0) { ?><tr><td><?php echo $quote_details->OptionalCostDesc42; ?></td><td width="30%"><?php echo $site_setting->currency_symbol;?><?php echo $quote_details->OptionalCostAmt42; ?></td></tr> <?php } ?>
                                           
                                           <?php if($quote_details->OptionalCostAmt43>0) { ?><tr><td><?php echo $quote_details->OptionalCostDesc43; ?></td><td width="30%"><?php echo $site_setting->currency_symbol;?><?php echo $quote_details->OptionalCostAmt43; ?></td></tr> <?php } ?>
                                           
                                           <?php if($quote_details->OptionalCostAmt44>0) { ?><tr><td><?php echo $quote_details->OptionalCostDesc44; ?></td><td width="30%"><?php echo $site_setting->currency_symbol;?><?php echo $quote_details->OptionalCostAmt44; ?></td></tr> <?php } ?>
                                           
                                           <?php if($quote_details->OptionalCostAmt45>0) { ?><tr><td><?php echo $quote_details->OptionalCostDesc45; ?></td><td width="30%"><?php echo $site_setting->currency_symbol;?><?php echo $quote_details->OptionalCostAmt45; ?></td></tr> <?php } ?>
                                           
                                       
                                       </table>
                                       
                                       
                                    
                                   </div>    
                                   <?php }} ?>
                                  
                                 
                                   
                                   
                                   
                                   
                              </div> <?php } ?>
                                    

                                    <!--end optonal serrvices-->
                                    
                                    
                                    
                                    <!---other service-->
                                    
                                    <div class="controls controls-row marAll padT10 borT">   
                                        <select class="span6" name="HourlyChargeDesc" id="HourlyChargeDesc">
                                   <option value="">Hourly Charge Type</option>
                                   <?php if(!empty($other_task)) { 
								   	foreach($other_task as $key=>$task) {?>
                                   	<option value="<?php echo $task;?>"><?php echo ucfirst($task);?></option>
                                    <?php } } ?>
                                   </select>
                                        
                                        
                                        <?php $defualt_charge=60; ?>
                                        
                                        <select class="span4" name="HourlyChargeAmt" id="HourlyChargeAmt">
                                   <option value="0">0</option>
                                    <?php for($i=40;$i<=65;$i++){?>
                                   <option value="<?php echo $i;?>" <?php if($i==$defualt_charge) { ?> selected <?php } ?>><?php echo $site_setting->currency_symbol.$i;?></option>
                                    <?php } ?>
                                   </select>
                                   <label class="span2">Per Hour</label>
                                    </div>
                                    
                                    
                                    <div class="controls controls-row marAll padT10">          
                                        <label class="span6">Budget T & M :</label>
                                        <input type="text" value="<?php echo $quote_details->budget_time_material; ?>" name="budget_time_material" id="budget_time_material" placeholder="0" class="span4" readonly >
                                        <label class="span2">Hours</label>
                                    </div>
                                    
                                    
                                    
                                     <?php $calculate_commit_total= $total_customer_labour_costs + $total_customer_material + $quote_details->AdditionalCostAmt1 + $quote_details->AdditionalCostAmt2 + $quote_details->AdditionalCostAmt3 + $quote_details->AdditionalCostAmt4 + $quote_details->AdditionalCostAmt5; ?>
                               
                                    
                                    <div class="controls controls-row marAll padT10">          
                                        <label class="span6">Computed Total(<?php echo $site_setting->currency_symbol;?>) :</label>
                                        <input type="text" value="<?php echo $calculate_commit_total; ?>" name="CalculatedTotal" id="CalculatedTotal" placeholder="0" class="span6" readonly="readonly">
                                    </div>
                                    
                                    
                                     <?php
                                 
                                
                                $total_customer_adjusted_total=0;
                                
                                $total_customer_sell_amount=0;
                                
                                if($quote_details->AdjustedTotal>0){
                                    $total_customer_adjusted_total=$quote_details->AdjustedTotal;
                                } else {
                                    $total_customer_adjusted_total=$calculate_commit_total;
                                }
                                
                                if($quote_details->SellAmount>0){
                                    $total_customer_sell_amount=$quote_details->SellAmount;
                                } else {
                                    $total_customer_sell_amount=$calculate_commit_total;
                                }
                                
                                
                                if($quote_details->confirm_opt1==1){
                                    
                                    $labour_cost_opt1=$total_optional_hours_1 * $jobrate;
                                    
                                    $CalculatedOptTotal1 =  $labour_cost_opt1 + $total_optional_material_1 + $quote_details->OptionalCostAmt11 + $quote_details->OptionalCostAmt12 + $quote_details->OptionalCostAmt13 + $quote_details->OptionalCostAmt14 + $quote_details->OptionalCostAmt15;
                                    
                                    if($quote_details->AdjustedOptTotal1>0){
                                        $total_customer_adjusted_total= $total_customer_adjusted_total + $quote_details->AdjustedOptTotal1;
                                    } else {
                                        $total_customer_adjusted_total= $total_customer_adjusted_total + $CalculatedOptTotal1;
                                    }
                                    
                                    
                                    if($quote_details->SellOptAmount1>0){
                                        $total_customer_sell_amount = $total_customer_sell_amount + $quote_details->SellOptAmount1;
                                    } else {
                                        $total_customer_sell_amount = $total_customer_sell_amount + $CalculatedOptTotal1;
                                    }

                                    
                                    
                                    
                                    
                                }
                                if($quote_details->confirm_opt2==1){
                                    
                                    
                                    $labour_cost_opt2=$total_optional_hours_2 * $jobrate;
                                    
                                    $CalculatedOptTotal2 =  $labour_cost_opt2 + $total_optional_material_2 + $quote_details->OptionalCostAmt21 + $quote_details->OptionalCostAmt22 + $quote_details->OptionalCostAmt23 + $quote_details->OptionalCostAmt24 + $quote_details->OptionalCostAmt25;
                                    
                                    if($quote_details->AdjustedOptTotal2>0){
                                        $total_customer_adjusted_total= $total_customer_adjusted_total + $quote_details->AdjustedOptTotal2;
                                    } else {
                                        $total_customer_adjusted_total= $total_customer_adjusted_total + $CalculatedOptTotal2;
                                    }
                                    
                                    
                                    if($quote_details->SellOptAmount2>0){
                                        $total_customer_sell_amount = $total_customer_sell_amount + $quote_details->SellOptAmount2;
                                    } else {
                                        $total_customer_sell_amount = $total_customer_sell_amount + $CalculatedOptTotal2;
                                    }

                                    
                                    
                                    
                                }
                                if($quote_details->confirm_opt3==1){
                                    
                                    $labour_cost_opt3 = $total_optional_hours_3 * $jobrate;
                                    
                                    $CalculatedOptTotal3 =  $labour_cost_opt3 + $total_optional_material_3 + $quote_details->OptionalCostAmt31 + $quote_details->OptionalCostAmt32 + $quote_details->OptionalCostAmt33 + $quote_details->OptionalCostAmt34 + $quote_details->OptionalCostAmt35;
                                    
                                    if($quote_details->AdjustedOptTotal3>0){
                                        $total_customer_adjusted_total= $total_customer_adjusted_total + $quote_details->AdjustedOptTotal3;
                                    } else {
                                        $total_customer_adjusted_total= $total_customer_adjusted_total + $CalculatedOptTotal3;
                                    }
                                    
                                    
                                    if($quote_details->SellOptAmount3>0){
                                        $total_customer_sell_amount = $total_customer_sell_amount + $quote_details->SellOptAmount3;
                                    } else {
                                        $total_customer_sell_amount = $total_customer_sell_amount + $CalculatedOptTotal3;
                                    }

                                    
                                    
                                    
                                    
                                }
                                if($quote_details->confirm_opt4==1){
                                    
                                    $labour_cost_opt4=$total_optional_hours_4 * $jobrate;
                                    
                                    $CalculatedOptTotal4 =  $labour_cost_opt4 + $total_optional_material_4 + $quote_details->OptionalCostAmt41 + $quote_details->OptionalCostAmt42 + $quote_details->OptionalCostAmt43 + $quote_details->OptionalCostAmt44 + $quote_details->OptionalCostAmt45;
                                    
                                    if($quote_details->AdjustedOptTotal4>0){
                                        $total_customer_adjusted_total= $total_customer_adjusted_total + $quote_details->AdjustedOptTotal4;
                                    } else {
                                        $total_customer_adjusted_total= $total_customer_adjusted_total + $CalculatedOptTotal4;
                                    }
                                    
                                    
                                    if($quote_details->SellOptAmount4>0){
                                        $total_customer_sell_amount = $total_customer_sell_amount + $quote_details->SellOptAmount4;
                                    } else {
                                        $total_customer_sell_amount = $total_customer_sell_amount + $CalculatedOptTotal4;
                                    }

                                    
                                    
                                    
                                    
                                    
                                    
                                }
                                
                                
                                
                                ?>
                                    
                                     <div class="controls controls-row marAll padT10">          
                                        <label class="span6">Sell Amount(<?php echo $site_setting->currency_symbol;?>) :</label>
                                        <input type="text" value="<?php echo $total_customer_adjusted_total; ?>" name="total_customer_adjusted_total" id="total_customer_adjusted_total" placeholder="0" class="span6" readonly="readonly">
                                    </div>
                                    
                                    <div class="controls controls-row marAll padT10 borB">          
                                        <label class="span6">Discount on Labour :</label>
                                       
                                        
                                         <select class="span6" name="DiscountRate" id="DiscountRate" disabled="disabled">
<?php if (!empty($all_discount)) {
    foreach ($all_discount as $key => $disc) { ?>
                                        <option value="<?php echo $disc; ?>" <?php if($quote_details->DiscountRate==$disc){ ?> selected <?php } ?>><?php echo $disc . '%'; ?></option>
    <?php }
} ?>
                                    </select>
                                        
                                        
                                    </div>
                                    
                                    
                                    <?php 
                                    $discount_cost=0;
                                    
                                    if($quote_details->DiscountRate>0) { 
                                        
                                        $discount_cost=($total_customer_labour_costs*$quote_details->DiscountRate)/100;
                                        
                                    } 
                                    
                                    $total_customer_sell_amount=$total_customer_adjusted_total-$discount_cost;
                                    
                                    $total_customer_sell_amount = round($total_customer_sell_amount,2);
                                    
                                    ?>
                                    
                                    
                                    
                                    <!----compute part--->
                                    
                                    <div class="controls controls-row marAll padT10 borT">          
                                        <label class="span6" style="font-weight:bold; font-size:14px;">Final Quote(<?php echo $site_setting->currency_symbol;?>) :</label>
                                        <input type="text" value="<?php echo $total_customer_sell_amount; ?>" name="total_customer_sell_amount" id="total_customer_sell_amount" placeholder="0" class="span6" readonly="readonly">  
                                    </div>
                                    
                                    
                                    <?php
                                    
                                      $minsum= $total_customer_material + $quote_details->AdditionalCostAmt1 + $quote_details->AdditionalCostAmt2 + $quote_details->AdditionalCostAmt3 + $quote_details->AdditionalCostAmt4 + $quote_details->AdditionalCostAmt5;
                                      
                                      
            
            
            $total_customer_ldh=($total_customer_sell_amount-$minsum)/ $total_customer_hours;
            
            $total_customer_ldh=  round($total_customer_ldh,2);
          
                                    ?>
                                    
                                     <div class="controls controls-row marAll padT10">          
                                        <label class="span6" style="font-weight:bold; font-size:14px;">LDH(<?php echo $site_setting->currency_symbol;?>) :</label>
                                        <input type="text" value="<?php echo $total_customer_ldh; ?>" name="LDH" id="LDH" placeholder="0" class="span6" readonly="readonly">
                                    </div>
                                    
                                     <!----compute part--->
                                     
                                     
                                     
                                    
                                    </div>
                                    <!---commited features--->
                                    
                                    
                                    <!---optional features--->
                                    <div class="span6">
                                    
                                    <div align="center" class="padB20" style="font-weight:bold;">Actual Costs</div>
                                     
                                  
                                    <?php 
                                    
                                    $actual_job_hours=quote_actual_job_hours($quote_details->quote_id); 
                                    $actual_job_hours=conver_time_to_decimal($actual_job_hours);
                                    
                                    
                                    $actual_labour_costs=quote_actual_labour_costs($quote_details->quote_id); 
                                    
                                    
                                    
                                    $chargetype_id=2;
                                    
                                    $extra_chargeable_hours=quote_actual_job_hours($quote_details->quote_id,$chargetype_id); 
                                    $extra_chargeable_hours=conver_time_to_decimal($extra_chargeable_hours);
                                    
                                   // echo $total_customer_hours."+".$extra_chargeable_hours."-".$actual_job_hours; die;
                                    
                                    $remaining_hours_on_job= ($total_customer_hours+$extra_chargeable_hours) - $actual_job_hours;
                                        
                                    
                                    
                                    
                                    ?>
                                    
                                    
                                    <!---upper top--->
                                    <div class="controls controls-row marAll2 padT10 borT">   
                                        <label class="span6">Actual Job Hours :</label>
                                       <input type="text" value="<?php echo $actual_job_hours; ?>" name="actual_job_hours" id="actual_job_hours" placeholder="0" class="span6" readonly="readonly">
                                    </div>
                                   <div class="controls controls-row marAll2 padT10">          
                                        <label class="span6">Hours Remaining on Job :</label>
                                        <input type="text" value="<?php echo $remaining_hours_on_job; ?>" name="remaining_hours_on_job" id="remaining_hours_on_job" placeholder="0" class="span6" readonly="readonly">
                                    </div>
                                    
                                    <div class="controls controls-row marAll2 padT10">          
                                        <label class="span6">Labour Costs(<?php echo $site_setting->currency_symbol;?>) :</label>
                                        <input type="text" value="<?php echo $actual_labour_costs; ?>" name="actual_labour_costs" id="actual_labour_costs" placeholder="0" class="span6" readonly="readonly">
                                    </div>
                                    
                                    <div class="controls controls-row marAll2 padT10">          
                                        <label class="span6">Actual Material Cost (w.PST)(<?php echo $site_setting->currency_symbol;?>) :</label>
                                        <input type="text" value="<?php echo $quote_details->FinalMaterialCostwPST; ?>" name="FinalMaterialCostwPST" id="FinalMaterialCostwPST" placeholder="0" class="span6">
                                    </div>
                                     <!---upper top--->
                                    
                                    
                                    <!---other service-->
                                    
                                    
                                    <div class="controls controls-row marAll2 padT10 borT">   
                                   <select class="span6" name="AdditionalCostDesc1" id="AdditionalCostDesc1">
                                   <option value="">Select Service 1</option>
                                   <?php if(!empty($other_service)) { 
								   	foreach($other_service as $key=>$service) {?>
                                   	<option value="<?php echo $key;?>" <?php if ($quote_details->AdditionalCostDesc1 == $key) {
                                            echo "selected";
                                        } ?>><?php echo ucfirst($service);?></option>
                                    <?php } } ?>
                                   </select>
                                      
                                        
                                         
                                           <div class="span6 input-append input-prepend">
                                               <span class="add-on"><?php echo $site_setting->currency_symbol;?></span>
                                               <input class="input-small" id="ActualCostAmt1" name="ActualCostAmt1" value="<?php echo $quote_details->ActualCostAmt1; ?>" type="text">
                                            <div class="btn-group">
                                                    <button class="button button-basic dropdown-toggle" data-toggle="dropdown">
                                                            Select
                                                            <span class="caret"></span>
                                                    </button>
                                                <ul class="dropdown-menu cstdropmenu">
                                                        
                                                         <?php if(!empty($other_service_pay)) { 
								   	foreach($other_service_pay as $key=>$pay) {?>
                                                    <li><a href="javascript:void(0)" data-field="ActualCostAmt1" data-val="<?php echo $pay; ?>"><?php echo $pay; ?></a></li>
                                                        
                                                     
                                                            <?php } } ?>
                                                        
                                                    </ul>
                                            </div>
                                    </div>
                                        
                                        
                                        
                                        
                                        
                                        
                                        
                                        
                                        
                                        
                                        
                                        
                                    </div>
                                    
                                    <div class="controls controls-row marAll2 padT10">   
                                   <select class="span6" name="AdditionalCostDesc2" id="AdditionalCostDesc2">
                                   <option value="">Select Service 2</option>
                                   <?php if(!empty($other_service)) { 
								   	foreach($other_service as $key=>$service) {?>
                                   	<option value="<?php echo $key;?>" <?php if ($quote_details->AdditionalCostDesc2 == $key) {
                                            echo "selected";
                                        } ?>><?php echo ucfirst($service);?></option>
                                    <?php } } ?>
                                   </select>
                                        
                                        
                                        
                                        
                                        
                                          
                                         
                                           <div class="span6 input-append input-prepend">
                                               <span class="add-on"><?php echo $site_setting->currency_symbol;?></span>
                                            <input class="input-small" id="ActualCostAmt2" name="ActualCostAmt2" value="<?php echo $quote_details->ActualCostAmt2; ?>" type="text">
                                            <div class="btn-group">
                                                    <button class="button button-basic dropdown-toggle" data-toggle="dropdown">
                                                            Select
                                                            <span class="caret"></span>
                                                    </button>
                                                <ul class="dropdown-menu cstdropmenu">
                                                        
                                                         <?php if(!empty($other_service_pay)) { 
								   	foreach($other_service_pay as $key=>$pay) {?>
                                                    <li><a href="javascript:void(0)" data-field="ActualCostAmt2" data-val="<?php echo $pay; ?>"><?php echo $pay; ?></a></li>
                                                        
                                                     
                                                            <?php } } ?>
                                                        
                                                    </ul>
                                            </div>
                                    </div>
                                        
                                        
                                        
                                        
                                        
                                        
                                        
                                    </div>
                                    
                                    <div class="controls controls-row marAll2 padT10">   
                                   <select class="span6" name="AdditionalCostDesc3" id="AdditionalCostDesc3">
                                   <option value="">Select Service 3</option>
                                   <?php if(!empty($other_service)) { 
								   	foreach($other_service as $key=>$service) {?>
                                   	<option value="<?php echo $key;?>" <?php if ($quote_details->AdditionalCostDesc3 == $key) {
                                            echo "selected";
                                        } ?>><?php echo ucfirst($service);?></option>
                                    <?php } } ?>
                                   </select>
                                      
                                        
                                        
                                        
                                         
                                           <div class="span6 input-append input-prepend">
                                               <span class="add-on"><?php echo $site_setting->currency_symbol;?></span>
                                            <input class="input-small" id="ActualCostAmt3" name="ActualCostAmt3" value="<?php echo $quote_details->ActualCostAmt3; ?>" type="text">
                                            <div class="btn-group">
                                                    <button class="button button-basic dropdown-toggle" data-toggle="dropdown">
                                                            Select
                                                            <span class="caret"></span>
                                                    </button>
                                                <ul class="dropdown-menu cstdropmenu">
                                                        
                                                         <?php if(!empty($other_service_pay)) { 
								   	foreach($other_service_pay as $key=>$pay) {?>
                                                    <li><a href="javascript:void(0)" data-field="ActualCostAmt3" data-val="<?php echo $pay; ?>"><?php echo $pay; ?></a></li>
                                                        
                                                     
                                                            <?php } } ?>
                                                        
                                                    </ul>
                                            </div>
                                    </div>
                                        
                                        
                                        
                                        
                                        
                                        
                                        
                                        
                                        
                                        
                                    </div>
                                    
                                    <div class="controls controls-row marAll2 padT10">   
                                   <select class="span6" name="AdditionalCostDesc4" id="AdditionalCostDesc4">
                                   <option value="">Select Service 4</option>
                                   <?php if(!empty($other_service)) { 
								   	foreach($other_service as $key=>$service) {?>
                                   	<option value="<?php echo $key;?>" <?php if ($quote_details->AdditionalCostDesc4 == $key) {
                                            echo "selected";
                                        } ?>><?php echo ucfirst($service);?></option>
                                    <?php } } ?>
                                   </select>
                                       
                                        
                                        
                                        
                                           <div class="span6 input-append input-prepend">
                                               <span class="add-on"><?php echo $site_setting->currency_symbol;?></span>
                                            <input class="input-small" id="ActualCostAmt4" name="ActualCostAmt4" value="<?php echo $quote_details->ActualCostAmt4; ?>" type="text">
                                            <div class="btn-group">
                                                    <button class="button button-basic dropdown-toggle" data-toggle="dropdown">
                                                            Select
                                                            <span class="caret"></span>
                                                    </button>
                                                <ul class="dropdown-menu cstdropmenu">
                                                        
                                                         <?php if(!empty($other_service_pay)) { 
								   	foreach($other_service_pay as $key=>$pay) {?>
                                                    <li><a href="javascript:void(0)" data-field="ActualCostAmt4" data-val="<?php echo $pay; ?>"><?php echo $pay; ?></a></li>
                                                        
                                                     
                                                            <?php } } ?>
                                                        
                                                    </ul>
                                            </div>
                                    </div>
                                        
                                        
                                        
                                    </div>
                                    
                                     <div class="controls controls-row marAll2 padT10 padB20 borB">   
                                         
                                      
                                   <select class="span6" name="AdditionalCostDesc5" id="AdditionalCostDesc5">
                                   <option value="">Select Service 5</option>
                                   <?php if(!empty($other_service)) { 
								   	foreach($other_service as $key=>$service) {?>
                                   	<option value="<?php echo $key;?>" <?php if ($quote_details->AdditionalCostDesc5 == $key) {
                                            echo "selected";
                                        } ?>><?php echo ucfirst($service);?></option>
                                    <?php } } ?>
                                   </select>
                                         
                                         
                                           <div class="span6 input-append input-prepend">
                                               <span class="add-on"><?php echo $site_setting->currency_symbol;?></span>
                                            <input class="input-small" id="ActualCostAmt5" name="ActualCostAmt5" value="<?php echo $quote_details->ActualCostAmt5; ?>" type="text">
                                            <div class="btn-group">
                                                    <button class="button button-basic dropdown-toggle" data-toggle="dropdown">
                                                            Select
                                                            <span class="caret"></span>
                                                    </button>
                                                <ul class="dropdown-menu cstdropmenu">
                                                        
                                                         <?php if(!empty($other_service_pay)) { 
								   	foreach($other_service_pay as $key=>$pay) {?>
                                                    <li><a href="javascript:void(0)" data-field="ActualCostAmt5" data-val="<?php echo $pay; ?>"><?php echo $pay; ?></a></li>
                                                        
                                                     
                                                            <?php } } ?>
                                                        
                                                    </ul>
                                            </div>
                                    </div>
                                         
                                     
                                       
                                    </div>
                                    
                                     <!---other service-->
                                    
                                    <div class="controls controls-row marAll2 padT10 borT">   
                                   <select class="span6" name="HourlyChargeDesc" id="HourlyChargeDesc">
                                   <option value="">Time & Material Rate</option>
                                   <?php if(!empty($other_task)) { 
								   	foreach($other_task as $key=>$task) {?>
                                   	<option value="<?php echo $task;?>"><?php echo ucfirst($task);?></option>
                                    <?php } } ?>
                                   </select>
                                   
                                        
                                        <?php $defualt_charge=60;
                                        
                                        if($quote_details->AdditionalHourlyChargeAmt>0){
                                            $defualt_charge=$quote_details->AdditionalHourlyChargeAmt;
                                        }
                                        
                                        ?>
                                        
                                        <select class="span4" name="AdditionalHourlyChargeAmt" id="AdditionalHourlyChargeAmt">
                                   
                                    <?php for($i=40;$i<=65;$i++){?>
                                   	<option value="<?php echo $i;?>" <?php if($i==$defualt_charge) { ?> selected <?php } ?>><?php echo $site_setting->currency_symbol.$i;?></option>
                                    <?php } ?>
                                   </select>
                                   <label class="span2">Per Hour</label>
                                    </div>
                                    
                                    
                                   
                                     
                                     <?php 
                                     
                                     $extra_labour_charge=$extra_chargeable_hours*$defualt_charge;
                                     
                                     $extra_work_materials=$quote_details->ExtraWorkMaterials;
                                     
                                     $final_job_cost=$actual_labour_costs+$quote_details->FinalMaterialCostwPST+$quote_details->ActualCostAmt1+$quote_details->ActualCostAmt2+$quote_details->ActualCostAmt3+$quote_details->ActualCostAmt4+$quote_details->ActualCostAmt5+$extra_labour_charge+$extra_work_materials;
                                     
                                     ?> 
                                    
                                    
                                    <div class="controls controls-row marAll2 padT10">          
                                        <label class="span6">Final Job Cost(<?php echo $site_setting->currency_symbol;?>) :</label>
                                        <input type="text" value="<?php echo $final_job_cost; ?>" name="FinalCost" id="FinalCost" placeholder="0" class="span6" readonly="readonly">
                                    </div>
                                    
                                    
                                     <div class="controls controls-row marAll2 padT10">          
                                        <label class="span6">Extra Chargeable Hours :</label>
                                        <input type="text" value="<?php echo $extra_chargeable_hours; ?>" name="extra_chargeable_hours" id="extra_chargeable_hours" placeholder="0" class="span6" readonly="readonly">
                                    </div>
                                     
                                     
                                     
                                    
                                    
                                    <div class="controls controls-row marAll2 padT10">          
                                        <label class="span6">Extra Labour Charge(<?php echo $site_setting->currency_symbol;?>) :</label>
                                        <input type="text" value="<?php echo $extra_labour_charge; ?>" name="extra_labour_charge" id="extra_labour_charge" placeholder="0" class="span6" readonly="readonly">
                                    </div>
                                    
                                     
                                    
                                    <div class="controls controls-row marAll2 padT10">          
                                        <label class="span6">Extra Material Charge(wPST)(<?php echo $site_setting->currency_symbol;?>) :</label>
                                        <input type="text" value="<?php echo $extra_work_materials; ?>" name="ExtraWorkMaterials" id="ExtraWorkMaterials" placeholder="0" class="span6">
                                    </div>
                                    
                                    
                                    <div class="controls controls-row marAll2 padT10">          
                                        <label class="span6">Final Customer Invoice(<?php echo $site_setting->currency_symbol;?>) :</label>
                                        <input type="text" value="0" name="final_customer_invoice" id="final_customer_invoice" placeholder="0" class="span6" readonly="readonly">
                                    </div>
                                    
                                    <?php $total_customer_payment_received=$quote_details->TotalCustPayment; ?>
                                    <div class="controls controls-row marAll2 padT10">          
                                        <label class="span6">Total Payment Received(<?php echo $site_setting->currency_symbol;?>) :</label>
                                        <input type="text" value="<?php echo $total_customer_payment_received; ?>" name="TotalCustPayment" id="TotalCustPayment" placeholder="0" class="span6">
                                    </div>
                                    
                                   
                                    
                                    
                                    <!----compute part--->
                                    <?php $final_profit=$quote_details->FinalProfit; 
                                    
                                    $final_profit_perc=0;
                                    
                                    $perc_1=$total_customer_payment_received-$extra_work_materials-($actual_labour_costs+$extra_labour_charge)-$quote_details->FinalMaterialCostwPST;
                                    if($total_customer_payment_received>0){
                                        $final_profit_perc=$perc_1/$total_customer_payment_received;
                                        
                                        $final_profit_perc=round($final_profit_perc,2);
                                    }
                                    
                                    
                                    
                                    $final_ldh=$quote_details->FinalLDH;
                                    
                                    
                                    
                                    
                                    ?>
                                    
                                    <div class="controls controls-row marAll2 padT10 borT">          
                                        <label class="span6" style="font-weight:bold; font-size:14px;"><span id="profitperc">%<?php echo $final_profit_perc; ?></span> Profit(<?php echo $site_setting->currency_symbol;?>) :</label>
                                        <input type="text" value="<?php echo $final_profit; ?>" name="FinalProfit" id="FinalProfit" placeholder="0" class="span6" readonly="readonly">
                                    </div>
                                    
                                    
                                     <div class="controls controls-row marAll2 padT10">          
                                        <label class="span6" style="font-weight:bold; font-size:14px;">LDH(<?php echo $site_setting->currency_symbol;?>) :</label>
                                        <input type="text" value="<?php echo $final_ldh; ?>" name="FinalLDH" id="FinalLDH" placeholder="0" class="span6" readonly="readonly">
                                    </div>
                                    
                                     <!----compute part--->
                                     
                                     
                                    
                                    
                                    </div>
                                    <!---optional features--->
                                    
                                   
                                        </div>
										
                                    
                                        <br /><br /><br />

                                   
                           
                                 
                                        
                               
                                        
                                        
                                        
                                        
                                        
									</div><!--step 1 div-->
                                    
									
                                    
                                    
									<div class="form-actions">
										
                            <button type="submit" class="button button-basic-blue savebtnquote">Save</button>

<button type="button" class="button button-basic" onClick="window.location.href='<?php echo site_url('quotes/managetype');?>'">Cancel</button>

                <input type="hidden" name="quote_id" id="quote_id" value="<?php echo $quote_id; ?>" />
											
										
									</div>
                                   
                                   
								</form>
							</div>
						</div>
					</div>
				</div>
				
				
				
			</div>
        
        
        <style>
            .cstdropmenu{
                min-width: 10px;
                max-height: 150px;
                overflow-y: scroll;
            }
            .cstdropmenu li{
                margin: 1px;
            }
            .cstdropmenu li a{
                padding: 1px 9px;
                line-height: 17px;
                border-bottom: 1px solid #F2F2F2;
                border-radius: 0px;
            }
        </style>
        
        
<script type="text/javascript">
    $(document).ready(function(){
        
        	
        
        $(".cstdropmenu li a").on("click",function(){
            
            if($(this).parent().hasClass('disabled')==false){
            
                var dval=$(this).attr('data-val');
                var dtarget=$(this).attr('data-field');
                $("#"+dtarget).val(dval);
                
                calculate_final();
            }
        });
        
        
        
         $("#FinalMaterialCostwPST,#ActualCostAmt1,#ActualCostAmt2,#ActualCostAmt3,#ActualCostAmt4,#ActualCostAmt5,#AdditionalHourlyChargeAmt,#ExtraWorkMaterials,#TotalCustPayment").live("change",function(){
           
            calculate_final();
        });
        
        function calculate_final(){
            
            var actual_job_hours=parseFloat($("#actual_job_hours").val());
            
            var extra_chargeable_hours=parseFloat($("#extra_chargeable_hours").val());
            
            var defualt_charge=parseFloat($("#AdditionalHourlyChargeAmt option:selected").val());
            
            
            var extra_labour_charge=extra_chargeable_hours*defualt_charge;
             $("#extra_labour_charge").val(extra_labour_charge.toFixed(2));                        
                                     
           
           var actual_labour_costs=parseFloat($("#actual_labour_costs").val());
           var FinalMaterialCostwPST=parseFloat($("#FinalMaterialCostwPST").val());
           
           var extra_work_materials=parseFloat($("#ExtraWorkMaterials").val());
           
         var ActualCostAmt1=parseFloat($("#ActualCostAmt1").val());
         var ActualCostAmt2=parseFloat($("#ActualCostAmt2").val());
         var ActualCostAmt3=parseFloat($("#ActualCostAmt3").val());
         var ActualCostAmt4=parseFloat($("#ActualCostAmt4").val());
         var ActualCostAmt5=parseFloat($("#ActualCostAmt5").val());
         
         
           
           var final_job_cost=actual_labour_costs+FinalMaterialCostwPST+ActualCostAmt1+ActualCostAmt2+ActualCostAmt3+ActualCostAmt4+ActualCostAmt5+extra_labour_charge+extra_work_materials;
                                     
                                     
               
            $("#FinalCost").val(final_job_cost.toFixed(2));
            
            
            var total_customer_payment_received=parseFloat($("#TotalCustPayment").val());
            
            var final_profit_perc=0;
                                    
            var perc_1= total_customer_payment_received-extra_work_materials-(actual_labour_costs+extra_labour_charge)-FinalMaterialCostwPST;
          
            if(total_customer_payment_received>0){
                final_profit_perc=perc_1/total_customer_payment_received;
            }

            $("#profitperc").html("%"+final_profit_perc.toFixed(2));
            
            
            
            
            
            
            var ldh_1= total_customer_payment_received - (FinalMaterialCostwPST+ActualCostAmt1+ActualCostAmt2+ActualCostAmt3+ActualCostAmt4+ActualCostAmt5+extra_work_materials);
            
            var ldh_2=ldh_1/(actual_job_hours+extra_chargeable_hours);
                                    
                   $("#FinalLDH").val(ldh_2.toFixed(2));
            
            
            
        }
        
        
        
	
        
});
        
        
        

var clicked = false;

$(document).ready(function(){
//    $("a[href]").click(function(){
//        clicked = true;
//    });
//
//    $(document).bind('keypress keydown keyup', function(e) {
//        if(e.which === 116) {
//            clicked = true;
//        }
//        if(e.which === 82 && e.ctrlKey) {
//            clicked = true;
//        }
//    });

    $(".savebtnquote").click(function(){
        clicked = true;
    });
    
});

window.addEventListener("beforeunload", function (e) {
    if(!clicked) {
        
         var confirmationMessage = 'Please click on stay on page to save the data you have entered and then continue.';    

        (e || window.event).returnValue = confirmationMessage; //Gecko + IE
        return confirmationMessage; //Gecko + Webkit, Safari, Chrome etc.
    }
});
//window.onbeforeunload = function(e){
//    if(!clicked) {
//        
//          if (typeof event == 'undefined') {
//            event = window.event;
//          }
//          if (event) {
//            event.returnValue = 'You have unsaved stuff.';
//          }
//          return message;
//        
//        //return 'You have unsaved stuff.';
//    }
//};


    </script>
    <style>
    #firstStep{
        opacity:1 !important;
    }
</style>