<?php $site_setting=site_setting();
 ?>
 	<!-- datepicker plugin -->
	<link rel="stylesheet" href="<?php echo base_url();?>css/bootstrap-datetimepicker.min.css">

    
	<!-- datepicker plugin -->
	<script src="<?php echo base_url();?>js/bootstrap-datetimepicker.min.js"></script>

 

<div class="page-header">
				<div class="pull-left">
					<h4><i class="icon-cogs"></i> <?php if($quote_id!='') {?>Edit Quote<?php } else { ?>Add Quote<?php }  ?></h4>
				</div>
				<div class="pull-right">
					<ul class="bread">
						<li><a href="<?php echo site_url('quotes/manage');?>">Manage Quotes</a><span class="divider">/</span></li>
                        <li class="active">Quotes</li>
					</ul>
				</div>
			</div>
            
            
            <div class="container-fluid" id="content-area">
				
<div class="row-fluid">
                <div class="span12">
                
                 <?php if($error!=''){ ?>

		<div class="alert alert-error">
											<button type="button" class="close" data-dismiss="alert">&times;</button>
											<strong>Warning !</strong> <?php echo $error;?>
										</div>    <?php }?>
                                        
                       <?php if($msg!='') { ?>
<div class="alert <?php if($msg=='notfound' || $msg=='cannot') {?>alert-danger<?php } else { ?>alert-success<?php } ?>">
											<button type="button" class="close" data-dismiss="alert">&times;</button>
											<strong><?php if($msg=='notfound' || $msg=='cannot') {?>Warning<?php } else { ?>Success<?php } ?>!</strong>

	<?php if($msg=='delete') { ?>Quote Materials has been deleted successfully. <?php } ?>	
	<?php if($msg=='insert') { ?>New Quote Materials has been added successfully. <?php } ?>	
	<?php if($msg=='update') { ?>Quote Materials detail has been updated successfully. <?php } ?>	
    <?php if($msg=='notfound') { ?>Quote Materials records not found. <?php } ?>
    <?php if($msg=='cannot') { ?>Cannot delete Quote Materials because it used in many records. <?php } ?>		
  
</div> 
<?php } ?>	                
                        
    
					
						<div class="box">
							<div class="box-head">
								<i class="icon-list-ul"></i>
								<span>Change Quote details</span>
                                
                                 <?php
						   $data['quote_details']=$quote_details;						 
						   $this->load->view('quotes/quote_title',$data); 
						   ?>
							</div>
							<div class="box-body box-body-nopadding">
                             <?php
									$attributes = array('name'=>'frm_addquote','class'=>'form-horizontal form-bordered form-wizard');
									echo form_open('quotes/add_quote',$attributes);
								  ?> 
                                  
								
                                	<?php $this->load->view('quotes/quote_buttons',$data);  ?>
                                    
                                    <div class="step" id="firstStep">
										
                                        
                                          <!-----steps--->   
                                         <?php 
										   $data['step_index']=2;
										 $this->load->view('quotes/quote_steps',$data);  ?>
                                        <!-----steps--->
                                        
                                        
                                        
                                        
                                        
                                         
                                        
                                   <br /><br />

                                    <div class="row-fluid">
                                    
                                    <!---commited features--->
                                    <div class="span6">
                                    
                                    <div align="center" style="font-weight:bold;">Committed - Materials Required</div>
                                    
                                    
                                    <div class="controls controls-row borB marAll padT20">                                    
                                     	<div class="span3">Feature</div>
                                        <div class="span3">Sheen</div>
                                        <div class="span3">SqFt</div>
                                        <div class="span3">Gallons</div>
                                    </div>
                                    
                                    <div class="controls controls-row marAll padT10">   
                                    <input class="span3" value="door" type="text" readonly="readonly" />
                                    <input class="span3" value="EggShell" type="text" readonly="readonly" />
                                    <input class="span3" value="100" type="text" readonly="readonly" />
                                    <input class="span3" value="3.8" type="text" readonly="readonly" />
                                    </div>
                                    
                                    <div class="controls controls-row marAll padT10">   
                                    <input class="span3" value="door" type="text" readonly="readonly" />
                                    <input class="span3" value="EggShell" type="text" readonly="readonly" />
                                    <input class="span3" value="100" type="text" readonly="readonly" />
                                    <input class="span3" value="3.8" type="text" readonly="readonly" />
                                    </div>
                                    
                                    <div class="controls controls-row marAll padT10">   
                                    <input class="span3" value="door" type="text" readonly="readonly" />
                                    <input class="span3" value="EggShell" type="text" readonly="readonly" />
                                    <input class="span3" value="100" type="text" readonly="readonly" />
                                    <input class="span3" value="3.8" type="text" readonly="readonly" />
                                    </div>
                                    
                                    <div class="controls controls-row marAll padT10">   
                                    <input class="span3" value="door" type="text" readonly="readonly" />
                                    <input class="span3" value="EggShell" type="text" readonly="readonly" />
                                    <input class="span3" value="100" type="text" readonly="readonly" />
                                    <input class="span3" value="3.8" type="text" readonly="readonly" />
                                    </div>
                                    
                                    
                                    </div>
                                    <!---commited features--->
                                    
                                    
                                    <!---optional features--->
                                    <div class="span6">
                                    
                                    <div align="center" style="font-weight:bold;">Optional - Materials Optional</div>
                                     
                                     <div class="controls controls-row borB marAll2 padT20">                                    
                                     	<div class="span3">Feature</div>
                                        <div class="span3">Sheen</div>
                                        <div class="span3">SqFt</div>
                                        <div class="span3">Gallons</div>
                                    </div>
                                    
                                    <div class="controls controls-row marAll2 padT10">   
                                    <input class="span3" value="door" type="text" readonly="readonly" />
                                    <input class="span3" value="EggShell" type="text" readonly="readonly" />
                                    <input class="span3" value="100" type="text" readonly="readonly" />
                                    <input class="span3" value="3.8" type="text" readonly="readonly" />
                                    </div>
                                    
                                    <div class="controls controls-row marAll2 padT10">   
                                    <input class="span3" value="door" type="text" readonly="readonly" />
                                    <input class="span3" value="EggShell" type="text" readonly="readonly" />
                                    <input class="span3" value="100" type="text" readonly="readonly" />
                                    <input class="span3" value="3.8" type="text" readonly="readonly" />
                                    </div>
                                    
                                    
                                    </div>
                                    <!---optional features--->
                                    
                                   
                                        </div>
										
                                    
                                        <br /><br /><br />

                                   
                                 
                                 <div class="control-group borT">
                                 <label for="textfield" class="control-label">Order Materials</label>
                                 </div>
                                 
                                 <div class="form-actions">
										
                                    <button type="button" class="button button-basic-green addmaterial">Add Material</button>
                                   
                                    
                                </div>
                                        
                               <style>
							   .marAll{ margin:0px 0px 0px 8px !important; }
							   .marAll2{ margin:0px 8px 0px 0px !important; }
							   .marL0 { margin:0px !important; }
							   .ht0 { height: 0px; }
							   .ht15 { height: 15px; }
							   .ht40 { height: 40px; }
							   .ht65 { height: 65px; }
							   .padT10 { padding-top:10px; }
							   .padT20 { padding-top:20px; }
							   .pad10 { padding: 10px; }
							   .roommain { border: 2px solid #ADADAD; margin-bottom:2px; }
							   .borT { border-top: 1px solid #ccc; } 
							   .borB { border-bottom: 1px solid #ccc; }
							   #maindiv .controls { margin:0px !important; }
							   </style>         
                                        <!----------room part--------->
                                        
                                        
                                <div class="control-group">
                                 
                                 <?php $ordercnt=1; ?>
                                 
                                
                                 
                                	<!--rroom type-->
                                    <div id="maindiv" class="roommain">
                                
                                    
                                	<div class="controls controls-row borB">                                    
                                     	<div class="span3">Product</div>
                                        <div class="span1">Qty.</div>
                                        <div class="span1">Opt. Qty.</div>
                                        <div class="span2">Sheen</div>
                                        <div class="span2">Colour</div>
                                        <div class="span1">Cost(<?php echo $site_setting->currency_symbol;?>)</div>
                                        <div class="span1">Opt. Cost(<?php echo $site_setting->currency_symbol;?>)</div>
                                        <div class="span1">&nbsp;</div>
                                    </div>
                                    
                                    
                                	<!--order material 1--->
                                	<div id="order<?php echo $ordercnt; ?>" class="borT">
                             
                                    <div class="controls controls-row borB">  
                                    Material <?php echo $ordercnt; ?>
                                    </div>                
                                    
                                    
                                    
<div class="controls controls-row borB ht65">
<select name="productcost_id<?php echo $ordercnt; ?>" id="productcost_id<?php echo $ordercnt; ?>" class="span3"><option>Select Product</option></select>
<select name="qty<?php echo $ordercnt; ?>" id="qty<?php echo $ordercnt; ?>" class="span1"><option>1</option></select>
<select name="ncqty<?php echo $ordercnt; ?>" id="ncqty<?php echo $ordercnt; ?>" class="span1"><option>2</option></select>
<select name="sheen_id<?php echo $ordercnt; ?>" id="sheen_id<?php echo $ordercnt; ?>" class="span2"><option>N/A</option></select>
<input name="colour<?php echo $ordercnt; ?>" id="colour<?php echo $ordercnt; ?>" class="span2" value="black" type="text" />
<input name="productcostsum<?php echo $ordercnt; ?>" id="productcostsum<?php echo $ordercnt; ?>" class="span1" value="0.0" type="text" readonly="readonly" />
<input name="optionalcostsum<?php echo $ordercnt; ?>" id="optionalcostsum<?php echo $ordercnt; ?>" class="span1" value="0.0" type="text" />
<a href="javascript:void(0)" data-id="" data-cnt="<?php echo $ordercnt; ?>" class="span1 pad10 removeorder"><i class="icon-remove"></i></a>          <br />
<input type="text" name="notes<?php echo $ordercnt; ?>" id="notes<?php echo $ordercnt; ?>" placeholder="Order Notes" class="span12">
</div>
                                    
                                
                                    
                                    </div><!--order material 1--->
                             
                                    
                                	</div><!---room 1--><!--maindiv-->
                                
                               
                                
                                
                                </div> 
                                
                                
                                <input type="hidden" id="ordercnt" name="ordercnt" value="<?php echo $ordercnt; ?>" />
                                       
                                <!----------room part--------->
                                        
                                        
                                        
                                        
                                        
                                        
									</div><!--step 1 div-->
                                    
									
                                    
                                    
									<div class="form-actions">
										 <?php if($quote_id=='') { ?>
												<button type="button" class="button button-basic-blue" onClick="window.location.href='<?php echo site_url('quotes/step3/'.$quote_id);?>'">Save & Next</button>
											<?php } else { ?>
											<button type="button" class="button button-basic-blue" onClick="window.location.href='<?php echo site_url('quotes/step3/'.$quote_id);?>'">Save & Next</button>
											<?php } ?>
                                            <button type="button" class="button button-basic" onClick="window.location.href='<?php echo site_url('quotes/managetype');?>'">Cancel</button>
                                           
											<input type="hidden" name="quote_id" id="quote_id" value="<?php echo $quote_id; ?>" />
											<input type="hidden" name="offset" id="offset" value="<?php echo $offset; ?>" />
										
									</div>
                                   
                                   
								</form>
							</div>
						</div>
					</div>
				</div>
				
				
				
			</div>
            
            
            
<script type="text/javascript">
  $(function() {
   
		////===add material===
		$(".addmaterial").on("click",function(){
			
			var ordercnt = $("#ordercnt").val();	
			var newordercnt = parseInt(ordercnt) + parseInt(1);		
			var materialhtml = addmaterial(newordercnt);
			
			$("#maindiv").append(materialhtml);
			$("#ordercnt").val(newordercnt);	
			
			
			$("div#order"+newordercnt+" .removeorder").bind("click",function(){
				var ordercnt = $(this).attr('data-cnt');			
				var orderdbid = $(this).attr('data-id');						
				deleteordermaterial(ordercnt,orderdbid);			
			});
			
			
		});
		
		
		
		///===remove roome===
		$(".removeorder").on("click",function(){			
			var ordercnt = $(this).attr('data-cnt');			
			var orderdbid = $(this).attr('data-id');				
			deleteordermaterial(ordercnt,orderdbid);			
		});
		
		
		
		
  });
  
  function deleteordermaterial(ordercnt,orderdbid){
		if(orderdbid>0) {
		
		} 
		
		$("div#order"+ordercnt).remove();
  }
  
  
  function addmaterial(ordercnt){
  	
	var html="";
	
	html +='<div id="order'+ordercnt+'" class="borT">';
	
	html +='<div class="controls controls-row borB">Material '+ordercnt+'</div>';
	 
	html +='<div class="controls controls-row borB ht65">';
	
	
	html +='<select name="productcost_id'+ordercnt+'" id="productcost_id'+ordercnt+'" class="span3">';
	
	<?php if(!empty($all_product_cost)) { foreach($all_product_cost as $pcost) { ?>
	html +='<option value="<?php echo $pcost->productcost_id;?>"><?php echo ucfirst($pcost->description)."&nbsp;&nbsp;|&nbsp;&nbsp;".ucfirst($pcost->paintbrand);?></option>';
	<?php }} ?>
	
	html +='</select><select name="qty'+ordercnt+'" id="qty'+ordercnt+'" class="span1">';
	
	<?php for($i=1;$i<=50;$i++){ ?>
	html +='<option value="<?php echo $i;?>"><?php echo $i;?></option>';
	<?php } ?>
	
	html +='</select><select name="ncqty'+ordercnt+'" id="ncqty'+ordercnt+'" class="span1">';
	
	<?php for($i=0;$i<=50;$i++){ ?>
	html +='<option value="<?php echo $i;?>"><?php echo $i;?></option>';
	<?php } ?>
	
	html +='</select><select name="sheen_id'+ordercnt+'" id="sheen_id'+ordercnt+'" class="span2">';
	
	<?php if(!empty($all_sheen)) { foreach($all_sheen as $sheen) { ?>
	html +='<option value="<?php echo $sheen->sheen_id;?>"><?php echo ucfirst($sheen->sheen_description);?></option>';
	<?php }} ?>
	
	html +='</select>';
	
	html +='<input name="colour'+ordercnt+'" id="colour'+ordercnt+'" class="span2" value="" type="text" />';
	html +='<input name="productcostsum'+ordercnt+'" id="productcostsum'+ordercnt+'" class="span1" value="0.0" type="text" readonly="readonly" />';
	html +='<input name="optionalcostsum'+ordercnt+'" id="optionalcostsum'+ordercnt+'" class="span1" value="0.0" type="text" />';
	
	html +='<a href="javascript:void(0)" data-id="" data-cnt="'+ordercnt+'" class="span1 pad10 removeorder"><i class="icon-remove"></i></a><br />';
	
	html +='<input type="text" name="notes'+ordercnt+'" id="notes'+ordercnt+'" placeholder="Order Notes" class="span12">';
	
	html +='</div>';                                    
	html +='</div>';
	
	return html;
  
  }
  
  
</script>   