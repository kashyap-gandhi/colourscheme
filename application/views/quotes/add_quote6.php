<?php $site_setting = site_setting(); ?>
<!-- datepicker plugin -->
<link rel="stylesheet" href="<?php echo base_url(); ?>css/bootstrap-datetimepicker.min.css">


<!-- datepicker plugin -->
<script src="<?php echo base_url(); ?>js/bootstrap-datetimepicker.min.js"></script>



<div class="page-header">
    <div class="pull-left">
        <h4><i class="icon-cogs"></i> <?php if ($quote_id != '') { ?>Edit Quote<?php } else { ?>Add Quote<?php } ?></h4>
    </div>
    <div class="pull-right">
        <ul class="bread">
            <li><a href="<?php echo site_url('quotes/manage'); ?>">Manage Quotes</a><span class="divider">/</span></li>
            <li class="active">Quotes</li>
        </ul>
    </div>
</div>


<div class="container-fluid" id="content-area">

    <div class="row-fluid">
        <div class="span12">

            <?php if ($error != '') { ?>

                <div class="alert alert-error">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    <strong>Warning !</strong> <?php echo $error; ?>
                </div>    <?php } ?>



            <?php if ($msg != '') { ?>
                <div class="alert <?php if ($msg == 'notfound' || $msg == 'cannot') { ?>alert-danger<?php } else { ?>alert-success<?php } ?>">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    <strong><?php if ($msg == 'notfound' || $msg == 'cannot') { ?>Warning<?php } else { ?>Success<?php } ?>!</strong>

                    <?php if ($msg == 'delete') { ?>Quote Timecharges has been deleted successfully. <?php } ?>	
                    <?php if ($msg == 'insert') { ?>New Quote Timecharges has been added successfully. <?php } ?>	
                    <?php if ($msg == 'update') { ?>Quote Timecharges detail has been updated successfully. <?php } ?>	
                    <?php if ($msg == 'notfound') { ?>Quote Timecharges records not found. <?php } ?>
                    <?php if ($msg == 'cannot') { ?>Cannot delete Quote Timecharges because it used in many records. <?php } ?>		

                </div> 
            <?php } ?>	



            <!---add-->

            <div class="box">
                <div class="box-head">
                    <i class="icon-list-ul"></i>
                    <span>Quote Timecharge details</span>

                    <?php
                    $data['quote_details'] = $quote_details;
                    $this->load->view('quotes/quote_title', $data);
                    ?>


                </div>
                <div class="box-body box-body-nopadding">
                    <?php
                    $attributes = array('name' => 'frm_addquote', 'class' => 'form-horizontal form-bordered form-wizard');
                    echo form_open('quotes/step6/' . $quote_id, $attributes);
                    ?> 

                    <?php $this->load->view('quotes/quote_buttons', $data); ?>

                 <div class="step" id="firstStep" style="opacity:1 !important;">


                        <!-----steps--->   
                        <?php
                        $data['step_index'] = 6;
                        $this->load->view('quotes/quote_steps', $data);
                        ?>
                        <!-----steps--->



                        <div class="control-group">&nbsp;</div>

                        <!---work start stop--->
                        <div class="control-group">
                            <label for="textfield" class="control-label">Total Work Duration</label>

                            <div class="controls controls-row">

                                <label class="span2">Work Start Date</label><label class="span2"><b><?php if (isset($quote_first) && !empty($quote_first)) {
                            if ($quote_first->date != '') {
                                echo date($site_setting->date_time_format, strtotime($quote_first->date));
                            }
                        } ?></b></label>

                                <label class="span2">Work End Date</label><label class="span2"><b><?php if (isset($quote_last) && !empty($quote_last)) {
                            if ($quote_last->date != '') {
                                echo date($site_setting->date_time_format, strtotime($quote_last->date));
                            }
                        } ?></b></label>


                                <label for="is_complete" class="span2">Complete Now</label><label class="span2"><input type="checkbox" value="1" name="is_complete" id="is_complete" <?php if ($quote_details->is_complete == 1) { ?> checked="checked" <?php } ?> /></label>


                            </div>


                        </div>
                        <!---work start stop--->

                        <div class="control-group" style="height: 30px;">&nbsp;</div>   


                        <div class="control-group">
                            <label for="textfield" class="control-label">Employee</label>
                            <div class="controls controls-row" style="height: 15px;">
                                <label for="team_id" class="span4">Team</label>
                                <label for="role_id" class="span4">Role</label>
                                <label for="chargetype_id" class="span4">Charge Type</label>
                            </div>
                            <div class="controls controls-row" style="height: 40px;">



                                        <?php
                                        $team_list = team_list();
                                        $role_list = role_list();
                                        $chargetype_list = chargetype_list();
                                        ?>

                                <div class="span4">
                                    <select name="team_id" id="team_id" class="chosen-select">
                                           <option value="">---select team---</option>
                                    <?php if (!empty($team_list)) {
                                        foreach ($team_list as $team) { ?>
                                                <option value="<?php echo $team->team_id; ?>" <?php if ($team_id == $team->team_id) { ?> selected="selected"<?php } ?>><?php echo ucfirst($team->name); ?></option>
    <?php }
} ?>
                                    </select>
                                </div>
                                
                                <?php if($role_id=='' || $role_id==0) { $role_id=3; } ?>

                                <select name="role_id" id="role_id" class="span4">
                                    <?php if (!empty($role_list)) {
                                        foreach ($role_list as $role) { ?>
                                            <option value="<?php echo $role->role_id; ?>" <?php if ($role_id == $role->role_id) { ?> selected="selected"<?php } ?>><?php echo ucfirst($role->role); ?></option>
                                        <?php }
                                    } ?>
                                </select>




<?php if($chargetype_id=='' || $chargetype_id==0) { $chargetype_id=1; } ?>

                                <select name="chargetype_id" id="chargetype_id" class="span4">
<?php if (!empty($chargetype_list)) {
    foreach ($chargetype_list as $type) { ?>
                                            <option value="<?php echo $type->chargetype_id; ?>" <?php if ($chargetype_id == $type->chargetype_id) { ?> selected="selected"<?php } ?>><?php echo ucfirst($type->charge_description); ?></option>
    <?php }
} ?>
                                </select>







                            </div>
                        </div>


                        <div class="control-group">&nbsp;</div>





                        <div class="control-group">
                            <label for="textfield" class="control-label">Date</label>
                            <div class="controls controls-row" style="height: 15px;">
                                 <label for="date" class="span2">Date</label>
                                <label for="begin_time" class="span3">Begin Time</label>
                                <label for="end_time" class="span3">End Time</label>
                                <label for="break" class="span2">Break</label>
                                <label for="hours" class="span2">Hours</label>
                            </div>
                            <div class="controls controls-row" style="height: 40px;">

                                <div  class="input-append date datetimepicker span2">
                                    <input name="date" id="date" type="text" value="<?php echo $date; ?>" placeholder="Date" class="input-small" readonly="readonly"/>
                                    <span class="add-on">
                                        <i data-time-icon="icon-time" data-date-icon="icon-calendar">
                                        </i>
                                    </span>
                                </div>





                                <select name="begin_time" id="begin_time" class="span3">                                  
                                    <option value="00" <?php if ($begin_time == 0) { ?> selected="selected" <?php } ?>>12:00 AM</option>       
                                    <option value="30" <?php if ($begin_time == 30) { ?> selected="selected" <?php } ?>>12:30 AM</option>            

                                    <option value="60" <?php if ($begin_time == 60) { ?> selected="selected" <?php } ?>>01:00 AM</option>
                                    <option value="90" <?php if ($begin_time == 90) { ?> selected="selected" <?php } ?>>01:30 AM</option>

                                    <option value="120" <?php if ($begin_time == 120) { ?> selected="selected" <?php } ?>>02:00 AM</option>
                                    <option value="150" <?php if ($begin_time == 150) { ?> selected="selected" <?php } ?>>02:30 AM</option>

                                    <option value="180" <?php if ($begin_time == 180) { ?> selected="selected" <?php } ?>>03:00 AM</option>
                                    <option value="210" <?php if ($begin_time == 210) { ?> selected="selected" <?php } ?>>03:30 AM</option>

                                    <option value="240" <?php if ($begin_time == 240) { ?> selected="selected" <?php } ?>>04:00 AM</option>
                                    <option value="270" <?php if ($begin_time == 270) { ?> selected="selected" <?php } ?>>04:30 AM</option>

                                    <option value="300" <?php if ($begin_time == 300) { ?> selected="selected" <?php } ?>>05:00 AM</option>
                                    <option value="330" <?php if ($begin_time == 330) { ?> selected="selected" <?php } ?>>05:30 AM</option>

                                    <option value="360" <?php if ($begin_time == 360) { ?> selected="selected" <?php } ?>>06:00 AM</option>
                                    <option value="390" <?php if ($begin_time == 390) { ?> selected="selected" <?php } ?>>06:30 AM</option>

                                    <option value="420" <?php if ($begin_time == 420) { ?> selected="selected" <?php } ?>>07:00 AM</option>
                                    <option value="450" <?php if ($begin_time == 450) { ?> selected="selected" <?php } ?>>07:30 AM</option>

                                    <option value="480" <?php if ($begin_time == 480) { ?> selected="selected" <?php } ?>>08:00 AM</option>
                                    <option value="510" <?php if ($begin_time == 510) { ?> selected="selected" <?php } ?>>08:30 AM</option>

                                    <option value="540" <?php if ($begin_time == 540) { ?> selected="selected" <?php } ?>>09:00 AM</option>
                                    <option value="570" <?php if ($begin_time == 570) { ?> selected="selected" <?php } ?>>09:30 AM</option>

                                    <option value="600" <?php if ($begin_time == 600) { ?> selected="selected" <?php } ?>>10:00 AM</option>
                                    <option value="630" <?php if ($begin_time == 630) { ?> selected="selected" <?php } ?>>10:30 AM</option>

                                    <option value="660" <?php if ($begin_time == 660) { ?> selected="selected" <?php } ?>>11:00 AM</option>
                                    <option value="690" <?php if ($begin_time == 690) { ?> selected="selected" <?php } ?>>11:30 AM</option>

                                    <option value="720" <?php if ($begin_time == 720) { ?> selected="selected" <?php } ?>>12:00 PM</option>
                                    <option value="750" <?php if ($begin_time == 750) { ?> selected="selected" <?php } ?>>12:30 PM</option>

                                    <option value="780" <?php if ($begin_time == 780) { ?> selected="selected" <?php } ?>>01:00 PM</option>
                                    <option value="810" <?php if ($begin_time == 810) { ?> selected="selected" <?php } ?>>01:30 PM</option>

                                    <option value="840" <?php if ($begin_time == 840) { ?> selected="selected" <?php } ?>>02:00 PM</option>
                                    <option value="870" <?php if ($begin_time == 870) { ?> selected="selected" <?php } ?>>02:30 PM</option>

                                    <option value="900" <?php if ($begin_time == 900) { ?> selected="selected" <?php } ?>>03:00 PM</option>
                                    <option value="930" <?php if ($begin_time == 930) { ?> selected="selected" <?php } ?>>03:30 PM</option>

                                    <option value="960" <?php if ($begin_time == 960) { ?> selected="selected" <?php } ?>>04:00 PM</option>
                                    <option value="990" <?php if ($begin_time == 990) { ?> selected="selected" <?php } ?>>04:30 PM</option>

                                    <option value="1020" <?php if ($begin_time == 1020) { ?> selected="selected" <?php } ?>>05:00 PM</option>
                                    <option value="1050" <?php if ($begin_time == 1050) { ?> selected="selected" <?php } ?>>05:30 PM</option>

                                    <option value="1080" <?php if ($begin_time == 1080) { ?> selected="selected" <?php } ?>>06:00 PM</option>
                                    <option value="1110" <?php if ($begin_time == 1110) { ?> selected="selected" <?php } ?>>06:30 PM</option>

                                    <option value="1140" <?php if ($begin_time == 1140) { ?> selected="selected" <?php } ?>>07:00 PM</option>
                                    <option value="1170" <?php if ($begin_time == 1170) { ?> selected="selected" <?php } ?>>07:30 PM</option>

                                    <option value="1200" <?php if ($begin_time == 1200) { ?> selected="selected" <?php } ?>>08:00 PM</option>
                                    <option value="1230" <?php if ($begin_time == 1230) { ?> selected="selected" <?php } ?>>08:30 PM</option>

                                    <option value="1260" <?php if ($begin_time == 1260) { ?> selected="selected" <?php } ?>>09:00 PM</option>
                                    <option value="1290" <?php if ($begin_time == 1290) { ?> selected="selected" <?php } ?>>09:30 PM</option>

                                    <option value="1320" <?php if ($begin_time == 1320) { ?> selected="selected" <?php } ?>>10:00 PM</option>
                                    <option value="1350" <?php if ($begin_time == 1350) { ?> selected="selected" <?php } ?>>10:30 PM</option>

                                    <option value="1380" <?php if ($begin_time == 1380) { ?> selected="selected" <?php } ?>>11:00 PM</option>
                                    <option value="1410" <?php if ($begin_time == 1410) { ?> selected="selected" <?php } ?>>11:30 PM</option>

                                    <option value="1440" <?php if ($begin_time == 1440) { ?> selected="selected" <?php } ?>>11:59 PM</option>

                                </select>         
                                <?php /* ?><select name="begin_time" id="begin_time" class="span3">

                                  <?php for ($x = 0; $x <= 23; $x++) {
                                  $H = $x % 24;
                                  $ap = ($H < 12)? "AM" : "PM";
                                  $h = $H % 12;

                                  if($h==0) {
                                  echo "<option value='".str_pad($H, 2, "0", STR_PAD_LEFT).":00:00'> "
                                  .str_pad(12, 2, "0", STR_PAD_LEFT).":00 $ap</option>\n";
                                  echo "<option value='".str_pad($H, 2, "0", STR_PAD_LEFT).":30:00'> "
                                  .str_pad(12, 2, "0", STR_PAD_LEFT).":30 $ap</option>\n";
                                  } else {
                                  echo "<option value='".str_pad($H, 2, "0", STR_PAD_LEFT).":00:00'> "
                                  .str_pad($h, 2, "0", STR_PAD_LEFT).":00 $ap</option>\n";
                                  echo "<option value='".str_pad($H, 2, "0", STR_PAD_LEFT).":30:00'> "
                                  .str_pad($h, 2, "0", STR_PAD_LEFT).":30 $ap</option>\n";
                                  }
                                  }
                                  echo "<option value='".str_pad(23, 2, "0", STR_PAD_LEFT).":59:59'> "
                                  .str_pad(11, 2, "0", STR_PAD_LEFT).":59 $ap</option>\n";
                                  ?>
                                  </select><?php */ ?>

                                <select name="end_time" id="end_time" class="span3">

                                    <option value="00" <?php if ($end_time == 0) { ?> selected="selected" <?php } ?>>12:00 AM</option>       
                                    <option value="30" <?php if ($end_time == 30) { ?> selected="selected" <?php } ?>>12:30 AM</option>            

                                    <option value="60" <?php if ($end_time == 60) { ?> selected="selected" <?php } ?>>01:00 AM</option>
                                    <option value="90" <?php if ($end_time == 90) { ?> selected="selected" <?php } ?>>01:30 AM</option>

                                    <option value="120" <?php if ($end_time == 120) { ?> selected="selected" <?php } ?>>02:00 AM</option>
                                    <option value="150" <?php if ($end_time == 150) { ?> selected="selected" <?php } ?>>02:30 AM</option>

                                    <option value="180" <?php if ($end_time == 180) { ?> selected="selected" <?php } ?>>03:00 AM</option>
                                    <option value="210" <?php if ($end_time == 210) { ?> selected="selected" <?php } ?>>03:30 AM</option>

                                    <option value="240" <?php if ($end_time == 240) { ?> selected="selected" <?php } ?>>04:00 AM</option>
                                    <option value="270" <?php if ($end_time == 270) { ?> selected="selected" <?php } ?>>04:30 AM</option>

                                    <option value="300" <?php if ($end_time == 300) { ?> selected="selected" <?php } ?>>05:00 AM</option>
                                    <option value="330" <?php if ($end_time == 330) { ?> selected="selected" <?php } ?>>05:30 AM</option>

                                    <option value="360" <?php if ($end_time == 360) { ?> selected="selected" <?php } ?>>06:00 AM</option>
                                    <option value="390" <?php if ($end_time == 390) { ?> selected="selected" <?php } ?>>06:30 AM</option>

                                    <option value="420" <?php if ($end_time == 420) { ?> selected="selected" <?php } ?>>07:00 AM</option>
                                    <option value="450" <?php if ($end_time == 450) { ?> selected="selected" <?php } ?>>07:30 AM</option>

                                    <option value="480" <?php if ($end_time == 480) { ?> selected="selected" <?php } ?>>08:00 AM</option>
                                    <option value="510" <?php if ($end_time == 510) { ?> selected="selected" <?php } ?>>08:30 AM</option>

                                    <option value="540" <?php if ($end_time == 540) { ?> selected="selected" <?php } ?>>09:00 AM</option>
                                    <option value="570" <?php if ($end_time == 570) { ?> selected="selected" <?php } ?>>09:30 AM</option>

                                    <option value="600" <?php if ($end_time == 600) { ?> selected="selected" <?php } ?>>10:00 AM</option>
                                    <option value="630" <?php if ($end_time == 630) { ?> selected="selected" <?php } ?>>10:30 AM</option>

                                    <option value="660" <?php if ($end_time == 660) { ?> selected="selected" <?php } ?>>11:00 AM</option>
                                    <option value="690" <?php if ($end_time == 690) { ?> selected="selected" <?php } ?>>11:30 AM</option>

                                    <option value="720" <?php if ($end_time == 720) { ?> selected="selected" <?php } ?>>12:00 PM</option>
                                    <option value="750" <?php if ($end_time == 750) { ?> selected="selected" <?php } ?>>12:30 PM</option>

                                    <option value="780" <?php if ($end_time == 780) { ?> selected="selected" <?php } ?>>01:00 PM</option>
                                    <option value="810" <?php if ($end_time == 810) { ?> selected="selected" <?php } ?>>01:30 PM</option>

                                    <option value="840" <?php if ($end_time == 840) { ?> selected="selected" <?php } ?>>02:00 PM</option>
                                    <option value="870" <?php if ($end_time == 870) { ?> selected="selected" <?php } ?>>02:30 PM</option>

                                    <option value="900" <?php if ($end_time == 900) { ?> selected="selected" <?php } ?>>03:00 PM</option>
                                    <option value="930" <?php if ($end_time == 930) { ?> selected="selected" <?php } ?>>03:30 PM</option>

                                    <option value="960" <?php if ($end_time == 960) { ?> selected="selected" <?php } ?>>04:00 PM</option>
                                    <option value="990" <?php if ($end_time == 990) { ?> selected="selected" <?php } ?>>04:30 PM</option>

                                    <option value="1020" <?php if ($end_time == 1020) { ?> selected="selected" <?php } ?>>05:00 PM</option>
                                    <option value="1050" <?php if ($end_time == 1050) { ?> selected="selected" <?php } ?>>05:30 PM</option>

                                    <option value="1080" <?php if ($end_time == 1080) { ?> selected="selected" <?php } ?>>06:00 PM</option>
                                    <option value="1110" <?php if ($end_time == 1110) { ?> selected="selected" <?php } ?>>06:30 PM</option>

                                    <option value="1140" <?php if ($end_time == 1140) { ?> selected="selected" <?php } ?>>07:00 PM</option>
                                    <option value="1170" <?php if ($end_time == 1170) { ?> selected="selected" <?php } ?>>07:30 PM</option>

                                    <option value="1200" <?php if ($end_time == 1200) { ?> selected="selected" <?php } ?>>08:00 PM</option>
                                    <option value="1230" <?php if ($end_time == 1230) { ?> selected="selected" <?php } ?>>08:30 PM</option>

                                    <option value="1260" <?php if ($end_time == 1260) { ?> selected="selected" <?php } ?>>09:00 PM</option>
                                    <option value="1290" <?php if ($end_time == 1290) { ?> selected="selected" <?php } ?>>09:30 PM</option>

                                    <option value="1320" <?php if ($end_time == 1320) { ?> selected="selected" <?php } ?>>10:00 PM</option>
                                    <option value="1350" <?php if ($end_time == 1350) { ?> selected="selected" <?php } ?>>10:30 PM</option>

                                    <option value="1380" <?php if ($end_time == 1380) { ?> selected="selected" <?php } ?>>11:00 PM</option>
                                    <option value="1410" <?php if ($end_time == 1410) { ?> selected="selected" <?php } ?>>11:30 PM</option>

                                    <option value="1440" <?php if ($end_time == 1440) { ?> selected="selected" <?php } ?>>11:59 PM</option>
                                </select>


                                <?php if($break=='' || $break==0) { $break=30; } ?>

                                <select name="break" id="break" class="span2">

                                    <option value="00" <?php if ($break == 0) { ?> selected="selected" <?php } ?>>00.00</option>
                                    <option value="15" <?php if ($break == 15) { ?> selected="selected" <?php } ?>>00:15</option>               
                                    <option value="30" <?php if ($break == 30) { ?> selected="selected" <?php } ?>>00:30</option>    
                                    <option value="45" <?php if ($break == 45) { ?> selected="selected" <?php } ?>>00:45</option>    
                                    <option value="60" <?php if ($break == 60) { ?> selected="selected" <?php } ?>>01:00</option>    
                                    <option value="75" <?php if ($break == 75) { ?> selected="selected" <?php } ?>>01:15</option>               
                                    <option value="90" <?php if ($break == 90) { ?> selected="selected" <?php } ?>>01:30</option>    
                                    <option value="105" <?php if ($break == 105) { ?> selected="selected" <?php } ?>>01:45</option>    
                                    <option value="120" <?php if ($break == 120) { ?> selected="selected" <?php } ?>>02:00</option>   

                                </select>




                                <input type="text" name="hours" id="hours" value="<?php echo $hours; ?>" placeholder="Hours" class="span2" readonly="readonly">





                            </div>
                        </div>
                        <script>
                            $("#begin_time,#end_time,#break").on('change',function(){
												
												
                                var begin_time = $("#begin_time").val();
                                var end_time = $("#end_time").val();
												
                                var break_time = $("#break").val();
												
															
                                //alert(begin_time+"=="+end_time+"=="+break_time);
												
												
                                var eq_time = parseInt(begin_time);
												
                                if(parseInt(end_time)-(parseInt(begin_time)+parseInt(break_time))>0) {
                                    var eq_time = parseInt(begin_time)+parseInt(break_time);
                                } else {
                                    alert('Please select proper Begin, End time and Break Time');
                                    return false;
                                }
												
												
                                var begindate = new Date(), enddate = new Date();
												
                                begindate.setHours(0);
                                begindate.setMinutes(eq_time);
                                begindate.setSeconds(0);
												
												
                                enddate.setHours(0);
                                enddate.setMinutes(end_time);
                                enddate.setSeconds(0);
												
                                if(begindate > enddate) {
                                    enddate.setDate(enddate.getDate() + 1)
                                }
                                var diff = (enddate.getTime() - begindate.getTime()) / 1000;
												
                                var hrs =  parseInt(diff / 3600);
                                diff = diff % 3600;
												
                                var minutes =  parseInt(diff / 60);
												
                                //alert(hrs +"=="+ minutes);
												
                                var fcalc=zeroFill(hrs,2)+":"+zeroFill(minutes,2);
												
                                $("#hours").val(fcalc);
					  calculatecost();							
												
                            });	
                            function zeroFill( number, width )
                            {
                                width -= number.toString().length;
                                if ( width > 0 )
                                {
                                    return new Array( width + (/\./.test( number ) ? 2 : 1) ).join( '0' ) + number;
                                }
                                return number; // always return a string
                            }										
                        </script>

                        <div class="control-group">&nbsp;</div>






                        <div class="control-group">
                            <label for="textfield" class="control-label">Cost(<?php echo $site_setting->currency_symbol; ?>)</label>

                            <div class="controls controls-row" >
                                <input type="text" name="resource_cost" id="resource_cost" placeholder="Cost" class="" value="<?php echo $resource_cost; ?>">
                            </div>
                        </div>





                        <div class="control-group">&nbsp;</div>








                    </div><!--step 1 div-->




                    <div class="form-actions">

                        <button type="submit" class="button button-basic-blue savebtnquote">Save</button>


                        <button type="button" class="button button-basic">Cancel</button>

                        <input type="hidden" name="quote_id" id="quote_id" value="<?php echo $quote_id; ?>" />
 <input type="hidden" name="team_wage" id="team_wage" value="0" />

                    </div>


                    </form>
                </div>
            </div>


            <!---add-->


            <!----lists-->
            <div class="box">
                <div class="box-head">
                    <i class="icon-list-ul"></i>
                    <span>Time Charges</span>
                </div>

                <!---sort-->

                <!-- dataTables plugin -->
                <script src="<?php echo base_url(); ?>js/jquery.dataTables.min.js"></script>
                <!-- TableTools for dataTables plguin -->
                <script src="<?php echo base_url(); ?>js/TableTools.min.js"></script>                         

                <div class="box-body box-body-nopadding">

                    <table class="table table-nomargin table-bordered timechargetable table-striped table-hover">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Employee</th>
                                <th>Date</th>
                                <th>Begin Time</th>
                                <th>End Time</th>
                                <th>Break</th>
                                <th>Hours</th>
                                <th>Cost(<?php echo $site_setting->currency_symbol; ?>)</th>
                                <th>Charge Type</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if ($result) {
                                foreach ($result as $row) { ?>
                                    <tr> 
                                        <td><?php echo $row->timecode_id; ?></td>                                   
                                        <td><?php echo $row->name; ?></td>
                                        <td><?php echo date($site_setting->date_format, strtotime($row->date)); ?></td>
                                        <td><?php echo date($site_setting->time_format, strtotime($row->begin_time)); ?></td>
                                        <td><?php echo date($site_setting->time_format, strtotime($row->end_time)); ?></td>
                                        <td><?php echo $row->break; ?></td>
                                        <td><?php echo $row->hours; ?></td>
                                        <td><?php echo $row->resource_cost; ?></td>
                                        <td><?php $chargetype_detail=chargetype_by_id($row->chargetype_id);  if($chargetype_detail) { echo $chargetype_detail->charge_description; } ?></td>
                                        <td> <a href="<?php echo site_url('quotes/timecharge_action/' . $quote_id . '/' . $row->timecode_id . '/delete/' . $offset); ?>" class='button button-basic button-icon' rel="tooltip" title="Delete"><i class="icon-trash"></i></a></td>
                                    </tr>
    <?php }
} ?>



                        </tbody>
                    </table>
                </div>


                <!---sort-->




            </div>
            <!---lists--->


        </div>
    </div>

</div>

<script type="text/javascript">
    $(function(){
        $('.datetimepicker').datetimepicker({
            format: 'dd-MM-yyyy hh:mm:ss'
        });
	
        $('.timechargetable').dataTable({
            "aaSorting": [[ 0, "desc" ]],
            "aoColumnDefs": [
                { "bSearchable": false, "bVisible": false, "aTargets": [ 0 ] }                        
            ]
        } );
	
	
        $("#is_complete").on("click",function(){
			
            var complete=0;
			
			
            if($("#is_complete:checked").val()==1){
                complete=1;
            }
			
			
            var res = $.ajax({						
                type: 'POST',
                url: '<?php echo site_url('quotes/complete_quote/'); ?>',
                data: {quote_id:<?php echo $quote_id; ?>,is_complete:complete},
                dataType: 'json', 
                cache: false,
                async: false                     
            }).responseText;	
			
            var rcom_res = jQuery.parseJSON(res);
	
            if(rcom_res.status=='success')
            {
                alert(rcom_res.msg);
            } else {
                alert(rcom_res.msg);					
            }						
			 
        });
        
            $("#team_id").live("change",function(){
           
           var team_id=$("#team_id option:selected").val();
           
           
           
           if(team_id!='' && team_id!=null && team_id!=undefined){
              
              
               var res = $.ajax({						
                    type: 'POST',
                    url: '<?php echo site_url('quotes/getemployeewage/'); ?>',
                    data: {team_id:team_id},
                    dataType: 'html', 
                    cache: false,
                    async: false                     
                }).responseText;							
			 
		
                
                var wage = parseFloat(res);
                
                $("#team_wage").val(wage);
                
                
                calculatecost();
        
       
              
              
           } else {
               $("#resource_cost").val(0);
           }
           
        });
        
        
		
    });	
    
    
       function calculatecost(){
            
            var wage=parseFloat($("#team_wage").val());
            var totalhours=$("#hours").val();
            
            
            var tsplit=totalhours.split(":");
            var thours=parseFloat(tsplit[0]);
            var tminute=parseFloat(tsplit[1]);
            
            //alert(thours+"=="+tminute);
            
            var total_cost=0;
            
            var min_wage=0;
            
            total_cost=wage * thours;
            
            if(tminute>0){
              
              
                if(tminute==15){
                    min_wage=wage/4;
                } else if(tminute==30){
                    min_wage=wage/2;
                } else if(tminute==45){
                    min_wage= ((wage * 3)/4);
                }

            }
            
            total_cost= total_cost + min_wage;
            
              if(total_cost==NaN){
                total_cost=0;
            }
             $("#resource_cost").val(total_cost.toFixed(2));
			
            
            
            
        }
    
    

var clicked = false;

$(document).ready(function(){
//    $("a[href]").click(function(){
//        clicked = true;
//    });
//
//    $(document).bind('keypress keydown keyup', function(e) {
//        if(e.which === 116) {
//            clicked = true;
//        }
//        if(e.which === 82 && e.ctrlKey) {
//            clicked = true;
//        }
//    });

    $(".savebtnquote").click(function(){
        clicked = true;
    });
    
});


window.addEventListener("beforeunload", function (e) {
    if(!clicked) {
        
         var confirmationMessage = 'Please click on stay on page to save the data you have entered and then continue.';    

        (e || window.event).returnValue = confirmationMessage; //Gecko + IE
        return confirmationMessage; //Gecko + Webkit, Safari, Chrome etc.
    }
});
//window.onbeforeunload = function(e){
//    if(!clicked) {
//        
//          if (typeof event == 'undefined') {
//            event = window.event;
//          }
//          if (event) {
//            event.returnValue = 'You have unsaved stuff.';
//          }
//          return message;
//        
//        //return 'You have unsaved stuff.';
//    }
//};

</script>
<style>
    #firstStep{
        opacity:1 !important;
    }
</style>