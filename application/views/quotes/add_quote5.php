<?php $site_setting=site_setting();
 ?>
 	<!-- datepicker plugin -->
	<link rel="stylesheet" href="<?php echo base_url();?>css/bootstrap-datetimepicker.min.css">

    
	<!-- datepicker plugin -->
	<script src="<?php echo base_url();?>js/bootstrap-datetimepicker.min.js"></script>

 

<div class="page-header">
				<div class="pull-left">
					<h4><i class="icon-cogs"></i> <?php if($quote_id!='') {?>Edit Quote<?php } else { ?>Add Quote<?php }  ?></h4>
				</div>
				<div class="pull-right">
					<ul class="bread">
						<li><a href="<?php echo site_url('quotes/manage');?>">Manage Quotes</a><span class="divider">/</span></li>
                        <li class="active">Quotes</li>
					</ul>
				</div>
			</div>
            
            
            <div class="container-fluid" id="content-area">
				
<div class="row-fluid">
                <div class="span12">
                
                 <?php if($error!=''){ ?>

		<div class="alert alert-error">
											<button type="button" class="close" data-dismiss="alert">&times;</button>
											<strong>Warning !</strong> <?php echo $error;?>
										</div>    <?php }?>
                                        
                          <?php if($msg!='') { ?>
<div class="alert <?php if($msg=='notfound' || $msg=='cannot') {?>alert-danger<?php } else { ?>alert-success<?php } ?>">
											<button type="button" class="close" data-dismiss="alert">&times;</button>
											<strong><?php if($msg=='notfound' || $msg=='cannot') {?>Warning<?php } else { ?>Success<?php } ?>!</strong>

	<?php if($msg=='delete') { ?>Quote Follow Up has been deleted successfully. <?php } ?>	
	<?php if($msg=='insert') { ?>New Quote Follow Up has been added successfully. <?php } ?>	
	<?php if($msg=='update') { ?>Quote Follow Up detail has been updated successfully. <?php } ?>	
    <?php if($msg=='notfound') { ?>Quote Follow Up records not found. <?php } ?>
    <?php if($msg=='cannot') { ?>Cannot delete Quote Follow Up because it used in many records. <?php } ?>		
  
</div> 
<?php } ?>	                
    
					
						<div class="box">
							<div class="box-head">
								<i class="icon-list-ul"></i>
								<span>Quote Follow Up</span>
                                
                           <?php
						   $data['quote_details']=$quote_details;						 
						   $this->load->view('quotes/quote_title',$data); 
						   ?>
                                
							</div>
							<div class="box-body box-body-nopadding">
                             <?php
									$attributes = array('name'=>'frm_addquote','class'=>'form-horizontal form-bordered form-wizard');
									echo form_open('quotes/step5/'.$quote_id,$attributes);
								  ?> 
                                  
								
								<?php $this->load->view('quotes/quote_buttons',$data);  ?>
                                    
                                   <div class="step" id="firstStep" style="opacity:1 !important;">
										
                                        
                                     <!-----steps--->   
                                         <?php 
										   $data['step_index']=5;
										 $this->load->view('quotes/quote_steps',$data);  ?>
                                        <!-----steps--->
                                        
                                        
                                        
                                        
                                        	<div class="control-group">
                                            <label for="textfield" class="control-label">Rate / Date</label>
                                            <div class="controls controls-row" style="height: 15px;">
                                                <label for="first_name" class="span3">Call Date</label>
                                                <label for="last_name" class="span3">Satisfaction Rating</label>
                                                 <label for="last_name" class="span3">#. Of Revisits</label>
                                                 <label for="last_name" class="span3">Team Lead</label>
                                            </div>
                                            <div class="controls controls-row" style="height: 40px;">
                                            
                                            <div  class="input-append date datetimepicker span3">
                                            <input name="satisfactioncalldate" id="satisfactioncalldate" type="text" value="<?php if($satisfactioncalldate!='') { echo date('d-m-Y H:i:s',strtotime($satisfactioncalldate)); } ?>" placeholder="Satisfaction Call Date" class="input-small" />
                                            <span class="add-on">
                                              <i data-time-icon="icon-time" data-date-icon="icon-calendar">
                                              </i>
                                            </span>
                                          </div>
                                          
                                          
                                            
                                            <select class="span3" name="satisfactionrating" id="satisfactionrating">
                                            <?php for($i=0;$i<=25;$i++){?>
                                   	<option value="<?php echo $i;?>" <?php if($satisfactionrating==$i) {?> selected="selected"<?php } ?>><?php echo $i;?></option>
                                    <?php } ?>
                                            </select>
                                            
                                              <select class="span3" name="satisfactioncall" id="satisfactioncall">
                                            <?php for($i=0;$i<=5;$i++){?>
                                   	<option value="<?php echo $i;?>" <?php if($satisfactioncall==$i) {?> selected="selected"<?php } ?>><?php echo $i;?></option>
                                    <?php } ?>
                                            </select>
                                            
                                            
                                            
                                             <?php $team_list=team_list(); ?>
                                               
                                          <div class="span3">
                                             <select class="chosen-select" name="satisfactionleadpainter" id="satisfactionleadpainter"                                            
                                              <?php if(!empty($team_list)){ 
										  foreach($team_list as $team) {?>
                                          <option value="<?php echo $team->team_id;?>" <?php if($satisfactionleadpainter==$team->team_id) {?> selected="selected"<?php } ?>><?php echo ucfirst($team->name);?></option>
                                          <?php } } ?>
                                          </select>
                                          </div>
                                            
                                            
                                            
                                          	
                                            
                                            
                                               
                                            </div>
                                        </div>
                                        
                                        
                                         <div class="control-group">&nbsp;</div>
                                         
                                         
                                         
                                         
                                         
                                        
                                         <div class="control-group">
                                            <label for="textfield" class="control-label">Contact Name</label>
                                           
                                            <div class="controls controls-row" style="height: 40px;">
                                                <input type="text" name="satisfactionname" id="satisfactionname" placeholder="Satisfaction Name" class="" value="<?php echo $satisfactionname; ?>">
                                            </div>
                                        </div>
                                        
                                    <div class="control-group">&nbsp;</div>
                                        
										<div class="control-group">
                                            <label for="textfield" class="control-label">Notes</label>
                                           
                                            <div class="controls controls-row" style="height: 100px;">
                                                <textarea rows="5" class="input-block-level" name="satisfactionnotes" id="satisfactionnotes" placeholder="Satisfaction Notes"><?php echo $satisfactionnotes; ?></textarea>
                                            </div>
                                        </div>
                                        
                                       
                                       
                                 <div class="control-group">&nbsp;</div>
                                 
                                 
                             
                                 
                                
                                        
                                  
                                        
                                        
                                        
                                        
									</div><!--step 1 div-->
                                    
									
                                    
                                    
									<div class="form-actions">
										
            <button type="submit" class="button button-basic-blue savebtnquote">Save</button>

<button type="button" class="button button-basic" onClick="window.location.href='<?php echo site_url('quotes/managetype');?>'">Cancel</button>

    <input type="hidden" name="quote_id" id="quote_id" value="<?php echo $quote_id; ?>" />

										
									</div>
                                   
                                   
								</form>
							</div>
						</div>
					</div>
				</div>
				
				
				
			</div>
            
            
            
<script type="text/javascript">
  $(function() {
    $('.datetimepicker').datetimepicker({
		format:'dd-MM-yyyy hh:mm:ss'
	});
		
	});	
        
        
        

var clicked = false;

$(document).ready(function(){
//    $("a[href]").click(function(){
//        clicked = true;
//    });
//
//    $(document).bind('keypress keydown keyup', function(e) {
//        if(e.which === 116) {
//            clicked = true;
//        }
//        if(e.which === 82 && e.ctrlKey) {
//            clicked = true;
//        }
//    });

    $(".savebtnquote").click(function(){
        clicked = true;
    });
    
});

window.addEventListener("beforeunload", function (e) {
    if(!clicked) {
        
         var confirmationMessage = 'Please click on stay on page to save the data you have entered and then continue.';    

        (e || window.event).returnValue = confirmationMessage; //Gecko + IE
        return confirmationMessage; //Gecko + Webkit, Safari, Chrome etc.
    }
});
//window.onbeforeunload = function(e){
//    if(!clicked) {
//        
//          if (typeof event == 'undefined') {
//            event = window.event;
//          }
//          if (event) {
//            event.returnValue = 'You have unsaved stuff.';
//          }
//          return message;
//        
//        //return 'You have unsaved stuff.';
//    }
//};


</script>   
<style>
    #firstStep{
        opacity:1 !important;
    }
</style>