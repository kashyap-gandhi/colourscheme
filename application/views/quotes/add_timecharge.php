<?php $site_setting = site_setting(); ?>


<!-- datepicker plugin -->
<link rel="stylesheet" href="<?php echo base_url(); ?>css/bootstrap-datetimepicker.min.css">


<!-- datepicker plugin -->
<script src="<?php echo base_url(); ?>js/bootstrap-datetimepicker.min.js"></script>





<div class="page-header">
    <div class="pull-left">
        <h4><i class="icon-cogs"></i> <?php if ($timecode_id != '') { ?>Edit TimeCharge<?php } else { ?>Add TimeCharge<?php } ?></h4>
    </div>
    <div class="pull-right">
        <ul class="bread">
            <li><a href="<?php echo site_url('quotes/manage'); ?>">Manage Quotes</a><span class="divider">/</span></li>
            <li class="active">TimeCharge</li>
        </ul>
    </div>
</div>


<div class="container-fluid" id="content-area">

    <div class="row-fluid">
        <div class="span12">

            <?php if ($error != '') { ?>

                <div class="alert alert-error">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    <strong>Warning !</strong> <?php echo $error; ?>
                </div>    <?php } ?>

            <?php if ($msg != '') { ?>

                <div class="alert alert-success">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    <strong>Success !</strong> <?php
            if ($msg == "insert") {
                echo "Time Charge has been added successfully.";
            }
            ?>
                </div>    <?php } ?>

            <!---add-->

            <div class="box">
                <div class="box-head">
                    <i class="icon-list-ul"></i>
                    <span>Add TimeCharges for Quotes</span>
                </div>
                <div class="box-body box-body-nopadding">
                    <?php
                    $attributes = array('name' => 'frm_addtimecharge', 'class' => 'form-horizontal form-bordered form-wizard');
                    echo form_open('quotes/add_timecharge', $attributes);
                    ?> 









                    <div class="control-group">
                        <label for="textfield" class="control-label">Quote</label>

                        <div class="controls controls-row" style="height: 40px;">
                            <!---either hidden quote id or select box id--->
                            <?php $quote_list = quote_list(); ?>
                            <div style="width:500px;"><select name="quote_id" id="quote_id" class="chosen-select">
                                    <?php if (!empty($quote_list)) {
                                        foreach ($quote_list as $quote) { ?>
                                            <option value="<?php echo $quote->quote_id; ?>" <?php if ($quote_id == $quote->quote_id) { ?> selected="selected"<?php } ?>><?php echo ucfirst($quote->quote_unique_id) . '&nbsp;&nbsp;|&nbsp;&nbsp;' . date($site_setting->date_format, strtotime($quote->date)) . '&nbsp;&nbsp;|&nbsp;&nbsp;' . ucfirst($quote->first_name) . ' ' . ucfirst($quote->last_name) . '&nbsp;&nbsp;|&nbsp;&nbsp;' . ucfirst($quote->description) . '&nbsp;&nbsp;|&nbsp;&nbsp;' . ucfirst($quote->quotetype); ?></option>
                                        <?php }
                                    } ?>
                                </select></div>


                        </div>
                    </div>


                    <div class="control-group">
                        <label for="textfield" class="control-label">Employee</label>
                        <div class="controls controls-row" style="height: 15px;">
                            <label for="team_id" class="span4">Team</label>
                                <label for="role_id" class="span4">Role</label>
                                <label for="chargetype_id" class="span4">Charge Type</label>
                        </div>
                        <div class="controls controls-row" style="height: 40px;">



                            <?php
                            $team_list = team_list();
                            $role_list = role_list();
                            $chargetype_list = chargetype_list();
                            ?>

                            <div class="span4">
                                <select name="team_id" id="team_id" class="chosen-select">
                                    <option value="">---select team---</option>
                                    <?php if (!empty($team_list)) {
                                        foreach ($team_list as $team) { ?>
                                            <option value="<?php echo $team->team_id; ?>" <?php if ($team_id == $team->team_id) { ?> selected="selected"<?php } ?>><?php echo ucfirst($team->name); ?></option>
    <?php }
} ?>
                                </select>
                            </div>
                            
                            <?php if($role_id=='' || $role_id==0) { $role_id=3; } ?>

                            <select name="role_id" id="role_id" class="span4">
<?php if (!empty($role_list)) {
    foreach ($role_list as $role) { ?>
                                        <option value="<?php echo $role->role_id; ?>" <?php if ($role_id == $role->role_id) { ?> selected="selected"<?php } ?>><?php echo ucfirst($role->role); ?></option>
    <?php }
} ?>
                            </select>




                            <?php if($chargetype_id=='' || $chargetype_id==0) { $chargetype_id=1; } ?>

                            <select name="chargetype_id" id="chargetype_id" class="span4">
<?php if (!empty($chargetype_list)) {
    foreach ($chargetype_list as $type) { ?>
                                        <option value="<?php echo $type->chargetype_id; ?>" <?php if ($chargetype_id == $type->chargetype_id) { ?> selected="selected"<?php } ?>><?php echo ucfirst($type->charge_description); ?></option>
    <?php }
} ?>
                            </select>







                        </div>
                    </div>


                    <div class="control-group">&nbsp;</div>





                    <div class="control-group">
                        <label for="textfield" class="control-label">Date</label>
                        <div class="controls controls-row" style="height: 15px;">
                            <label for="date" class="span2">Date</label>
                            <label for="begin_time" class="span3">Begin Time</label>
                            <label for="end_time" class="span3">End Time</label>
                            <label for="break" class="span2">Break</label>
                            <label for="hours" class="span2">Hours</label>
                        </div>
                        <div class="controls controls-row" style="height: 40px;">

                            <div  class="input-append date datetimepicker span2">
                                <input data-format="dd/MM/yyyy hh:mm:ss" name="date" id="date" type="text" value="<?php echo $date; ?>" placeholder="Date" class="input-small" readonly="readonly"/>
                                <span class="add-on">
                                    <i data-time-icon="icon-time" data-date-icon="icon-calendar">
                                    </i>
                                </span>
                            </div>





                            <select name="begin_time" id="begin_time" class="span3">                                  
                                <option value="00" <?php if ($begin_time == 0) { ?> selected="selected" <?php } ?>>12:00 AM</option>       
                                <option value="30" <?php if ($begin_time == 30) { ?> selected="selected" <?php } ?>>12:30 AM</option>            

                                <option value="60" <?php if ($begin_time == 60) { ?> selected="selected" <?php } ?>>01:00 AM</option>
                                <option value="90" <?php if ($begin_time == 90) { ?> selected="selected" <?php } ?>>01:30 AM</option>

                                <option value="120" <?php if ($begin_time == 120) { ?> selected="selected" <?php } ?>>02:00 AM</option>
                                <option value="150" <?php if ($begin_time == 150) { ?> selected="selected" <?php } ?>>02:30 AM</option>

                                <option value="180" <?php if ($begin_time == 180) { ?> selected="selected" <?php } ?>>03:00 AM</option>
                                <option value="210" <?php if ($begin_time == 210) { ?> selected="selected" <?php } ?>>03:30 AM</option>

                                <option value="240" <?php if ($begin_time == 240) { ?> selected="selected" <?php } ?>>04:00 AM</option>
                                <option value="270" <?php if ($begin_time == 270) { ?> selected="selected" <?php } ?>>04:30 AM</option>

                                <option value="300" <?php if ($begin_time == 300) { ?> selected="selected" <?php } ?>>05:00 AM</option>
                                <option value="330" <?php if ($begin_time == 330) { ?> selected="selected" <?php } ?>>05:30 AM</option>

                                <option value="360" <?php if ($begin_time == 360) { ?> selected="selected" <?php } ?>>06:00 AM</option>
                                <option value="390" <?php if ($begin_time == 390) { ?> selected="selected" <?php } ?>>06:30 AM</option>

                                <option value="420" <?php if ($begin_time == 420) { ?> selected="selected" <?php } ?>>07:00 AM</option>
                                <option value="450" <?php if ($begin_time == 450) { ?> selected="selected" <?php } ?>>07:30 AM</option>

                                <option value="480" <?php if ($begin_time == 480) { ?> selected="selected" <?php } ?>>08:00 AM</option>
                                <option value="510" <?php if ($begin_time == 510) { ?> selected="selected" <?php } ?>>08:30 AM</option>

                                <option value="540" <?php if ($begin_time == 540) { ?> selected="selected" <?php } ?>>09:00 AM</option>
                                <option value="570" <?php if ($begin_time == 570) { ?> selected="selected" <?php } ?>>09:30 AM</option>

                                <option value="600" <?php if ($begin_time == 600) { ?> selected="selected" <?php } ?>>10:00 AM</option>
                                <option value="630" <?php if ($begin_time == 630) { ?> selected="selected" <?php } ?>>10:30 AM</option>

                                <option value="660" <?php if ($begin_time == 660) { ?> selected="selected" <?php } ?>>11:00 AM</option>
                                <option value="690" <?php if ($begin_time == 690) { ?> selected="selected" <?php } ?>>11:30 AM</option>

                                <option value="720" <?php if ($begin_time == 720) { ?> selected="selected" <?php } ?>>12:00 PM</option>
                                <option value="750" <?php if ($begin_time == 750) { ?> selected="selected" <?php } ?>>12:30 PM</option>

                                <option value="780" <?php if ($begin_time == 780) { ?> selected="selected" <?php } ?>>01:00 PM</option>
                                <option value="810" <?php if ($begin_time == 810) { ?> selected="selected" <?php } ?>>01:30 PM</option>

                                <option value="840" <?php if ($begin_time == 840) { ?> selected="selected" <?php } ?>>02:00 PM</option>
                                <option value="870" <?php if ($begin_time == 870) { ?> selected="selected" <?php } ?>>02:30 PM</option>

                                <option value="900" <?php if ($begin_time == 900) { ?> selected="selected" <?php } ?>>03:00 PM</option>
                                <option value="930" <?php if ($begin_time == 930) { ?> selected="selected" <?php } ?>>03:30 PM</option>

                                <option value="960" <?php if ($begin_time == 960) { ?> selected="selected" <?php } ?>>04:00 PM</option>
                                <option value="990" <?php if ($begin_time == 990) { ?> selected="selected" <?php } ?>>04:30 PM</option>

                                <option value="1020" <?php if ($begin_time == 1020) { ?> selected="selected" <?php } ?>>05:00 PM</option>
                                <option value="1050" <?php if ($begin_time == 1050) { ?> selected="selected" <?php } ?>>05:30 PM</option>

                                <option value="1080" <?php if ($begin_time == 1080) { ?> selected="selected" <?php } ?>>06:00 PM</option>
                                <option value="1110" <?php if ($begin_time == 1110) { ?> selected="selected" <?php } ?>>06:30 PM</option>

                                <option value="1140" <?php if ($begin_time == 1140) { ?> selected="selected" <?php } ?>>07:00 PM</option>
                                <option value="1170" <?php if ($begin_time == 1170) { ?> selected="selected" <?php } ?>>07:30 PM</option>

                                <option value="1200" <?php if ($begin_time == 1200) { ?> selected="selected" <?php } ?>>08:00 PM</option>
                                <option value="1230" <?php if ($begin_time == 1230) { ?> selected="selected" <?php } ?>>08:30 PM</option>

                                <option value="1260" <?php if ($begin_time == 1260) { ?> selected="selected" <?php } ?>>09:00 PM</option>
                                <option value="1290" <?php if ($begin_time == 1290) { ?> selected="selected" <?php } ?>>09:30 PM</option>

                                <option value="1320" <?php if ($begin_time == 1320) { ?> selected="selected" <?php } ?>>10:00 PM</option>
                                <option value="1350" <?php if ($begin_time == 1350) { ?> selected="selected" <?php } ?>>10:30 PM</option>

                                <option value="1380" <?php if ($begin_time == 1380) { ?> selected="selected" <?php } ?>>11:00 PM</option>
                                <option value="1410" <?php if ($begin_time == 1410) { ?> selected="selected" <?php } ?>>11:30 PM</option>

                                <option value="1440" <?php if ($begin_time == 1440) { ?> selected="selected" <?php } ?>>11:59 PM</option>

                            </select>         
                            <?php /* ?><select name="begin_time" id="begin_time" class="span3">

                              <?php for ($x = 0; $x <= 23; $x++) {
                              $H = $x % 24;
                              $ap = ($H < 12)? "AM" : "PM";
                              $h = $H % 12;

                              if($h==0) {
                              echo "<option value='".str_pad($H, 2, "0", STR_PAD_LEFT).":00:00'> "
                              .str_pad(12, 2, "0", STR_PAD_LEFT).":00 $ap</option>\n";
                              echo "<option value='".str_pad($H, 2, "0", STR_PAD_LEFT).":30:00'> "
                              .str_pad(12, 2, "0", STR_PAD_LEFT).":30 $ap</option>\n";
                              } else {
                              echo "<option value='".str_pad($H, 2, "0", STR_PAD_LEFT).":00:00'> "
                              .str_pad($h, 2, "0", STR_PAD_LEFT).":00 $ap</option>\n";
                              echo "<option value='".str_pad($H, 2, "0", STR_PAD_LEFT).":30:00'> "
                              .str_pad($h, 2, "0", STR_PAD_LEFT).":30 $ap</option>\n";
                              }
                              }
                              echo "<option value='".str_pad(23, 2, "0", STR_PAD_LEFT).":59:59'> "
                              .str_pad(11, 2, "0", STR_PAD_LEFT).":59 $ap</option>\n";
                              ?>
                              </select><?php */ ?>

                            <select name="end_time" id="end_time" class="span3">

                                <option value="00" <?php if ($end_time == 0) { ?> selected="selected" <?php } ?>>12:00 AM</option>       
                                <option value="30" <?php if ($end_time == 30) { ?> selected="selected" <?php } ?>>12:30 AM</option>            

                                <option value="60" <?php if ($end_time == 60) { ?> selected="selected" <?php } ?>>01:00 AM</option>
                                <option value="90" <?php if ($end_time == 90) { ?> selected="selected" <?php } ?>>01:30 AM</option>

                                <option value="120" <?php if ($end_time == 120) { ?> selected="selected" <?php } ?>>02:00 AM</option>
                                <option value="150" <?php if ($end_time == 150) { ?> selected="selected" <?php } ?>>02:30 AM</option>

                                <option value="180" <?php if ($end_time == 180) { ?> selected="selected" <?php } ?>>03:00 AM</option>
                                <option value="210" <?php if ($end_time == 210) { ?> selected="selected" <?php } ?>>03:30 AM</option>

                                <option value="240" <?php if ($end_time == 240) { ?> selected="selected" <?php } ?>>04:00 AM</option>
                                <option value="270" <?php if ($end_time == 270) { ?> selected="selected" <?php } ?>>04:30 AM</option>

                                <option value="300" <?php if ($end_time == 300) { ?> selected="selected" <?php } ?>>05:00 AM</option>
                                <option value="330" <?php if ($end_time == 330) { ?> selected="selected" <?php } ?>>05:30 AM</option>

                                <option value="360" <?php if ($end_time == 360) { ?> selected="selected" <?php } ?>>06:00 AM</option>
                                <option value="390" <?php if ($end_time == 390) { ?> selected="selected" <?php } ?>>06:30 AM</option>

                                <option value="420" <?php if ($end_time == 420) { ?> selected="selected" <?php } ?>>07:00 AM</option>
                                <option value="450" <?php if ($end_time == 450) { ?> selected="selected" <?php } ?>>07:30 AM</option>

                                <option value="480" <?php if ($end_time == 480) { ?> selected="selected" <?php } ?>>08:00 AM</option>
                                <option value="510" <?php if ($end_time == 510) { ?> selected="selected" <?php } ?>>08:30 AM</option>

                                <option value="540" <?php if ($end_time == 540) { ?> selected="selected" <?php } ?>>09:00 AM</option>
                                <option value="570" <?php if ($end_time == 570) { ?> selected="selected" <?php } ?>>09:30 AM</option>

                                <option value="600" <?php if ($end_time == 600) { ?> selected="selected" <?php } ?>>10:00 AM</option>
                                <option value="630" <?php if ($end_time == 630) { ?> selected="selected" <?php } ?>>10:30 AM</option>

                                <option value="660" <?php if ($end_time == 660) { ?> selected="selected" <?php } ?>>11:00 AM</option>
                                <option value="690" <?php if ($end_time == 690) { ?> selected="selected" <?php } ?>>11:30 AM</option>

                                <option value="720" <?php if ($end_time == 720) { ?> selected="selected" <?php } ?>>12:00 PM</option>
                                <option value="750" <?php if ($end_time == 750) { ?> selected="selected" <?php } ?>>12:30 PM</option>

                                <option value="780" <?php if ($end_time == 780) { ?> selected="selected" <?php } ?>>01:00 PM</option>
                                <option value="810" <?php if ($end_time == 810) { ?> selected="selected" <?php } ?>>01:30 PM</option>

                                <option value="840" <?php if ($end_time == 840) { ?> selected="selected" <?php } ?>>02:00 PM</option>
                                <option value="870" <?php if ($end_time == 870) { ?> selected="selected" <?php } ?>>02:30 PM</option>

                                <option value="900" <?php if ($end_time == 900) { ?> selected="selected" <?php } ?>>03:00 PM</option>
                                <option value="930" <?php if ($end_time == 930) { ?> selected="selected" <?php } ?>>03:30 PM</option>

                                <option value="960" <?php if ($end_time == 960) { ?> selected="selected" <?php } ?>>04:00 PM</option>
                                <option value="990" <?php if ($end_time == 990) { ?> selected="selected" <?php } ?>>04:30 PM</option>

                                <option value="1020" <?php if ($end_time == 1020) { ?> selected="selected" <?php } ?>>05:00 PM</option>
                                <option value="1050" <?php if ($end_time == 1050) { ?> selected="selected" <?php } ?>>05:30 PM</option>

                                <option value="1080" <?php if ($end_time == 1080) { ?> selected="selected" <?php } ?>>06:00 PM</option>
                                <option value="1110" <?php if ($end_time == 1110) { ?> selected="selected" <?php } ?>>06:30 PM</option>

                                <option value="1140" <?php if ($end_time == 1140) { ?> selected="selected" <?php } ?>>07:00 PM</option>
                                <option value="1170" <?php if ($end_time == 1170) { ?> selected="selected" <?php } ?>>07:30 PM</option>

                                <option value="1200" <?php if ($end_time == 1200) { ?> selected="selected" <?php } ?>>08:00 PM</option>
                                <option value="1230" <?php if ($end_time == 1230) { ?> selected="selected" <?php } ?>>08:30 PM</option>

                                <option value="1260" <?php if ($end_time == 1260) { ?> selected="selected" <?php } ?>>09:00 PM</option>
                                <option value="1290" <?php if ($end_time == 1290) { ?> selected="selected" <?php } ?>>09:30 PM</option>

                                <option value="1320" <?php if ($end_time == 1320) { ?> selected="selected" <?php } ?>>10:00 PM</option>
                                <option value="1350" <?php if ($end_time == 1350) { ?> selected="selected" <?php } ?>>10:30 PM</option>

                                <option value="1380" <?php if ($end_time == 1380) { ?> selected="selected" <?php } ?>>11:00 PM</option>
                                <option value="1410" <?php if ($end_time == 1410) { ?> selected="selected" <?php } ?>>11:30 PM</option>

                                <option value="1440" <?php if ($end_time == 1440) { ?> selected="selected" <?php } ?>>11:59 PM</option>
                            </select>


<?php if($break=='' || $break==0) { $break=30; } ?>

                            <select name="break" id="break" class="span2">

                                <option value="00" <?php if ($break == 0) { ?> selected="selected" <?php } ?>>00.00</option>
                                <option value="15" <?php if ($break == 15) { ?> selected="selected" <?php } ?>>00:15</option>               
                                <option value="30" <?php if ($break == 30) { ?> selected="selected" <?php } ?>>00:30</option>    
                                <option value="45" <?php if ($break == 45) { ?> selected="selected" <?php } ?>>00:45</option>    
                                <option value="60" <?php if ($break == 60) { ?> selected="selected" <?php } ?>>01:00</option>    
                                <option value="75" <?php if ($break == 75) { ?> selected="selected" <?php } ?>>01:15</option>               
                                <option value="90" <?php if ($break == 90) { ?> selected="selected" <?php } ?>>01:30</option>    
                                <option value="105" <?php if ($break == 105) { ?> selected="selected" <?php } ?>>01:45</option>    
                                <option value="120" <?php if ($break == 120) { ?> selected="selected" <?php } ?>>02:00</option>   

                            </select>




                            <input type="text" name="hours" id="hours" value="<?php echo $hours; ?>" placeholder="Hours" class="span2" readonly="readonly">





                        </div>
                    </div>
                    <script>
                        $("#begin_time,#end_time,#break").live('change',function(){
												
												
                            var begin_time = $("#begin_time").val();
                            var end_time = $("#end_time").val();
												
                            var break_time = $("#break").val();
												
															
                            //alert(begin_time+"=="+end_time+"=="+break_time);
												
												
                            var eq_time = parseInt(begin_time);
												
                            if(parseInt(end_time)-(parseInt(begin_time)+parseInt(break_time))>0) {
                                var eq_time = parseInt(begin_time)+parseInt(break_time);
                            } else {
                                alert('Please select proper Begin, End time and Break Time');
                                return false;
                            }
				
                                
                               // alert(eq_time);
												
                            var begindate = new Date(), enddate = new Date();
												
                            begindate.setHours(0);
                            begindate.setMinutes(eq_time);
                            begindate.setSeconds(0);
												
												
                            enddate.setHours(0);
                            enddate.setMinutes(end_time);
                            enddate.setSeconds(0);
												
                            if(begindate > enddate) {
                                enddate.setDate(enddate.getDate() + 1)
                            }
                            var diff = (enddate.getTime() - begindate.getTime()) / 1000;
                            
                            //alert(diff);
												
                            var hrs =  parseInt(diff / 3600);
                            diff = diff % 3600;
												
                            var minutes =  parseInt(diff / 60);
												
                            //alert(hrs +"=="+ minutes);
												
                            var fcalc=zeroFill(hrs,2)+":"+zeroFill(minutes,2);
												
                            $("#hours").val(fcalc);
                            
                             calculatecost();
												
												
                        });	
                        function zeroFill( number, width )
                        {
                            width -= number.toString().length;
                            if ( width > 0 )
                            {
                                return new Array( width + (/\./.test( number ) ? 2 : 1) ).join( '0' ) + number;
                            }
                            return number; // always return a string
                        }										
                    </script>

                    <div class="control-group">&nbsp;</div>






                    <div class="control-group">
                        <label for="textfield" class="control-label">Cost(<?php echo $site_setting->currency_symbol; ?>)</label>

                        <div class="controls controls-row" >
                            <input type="text" name="resource_cost" id="resource_cost" placeholder="Cost" class="" value="<?php echo $resource_cost; ?>">
                        </div>
                    </div>





                    <div class="control-group">&nbsp;</div>










                    <div class="form-actions">
                        
                        
                        
<?php if ($timecode_id == '') { ?>
                            <button type="submit" class="button button-basic-blue" id="submitbtn">Save</button>
<?php } else { ?>
                            <button type="submit" class="button button-basic-blue">Save changes</button>
<?php } ?>
                        <button type="button" class="button button-basic" onClick="window.location.href='<?php echo site_url('quotes/managetype'); ?>'">Cancel</button>

                        <input type="hidden" name="timecode_id" id="timecode_id" value="<?php echo $timecode_id; ?>" />
                        <input type="hidden" name="team_wage" id="team_wage" value="0" />


                    </div>


                    </form>
                </div>
            </div>

            <!---add-->










        </div>
    </div>



</div>



<script type="text/javascript">
    $(function() {
        $('.datetimepicker').datetimepicker({
            format: 'mm-dd-yyyy'
        });
	
        $("#submitbtn").removeClass('ui-state-disabled').removeAttr('disabled');
        
        $("#team_id").live("change",function(){
           
           var team_id=$("#team_id option:selected").val();
           
           
           
           if(team_id!='' && team_id!=null && team_id!=undefined){
              
              
               var res = $.ajax({						
                    type: 'POST',
                    url: '<?php echo site_url('quotes/getemployeewage/'); ?>',
                    data: {team_id:team_id},
                    dataType: 'html', 
                    cache: false,
                    async: false                     
                }).responseText;							
			 
		
                
                var wage = parseFloat(res);
                
                $("#team_wage").val(wage);
                
                
                calculatecost();
        
       
              
              
           } else {
               $("#resource_cost").val(0);
           }
           
        });
        
     	
    });	
    
     
        function calculatecost(){
            
            var wage=parseFloat($("#team_wage").val());
            var totalhours=$("#hours").val();
            
            
            var tsplit=totalhours.split(":");
            var thours=parseFloat(tsplit[0]);
            var tminute=parseFloat(tsplit[1]);
            
            //alert(thours+"=="+tminute);
            
            var total_cost=0;
            
            var min_wage=0;
            
            total_cost=wage * thours;
            
            if(tminute>0){
              
              
                if(tminute==15){
                    min_wage=wage/4;
                } else if(tminute==30){
                    min_wage=wage/2;
                } else if(tminute==45){
                    min_wage= ((wage * 3)/4);
                }

            }
            
            total_cost= total_cost + min_wage;
            
            
            if(total_cost==NaN){
                total_cost=0;
            }
            
             $("#resource_cost").val(total_cost);
			
            
            
            
        }
</script>  