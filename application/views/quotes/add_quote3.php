<?php $site_setting = site_setting(); ?>
<!-- datepicker plugin -->
<link rel="stylesheet" href="<?php echo base_url(); ?>css/bootstrap-datetimepicker.min.css">


<!-- datepicker plugin -->
<script src="<?php echo base_url(); ?>js/bootstrap-datetimepicker.min.js"></script>



<div class="page-header">
    <div class="pull-left">
        <h4><i class="icon-cogs"></i> <?php if ($quote_id != '') { ?>Edit Quote<?php } else { ?>Add Quote<?php } ?></h4>
    </div>
    <div class="pull-right">
        <ul class="bread">
            <li><a href="<?php echo site_url('quotes/manage'); ?>">Manage Quotes</a><span class="divider">/</span></li>
            <li class="active">Quotes</li>
        </ul>
    </div>
</div>


<div class="container-fluid" id="content-area">

    <div class="row-fluid">
        <div class="span12">

            <?php if ($error != '') { ?>

                <div class="alert alert-error">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    <strong>Warning !</strong> <?php echo $error; ?>
                </div>    <?php } ?>


            <?php if ($msg != '') { ?>
                <div class="alert <?php if ($msg == 'notfound' || $msg == 'cannot') { ?>alert-danger<?php } else { ?>alert-success<?php } ?>">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    <strong><?php if ($msg == 'notfound' || $msg == 'cannot') { ?>Warning<?php } else { ?>Success<?php } ?>!</strong>

                    <?php if ($msg == 'delete') { ?>Quote has been deleted successfully. <?php } ?>	
                    <?php if ($msg == 'insert') { ?>New Quote has been added successfully. <?php } ?>	
                    <?php if ($msg == 'update') { ?>Quote detail has been updated successfully. <?php } ?>	
                    <?php if ($msg == 'notfound') { ?>Quote records not found. <?php } ?>
                    <?php if ($msg == 'cannot') { ?>Cannot delete Quote because it used in many records. <?php } ?>		

                </div> 
            <?php } ?>	              

            <div class="box">
                <div class="box-head">
                    <i class="icon-list-ul"></i>
                    <span>Change Quote details</span>

                    <?php
                    $data['quote_details'] = $quote_details;
                    $this->load->view('quotes/quote_title', $data);
                    ?>
                </div>
                <div class="box-body box-body-nopadding">
                    <?php
                    $attributes = array('name' => 'frm_addquote', 'class' => 'form-horizontal form-bordered form-wizard');
                    echo form_open('quotes/step3/' . $quote_id, $attributes);
                    ?> 

                    <?php $this->load->view('quotes/quote_buttons', $data); ?>

                    <div class="step" id="firstStep" style="opacity:1 !important;">


                        <!-----steps--->   
                        <?php
                        $data['step_index'] = 3;
                        $this->load->view('quotes/quote_steps', $data);
                        ?>
                        <!-----steps--->





                        <style>
                            .marAll{ margin:0px 0px 0px 8px !important; }
                            .marAll2{ margin:0px 8px 0px 0px !important; }
                            .marL0 { margin:0px !important; }
                            .ht0 { height: 0px; }
                            .ht15 { height: 15px; }
                            .ht40 { height: 40px; }
                            .ht65 { height: 65px; }
                            .padT10 { padding-top:10px; }
                            .padT20 { padding-top:20px; }
                            .padB20 { padding-bottom:20px; }
                            .pad10 { padding: 10px; }
                            .roommain { border: 2px solid #ADADAD; margin-bottom:2px; }
                            .borT { border-top: 1px solid #ccc; } 
                            .borB { border-bottom: 1px solid #ccc; }
                            #maindiv .controls { margin:0px !important; }
                            .pad6 { padding: 6px; }
                        </style>             

                        <br /><br />
                        
                        
                        <?php $jobrate=$quote_details->rate; ?>

                        <div class="row-fluid">

                            <!---commited features--->
                            <div class="span6">
                                
                                
                                
                                <!--base quote start-->
                                
                                <div class="pad6" style="">
                                
                                <div align="center" class="padB20" style="font-weight:bold;">Base Totals</div>


                                <?php
                                $total_commited_hours = $total_commit_hours;
                                //+ $total_commit_prep + $total_commit_closets + $total_commit_misc + $total_commit_setclean;

                                $total_commited_labour_costs = $total_commited_hours * $jobrate;
                                ?>

                                <!---upper top--->
                                <div class="controls controls-row marAll padT10 borT">   
                                    <label class="span6">Total Hours :</label>
                                    <input type="text" value="<?php echo $total_commited_hours; ?>" name="totalcommitedhours" id="totalcommitedhours" placeholder="0" class="span6" readonly="readonly">
                                </div>
                                <div class="controls controls-row marAll padT10">          
                                    <label class="span6">Job Rate(<?php echo $site_setting->currency_symbol; ?>) :</label>
                                    <input type="text" value="<?php echo $jobrate; ?>" name="jobrate" id="jobrate" placeholder="0" class="span6" readonly="readonly">
                                </div>

                                <div class="controls controls-row marAll padT10">          
                                    <label class="span6">Labour Costs(<?php echo $site_setting->currency_symbol; ?>) :</label>
                                    <input type="text" value="<?php echo $total_commited_labour_costs; ?>" name="total_commited_labour_costs" id="total_commited_labour_costs"  placeholder="0" class="span6" readonly="readonly">
                                </div>

                                <div class="controls controls-row marAll padT10">          
                                    <label class="span6">Gross Material(w.PST)(<?php echo $site_setting->currency_symbol; ?>) :</label>
                                    <input type="text" value="<?php echo $total_commit_material; ?>" name="total_commit_material" id="total_commit_material" placeholder="0" class="span6" readonly="readonly">
                                </div>
                                <!---upper top--->


                                <!---other service-->


                                <div class="controls controls-row marAll padT10 borT">   
                                    <select class="span6" name="AdditionalCostDesc1" id="AdditionalCostDesc1">
                                        <!-- <option value="">Select Service 1</option>-->
                                        <?php if (!empty($other_service)) {
                                            foreach ($other_service as $key => $service) { ?>
                                                <option value="<?php echo $key; ?>" <?php if ($quote_details->AdditionalCostDesc1 == $key) {
                                            echo "selected";
                                        } ?>><?php echo ucfirst($service); ?></option>
    <?php }
} ?>
                                    </select>







                                    <div class="span6 input-append input-prepend">
                                        <span class="add-on"><?php echo $site_setting->currency_symbol; ?></span>
                                        <input class="input-small" id="AdditionalCostAmt1" name="AdditionalCostAmt1" type="text" value="<?php echo $quote_details->AdditionalCostAmt1; ?>"  />
                                        <div class="btn-group">
                                            <button class="button button-basic dropdown-toggle" data-toggle="dropdown">
                                                Select
                                                <span class="caret"></span>
                                            </button>
                                            <ul class="dropdown-menu cstdropmenu">

<?php if (!empty($other_service_pay)) {
    foreach ($other_service_pay as $key => $pay) { ?>
                                                        <li><a href="javascript:void(0)" data-field="AdditionalCostAmt1" data-val="<?php echo $pay; ?>"><?php echo $pay; ?></a></li>


    <?php }
} ?>

                                            </ul>
                                        </div>
                                    </div>







                                </div>

                                <div class="controls controls-row marAll padT10">   
                                    <select class="span6" name="AdditionalCostDesc2" id="AdditionalCostDesc2">
                                        <!--                                   <option value="">Select Service 2</option>-->
<?php if (!empty($other_service)) {
    foreach ($other_service as $key => $service) { ?>
                                                <option value="<?php echo $key; ?>" <?php if ($quote_details->AdditionalCostDesc2 == $key) {
            echo "selected";
        } ?>><?php echo ucfirst($service); ?></option>
    <?php }
} ?>
                                    </select>



                                    <div class="span6 input-append input-prepend">
                                        <span class="add-on"><?php echo $site_setting->currency_symbol; ?></span>
                                        <input class="input-small" id="AdditionalCostAmt2" name="AdditionalCostAmt2" type="text" value="<?php echo $quote_details->AdditionalCostAmt2; ?>"   />
                                        <div class="btn-group">
                                            <button class="button button-basic dropdown-toggle" data-toggle="dropdown">
                                                Select
                                                <span class="caret"></span>
                                            </button>
                                            <ul class="dropdown-menu cstdropmenu">

<?php if (!empty($other_service_pay)) {
    foreach ($other_service_pay as $key => $pay) { ?>
                                                        <li><a href="javascript:void(0)" data-field="AdditionalCostAmt2" data-val="<?php echo $pay; ?>"><?php echo $pay; ?></a></li>


    <?php }
} ?>

                                            </ul>
                                        </div>
                                    </div>       






                                </div>

                                <div class="controls controls-row marAll padT10">   
                                    <select class="span6" name="AdditionalCostDesc3" id="AdditionalCostDesc3">
                                        <!--                                   <option value="">Select Service 3</option>-->
<?php if (!empty($other_service)) {
    foreach ($other_service as $key => $service) { ?>
                                                <option value="<?php echo $key; ?>" <?php if ($quote_details->AdditionalCostDesc3 == $key) {
            echo "selected";
        } ?>><?php echo ucfirst($service); ?></option>
    <?php }
} ?>
                                    </select>






                                    <div class="span6 input-append input-prepend">
                                        <span class="add-on"><?php echo $site_setting->currency_symbol; ?></span>
                                        <input class="input-small" id="AdditionalCostAmt3" name="AdditionalCostAmt3" type="text" value="<?php echo $quote_details->AdditionalCostAmt3; ?>"  />
                                        <div class="btn-group">
                                            <button class="button button-basic dropdown-toggle" data-toggle="dropdown">
                                                Select
                                                <span class="caret"></span>
                                            </button>
                                            <ul class="dropdown-menu cstdropmenu">

<?php if (!empty($other_service_pay)) {
    foreach ($other_service_pay as $key => $pay) { ?>
                                                        <li><a href="javascript:void(0)" data-field="AdditionalCostAmt3" data-val="<?php echo $pay; ?>"><?php echo $pay; ?></a></li>


    <?php }
} ?>

                                            </ul>
                                        </div>
                                    </div>










                                </div>

                                <div class="controls controls-row marAll padT10 ">   
                                    <select class="span6" name="AdditionalCostDesc4" id="AdditionalCostDesc4">
                                        <!--                                   <option value="">Select Service 4</option>-->
<?php if (!empty($other_service)) {
    foreach ($other_service as $key => $service) { ?>
                                                <option value="<?php echo $key; ?>" <?php if ($quote_details->AdditionalCostDesc4 == $key) {
            echo "selected";
        } ?>><?php echo ucfirst($service); ?></option>
    <?php }
} ?>
                                    </select>




                                    <div class="span6 input-append input-prepend">
                                        <span class="add-on"><?php echo $site_setting->currency_symbol; ?></span>
                                        <input class="input-small" id="AdditionalCostAmt4" name="AdditionalCostAmt4" type="text"  value="<?php echo $quote_details->AdditionalCostAmt4; ?>"  />
                                        <div class="btn-group">
                                            <button class="button button-basic dropdown-toggle" data-toggle="dropdown">
                                                Select
                                                <span class="caret"></span>
                                            </button>
                                            <ul class="dropdown-menu cstdropmenu">

<?php if (!empty($other_service_pay)) {
    foreach ($other_service_pay as $key => $pay) { ?>
                                                        <li><a href="javascript:void(0)" data-field="AdditionalCostAmt4" data-val="<?php echo $pay; ?>"><?php echo $pay; ?></a></li>


    <?php }
} ?>

                                            </ul>
                                        </div>
                                    </div>








                                </div>


                                <div class="controls controls-row marAll padT10 padB20 borB">   
                                    <select class="span6" name="AdditionalCostDesc5" id="AdditionalCostDesc5">
                                        <!--                                   <option value="">Select Service 5</option>-->
<?php if (!empty($other_service)) {
    foreach ($other_service as $key => $service) { ?>
                                                <option value="<?php echo $key; ?>" <?php if ($quote_details->AdditionalCostDesc5 == $key) {
                                                    echo "selected";
                                                } ?>><?php echo ucfirst($service); ?></option>
    <?php }
} ?>
                                    </select>



                                    <div class="span6 input-append input-prepend">
                                        <span class="add-on"><?php echo $site_setting->currency_symbol; ?></span>
                                        <input class="input-small" id="AdditionalCostAmt5" name="AdditionalCostAmt5" type="text" value="<?php echo $quote_details->AdditionalCostAmt5; ?>"   />
                                        <div class="btn-group">
                                            <button class="button button-basic dropdown-toggle" data-toggle="dropdown">
                                                Select
                                                <span class="caret"></span>
                                            </button>
                                            <ul class="dropdown-menu cstdropmenu">

                                <?php if (!empty($other_service_pay)) {
                                    foreach ($other_service_pay as $key => $pay) { ?>
                                                        <li><a href="javascript:void(0)" data-field="AdditionalCostAmt5" data-val="<?php echo $pay; ?>"><?php echo $pay; ?></a></li>


                                    <?php }
                                } ?>

                                            </ul>
                                        </div>
                                    </div>




                                </div>



                                <!---other service-->

<?php /* ?><div class="controls controls-row marAll padT10 borT">   
  <select class="span6">
  <option value="">Select Service 5</option>
  <?php if(!empty($other_task)) {
  foreach($other_task as $key=>$task) {?>
  <option value="<?php echo $task;?>"><?php echo ucfirst($task);?></option>
  <?php } } ?>
  </select>
  <select class="span4">
  <option value="0">0</option>
  <?php for($i=40;$i<=65;$i++){?>
  <option value="<?php echo $i;?>"><?php echo $site_setting->currency_symbol.$i;?></option>
  <?php } ?>
  </select>
  <label class="span2">Per Hour</label>
  </div><?php */ ?>


                                <div class="controls controls-row marAll padT10 borT">          
                                    <label class="span6">Budget T & M :</label>
                                    <input type="text" value="<?php echo $quote_details->budget_time_material; ?>" name="budget_time_material" id="budget_time_material" placeholder="0" class="span4">
                                    <label class="span2"></label>
                                </div>


                                <?php $calculate_commit_total= $total_commited_labour_costs + $total_commit_material + $quote_details->AdditionalCostAmt1 + $quote_details->AdditionalCostAmt2 + $quote_details->AdditionalCostAmt3 + $quote_details->AdditionalCostAmt4 + $quote_details->AdditionalCostAmt5; ?>
                                

                                <div class="controls controls-row marAll padT10 ">          
                                    <label class="span6">Computed Total(<?php echo $site_setting->currency_symbol; ?>) :</label>
                                    <input type="text" value="<?php echo $calculate_commit_total; ?>" name="CalculatedTotal" id="CalculatedTotal" placeholder="0" class="span6" readonly="" >
                                </div>
                                
                                
                                <?php
                                    
                                if($quote_details->AdjustedTotal>0){
                                    $AdjustedTotal=$quote_details->AdjustedTotal;
                                } else {
                                    $AdjustedTotal=$calculate_commit_total;
                                }
                                
                                
                                ?>
                                

                                <div class="controls controls-row marAll padT10">          
                                    <label class="span6">Sell Amount(<?php echo $site_setting->currency_symbol; ?>) :</label>
                                    <input type="text" value="<?php echo $AdjustedTotal; ?>" name="AdjustedTotal" id="AdjustedTotal" placeholder="0" class="span6" >
                                </div>

                                <div class="controls controls-row marAll padT10 borB">          
                                    <label class="span6">Discount on Labour :</label>
                                    <select class="span6" name="DiscountRate" id="DiscountRate">
<?php if (!empty($all_discount)) {
    foreach ($all_discount as $key => $disc) { ?>
                                        <option value="<?php echo $disc; ?>" <?php if($quote_details->DiscountRate==$disc){ ?> selected <?php } ?>><?php echo $disc . '%'; ?></option>
    <?php }
} ?>
                                    </select>
                                </div>



                                <!----compute part--->

                                <?php
                                    
                                if($quote_details->SellAmount>0){
                                    $SellAmount=$quote_details->SellAmount;
                                } else {
                                    $SellAmount=$calculate_commit_total;
                                }
                                
                                
                                ?>
                                
                                
                                <div class="controls controls-row marAll padT10 borT">          
                                    <label class="span6" style="font-weight:bold; font-size:14px;">Final Quote(<?php echo $site_setting->currency_symbol; ?>) :</label>
                                    <input type="text" value="<?php echo $SellAmount; ?>" name="SellAmount" id="SellAmount" placeholder="0" class="span6" readonly>
                                </div>


                                <div class="controls controls-row marAll padT10">          
                                    <label class="span6" style="font-weight:bold; font-size:14px;">LDH(<?php echo $site_setting->currency_symbol; ?>) :</label>
                                    <input type="text" value="<?php echo $quote_details->LDH; ?>" name="LDH" id="LDH" placeholder="0" class="span6" readonly>
                                </div>

                                <!----compute part--->

                                
                                
                                </div><!---base quote -->


                            </div>
                            <!---commited features--->



                            <!---optional features--->
                            <div class="span6">
                                
                                
                                <div align="center" class="padB20" style="font-weight:bold;"><a href="<?php echo site_url('clients/quotes/'.$quote_details->client_id); ?>" style="color:#3570cf;" target="_blank">LOOK UP</a></div>
                                
                                

<!--                            <div align="center" class="padB20" style="font-weight:bold;">Optional - Materials</div>

                                <div class="controls controls-row borB marAll2 pad6">      
                                    <div class="span2">Total Hours</div>
                                    <div class="span3">Job Rate(<?php //echo $site_setting->currency_symbol; ?>)</div>
                                    <div class="span3">Labour Costs(<?php //echo $site_setting->currency_symbol; ?>)</div>
                                    <div class="span4">Gross Material(w.PST)(<?php //echo $site_setting->currency_symbol; ?>)</div>
                                </div>

                                <div class="controls controls-row marAll2 pad6" style="background: #E3FFD9;">                                       
                                    <input class="span2" value="10" type="text" readonly="readonly" />
                                    <input class="span3" value="25" type="text" readonly="readonly" />
                                    <input class="span3" value="250" type="text" readonly="readonly" />
                                    <input class="span4" value="2500" type="text" readonly="readonly" />
                                </div>

                                <div class="controls controls-row marAll2 pad6" style="background: #FDDEC1;">                                       
                                    <input class="span2" value="10" type="text" readonly="readonly" />
                                    <input class="span3" value="25" type="text" readonly="readonly" />
                                    <input class="span3" value="250" type="text" readonly="readonly" />
                                    <input class="span4" value="2500" type="text" readonly="readonly" />
                                </div>

                                <div class="controls controls-row marAll2 pad6" style="background: #DFF6FF;">                                       
                                    <input class="span2" value="10" type="text" readonly="readonly" />
                                    <input class="span3" value="25" type="text" readonly="readonly" />
                                    <input class="span3" value="250" type="text" readonly="readonly" />
                                    <input class="span4" value="2500" type="text" readonly="readonly" />
                                </div>

                                <div class="controls controls-row marAll2 pad6" style="background: #FCDFDF;">                                       
                                    <input class="span2" value="10" type="text" readonly="readonly" />
                                    <input class="span3" value="25" type="text" readonly="readonly" />
                                    <input class="span3" value="250" type="text" readonly="readonly" />
                                    <input class="span4" value="2500" type="text" readonly="readonly" />
                                </div>-->




<!---optinon 1 start -->
<div class="pad6" style="background: #E3FFD9;">       
            <!---upper top 1--->

            <input type="checkbox" name="confirm_opt1" id="confirm_opt1" value="1" <?php if($quote_details->confirm_opt1==1){ ?> checked <?php } ?> /><div align="center" class="padB20 opttitle" data-id="divopt1" style="font-weight:bold;">Optional 1 - Materials <i class="icon-sort-down"></i></div>


     <div id="divopt1" class="optdiv" style="display:none;">

<?php
$total_optionaled_hours_1 = $total_optional_hours_1; //$total_optional_prep + $total_optional_closets + $total_optional_misc + $total_optional_setclean;

$total_optionaled_labour_costs_1 = $total_optionaled_hours_1 * $jobrate;
?>


                              
                                <div class="controls controls-row marAll2 padT10 borT">   
                                    <label class="span6">Total Hours :</label>
                                    <input type="text" value="<?php echo $total_optionaled_hours_1; ?>" name="total_optionaled_hours_1" id="total_optionaled_hours_1" placeholder="0" class="span6" readonly="readonly">
                                </div>
                                <div class="controls controls-row marAll2 padT10">          
                                    <label class="span6">Job Rate(<?php echo $site_setting->currency_symbol; ?>) :</label>
                                    <input type="text" value="<?php echo $jobrate; ?>" name="rate" id="rate" placeholder="0" class="span6" readonly="readonly" >
                                </div>

                                <div class="controls controls-row marAll2 padT10">          
                                    <label class="span6">Labour Costs(<?php echo $site_setting->currency_symbol; ?>) :</label>
                                    <input type="text" value="<?php echo $total_optionaled_labour_costs_1; ?>" name="total_optionaled_labour_costs_1" id="total_optionaled_labour_costs_1" placeholder="0" class="span6" readonly="readonly">
                                </div>

                                <div class="controls controls-row marAll2 padT10">          
                                    <label class="span6">Gross Material(w.PST)(<?php echo $site_setting->currency_symbol; ?>) :</label>
                                    <input type="text" value="<?php echo $total_optional_material_1; ?>" name="total_optional_material_1" id="total_optional_material_1" placeholder="0" class="span6" readonly="readonly">
                                </div>
                                
                                
                                
                                <!---upper top 1--->

  
                                <!---other service 1-->

                                <div class="controls controls-row marAll2 padT10 borT">   
                                    <select class="span6" name="OptionalCostDesc11" id="OptionalCostDesc11">
<!--                                        <option value="">Select Service 1</option>-->
<?php if (!empty($other_service)) {
    foreach ($other_service as $key => $service) { ?>
                                                <option value="<?php echo $key; ?>"  <?php if ($quote_details->OptionalCostDesc11 == $key) {
                                                    echo "selected";
                                                } ?>><?php echo ucfirst($service); ?></option>
    <?php }
} ?>
                                    </select>



<div class="span6 input-append input-prepend">
<span class="add-on"><?php echo $site_setting->currency_symbol; ?></span>
<input class="input-small" id="OptionalCostAmt11" name="OptionalCostAmt11" type="text" value="<?php echo $quote_details->OptionalCostAmt11; ?>"  />
<div class="btn-group">
<button class="button button-basic dropdown-toggle" data-toggle="dropdown">
Select
<span class="caret"></span>
</button>
<ul class="dropdown-menu cstdropmenu">

<?php if (!empty($other_service_pay)) {
foreach ($other_service_pay as $key => $pay) { ?>
<li><a href="javascript:void(0)" data-field="OptionalCostAmt11" data-val="<?php echo $pay; ?>"><?php echo $pay; ?></a></li>
<?php }
} ?>

</ul>
</div>
</div>

</div>

                                <div class="controls controls-row marAll2 padT10">   
                                    <select class="span6" name="OptionalCostDesc12" id="OptionalCostDesc12">
<!--                                        <option value="">Select Service 2</option>-->
                                                <?php if (!empty($other_service)) {
                                                    foreach ($other_service as $key => $service) { ?>
                                                <option value="<?php echo $key; ?>" <?php if ($quote_details->OptionalCostDesc12 == $key) {
                                                            echo "selected";
                                                        } ?>><?php echo ucfirst($service); ?></option>
    <?php }
} ?>
                                    </select>


                                    <div class="span6 input-append input-prepend">
                                        <span class="add-on"><?php echo $site_setting->currency_symbol; ?></span>
                                        <input class="input-small" id="OptionalCostAmt12" name="OptionalCostAmt12" type="text" value="<?php echo $quote_details->OptionalCostAmt12; ?>"  />
                                        <div class="btn-group">
                                            <button class="button button-basic dropdown-toggle" data-toggle="dropdown">
                                                Select
                                                <span class="caret"></span>
                                            </button>
                                            <ul class="dropdown-menu cstdropmenu">

<?php if (!empty($other_service_pay)) {
    foreach ($other_service_pay as $key => $pay) { ?>
                                                        <li><a href="javascript:void(0)" data-field="OptionalCostAmt12" data-val="<?php echo $pay; ?>"><?php echo $pay; ?></a></li>
    <?php }
} ?>

                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                
                                

                                <div class="controls controls-row marAll2 padT10">   
                                    <select class="span6" name="OptionalCostDesc13" id="OptionalCostDesc13">
<!--                                        <option value="">Select Service 3</option>-->
<?php if (!empty($other_service)) {
    foreach ($other_service as $key => $service) { ?>
                                                <option value="<?php echo $key; ?>" <?php if ($quote_details->OptionalCostDesc13 == $key) {
            echo "selected";
        } ?>><?php echo ucfirst($service); ?></option>
    <?php }
} ?>
                                    </select>

                                    <div class="span6 input-append input-prepend">
                                        <span class="add-on"><?php echo $site_setting->currency_symbol; ?></span>
                                        <input class="input-small" id="OptionalCostAmt13" name="OptionalCostAmt13" type="text" value="<?php echo $quote_details->OptionalCostAmt13; ?>"  />
                                        <div class="btn-group">
                                            <button class="button button-basic dropdown-toggle" data-toggle="dropdown">
                                                Select
                                                <span class="caret"></span>
                                            </button>
                                            <ul class="dropdown-menu cstdropmenu">

<?php if (!empty($other_service_pay)) {
    foreach ($other_service_pay as $key => $pay) { ?>
                                                        <li><a href="javascript:void(0)" data-field="OptionalCostAmt13" data-val="<?php echo $pay; ?>"><?php echo $pay; ?></a></li>
    <?php }
} ?>

                                            </ul>
                                        </div>
                                    </div>

                                </div>

                                <div class="controls controls-row marAll2 padT10 ">   
                                    <select class="span6" name="OptionalCostDesc14" id="OptionalCostDesc14">
<!--                                        <option value="">Select Service 4</option>-->
<?php if (!empty($other_service)) {
    foreach ($other_service as $key => $service) { ?>
                                                <option value="<?php echo $key; ?>" <?php if ($quote_details->OptionalCostDesc14 == $key) {
            echo "selected";
        } ?>><?php echo ucfirst($service); ?></option>
    <?php }
} ?>
                                    </select>

                                    <div class="span6 input-append input-prepend">
                                        <span class="add-on"><?php echo $site_setting->currency_symbol; ?></span>
                                        <input class="input-small" id="OptionalCostAmt14" name="OptionalCostAmt14" type="text" value="<?php echo $quote_details->OptionalCostAmt14; ?>"  />
                                        <div class="btn-group">
                                            <button class="button button-basic dropdown-toggle" data-toggle="dropdown">
                                                Select
                                                <span class="caret"></span>
                                            </button>
                                            <ul class="dropdown-menu cstdropmenu">

<?php if (!empty($other_service_pay)) {
    foreach ($other_service_pay as $key => $pay) { ?>
                                                        <li><a href="javascript:void(0)" data-field="OptionalCostAmt14" data-val="<?php echo $pay; ?>"><?php echo $pay; ?></a></li>
    <?php }
} ?>

                                            </ul>
                                        </div>
                                    </div>

                                </div>


                                <div class="controls controls-row marAll2 padT10 padB20 borB">   
                                    <select class="span6" name="OptionalCostDesc15" id="OptionalCostDesc15">
<!--                                        <option value="">Select Service 5</option>-->
<?php if (!empty($other_service)) {
    foreach ($other_service as $key => $service) { ?>
                                                <option value="<?php echo $key; ?>" <?php if ($quote_details->OptionalCostDesc15 == $key) {
            echo "selected";
        } ?>><?php echo ucfirst($service); ?></option>
    <?php }
} ?>
                                    </select>


                                    <div class="span6 input-append input-prepend">
                                        <span class="add-on"><?php echo $site_setting->currency_symbol; ?></span>
                                        <input class="input-small" id="OptionalCostAmt15" name="OptionalCostAmt15" type="text" value="<?php echo $quote_details->OptionalCostAmt15; ?>"  />
                                        <div class="btn-group">
                                            <button class="button button-basic dropdown-toggle" data-toggle="dropdown">
                                                Select
                                                <span class="caret"></span>
                                            </button>
                                            <ul class="dropdown-menu cstdropmenu">

<?php if (!empty($other_service_pay)) {
    foreach ($other_service_pay as $key => $pay) { ?>
                                                        <li><a href="javascript:void(0)" data-field="OptionalCostAmt15" data-val="<?php echo $pay; ?>"><?php echo $pay; ?></a></li>
    <?php }
} ?>

                                            </ul>
                                        </div>
                                    </div>

                                </div>



                                <!---other service  1-->
                                
                                 <?php $CalculatedOptTotal1 = $total_optionaled_labour_costs_1 + $total_optional_material_1 + $quote_details->OptionalCostAmt11 + $quote_details->OptionalCostAmt12 + $quote_details->OptionalCostAmt13 + $quote_details->OptionalCostAmt14 + $quote_details->OptionalCostAmt15; ?>
                                
                                
                                <div class="controls controls-row marAll2 padT10 borT">          
                                    <label class="span6">Computed Total(<?php echo $site_setting->currency_symbol; ?>) :</label>
                                    <input type="text" value="<?php echo $CalculatedOptTotal1; ?>" name="CalculatedOptTotal1" id="CalculatedOptTotal1" placeholder="0" class="span6" readonly>
                                </div>
                                
                                
                                <?php
                                    
                                if($quote_details->AdjustedOptTotal1>0){
                                    $AdjustedOptTotal1=$quote_details->AdjustedOptTotal1;
                                } else {
                                    $AdjustedOptTotal1=$CalculatedOptTotal1;
                                }
                                
                                
                                ?>

                                <div class="controls controls-row marAll2 padT10">          
                                    <label class="span6">Sell Amount(<?php echo $site_setting->currency_symbol; ?>) :</label>
                                    <input type="text" value="<?php echo $AdjustedOptTotal1; ?>" name="AdjustedOptTotal1" id="AdjustedOptTotal1" placeholder="0" class="span6">
                                </div>

                                <div class="controls controls-row marAll2 padT10 borB">          
                                    <label class="span6">Discount on Labour :</label>
                                    <select class="span6" name="DiscountOptRate1" id="DiscountOptRate1">
<?php if (!empty($all_discount)) {
    foreach ($all_discount as $key => $disc) { ?>
                                                <option value="<?php echo $disc; ?>"  <?php if($quote_details->DiscountOptRate1==$disc){ ?> selected <?php } ?>><?php echo $disc . '%'; ?></option>
    <?php }
} ?>
                                    </select>
                                </div>



                                <!----compute part--->
                                
                                <?php
                                    
                                if($quote_details->SellOptAmount1>0){
                                    $SellOptAmount1=$quote_details->SellOptAmount1;
                                } else {
                                    $SellOptAmount1=$CalculatedOptTotal1;
                                }
                                
                                
                                ?>
                                
                                <div class="controls controls-row marAll2 padT10 borT">          
                                    <label class="span6" style="font-weight:bold; font-size:14px;">Final Quote(<?php echo $site_setting->currency_symbol; ?>) :</label>
                                    <input type="text" value="<?php echo $SellOptAmount1; ?>" name="SellOptAmount1" id="SellOptAmount1" placeholder="0" class="span6" readonly>
                                </div>


                                <div class="controls controls-row marAll2 padT10">          
                                    <label class="span6" style="font-weight:bold; font-size:14px;">LDH(<?php echo $site_setting->currency_symbol; ?>) :</label>
                                    <input type="text" value="<?php echo $quote_details->OptLDH1; ?>" name="OptLDH1" id="OptLDH1" placeholder="0" class="span6" readonly>
                                </div>

                                <!----compute part--->
                                
                                
     </div> <!--optdiv-->
                                
                                </div> <!---optinon 1 complete -->

                                
   

<!---optinon 2 start -->
<div class="pad6" style="background: #FDDEC1;">       
            <!---upper top 1--->

     <input type="checkbox" name="confirm_opt2" id="confirm_opt2" value="1" <?php if($quote_details->confirm_opt2==1){ ?> checked <?php } ?> /><div align="center" class="padB20 opttitle" data-id="divopt2" style="font-weight:bold;">Optional 2 - Materials <i class="icon-sort-down"></i></div>

<div id="divopt2" class="optdiv" style="display:none;">

<?php
$total_optionaled_hours_2 = $total_optional_hours_2; //$total_optional_prep + $total_optional_closets + $total_optional_misc + $total_optional_setclean;

$total_optionaled_labour_costs_2 = $total_optionaled_hours_2 * $jobrate;
?>


                              
                                <div class="controls controls-row marAll2 padT10 borT">   
                                    <label class="span6">Total Hours :</label>
                                    <input type="text" value="<?php echo $total_optionaled_hours_2; ?>" name="total_optionaled_hours_2" id="total_optionaled_hours_2" placeholder="0" class="span6" readonly="readonly">
                                </div>
                                <div class="controls controls-row marAll2 padT10">          
                                    <label class="span6">Job Rate(<?php echo $site_setting->currency_symbol; ?>) :</label>
                                    <input type="text" value="<?php echo $jobrate; ?>" name="rate" id="rate" placeholder="0" class="span6" readonly="readonly" >
                                </div>

                                <div class="controls controls-row marAll2 padT10">          
                                    <label class="span6">Labour Costs(<?php echo $site_setting->currency_symbol; ?>) :</label>
                                    <input type="text" value="<?php echo $total_optionaled_labour_costs_2; ?>" name="total_optionaled_labour_costs_2" id="total_optionaled_labour_costs_2" placeholder="0" class="span6" readonly="readonly">
                                </div>

                                <div class="controls controls-row marAll2 padT10">          
                                    <label class="span6">Gross Material(w.PST)(<?php echo $site_setting->currency_symbol; ?>) :</label>
                                    <input type="text" value="<?php echo $total_optional_material_2; ?>" name="total_optional_material_2" id="total_optional_material_2" placeholder="0" class="span6" readonly="readonly">
                                </div>
                                
                                
                                
                                <!---upper top 1--->

  
                                <!---other service 1-->

                                <div class="controls controls-row marAll2 padT10 borT">   
                                    <select class="span6" name="OptionalCostDesc21" id="OptionalCostDesc21">
<!--                                        <option value="">Select Service 1</option>-->
<?php if (!empty($other_service)) {
    foreach ($other_service as $key => $service) { ?>
                                                <option value="<?php echo $key; ?>"  <?php if ($quote_details->OptionalCostDesc21 == $key) {
                                                    echo "selected";
                                                } ?>><?php echo ucfirst($service); ?></option>
    <?php }
} ?>
                                    </select>



<div class="span6 input-append input-prepend">
<span class="add-on"><?php echo $site_setting->currency_symbol; ?></span>
<input class="input-small" id="OptionalCostAmt21" name="OptionalCostAmt21" type="text" value="<?php echo $quote_details->OptionalCostAmt21; ?>"  />
<div class="btn-group">
<button class="button button-basic dropdown-toggle" data-toggle="dropdown">
Select
<span class="caret"></span>
</button>
<ul class="dropdown-menu cstdropmenu">

<?php if (!empty($other_service_pay)) {
foreach ($other_service_pay as $key => $pay) { ?>
<li><a href="javascript:void(0)" data-field="OptionalCostAmt21" data-val="<?php echo $pay; ?>"><?php echo $pay; ?></a></li>
<?php }
} ?>

</ul>
</div>
</div>

</div>

                                <div class="controls controls-row marAll2 padT10">   
                                    <select class="span6" name="OptionalCostDesc22" id="OptionalCostDesc22">
<!--                                        <option value="">Select Service 2</option>-->
                                                <?php if (!empty($other_service)) {
                                                    foreach ($other_service as $key => $service) { ?>
                                                <option value="<?php echo $key; ?>" <?php if ($quote_details->OptionalCostDesc22 == $key) {
                                                            echo "selected";
                                                        } ?>><?php echo ucfirst($service); ?></option>
    <?php }
} ?>
                                    </select>


                                    <div class="span6 input-append input-prepend">
                                        <span class="add-on"><?php echo $site_setting->currency_symbol; ?></span>
                                        <input class="input-small" id="OptionalCostAmt22" name="OptionalCostAmt22" type="text" value="<?php echo $quote_details->OptionalCostAmt22; ?>"  />
                                        <div class="btn-group">
                                            <button class="button button-basic dropdown-toggle" data-toggle="dropdown">
                                                Select
                                                <span class="caret"></span>
                                            </button>
                                            <ul class="dropdown-menu cstdropmenu">

<?php if (!empty($other_service_pay)) {
    foreach ($other_service_pay as $key => $pay) { ?>
                                                        <li><a href="javascript:void(0)" data-field="OptionalCostAmt22" data-val="<?php echo $pay; ?>"><?php echo $pay; ?></a></li>
    <?php }
} ?>

                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                
                                

                                <div class="controls controls-row marAll2 padT10">   
                                    <select class="span6" name="OptionalCostDesc23" id="OptionalCostDesc23">
<!--                                        <option value="">Select Service 3</option>-->
<?php if (!empty($other_service)) {
    foreach ($other_service as $key => $service) { ?>
                                                <option value="<?php echo $key; ?>" <?php if ($quote_details->OptionalCostDesc23 == $key) {
            echo "selected";
        } ?>><?php echo ucfirst($service); ?></option>
    <?php }
} ?>
                                    </select>

                                    <div class="span6 input-append input-prepend">
                                        <span class="add-on"><?php echo $site_setting->currency_symbol; ?></span>
                                        <input class="input-small" id="OptionalCostAmt23" name="OptionalCostAmt23" type="text" value="<?php echo $quote_details->OptionalCostAmt23; ?>"  />
                                        <div class="btn-group">
                                            <button class="button button-basic dropdown-toggle" data-toggle="dropdown">
                                                Select
                                                <span class="caret"></span>
                                            </button>
                                            <ul class="dropdown-menu cstdropmenu">

<?php if (!empty($other_service_pay)) {
    foreach ($other_service_pay as $key => $pay) { ?>
                                                        <li><a href="javascript:void(0)" data-field="OptionalCostAmt23" data-val="<?php echo $pay; ?>"><?php echo $pay; ?></a></li>
    <?php }
} ?>

                                            </ul>
                                        </div>
                                    </div>

                                </div>

                                <div class="controls controls-row marAll2 padT10 ">   
                                    <select class="span6" name="OptionalCostDesc24" id="OptionalCostDesc24">
<!--                                        <option value="">Select Service 4</option>-->
<?php if (!empty($other_service)) {
    foreach ($other_service as $key => $service) { ?>
                                                <option value="<?php echo $key; ?>" <?php if ($quote_details->OptionalCostDesc24 == $key) {
            echo "selected";
        } ?>><?php echo ucfirst($service); ?></option>
    <?php }
} ?>
                                    </select>

                                    <div class="span6 input-append input-prepend">
                                        <span class="add-on"><?php echo $site_setting->currency_symbol; ?></span>
                                        <input class="input-small" id="OptionalCostAmt24" name="OptionalCostAmt24" type="text" value="<?php echo $quote_details->OptionalCostAmt24; ?>"  />
                                        <div class="btn-group">
                                            <button class="button button-basic dropdown-toggle" data-toggle="dropdown">
                                                Select
                                                <span class="caret"></span>
                                            </button>
                                            <ul class="dropdown-menu cstdropmenu">

<?php if (!empty($other_service_pay)) {
    foreach ($other_service_pay as $key => $pay) { ?>
                                                        <li><a href="javascript:void(0)" data-field="OptionalCostAmt24" data-val="<?php echo $pay; ?>"><?php echo $pay; ?></a></li>
    <?php }
} ?>

                                            </ul>
                                        </div>
                                    </div>

                                </div>


                                <div class="controls controls-row marAll2 padT10 padB20 borB">   
                                    <select class="span6" name="OptionalCostDesc25" id="OptionalCostDesc25">
<!--                                        <option value="">Select Service 5</option>-->
<?php if (!empty($other_service)) {
    foreach ($other_service as $key => $service) { ?>
                                                <option value="<?php echo $key; ?>" <?php if ($quote_details->OptionalCostDesc25 == $key) {
            echo "selected";
        } ?>><?php echo ucfirst($service); ?></option>
    <?php }
} ?>
                                    </select>


                                    <div class="span6 input-append input-prepend">
                                        <span class="add-on"><?php echo $site_setting->currency_symbol; ?></span>
                                        <input class="input-small" id="OptionalCostAmt25" name="OptionalCostAmt25" type="text" value="<?php echo $quote_details->OptionalCostAmt25; ?>"  />
                                        <div class="btn-group">
                                            <button class="button button-basic dropdown-toggle" data-toggle="dropdown">
                                                Select
                                                <span class="caret"></span>
                                            </button>
                                            <ul class="dropdown-menu cstdropmenu">

<?php if (!empty($other_service_pay)) {
    foreach ($other_service_pay as $key => $pay) { ?>
                                                        <li><a href="javascript:void(0)" data-field="OptionalCostAmt25" data-val="<?php echo $pay; ?>"><?php echo $pay; ?></a></li>
    <?php }
} ?>

                                            </ul>
                                        </div>
                                    </div>

                                </div>



                                <!---other service  1-->
                                
                                 <?php $CalculatedOptTotal2 = $total_optionaled_labour_costs_2 + $total_optional_material_2 + $quote_details->OptionalCostAmt21 + $quote_details->OptionalCostAmt22 + $quote_details->OptionalCostAmt23 + $quote_details->OptionalCostAmt24 + $quote_details->OptionalCostAmt25; ?>
                                
                                
                                <div class="controls controls-row marAll2 padT10 borT">          
                                    <label class="span6">Computed Total(<?php echo $site_setting->currency_symbol; ?>) :</label>
                                    <input type="text" value="<?php echo $CalculatedOptTotal2; ?>" name="CalculatedOptTotal2" id="CalculatedOptTotal2" placeholder="0" class="span6" readonly>
                                </div>
                                
                                
                                <?php
                                    
                                if($quote_details->AdjustedOptTotal2>0){
                                    $AdjustedOptTotal2=$quote_details->AdjustedOptTotal2;
                                } else {
                                    $AdjustedOptTotal2=$CalculatedOptTotal2;
                                }
                                
                                
                                ?>

                                <div class="controls controls-row marAll2 padT10">          
                                    <label class="span6">Sell Amount(<?php echo $site_setting->currency_symbol; ?>) :</label>
                                    <input type="text" value="<?php echo $AdjustedOptTotal2; ?>" name="AdjustedOptTotal2" id="AdjustedOptTotal2" placeholder="0" class="span6">
                                </div>

                                <div class="controls controls-row marAll2 padT10 borB">          
                                    <label class="span6">Discount on Labour :</label>
                                    <select class="span6" name="DiscountOptRate2" id="DiscountOptRate2">
<?php if (!empty($all_discount)) {
    foreach ($all_discount as $key => $disc) { ?>
                                                <option value="<?php echo $disc; ?>"  <?php if($quote_details->DiscountOptRate2==$disc){ ?> selected <?php } ?>><?php echo $disc . '%'; ?></option>
    <?php }
} ?>
                                    </select>
                                </div>



                                <!----compute part--->
                                
                                <?php
                                    
                                if($quote_details->SellOptAmount2>0){
                                    $SellOptAmount2=$quote_details->SellOptAmount2;
                                } else {
                                    $SellOptAmount2=$CalculatedOptTotal2;
                                }
                                
                                
                                ?>
                                
                                <div class="controls controls-row marAll2 padT10 borT">          
                                    <label class="span6" style="font-weight:bold; font-size:14px;">Final Quote(<?php echo $site_setting->currency_symbol; ?>) :</label>
                                    <input type="text" value="<?php echo $SellOptAmount2; ?>" name="SellOptAmount2" id="SellOptAmount2" placeholder="0" class="span6" readonly>
                                </div>


                                <div class="controls controls-row marAll2 padT10">          
                                    <label class="span6" style="font-weight:bold; font-size:14px;">LDH(<?php echo $site_setting->currency_symbol; ?>) :</label>
                                    <input type="text" value="<?php echo $quote_details->OptLDH2; ?>" name="OptLDH2" id="OptLDH2" placeholder="0" class="span6" readonly>
                                </div>

                                <!----compute part--->
                                
                                
     </div> <!--optdiv-->
                                
                                </div> <!---optinon 2 complete -->

                                
         

<!---optinon 3 start -->
<div class="pad6" style="background: #DFF6FF;">       
            <!---upper top 1--->

     <input type="checkbox" name="confirm_opt3" id="confirm_opt3" value="1" <?php if($quote_details->confirm_opt3==1){ ?> checked <?php } ?> /><div align="center" class="padB20 opttitle" data-id="divopt3" style="font-weight:bold;">Optional 3 - Materials <i class="icon-sort-down"></i></div>


     <div id="divopt3" class="optdiv" style="display:none;">

<?php
$total_optionaled_hours_3 = $total_optional_hours_3; //$total_optional_prep + $total_optional_closets + $total_optional_misc + $total_optional_setclean;

$total_optionaled_labour_costs_3 = $total_optionaled_hours_3 * $jobrate;
?>


                              
                                <div class="controls controls-row marAll2 padT10 borT">   
                                    <label class="span6">Total Hours :</label>
                                    <input type="text" value="<?php echo $total_optionaled_hours_3; ?>" name="total_optionaled_hours_3" id="total_optionaled_hours_3" placeholder="0" class="span6" readonly="readonly">
                                </div>
                                <div class="controls controls-row marAll2 padT10">          
                                    <label class="span6">Job Rate(<?php echo $site_setting->currency_symbol; ?>) :</label>
                                    <input type="text" value="<?php echo $jobrate; ?>" name="rate" id="rate" placeholder="0" class="span6" readonly="readonly" >
                                </div>

                                <div class="controls controls-row marAll2 padT10">          
                                    <label class="span6">Labour Costs(<?php echo $site_setting->currency_symbol; ?>) :</label>
                                    <input type="text" value="<?php echo $total_optionaled_labour_costs_3; ?>" name="total_optionaled_labour_costs_3" id="total_optionaled_labour_costs_3" placeholder="0" class="span6" readonly="readonly">
                                </div>

                                <div class="controls controls-row marAll2 padT10">          
                                    <label class="span6">Gross Material(w.PST)(<?php echo $site_setting->currency_symbol; ?>) :</label>
                                    <input type="text" value="<?php echo $total_optional_material_3; ?>" name="total_optional_material_3" id="total_optional_material_3" placeholder="0" class="span6" readonly="readonly">
                                </div>
                                
                                
                                
                                <!---upper top 1--->

  
                                <!---other service 1-->

                                <div class="controls controls-row marAll2 padT10 borT">   
                                    <select class="span6" name="OptionalCostDesc31" id="OptionalCostDesc31">
<!--                                        <option value="">Select Service 1</option>-->
<?php if (!empty($other_service)) {
    foreach ($other_service as $key => $service) { ?>
                                                <option value="<?php echo $key; ?>"  <?php if ($quote_details->OptionalCostDesc31 == $key) {
                                                    echo "selected";
                                                } ?>><?php echo ucfirst($service); ?></option>
    <?php }
} ?>
                                    </select>



<div class="span6 input-append input-prepend">
<span class="add-on"><?php echo $site_setting->currency_symbol; ?></span>
<input class="input-small" id="OptionalCostAmt31" name="OptionalCostAmt31" type="text" value="<?php echo $quote_details->OptionalCostAmt31; ?>"  />
<div class="btn-group">
<button class="button button-basic dropdown-toggle" data-toggle="dropdown">
Select
<span class="caret"></span>
</button>
<ul class="dropdown-menu cstdropmenu">

<?php if (!empty($other_service_pay)) {
foreach ($other_service_pay as $key => $pay) { ?>
<li><a href="javascript:void(0)" data-field="OptionalCostAmt31" data-val="<?php echo $pay; ?>"><?php echo $pay; ?></a></li>
<?php }
} ?>

</ul>
</div>
</div>

</div>

                                <div class="controls controls-row marAll2 padT10">   
                                    <select class="span6" name="OptionalCostDesc32" id="OptionalCostDesc32">
<!--                                        <option value="">Select Service 2</option>-->
                                                <?php if (!empty($other_service)) {
                                                    foreach ($other_service as $key => $service) { ?>
                                                <option value="<?php echo $key; ?>" <?php if ($quote_details->OptionalCostDesc32 == $key) {
                                                            echo "selected";
                                                        } ?>><?php echo ucfirst($service); ?></option>
    <?php }
} ?>
                                    </select>


                                    <div class="span6 input-append input-prepend">
                                        <span class="add-on"><?php echo $site_setting->currency_symbol; ?></span>
                                        <input class="input-small" id="OptionalCostAmt32" name="OptionalCostAmt32" type="text" value="<?php echo $quote_details->OptionalCostAmt32; ?>"  />
                                        <div class="btn-group">
                                            <button class="button button-basic dropdown-toggle" data-toggle="dropdown">
                                                Select
                                                <span class="caret"></span>
                                            </button>
                                            <ul class="dropdown-menu cstdropmenu">

<?php if (!empty($other_service_pay)) {
    foreach ($other_service_pay as $key => $pay) { ?>
                                                        <li><a href="javascript:void(0)" data-field="OptionalCostAmt32" data-val="<?php echo $pay; ?>"><?php echo $pay; ?></a></li>
    <?php }
} ?>

                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                
                                

                                <div class="controls controls-row marAll2 padT10">   
                                    <select class="span6" name="OptionalCostDesc33" id="OptionalCostDesc33">
<!--                                        <option value="">Select Service 3</option>-->
<?php if (!empty($other_service)) {
    foreach ($other_service as $key => $service) { ?>
                                                <option value="<?php echo $key; ?>" <?php if ($quote_details->OptionalCostDesc33 == $key) {
            echo "selected";
        } ?>><?php echo ucfirst($service); ?></option>
    <?php }
} ?>
                                    </select>

                                    <div class="span6 input-append input-prepend">
                                        <span class="add-on"><?php echo $site_setting->currency_symbol; ?></span>
                                        <input class="input-small" id="OptionalCostAmt33" name="OptionalCostAmt33" type="text" value="<?php echo $quote_details->OptionalCostAmt33; ?>"  />
                                        <div class="btn-group">
                                            <button class="button button-basic dropdown-toggle" data-toggle="dropdown">
                                                Select
                                                <span class="caret"></span>
                                            </button>
                                            <ul class="dropdown-menu cstdropmenu">

<?php if (!empty($other_service_pay)) {
    foreach ($other_service_pay as $key => $pay) { ?>
                                                        <li><a href="javascript:void(0)" data-field="OptionalCostAmt33" data-val="<?php echo $pay; ?>"><?php echo $pay; ?></a></li>
    <?php }
} ?>

                                            </ul>
                                        </div>
                                    </div>

                                </div>

                                <div class="controls controls-row marAll2 padT10 ">   
                                    <select class="span6" name="OptionalCostDesc34" id="OptionalCostDesc34">
<!--                                        <option value="">Select Service 4</option>-->
<?php if (!empty($other_service)) {
    foreach ($other_service as $key => $service) { ?>
                                                <option value="<?php echo $key; ?>" <?php if ($quote_details->OptionalCostDesc34 == $key) {
            echo "selected";
        } ?>><?php echo ucfirst($service); ?></option>
    <?php }
} ?>
                                    </select>

                                    <div class="span6 input-append input-prepend">
                                        <span class="add-on"><?php echo $site_setting->currency_symbol; ?></span>
                                        <input class="input-small" id="OptionalCostAmt34" name="OptionalCostAmt34" type="text" value="<?php echo $quote_details->OptionalCostAmt34; ?>"  />
                                        <div class="btn-group">
                                            <button class="button button-basic dropdown-toggle" data-toggle="dropdown">
                                                Select
                                                <span class="caret"></span>
                                            </button>
                                            <ul class="dropdown-menu cstdropmenu">

<?php if (!empty($other_service_pay)) {
    foreach ($other_service_pay as $key => $pay) { ?>
                                                        <li><a href="javascript:void(0)" data-field="OptionalCostAmt34" data-val="<?php echo $pay; ?>"><?php echo $pay; ?></a></li>
    <?php }
} ?>

                                            </ul>
                                        </div>
                                    </div>

                                </div>


                                <div class="controls controls-row marAll2 padT10 padB20 borB">   
                                    <select class="span6" name="OptionalCostDesc35" id="OptionalCostDesc35">
<!--                                        <option value="">Select Service 5</option>-->
<?php if (!empty($other_service)) {
    foreach ($other_service as $key => $service) { ?>
                                                <option value="<?php echo $key; ?>" <?php if ($quote_details->OptionalCostDesc35 == $key) {
            echo "selected";
        } ?>><?php echo ucfirst($service); ?></option>
    <?php }
} ?>
                                    </select>


                                    <div class="span6 input-append input-prepend">
                                        <span class="add-on"><?php echo $site_setting->currency_symbol; ?></span>
                                        <input class="input-small" id="OptionalCostAmt35" name="OptionalCostAmt35" type="text" value="<?php echo $quote_details->OptionalCostAmt35; ?>"  />
                                        <div class="btn-group">
                                            <button class="button button-basic dropdown-toggle" data-toggle="dropdown">
                                                Select
                                                <span class="caret"></span>
                                            </button>
                                            <ul class="dropdown-menu cstdropmenu">

<?php if (!empty($other_service_pay)) {
    foreach ($other_service_pay as $key => $pay) { ?>
                                                        <li><a href="javascript:void(0)" data-field="OptionalCostAmt35" data-val="<?php echo $pay; ?>"><?php echo $pay; ?></a></li>
    <?php }
} ?>

                                            </ul>
                                        </div>
                                    </div>

                                </div>



                                <!---other service  1-->
                                
                                 <?php $CalculatedOptTotal3 = $total_optionaled_labour_costs_3 + $total_optional_material_3 + $quote_details->OptionalCostAmt31 + $quote_details->OptionalCostAmt32 + $quote_details->OptionalCostAmt33 + $quote_details->OptionalCostAmt34 + $quote_details->OptionalCostAmt35; ?>
                                
                                
                                <div class="controls controls-row marAll2 padT10 borT">          
                                    <label class="span6">Computed Total(<?php echo $site_setting->currency_symbol; ?>) :</label>
                                    <input type="text" value="<?php echo $CalculatedOptTotal3; ?>" name="CalculatedOptTotal3" id="CalculatedOptTotal3" placeholder="0" class="span6" readonly>
                                </div>
                                
                                
                                <?php
                                    
                                if($quote_details->AdjustedOptTotal3>0){
                                    $AdjustedOptTotal3=$quote_details->AdjustedOptTotal3;
                                } else {
                                    $AdjustedOptTotal3=$CalculatedOptTotal3;
                                }
                                
                                
                                ?>

                                <div class="controls controls-row marAll2 padT10">          
                                    <label class="span6">Sell Amount(<?php echo $site_setting->currency_symbol; ?>) :</label>
                                    <input type="text" value="<?php echo $AdjustedOptTotal3; ?>" name="AdjustedOptTotal3" id="AdjustedOptTotal3" placeholder="0" class="span6">
                                </div>

                                <div class="controls controls-row marAll2 padT10 borB">          
                                    <label class="span6">Discount on Labour :</label>
                                    <select class="span6" name="DiscountOptRate3" id="DiscountOptRate3">
<?php if (!empty($all_discount)) {
    foreach ($all_discount as $key => $disc) { ?>
                                                <option value="<?php echo $disc; ?>"  <?php if($quote_details->DiscountOptRate3==$disc){ ?> selected <?php } ?>><?php echo $disc . '%'; ?></option>
    <?php }
} ?>
                                    </select>
                                </div>



                                <!----compute part--->
                                
                                <?php
                                    
                                if($quote_details->SellOptAmount3>0){
                                    $SellOptAmount3=$quote_details->SellOptAmount3;
                                } else {
                                    $SellOptAmount3=$CalculatedOptTotal3;
                                }
                                
                                
                                ?>
                                
                                <div class="controls controls-row marAll2 padT10 borT">          
                                    <label class="span6" style="font-weight:bold; font-size:14px;">Final Quote(<?php echo $site_setting->currency_symbol; ?>) :</label>
                                    <input type="text" value="<?php echo $SellOptAmount3; ?>" name="SellOptAmount3" id="SellOptAmount3" placeholder="0" class="span6" readonly>
                                </div>


                                <div class="controls controls-row marAll2 padT10">          
                                    <label class="span6" style="font-weight:bold; font-size:14px;">LDH(<?php echo $site_setting->currency_symbol; ?>) :</label>
                                    <input type="text" value="<?php echo $quote_details->OptLDH3; ?>" name="OptLDH3" id="OptLDH3" placeholder="0" class="span6" readonly>
                                </div>

                                <!----compute part--->
                                
                                
     </div> <!--optdiv-->
                                
                                </div> <!---optinon 3 complete -->

                                
                               
             

<!---optinon 4 start -->
<div class="pad6" style="background: #FCDFDF;">       
            <!---upper top 1--->

     <input type="checkbox" name="confirm_opt4" id="confirm_opt4" value="1" <?php if($quote_details->confirm_opt4==1){ ?> checked <?php } ?> /><div align="center" class="padB20 opttitle" data-id="divopt4" style="font-weight:bold;">Optional 4 - Materials <i class="icon-sort-down"></i></div>


     <div id="divopt4" class="optdiv" style="display:none;">

<?php
$total_optionaled_hours_4 = $total_optional_hours_4; //$total_optional_prep + $total_optional_closets + $total_optional_misc + $total_optional_setclean;

$total_optionaled_labour_costs_4 = $total_optionaled_hours_4 * $jobrate;
?>


                              
                                <div class="controls controls-row marAll2 padT10 borT">   
                                    <label class="span6">Total Hours :</label>
                                    <input type="text" value="<?php echo $total_optionaled_hours_4; ?>" name="total_optionaled_hours_4" id="total_optionaled_hours_4" placeholder="0" class="span6" readonly="readonly">
                                </div>
                                <div class="controls controls-row marAll2 padT10">          
                                    <label class="span6">Job Rate(<?php echo $site_setting->currency_symbol; ?>) :</label>
                                    <input type="text" value="<?php echo $jobrate; ?>" name="rate" id="rate" placeholder="0" class="span6" readonly="readonly" >
                                </div>

                                <div class="controls controls-row marAll2 padT10">          
                                    <label class="span6">Labour Costs(<?php echo $site_setting->currency_symbol; ?>) :</label>
                                    <input type="text" value="<?php echo $total_optionaled_labour_costs_4; ?>" name="total_optionaled_labour_costs_4" id="total_optionaled_labour_costs_4" placeholder="0" class="span6" readonly="readonly">
                                </div>

                                <div class="controls controls-row marAll2 padT10">          
                                    <label class="span6">Gross Material(w.PST)(<?php echo $site_setting->currency_symbol; ?>) :</label>
                                    <input type="text" value="<?php echo $total_optional_material_4; ?>" name="total_optional_material_4" id="total_optional_material_4" placeholder="0" class="span6" readonly="readonly">
                                </div>
                                
                                
                                
                                <!---upper top 1--->

  
                                <!---other service 1-->

                                <div class="controls controls-row marAll2 padT10 borT">   
                                    <select class="span6" name="OptionalCostDesc41" id="OptionalCostDesc41">
<!--                                        <option value="">Select Service 1</option>-->
<?php if (!empty($other_service)) {
    foreach ($other_service as $key => $service) { ?>
                                                <option value="<?php echo $key; ?>"  <?php if ($quote_details->OptionalCostDesc41 == $key) {
                                                    echo "selected";
                                                } ?>><?php echo ucfirst($service); ?></option>
    <?php }
} ?>
                                    </select>



<div class="span6 input-append input-prepend">
<span class="add-on"><?php echo $site_setting->currency_symbol; ?></span>
<input class="input-small" id="OptionalCostAmt41" name="OptionalCostAmt41" type="text" value="<?php echo $quote_details->OptionalCostAmt41; ?>"  />
<div class="btn-group">
<button class="button button-basic dropdown-toggle" data-toggle="dropdown">
Select
<span class="caret"></span>
</button>
<ul class="dropdown-menu cstdropmenu">

<?php if (!empty($other_service_pay)) {
foreach ($other_service_pay as $key => $pay) { ?>
<li><a href="javascript:void(0)" data-field="OptionalCostAmt41" data-val="<?php echo $pay; ?>"><?php echo $pay; ?></a></li>
<?php }
} ?>

</ul>
</div>
</div>

</div>

                                <div class="controls controls-row marAll2 padT10">   
                                    <select class="span6" name="OptionalCostDesc42" id="OptionalCostDesc42">
<!--                                        <option value="">Select Service 2</option>-->
                                                <?php if (!empty($other_service)) {
                                                    foreach ($other_service as $key => $service) { ?>
                                                <option value="<?php echo $key; ?>" <?php if ($quote_details->OptionalCostDesc42 == $key) {
                                                            echo "selected";
                                                        } ?>><?php echo ucfirst($service); ?></option>
    <?php }
} ?>
                                    </select>


                                    <div class="span6 input-append input-prepend">
                                        <span class="add-on"><?php echo $site_setting->currency_symbol; ?></span>
                                        <input class="input-small" id="OptionalCostAmt42" name="OptionalCostAmt42" type="text" value="<?php echo $quote_details->OptionalCostAmt42; ?>"  />
                                        <div class="btn-group">
                                            <button class="button button-basic dropdown-toggle" data-toggle="dropdown">
                                                Select
                                                <span class="caret"></span>
                                            </button>
                                            <ul class="dropdown-menu cstdropmenu">

<?php if (!empty($other_service_pay)) {
    foreach ($other_service_pay as $key => $pay) { ?>
                                                        <li><a href="javascript:void(0)" data-field="OptionalCostAmt42" data-val="<?php echo $pay; ?>"><?php echo $pay; ?></a></li>
    <?php }
} ?>

                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                
                                

                                <div class="controls controls-row marAll2 padT10">   
                                    <select class="span6" name="OptionalCostDesc43" id="OptionalCostDesc43">
<!--                                        <option value="">Select Service 3</option>-->
<?php if (!empty($other_service)) {
    foreach ($other_service as $key => $service) { ?>
                                                <option value="<?php echo $key; ?>" <?php if ($quote_details->OptionalCostDesc43 == $key) {
            echo "selected";
        } ?>><?php echo ucfirst($service); ?></option>
    <?php }
} ?>
                                    </select>

                                    <div class="span6 input-append input-prepend">
                                        <span class="add-on"><?php echo $site_setting->currency_symbol; ?></span>
                                        <input class="input-small" id="OptionalCostAmt43" name="OptionalCostAmt43" type="text" value="<?php echo $quote_details->OptionalCostAmt43; ?>"  />
                                        <div class="btn-group">
                                            <button class="button button-basic dropdown-toggle" data-toggle="dropdown">
                                                Select
                                                <span class="caret"></span>
                                            </button>
                                            <ul class="dropdown-menu cstdropmenu">

<?php if (!empty($other_service_pay)) {
    foreach ($other_service_pay as $key => $pay) { ?>
                                                        <li><a href="javascript:void(0)" data-field="OptionalCostAmt43" data-val="<?php echo $pay; ?>"><?php echo $pay; ?></a></li>
    <?php }
} ?>

                                            </ul>
                                        </div>
                                    </div>

                                </div>

                                <div class="controls controls-row marAll2 padT10 ">   
                                    <select class="span6" name="OptionalCostDesc44" id="OptionalCostDesc44">
<!--                                        <option value="">Select Service 4</option>-->
<?php if (!empty($other_service)) {
    foreach ($other_service as $key => $service) { ?>
                                                <option value="<?php echo $key; ?>" <?php if ($quote_details->OptionalCostDesc44 == $key) {
            echo "selected";
        } ?>><?php echo ucfirst($service); ?></option>
    <?php }
} ?>
                                    </select>

                                    <div class="span6 input-append input-prepend">
                                        <span class="add-on"><?php echo $site_setting->currency_symbol; ?></span>
                                        <input class="input-small" id="OptionalCostAmt44" name="OptionalCostAmt44" type="text" value="<?php echo $quote_details->OptionalCostAmt44; ?>"  />
                                        <div class="btn-group">
                                            <button class="button button-basic dropdown-toggle" data-toggle="dropdown">
                                                Select
                                                <span class="caret"></span>
                                            </button>
                                            <ul class="dropdown-menu cstdropmenu">

<?php if (!empty($other_service_pay)) {
    foreach ($other_service_pay as $key => $pay) { ?>
                                                        <li><a href="javascript:void(0)" data-field="OptionalCostAmt44" data-val="<?php echo $pay; ?>"><?php echo $pay; ?></a></li>
    <?php }
} ?>

                                            </ul>
                                        </div>
                                    </div>

                                </div>


                                <div class="controls controls-row marAll2 padT10 padB20 borB">   
                                    <select class="span6" name="OptionalCostDesc45" id="OptionalCostDesc45">
<!--                                        <option value="">Select Service 5</option>-->
<?php if (!empty($other_service)) {
    foreach ($other_service as $key => $service) { ?>
                                                <option value="<?php echo $key; ?>" <?php if ($quote_details->OptionalCostDesc45 == $key) {
            echo "selected";
        } ?>><?php echo ucfirst($service); ?></option>
    <?php }
} ?>
                                    </select>


                                    <div class="span6 input-append input-prepend">
                                        <span class="add-on"><?php echo $site_setting->currency_symbol; ?></span>
                                        <input class="input-small" id="OptionalCostAmt45" name="OptionalCostAmt45" type="text" value="<?php echo $quote_details->OptionalCostAmt45; ?>"  />
                                        <div class="btn-group">
                                            <button class="button button-basic dropdown-toggle" data-toggle="dropdown">
                                                Select
                                                <span class="caret"></span>
                                            </button>
                                            <ul class="dropdown-menu cstdropmenu">

<?php if (!empty($other_service_pay)) {
    foreach ($other_service_pay as $key => $pay) { ?>
                                                        <li><a href="javascript:void(0)" data-field="OptionalCostAmt45" data-val="<?php echo $pay; ?>"><?php echo $pay; ?></a></li>
    <?php }
} ?>

                                            </ul>
                                        </div>
                                    </div>

                                </div>



                                <!---other service  1-->
                                
                                 <?php $CalculatedOptTotal4 = $total_optionaled_labour_costs_4 + $total_optional_material_4 + $quote_details->OptionalCostAmt41 + $quote_details->OptionalCostAmt42 + $quote_details->OptionalCostAmt43 + $quote_details->OptionalCostAmt44 + $quote_details->OptionalCostAmt45; ?>
                                
                                
                                <div class="controls controls-row marAll2 padT10 borT">          
                                    <label class="span6">Computed Total(<?php echo $site_setting->currency_symbol; ?>) :</label>
                                    <input type="text" value="<?php echo $CalculatedOptTotal4; ?>" name="CalculatedOptTotal4" id="CalculatedOptTotal4" placeholder="0" class="span6" readonly>
                                </div>
                                
                                
                                <?php
                                    
                                if($quote_details->AdjustedOptTotal4>0){
                                    $AdjustedOptTotal4=$quote_details->AdjustedOptTotal4;
                                } else {
                                    $AdjustedOptTotal4=$CalculatedOptTotal4;
                                }
                                
                                
                                ?>

                                <div class="controls controls-row marAll2 padT10">          
                                    <label class="span6">Sell Amount(<?php echo $site_setting->currency_symbol; ?>) :</label>
                                    <input type="text" value="<?php echo $AdjustedOptTotal4; ?>" name="AdjustedOptTotal4" id="AdjustedOptTotal4" placeholder="0" class="span6">
                                </div>

                                <div class="controls controls-row marAll2 padT10 borB">          
                                    <label class="span6">Discount on Labour :</label>
                                    <select class="span6" name="DiscountOptRate4" id="DiscountOptRate4">
<?php if (!empty($all_discount)) {
    foreach ($all_discount as $key => $disc) { ?>
                                                <option value="<?php echo $disc; ?>"  <?php if($quote_details->DiscountOptRate4==$disc){ ?> selected <?php } ?>><?php echo $disc . '%'; ?></option>
    <?php }
} ?>
                                    </select>
                                </div>



                                <!----compute part--->
                                
                                <?php
                                    
                                if($quote_details->SellOptAmount4>0){
                                    $SellOptAmount4=$quote_details->SellOptAmount4;
                                } else {
                                    $SellOptAmount4=$CalculatedOptTotal4;
                                }
                                
                                
                                ?>
                                
                                <div class="controls controls-row marAll2 padT10 borT">          
                                    <label class="span6" style="font-weight:bold; font-size:14px;">Final Quote(<?php echo $site_setting->currency_symbol; ?>) :</label>
                                    <input type="text" value="<?php echo $SellOptAmount4; ?>" name="SellOptAmount4" id="SellOptAmount4" placeholder="0" class="span6" readonly>
                                </div>


                                <div class="controls controls-row marAll2 padT10">          
                                    <label class="span6" style="font-weight:bold; font-size:14px;">LDH(<?php echo $site_setting->currency_symbol; ?>) :</label>
                                    <input type="text" value="<?php echo $quote_details->OptLDH4; ?>" name="OptLDH4" id="OptLDH4" placeholder="0" class="span6" readonly>
                                </div>

                                <!----compute part--->
                                
                                
     </div> <!--optdiv-->
                                
                                </div> <!---optinon 4 complete -->

                                
                               
                                     

                            



                            </div>
                            <!---optional features main span6 div--->


                        </div>


                        <br /><br /><br />











                    </div><!--step 1 div-->




                    <div class="form-actions">
                        <button type="submit" class="button button-basic-blue savebtnquote">Save</button>

                        <button type="button" class="button button-basic" onClick="window.location.href='<?php echo site_url('quotes/managetype'); ?>'">Cancel</button>

                        <input type="hidden" name="quote_id" id="quote_id" value="<?php echo $quote_id; ?>" />

                    </div>


                    </form>
                </div>
            </div>
        </div>
    </div>



</div>



<style>
    .opttitle {
        cursor: pointer;
    }
    .cstdropmenu{
        min-width: 10px;
        max-height: 150px;
        overflow-y: scroll;
    }
    .cstdropmenu li{
        margin: 1px;
    }
    .cstdropmenu li a{
        padding: 1px 9px;
        line-height: 17px;
        border-bottom: 1px solid #F2F2F2;
        border-radius: 0px;
    }
</style>


<script type="text/javascript">
    $(document).ready(function(){
        
        
        
        $(".opttitle").on('click',function(){
           
           var did=$(this).attr('data-id');
           
           //$(".opttitle i").removeClass('icon-sort-up').addClass('icon-sort-down');
           
           //$(this).find('i').removeClass('icon-sort-down').addClass('icon-sort-up');
           
           //$(".optdiv").hide();              
           
           //$("#"+did).toggle("slow");
           
            if( $("#"+did).is(':visible') ) {
               
                //$("#"+did).hide();
                $("#"+did).slideUp("fast");
                $(this).find('i').removeClass('icon-sort-up').addClass('icon-sort-down');
            }
            else {
               
                //$("#"+did).show();
                $("#"+did).slideDown("fast");
                $(this).find('i').removeClass('icon-sort-down').addClass('icon-sort-up');
            }
           
           
           
        });
        
        
         ///===optional 4 part===
        
         $("#OptionalCostAmt41,#OptionalCostAmt42,#OptionalCostAmt43,#OptionalCostAmt44,#OptionalCostAmt45,#AdjustedOptTotal4,#DiscountOptRate4").live("change",function(){
           
            calculateopt4();
        });
        
        function calculateopt4(){
            
         var totalTime=$("#total_optionaled_hours_4").val();
         if(totalTime=='' || totalTime==NaN || totalTime==undefined){ totalTime =0; }
         var total_commited_labour_costs = $("#total_optionaled_labour_costs_4").val();
         if(total_commited_labour_costs=='' || total_commited_labour_costs==NaN || total_commited_labour_costs==undefined){ total_commited_labour_costs =0; }
         var total_commit_material = $("#total_optional_material_4").val();
         if(total_commit_material=='' || total_commit_material==NaN || total_commit_material==undefined){ total_commit_material =0; }
         
         var AdditionalCostDesc1=$("#OptionalCostDesc41 option:selected").val();
         var AdditionalCostDesc2=$("#OptionalCostDesc42 option:selected").val();
         var AdditionalCostDesc3=$("#OptionalCostDesc43 option:selected").val();
         var AdditionalCostDesc4=$("#OptionalCostDesc44 option:selected").val();
         var AdditionalCostDesc5=$("#OptionalCostDesc45 option:selected").val();
         
         var AdditionalCostAmt1=$("#OptionalCostAmt41").val();
         var AdditionalCostAmt2=$("#OptionalCostAmt42").val();
         var AdditionalCostAmt3=$("#OptionalCostAmt43").val();
         var AdditionalCostAmt4=$("#OptionalCostAmt44").val();
         var AdditionalCostAmt5=$("#OptionalCostAmt45").val();
         
         if(AdditionalCostAmt1=='' || AdditionalCostAmt1==NaN || AdditionalCostAmt1==undefined){ AdditionalCostAmt1 =0; }
         if(AdditionalCostAmt2=='' || AdditionalCostAmt2==NaN || AdditionalCostAmt2==undefined){ AdditionalCostAmt2 =0; }
         if(AdditionalCostAmt3=='' || AdditionalCostAmt3==NaN || AdditionalCostAmt3==undefined){ AdditionalCostAmt3 =0; }
         if(AdditionalCostAmt4=='' || AdditionalCostAmt4==NaN || AdditionalCostAmt4==undefined){ AdditionalCostAmt4 =0; }
         if(AdditionalCostAmt5=='' || AdditionalCostAmt5==NaN || AdditionalCostAmt5==undefined){ AdditionalCostAmt5 =0; }
           
         var calculate_commit_total = parseFloat(total_commited_labour_costs) + parseFloat(total_commit_material) + parseFloat(AdditionalCostAmt1) + parseFloat(AdditionalCostAmt2) + parseFloat(AdditionalCostAmt3) + parseFloat(AdditionalCostAmt4) + parseFloat(AdditionalCostAmt5);
            
            calculate_commit_total=calculate_commit_total.toFixed(2);
            
            $("#CalculatedOptTotal4").val(calculate_commit_total);
            
            var AdjustedTotal=$("#AdjustedOptTotal4").val();
            if(AdjustedTotal==0 || AdjustedTotal==NaN || AdjustedTotal==undefined){
                $("#AdjustedOptTotal4").val(calculate_commit_total);
                AdjustedTotal=calculate_commit_total;
            }
            
            
            
            var DiscountRate=$("#DiscountOptRate4 option:selected").val();
            
            var step1=0;
            if(DiscountRate>0){
                step1=(AdjustedTotal * DiscountRate) / 100;
            }
            
            var step2=parseFloat(AdjustedTotal)-parseFloat(step1); 
            
            step2=step2.toFixed(2);
            
            $("#SellOptAmount4").val(step2);
            
            
            var minsum=parseFloat(total_commit_material) + parseFloat(AdditionalCostAmt1) + parseFloat(AdditionalCostAmt2) + parseFloat(AdditionalCostAmt3) + parseFloat(AdditionalCostAmt4) + parseFloat(AdditionalCostAmt5);
            
            
            
            var step3=(step2-minsum)/ totalTime;
            
           
            var ldh=0;
            
            if(step3!=NaN && step3>0){
                ldh=step3.toFixed(2);
            }
            
            $("#OptLDH4").val(ldh);
            
        }
        
         ///===optional 3 part===
        
         $("#OptionalCostAmt31,#OptionalCostAmt32,#OptionalCostAmt33,#OptionalCostAmt34,#OptionalCostAmt35,#AdjustedOptTotal3,#DiscountOptRate3").live("change",function(){
           
            calculateopt3();
        });
        
        function calculateopt3(){
         
         var totalTime=$("#total_optionaled_hours_3").val();
         if(totalTime=='' || totalTime==NaN || totalTime==undefined){ totalTime =0; }
         var total_commited_labour_costs = $("#total_optionaled_labour_costs_3").val();
          if(total_commited_labour_costs=='' || total_commited_labour_costs==NaN || total_commited_labour_costs==undefined){ total_commited_labour_costs =0; }
         var total_commit_material = $("#total_optional_material_3").val();
         if(total_commit_material=='' || total_commit_material==NaN || total_commit_material==undefined){ total_commit_material =0; }
         
         var AdditionalCostDesc1=$("#OptionalCostDesc31 option:selected").val();
         var AdditionalCostDesc2=$("#OptionalCostDesc32 option:selected").val();
         var AdditionalCostDesc3=$("#OptionalCostDesc33 option:selected").val();
         var AdditionalCostDesc4=$("#OptionalCostDesc34 option:selected").val();
         var AdditionalCostDesc5=$("#OptionalCostDesc35 option:selected").val();
         
         var AdditionalCostAmt1=$("#OptionalCostAmt31").val();
         var AdditionalCostAmt2=$("#OptionalCostAmt32").val();
         var AdditionalCostAmt3=$("#OptionalCostAmt33").val();
         var AdditionalCostAmt4=$("#OptionalCostAmt34").val();
         var AdditionalCostAmt5=$("#OptionalCostAmt35").val();
         
         if(AdditionalCostAmt1=='' || AdditionalCostAmt1==NaN || AdditionalCostAmt1==undefined){ AdditionalCostAmt1 =0; }
         if(AdditionalCostAmt2=='' || AdditionalCostAmt2==NaN || AdditionalCostAmt2==undefined){ AdditionalCostAmt2 =0; }
         if(AdditionalCostAmt3=='' || AdditionalCostAmt3==NaN || AdditionalCostAmt3==undefined){ AdditionalCostAmt3 =0; }
         if(AdditionalCostAmt4=='' || AdditionalCostAmt4==NaN || AdditionalCostAmt4==undefined){ AdditionalCostAmt4 =0; }
         if(AdditionalCostAmt5=='' || AdditionalCostAmt5==NaN || AdditionalCostAmt5==undefined){ AdditionalCostAmt5 =0; }
           
         var calculate_commit_total = parseFloat(total_commited_labour_costs) + parseFloat(total_commit_material) + parseFloat(AdditionalCostAmt1) + parseFloat(AdditionalCostAmt2) + parseFloat(AdditionalCostAmt3) + parseFloat(AdditionalCostAmt4) + parseFloat(AdditionalCostAmt5);
            
            calculate_commit_total=calculate_commit_total.toFixed(2);
            
            $("#CalculatedOptTotal3").val(calculate_commit_total);
            
            var AdjustedTotal=$("#AdjustedOptTotal3").val();
            if(AdjustedTotal==0 || AdjustedTotal==NaN || AdjustedTotal==undefined){
                $("#AdjustedOptTotal3").val(calculate_commit_total);
                AdjustedTotal=calculate_commit_total;
            }
            
            
            
            var DiscountRate=$("#DiscountOptRate3 option:selected").val();
            
            var step1=0;
            if(DiscountRate>0){
                step1=(AdjustedTotal * DiscountRate) / 100;
            }
            
            var step2=parseFloat(AdjustedTotal)-parseFloat(step1); 
            
            step2=step2.toFixed(2);
            
            $("#SellOptAmount3").val(step2);
            
            
            var minsum=parseFloat(total_commit_material) + parseFloat(AdditionalCostAmt1) + parseFloat(AdditionalCostAmt2) + parseFloat(AdditionalCostAmt3) + parseFloat(AdditionalCostAmt4) + parseFloat(AdditionalCostAmt5);
            
            
            
            var step3=(step2-minsum)/ totalTime;
            
           
            var ldh=0;
            
            if(step3!=NaN && step3>0){
                ldh=step3.toFixed(2);
            }
            
            $("#OptLDH3").val(ldh);
            
        }
        
        ///===optional 2 part===
        
         $("#OptionalCostAmt21,#OptionalCostAmt22,#OptionalCostAmt23,#OptionalCostAmt24,#OptionalCostAmt25,#AdjustedOptTotal2,#DiscountOptRate2").live("change",function(){
           
            calculateopt2();
        });
        
        
        
        
        
        function calculateopt2(){
	 
         var totalTime=$("#total_optionaled_hours_2").val();
         if(totalTime=='' || totalTime==NaN || totalTime==undefined){ totalTime =0; }
         var total_commited_labour_costs = $("#total_optionaled_labour_costs_2").val();
          if(total_commited_labour_costs=='' || total_commited_labour_costs==NaN || total_commited_labour_costs==undefined){ total_commited_labour_costs =0; }
         var total_commit_material = $("#total_optional_material_2").val();
         if(total_commit_material=='' || total_commit_material==NaN || total_commit_material==undefined){ total_commit_material =0; }
         
         var AdditionalCostDesc1=$("#OptionalCostDesc21 option:selected").val();
         var AdditionalCostDesc2=$("#OptionalCostDesc22 option:selected").val();
         var AdditionalCostDesc3=$("#OptionalCostDesc23 option:selected").val();
         var AdditionalCostDesc4=$("#OptionalCostDesc24 option:selected").val();
         var AdditionalCostDesc5=$("#OptionalCostDesc25 option:selected").val();
         
         var AdditionalCostAmt1=$("#OptionalCostAmt21").val();
         var AdditionalCostAmt2=$("#OptionalCostAmt22").val();
         var AdditionalCostAmt3=$("#OptionalCostAmt23").val();
         var AdditionalCostAmt4=$("#OptionalCostAmt24").val();
         var AdditionalCostAmt5=$("#OptionalCostAmt25").val();
         
         
         if(AdditionalCostAmt1=='' || AdditionalCostAmt1==NaN || AdditionalCostAmt1==undefined){ AdditionalCostAmt1 =0; }
         if(AdditionalCostAmt2=='' || AdditionalCostAmt2==NaN || AdditionalCostAmt2==undefined){ AdditionalCostAmt2 =0; }
         if(AdditionalCostAmt3=='' || AdditionalCostAmt3==NaN || AdditionalCostAmt3==undefined){ AdditionalCostAmt3 =0; }
         if(AdditionalCostAmt4=='' || AdditionalCostAmt4==NaN || AdditionalCostAmt4==undefined){ AdditionalCostAmt4 =0; }
         if(AdditionalCostAmt5=='' || AdditionalCostAmt5==NaN || AdditionalCostAmt5==undefined){ AdditionalCostAmt5 =0; }
           
         var calculate_commit_total = parseFloat(total_commited_labour_costs) + parseFloat(total_commit_material) + parseFloat(AdditionalCostAmt1) + parseFloat(AdditionalCostAmt2) + parseFloat(AdditionalCostAmt3) + parseFloat(AdditionalCostAmt4) + parseFloat(AdditionalCostAmt5);
            
            calculate_commit_total=calculate_commit_total.toFixed(2);
            
            $("#CalculatedOptTotal2").val(calculate_commit_total);
            
            var AdjustedTotal=$("#AdjustedOptTotal2").val();
            if(AdjustedTotal==0 || AdjustedTotal==NaN || AdjustedTotal==undefined){
                $("#AdjustedOptTotal2").val(calculate_commit_total);
                AdjustedTotal=calculate_commit_total;
            }
            
            
            
            var DiscountRate=$("#DiscountOptRate2 option:selected").val();
            
            var step1=0;
            if(DiscountRate>0){
                step1=(AdjustedTotal * DiscountRate) / 100;
            }
            
            var step2=parseFloat(AdjustedTotal)-parseFloat(step1); 
            
            step2=step2.toFixed(2);
            
            $("#SellOptAmount2").val(step2);
            
            
            var minsum=parseFloat(total_commit_material) + parseFloat(AdditionalCostAmt1) + parseFloat(AdditionalCostAmt2) + parseFloat(AdditionalCostAmt3) + parseFloat(AdditionalCostAmt4) + parseFloat(AdditionalCostAmt5);
            
            
            
            var step3=(step2-minsum)/ totalTime;
            
           
            var ldh=0;
            
            if(step3!=NaN && step3>0){
                ldh=step3.toFixed(2);
            }
            
            $("#OptLDH2").val(ldh);
            
            
        
        }
        
        
        
        ///===optional 1 part===
        
         $("#OptionalCostAmt11,#OptionalCostAmt12,#OptionalCostAmt13,#OptionalCostAmt14,#OptionalCostAmt15,#AdjustedOptTotal1,#DiscountOptRate1").live("change",function(){
           
            calculateopt1();
        });
        
        
        
        
        
        function calculateopt1(){
	 
         var totalTime=$("#total_optionaled_hours_1").val();
         if(totalTime=='' || totalTime==NaN || totalTime==undefined){ totalTime =0; }
         var total_commited_labour_costs = $("#total_optionaled_labour_costs_1").val();
          if(total_commited_labour_costs=='' || total_commited_labour_costs==NaN || total_commited_labour_costs==undefined){ total_commited_labour_costs =0; }
         var total_commit_material = $("#total_optional_material_1").val();
         if(total_commit_material=='' || total_commit_material==NaN || total_commit_material==undefined){ total_commit_material =0; }
         
         var AdditionalCostDesc1=$("#OptionalCostDesc11 option:selected").val();
         var AdditionalCostDesc2=$("#OptionalCostDesc12 option:selected").val();
         var AdditionalCostDesc3=$("#OptionalCostDesc13 option:selected").val();
         var AdditionalCostDesc4=$("#OptionalCostDesc14 option:selected").val();
         var AdditionalCostDesc5=$("#OptionalCostDesc15 option:selected").val();
         
         var AdditionalCostAmt1=$("#OptionalCostAmt11").val();
         var AdditionalCostAmt2=$("#OptionalCostAmt12").val();
         var AdditionalCostAmt3=$("#OptionalCostAmt13").val();
         var AdditionalCostAmt4=$("#OptionalCostAmt14").val();
         var AdditionalCostAmt5=$("#OptionalCostAmt15").val();
         
         
         if(AdditionalCostAmt1=='' || AdditionalCostAmt1==NaN || AdditionalCostAmt1==undefined){ AdditionalCostAmt1 =0; }
         if(AdditionalCostAmt2=='' || AdditionalCostAmt2==NaN || AdditionalCostAmt2==undefined){ AdditionalCostAmt2 =0; }
         if(AdditionalCostAmt3=='' || AdditionalCostAmt3==NaN || AdditionalCostAmt3==undefined){ AdditionalCostAmt3 =0; }
         if(AdditionalCostAmt4=='' || AdditionalCostAmt4==NaN || AdditionalCostAmt4==undefined){ AdditionalCostAmt4 =0; }
         if(AdditionalCostAmt5=='' || AdditionalCostAmt5==NaN || AdditionalCostAmt5==undefined){ AdditionalCostAmt5 =0; }
           
         var calculate_commit_total = parseFloat(total_commited_labour_costs) + parseFloat(total_commit_material) + parseFloat(AdditionalCostAmt1) + parseFloat(AdditionalCostAmt2) + parseFloat(AdditionalCostAmt3) + parseFloat(AdditionalCostAmt4) + parseFloat(AdditionalCostAmt5);
            
            calculate_commit_total=calculate_commit_total.toFixed(2);
            
            $("#CalculatedOptTotal1").val(calculate_commit_total);
            
            var AdjustedTotal=$("#AdjustedOptTotal1").val();
            if(AdjustedTotal==0 || AdjustedTotal==NaN || AdjustedTotal==undefined){
                $("#AdjustedOptTotal1").val(calculate_commit_total);
                AdjustedTotal=calculate_commit_total;
            }
            
            
            
            var DiscountRate=$("#DiscountOptRate1 option:selected").val();
            
            var step1=0;
            if(DiscountRate>0){
                step1=(AdjustedTotal * DiscountRate) / 100;
            }
            
            var step2=parseFloat(AdjustedTotal)-parseFloat(step1); 
            
            step2=step2.toFixed(2);
            
            $("#SellOptAmount1").val(step2);
            
            
            var minsum=parseFloat(total_commit_material) + parseFloat(AdditionalCostAmt1) + parseFloat(AdditionalCostAmt2) + parseFloat(AdditionalCostAmt3) + parseFloat(AdditionalCostAmt4) + parseFloat(AdditionalCostAmt5);
            
            
            
            var step3=(step2-minsum)/ totalTime;
            
           
            var ldh=0;
            
            if(step3!=NaN && step3>0){
                ldh=step3.toFixed(2);
            }
            
            $("#OptLDH1").val(ldh);
            
            
        
        }
        
        
        
        
        
        ///===commited part===
        
         $("#AdditionalCostAmt1,#AdditionalCostAmt2,#AdditionalCostAmt3,#AdditionalCostAmt4,#AdditionalCostAmt5,#AdjustedTotal,#DiscountRate").live("change",function(){
           
            calculatecommited();
        });
        
        
        
        
        
        function calculatecommited(){
	 
         var totalTime=$("#totalcommitedhours").val();
         if(totalTime=='' || totalTime==NaN || totalTime==undefined){ totalTime =0; }
         var total_commited_labour_costs = $("#total_commited_labour_costs").val();
         if(total_commited_labour_costs=='' || total_commited_labour_costs==NaN || total_commited_labour_costs==undefined){ total_commited_labour_costs =0; }
         var total_commit_material = $("#total_commit_material").val();
         if(total_commit_material=='' || total_commit_material==NaN || total_commit_material==undefined){ total_commit_material =0; }
         
         var AdditionalCostDesc1=$("#AdditionalCostDesc1 option:selected").val();
         var AdditionalCostDesc2=$("#AdditionalCostDesc2 option:selected").val();
         var AdditionalCostDesc3=$("#AdditionalCostDesc3 option:selected").val();
         var AdditionalCostDesc4=$("#AdditionalCostDesc4 option:selected").val();
         var AdditionalCostDesc5=$("#AdditionalCostDesc5 option:selected").val();
         
         var AdditionalCostAmt1=$("#AdditionalCostAmt1").val();
         var AdditionalCostAmt2=$("#AdditionalCostAmt2").val();
         var AdditionalCostAmt3=$("#AdditionalCostAmt3").val();
         var AdditionalCostAmt4=$("#AdditionalCostAmt4").val();
         var AdditionalCostAmt5=$("#AdditionalCostAmt5").val();
         
         if(AdditionalCostAmt1=='' || AdditionalCostAmt1==NaN || AdditionalCostAmt1==undefined){ AdditionalCostAmt1 =0; }
         if(AdditionalCostAmt2=='' || AdditionalCostAmt2==NaN || AdditionalCostAmt2==undefined){ AdditionalCostAmt2 =0; }
         if(AdditionalCostAmt3=='' || AdditionalCostAmt3==NaN || AdditionalCostAmt3==undefined){ AdditionalCostAmt3 =0; }
         if(AdditionalCostAmt4=='' || AdditionalCostAmt4==NaN || AdditionalCostAmt4==undefined){ AdditionalCostAmt4 =0; }
         if(AdditionalCostAmt5=='' || AdditionalCostAmt5==NaN || AdditionalCostAmt5==undefined){ AdditionalCostAmt5 =0; }
         
         var calculate_commit_total = parseFloat(total_commited_labour_costs) + parseFloat(total_commit_material) + parseFloat(AdditionalCostAmt1) + parseFloat(AdditionalCostAmt2) + parseFloat(AdditionalCostAmt3) + parseFloat(AdditionalCostAmt4) + parseFloat(AdditionalCostAmt5);
            
            calculate_commit_total=calculate_commit_total.toFixed(2);
            
            $("#CalculatedTotal").val(calculate_commit_total);
            
            var AdjustedTotal=$("#AdjustedTotal").val();
            if(AdjustedTotal==0 || AdjustedTotal==NaN || AdjustedTotal==undefined){
                $("#AdjustedTotal").val(calculate_commit_total);
                AdjustedTotal=calculate_commit_total;
            }
            
            
            
            var DiscountRate=$("#DiscountRate option:selected").val();
            
            var step1=0;
            if(DiscountRate>0){
                step1=(AdjustedTotal * DiscountRate) / 100;
            }
            
            var step2=parseFloat(AdjustedTotal)-parseFloat(step1); 
            
            step2=step2.toFixed(2);
            
            $("#SellAmount").val(step2);
            
            
            var minsum=parseFloat(total_commit_material) + parseFloat(AdditionalCostAmt1) + parseFloat(AdditionalCostAmt2) + parseFloat(AdditionalCostAmt3) + parseFloat(AdditionalCostAmt4) + parseFloat(AdditionalCostAmt5);
            
            
            
            var step3=(step2-minsum)/ totalTime;
            
           
            var ldh=0;
            
            if(step3!=NaN && step3>0){
                ldh=step3.toFixed(2);
            }
            
            $("#LDH").val(ldh);
            
            
        
        }
        
        
        
        
        
        $(".cstdropmenu li a").on("click",function(){
            
            if($(this).parent().hasClass('disabled')==false){
            
                var dval=$(this).attr('data-val');
                var dtarget=$(this).attr('data-field');
                $("#"+dtarget).val(dval);     
                
                calculatecommited();
                calculateopt1();
                calculateopt2();
                calculateopt3();
                calculateopt4();
                
            } 
        });
        
        
        
        
        
        
    });
        
        
        

    var clicked = false;

    $(document).ready(function(){
        //    $("a[href]").click(function(){
        //        clicked = true;
        //    });
        //
        //    $(document).bind('keypress keydown keyup', function(e) {
        //        if(e.which === 116) {
        //            clicked = true;
        //        }
        //        if(e.which === 82 && e.ctrlKey) {
        //            clicked = true;
        //        }
        //    });

        $(".savebtnquote").click(function(){
            clicked = true;
        });
    
    });

    window.addEventListener("beforeunload", function (e) {
        if(!clicked) {
        
            var confirmationMessage = 'Please click on stay on page to save the data you have entered and then continue.';    

            (e || window.event).returnValue = confirmationMessage; //Gecko + IE
            return confirmationMessage; //Gecko + Webkit, Safari, Chrome etc.
        }
    });
    //window.onbeforeunload = function(e){
    //    if(!clicked) {
    //        
    //          if (typeof event == 'undefined') {
    //            event = window.event;
    //          }
    //          if (event) {
    //            event.returnValue = 'You have unsaved stuff.';
    //          }
    //          return message;
    //        
    //        //return 'You have unsaved stuff.';
    //    }
    //};


</script>
<style>
    #firstStep{
        opacity:1 !important;
    }
</style>