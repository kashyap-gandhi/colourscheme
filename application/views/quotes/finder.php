<?php $site_setting = site_setting(); ?>

<div class="page-header">
    <div class="pull-left">
        <h4><i class="icon-cogs"></i> Finder</h4>
    </div>
    <div class="pull-right">
        <ul class="bread">
            <li><a href="<?php echo site_url('quotes/manage'); ?>">Manage Quotes</a><span class="divider">/</span></li>
            <li class="active">Finder</li>
        </ul>
        
        
        
    </div>
</div>






<div class="container-fluid" id="content-area">

    <div class="row-fluid">
        <div class="span12">
            
            
            <div class="pull-right">
<div style="clear: both;"><span style="color:red;">*</span> indicates required fields for <b>"Add Client"</b></div>
</div>
<div style="clear: both;"></div>


            <?php if ($error != '') { ?>

                <div class="alert alert-error">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    <strong>Warning !</strong> <?php echo $error; ?>
                </div>    <?php } ?>






            <!---find part--->
            <div class="box">
                <div class="box-head">
                    <i class="icon-list-ul"></i>
                    <span>Find Client</span>
                </div>
                <div class="box-body box-body-nopadding">
                    <?php
                    $attributes = array('name' => 'frm_findclient', 'class' => 'form-horizontal form-bordered');
                    echo form_open('quotes/finder#clients', $attributes);
                    ?> 


                    <div class="control-group">
                        <label for="textfield" class="control-label">Name</label>
                        <div class="controls controls-row" style="height: 15px;">
                            <label for="first_name" class="span4">Company<span style="color:red;">*</span></label>
                            <label for="last_name" class="span4">Contact</label>
                            <label for="email" class="span4">Email</label>
                        </div>
                        <div class="controls controls-row" style="height: 40px;">
                            <input type="text" name="first_name" id="first_name" value="<?php echo $first_name; ?>" placeholder="Company" class="span4">
                            <input type="text" name="last_name" id="last_name" value="<?php echo $last_name; ?>" placeholder="Contact" class="span4">
                            <input type="text" name="email" id="email" value="<?php echo $email; ?>" placeholder="Email" class="span4">
                        </div>
                    </div>

                    <div class="control-group">&nbsp;</div>


                    <div class="control-group">
                        <label for="textfield" class="control-label">Phone</label>
                        <div class="controls controls-row" style="height: 15px;">
                            <label for="cell_phone" class="span3">Cell Phone<span style="color:red;">*</span></label>
                            <label for="home_phone" class="span3">Home Phone</label>
                            <label for="business_phone" class="span3">Business Phone</label>
                            <label for="fax" class="span3">Fax Phone</label>
                        </div>
                        <div class="controls controls-row" style="height: 40px;">
                            <input type="text" name="cell_phone" id="cell_phone" value="<?php echo $cell_phone; ?>" placeholder="Cell Phone" class="span3">
                            <input type="text" name="home_phone" id="home_phone" value="<?php echo $home_phone; ?>" placeholder="Home Phone" class="span3">
                            <input type="text" name="business_phone" id="business_phone" value="<?php echo $business_phone; ?>" placeholder="Business Phone" class="span3">
                            <input type="text" name="fax" id="fax" value="<?php echo $fax; ?>" placeholder="Fax" class="span3">
                        </div>
                    </div>

                    <div class="control-group">&nbsp;</div>


                    <div class="control-group">
                        <label for="textfield" class="control-label">Location</label>
                        <div class="controls controls-row" style="height: 15px;">
                            <label for="address" class="span3">Address<span style="color:red;">*</span></label>
                            <label for="city" class="span3">City<span style="color:red;">*</span></label>
                            <label for="province" class="span3">Province<span style="color:red;">*</span></label>
                            <label for="postal_code" class="span3">Postal<span style="color:red;">*</span></label>
                        </div>
                        <div class="controls controls-row" style="height: 40px;">
                            <input type="text" name="address" id="address" value="<?php echo $address; ?>" placeholder="Address" class="span3">
                            <input type="text" name="city" id="city" value="<?php if(trim($city)==''){ echo "Winnipeg"; } else { echo $city; } ?>" placeholder="City" class="span3">
                            <input type="text" name="province" id="province" value="<?php if(trim($province)==''){ echo "Manitoba"; } else { echo $province; } ?>" placeholder="Province" class="span3">
                            <input type="text" name="postal_code" id="postal_code" value="<?php echo $postal_code; ?>" placeholder="Postal" class="span3">
                        </div>
                    </div>







                    <div class="form-actions">

                        <button type="submit" class="button button-basic-blue">Find</button>

                        <button type="button" class="button button-basic-green btnaddquote">Add Client</button>

                        <button type="button" class="button button-basic" onClick="window.location.href='<?php echo site_url('quotes/manage'); ?>'">Cancel</button>

                    </div>


                    </form>
                </div>
            </div>
            <!---find part--->



            <!----lists-->
            <div class="box">
                <div class="box-head">
                    <i class="icon-list-ul"></i>
                    <span>Clients</span>
                </div>

                <!---sort-->

                <!-- dataTables plugin -->
                <script src="<?php echo base_url(); ?>js/jquery.dataTables.min.js"></script>
                <!-- TableTools for dataTables plguin -->
                <script src="<?php echo base_url(); ?>js/TableTools.min.js"></script>                         

                <div class="box-body box-body-nopadding">
                    <?php /* ?><div class="highlight-toolbar">
                      <div class="pull-left"></div>

                      <div class="pull-right"><div class="btn-toolbar">
                      <div class="btn-group">
                      <a href="<?php echo site_url('clients/add_client');?>" class='button button-basic button-icon' rel="tooltip" title="Add Client"><i class="icon-plus-sign"></i></a>
                      </div>
                      </div></div>

                      </div><?php */ ?>
                    <table id="clients" class="table table-nomargin table-bordered dataTable table-striped table-hover">
                        <thead>
                            <tr>
                                <th>Company</th>
                                <th>Contact</th>
                                <th>Email</th>
                                <th>Cell Ph.</th>
                                <th>Home Ph.</th>
                                <th>Business Ph.</th>

                                <th>Area</th>
                                <th>Postal</th>
                                <th>Quote</th>
                            </tr>
                        </thead>
                        <tbody>

                            <?php if ($result) {
                                foreach ($result as $row) { ?>

                                    <tr> 
                                        <td><?php if ($row->first_name != '') {
                                echo anchor('clients/edit_client/' . $row->client_id, $row->first_name, ' rel="tooltip" title="Edit" ');
                            } else {
                                
                            } ?></td> 
                                        <td><?php echo $row->last_name; ?></td>
                                        <td><?php echo $row->email; ?></td>
                                        <td><?php echo $row->cell_phone; ?></td>
                                        <td><?php echo $row->home_phone; ?></td>
                                        <td><?php echo $row->business_phone; ?></td>                                      
                                        <td><?php if ($row->city != '' && $row->province != '') {
                                        echo $row->city . ',' . $row->province;
                                    } elseif ($row->city != '') {
                                        echo $row->city;
                                    } elseif ($row->province != '') {
                                        echo $row->province;
                                    } ?></td>                                      
                                        <td><?php echo $row->postal_code; ?></td>                                     
                                        <td>                                               
                                            <a href="javascript:void(0)" class='button button-basic button-icon addquote' data-id="<?php echo $row->client_id; ?>" data-company="<?php if ($row->first_name != '') {
                                echo $row->first_name . '&nbsp;';
                            } if ($row->last_name != '') {
                                echo $row->last_name;
                            } ?>" rel="tooltip" title="Add Quote"><i class="icon-file-alt"></i></a>
                                        </td>

                                    </tr> 



    <?php } ?>
<?php } ?>

                        </tbody>
                    </table>
                </div>


                <!---sort-->




            </div>
            <!---lists--->




        </div>
    </div>


</div>



<?php
$quotetype_list = quotetype_list();
$bustype_list = bustype_list();
$team_list = team_list_by_role_id(1);
?>
<!-- Modal -->

<div class="modal" id="addquote" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display:none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Add Quote</h4>
            </div>
            <div class="modal-body">


                <form name="frmaddquote" class="form-horizontal">

                    <div class="control-group">
                        <label class="control-label"><b>Client</b></label>
                        <div class="controls">              
                            <input type="text" id="company_name" value="" readonly="readonly" />
                            <input type="hidden" name="client_id" id="client_id" value="" />
                        </div>
                    </div>


                    <div class="control-group">
                        <label class="control-label"><b>Quote Type</b></label>
                        <div class="controls">
                            <select name="quotetype_id" id="quotetype_id" class="input-large">
                                <?php if (!empty($quotetype_list)) {
                                    foreach ($quotetype_list as $type) { ?>
                                        <option value="<?php echo $type->quotetype_id; ?>"><?php echo ucfirst($type->quotetype); ?></option>
    <?php }
} ?>
                            </select>
                        </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label"><b>Comm/Res</b></label>
                        <div class="controls">
                            <select name="bustype_id" id="bustype_id" class="input-large">
                                <?php if (!empty($bustype_list)) {
                                    foreach ($bustype_list as $type) { ?>
                                        <option value="<?php echo $type->bustype_id; ?>"><?php echo ucfirst($type->description); ?></option>
    <?php }
} ?>
                            </select>
                        </div>
                    </div>                         

                    
                    
                    
                    
                    <div class="control-group">
                        <label class="control-label"><b>Paint</b></label>
                        <div class="controls paint1" style="width:210px;">
                            <select name="PaintQuality_1" id="PaintQuality_1" class="input-large chosen-select">
                                        <option value="">---Select Interior---</option>
<?php if (!empty($quote_paint_1)) {
    foreach ($quote_paint_1 as $paint) { ?>
                                                <option value="<?php echo $paint->productcost_id; ?>" ><?php echo ucfirst(trim($paint->description)) . '&nbsp;&nbsp;|&nbsp;&nbsp;' . ucfirst(trim($paint->paintbrand)); ?></option>
                                        <?php }
                                    } ?>
                                    </select>
                        </div>
                        
                        <div class="controls paint2" style="width:210px; display: none;">
                            <select name="PaintQuality_2" id="PaintQuality_2" class="input-large chosen-select">
                                        <option value="">---Select Exterior---</option>
<?php if (!empty($quote_paint_2)) {
    foreach ($quote_paint_2 as $paint) { ?>
                                                <option value="<?php echo $paint->productcost_id; ?>" ><?php echo ucfirst(trim($paint->description)) . '&nbsp;&nbsp;|&nbsp;&nbsp;' . ucfirst(trim($paint->paintbrand)); ?></option>
                                        <?php }
                                    } ?>
                                    </select>
                        </div>
                        
                        
                    </div>                         

                    
                    
                     <div class="control-group">
                        <label class="control-label"><b>Job Rate</b></label>
                        <div class="controls">
                                <select name="rate" id="rate" class="input-large">
                                    
<?php for ($i = 75; $i >= 46; $i--) { ?>
                                        <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
<?php } ?>
                                </select>
                    
                     </div>
                    </div>                         

                    

                    <div class="control-group">
                        <label class="control-label"><b>Sales Lead</b></label>
                        <div class="controls" style="height: 150px; width:210px;">
                            <select name="team_id" id="team_id" class="input-large chosen-select">
<?php if (!empty($team_list)) {
    foreach ($team_list as $team) { ?>
                                        <option value="<?php echo $team->team_id; ?>"><?php echo ucfirst($team->name); ?></option>
    <?php }
} ?>
                            </select>
                        </div>
                    </div>

                </form>

                <style>
                    .chzn-results { height:80px; } 
                </style> 



            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary savequote">Save changes</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<script type="text/javascript">
    $(".addquote").on("click",function(){
        var client_id=$(this).attr('data-id');
        var company = $(this).attr('data-company');
        $("#client_id").val(client_id);
        $("#company_name").val(company);
        $("#addquote").modal('show');
				
    });
    
    
    $("#quotetype_id").live("change",function(){
       
       var paint_type=$("#quotetype_id option:selected").val();
       
       if(paint_type==1){
           $("div.paint1").show();
           $("div.paint2").hide();
       } else if(paint_type==2){
           $("div.paint2").show();
           $("div.paint1").hide();
       } else {
           $("div.paint1").show();
           $("div.paint2").hide();
       }
       
    });
    
    
			
    $(".btnaddquote").on("click",function(){
				
        ////ajax calll
				
        var cell_phone=$.trim($('#cell_phone').val());
        var home_phone=$.trim($('#home_phone').val());
        var business_phone=$.trim($('#business_phone').val());
        var fax=$.trim($('#fax').val());
				
        var first_name=$.trim($('#first_name').val());
        var last_name=$.trim($('#last_name').val());
        var email=$.trim($('#email').val());
				
        var address=$.trim($('#address').val());
        var city=$.trim($('#city').val());
        var province=$.trim($('#province').val());
        var postal_code=$.trim($('#postal_code').val());
				
        var client_id=0;
        var company='';
				
        if(first_name==''){
            alert('Please enter company.');
            $('#first_name').focus();
            return false;
        }
				
        if(cell_phone==''){
            alert('Please enter Cell Phone.');
            $('#cell_phone').focus();
            return false;
        }
				
				
        if(address==''){
            alert('Please enter Address.');
            $('#address').focus();
            return false;
        }
        if(city==''){
            alert('Please enter City.');
            $('#city').focus();
            return false;
        }
        if(province==''){
            alert('Please enter Province.');
            $('#province').focus();
            return false;
        }
        if(postal_code==''){
            alert('Please enter Postal Code.');
            $('#postal_code').focus();
            return false;
        }
				
        var res = $.ajax({						
            type: 'POST',
            url: '<?php echo site_url('quotes/add_client/'); ?>',
            data: {first_name:first_name,last_name:last_name,email:email,cell_phone:cell_phone,home_phone:home_phone,business_phone:business_phone,fax:fax,address:address,city:city,province:province,postal_code:postal_code},
            dataType: 'json', 
            cache: false,
            async: false                     
        }).responseText;							
			 
		
			 
        var rcom_res = jQuery.parseJSON(res);
	
        if(rcom_res.status=='success')
        {
            client_id=rcom_res.client_id;
            company=first_name+' '+last_name;
        }
        else {
            alert("Unable to create Client. Please do once again.");
            return false;
        }
				
				
        ///redirect to another page
				
        $("#client_id").val(client_id);
        $("#company_name").val(company);
        $("#addquote").modal('show');	
    });
			
    $(".savequote").on("click",function(){
        var quote_id=0;
				
				
        var client_id=$('#client_id').val();
        var quotetype_id=$('#quotetype_id').val();
        var bustype_id=$('#bustype_id').val();
        var team_id=$('#team_id').val();
				
        if(client_id=='' || client_id==0) {
            alert('Client cannot be blank');
            return false;
        }
        if(quotetype_id=='' || quotetype_id==0) {
            alert('Quote type cannot be blank');
            return false;
        }
        if(bustype_id=='' || bustype_id==0) {
            alert('Bus type cannot be blank');
            return false;
        }
        if(team_id=='' || team_id==0) {
            alert('Sales Lead cannot be blank');
            return false;
        }
        
        
        var paint_type=$("#quotetype_id option:selected").val();
       
      
       
       if(paint_type==1){
           var paint=$("#PaintQuality_1 option:selected").val();
       } else if(paint_type==2){
           var paint=$("#PaintQuality_2 option:selected").val();
       } else {
           var paint=$("#PaintQuality_1 option:selected").val();
       }
        
        if(paint=='' || paint==0){
            alert('Please select paint.');
            return false;
        }
        
      var rate=$("#rate option:selected").val();
      if(rate=='' || rate==0){
            alert('Please select Job rate.');
            return false;
        }
				
        var res = $.ajax({						
            type: 'POST',
            url: '<?php echo site_url('quotes/add_client_quote/'); ?>',
            data: {client_id:client_id,quotetype_id:quotetype_id,bustype_id:bustype_id,team_id:team_id,PaintQuality:paint,job_rate:rate},
            dataType: 'json', 
            cache: false,
            async: false                     
        }).responseText;							
			 
		
			 	
        var rcom_res = jQuery.parseJSON(res);
	
        if(rcom_res.status=='success')
        {
            quote_id=rcom_res.quote_id;					
        }
        else {
            alert("Unable to create Quote. Please do once again.");
            return false;
        }
				
        window.location.href="<?php echo site_url('quotes/add_quote/'); ?>/"+quote_id;
    });
			
</script>