
<ul class="wizard-steps wizard-style-2 steps-6">
    <li class='active'>
          <button type="button" class="button button-basic-red">Quote Details</button>
        <div class="single-step">
            
            <span class="title">
                Step 1
            </span>
            <span class="circle">
                <?php if ($step_index == 1) { ?><span class="active" style="background: #D24141;"></span><?php } ?>
            </span>
            <span class="description">
                <a href="<?php echo site_url('quotes/add_quote/' . $quote_id); ?>">Quote Details</a>
            </span>
        </div>
    </li>
    <li>
         <button type="button" class="button button-basic-blue">Material Order</button>
        <div class="single-step">
            <span class="title">
                Step 2
            </span>
            <span class="circle">
                <?php if ($step_index == 2) { ?><span class="active"></span><?php } ?>
            </span>
            <span class="description">
                <a href="<?php echo site_url('quotes/step2/' . $quote_id); ?>">Materials</a>
            </span>
        </div>
    </li>
    <li>
          <button type="button" class="button button-basic-darkblue">Work Order</button>
        <div class="single-step">
            <span class="title">
                Step 3
            </span>
            <span class="circle">
                <?php if ($step_index == 3) { ?><span class="active" style="background: #2D2F30;"></span><?php } ?>
            </span>
            <span class="description">
                <a href="<?php echo site_url('quotes/step3/' . $quote_id); ?>">Final Quote</a>
            </span>
        </div>
    </li>
    <li>
        
        <div style="margin: 18px; padding: 3px 12px 3px 12px;"></div>
        
        <div class="single-step">
            <span class="title">
                Step 4
            </span>
            <span class="circle">
                <?php if ($step_index == 4) { ?><span class="active" style="background: #2D2F30;"></span><?php } ?>
            </span>
            <span class="description">
                <a href="<?php echo site_url('quotes/step4/' . $quote_id); ?>">Financial</a>
            </span>
        </div>
    </li>
    <li>
         <button type="button" class="button button-basic-green">Follow Up Form</button>
        <div class="single-step">
            <span class="title">
                Step 5
            </span>
            <span class="circle">
                <?php if ($step_index == 5) { ?><span class="active" style="background: #35A032;"></span><?php } ?>
            </span>
            <span class="description">
                <a href="<?php echo site_url('quotes/step5/' . $quote_id); ?>">Follow Up</a>
            </span>
        </div>
    </li>
    <li>
          <div style="margin: 18px; padding: 3px 12px 3px 12px;"></div>
        <div class="single-step">
            <span class="title">
                Step 6
            </span>
            <span class="circle">
                <?php if ($step_index == 6) { ?><span class="active"></span><?php } ?>
            </span>
            <span class="description">
                <a href="<?php echo site_url('quotes/step6/' . $quote_id); ?>">Emp. TimeCharge</a>
            </span>
        </div>
    </li>
</ul>

<style>
    .single-step{
        margin-top: 5px;
    }
</style>
