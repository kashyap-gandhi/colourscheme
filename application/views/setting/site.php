<div class="page-header">
				<div class="pull-left">
					<h4><i class="icon-cogs"></i> Site Setting</h4>
				</div>
				<div class="pull-right">
					<ul class="bread">
						<li>System Setting<span class="divider">/</span></li>						
						<li class='active'>Site</li>
					</ul>
				</div>
			</div>
            
            
            <div class="container-fluid" id="content-area">
				
<div class="row-fluid">
                <div class="span12">
                
                 <?php if($error!=''){ ?>

		<div class="alert alert-error">
											<button type="button" class="close" data-dismiss="alert">&times;</button>
											<strong>Warning !</strong> <?php echo $error;?>
										</div>    <?php }?>
                                        
                                         <?php if($msg!=''){ ?>

		<div class="alert alert-success">
											<button type="button" class="close" data-dismiss="alert">&times;</button>
											<strong>Success !</strong> <?php echo $msg;?>
										</div>    <?php }?>
    
					
						<div class="box">
							<div class="box-head">
								<i class="icon-list-ul"></i>
								<span>Change your site setting</span>
							</div>
							<div class="box-body box-body-nopadding">
                             <?php
									$attributes = array('name'=>'frm_sitesetting','class'=>'form-horizontal form-bordered');
									echo form_open('setting/site',$attributes);
								  ?> 
                                  
								
									<div class="control-group">
										<label for="textfield" class="control-label">Site Name</label>
										<div class="controls">
											<input name="site_name" id="site_name" type="text" value="<?php echo $site_name; ?>" placeholder="site name" class="input-xlarge">
										</div>
									</div>
									<div class="control-group">
										<label for="password" class="control-label">Currency Symbol</label>
										<div class="controls">
											<input name="currency_symbol" id="currency_symbol" type="text" value="<?php echo $currency_symbol; ?>" placeholder="Currency Symbol" class="input-xlarge">
										</div>
									</div>
									
                                    
                                    
                                    
                                    
                                   
									<div class="control-group">
										<label for="textarea" class="control-label">Currency Code</label>
										<div class="controls">
											<input name="currency_code" id="currency_code" type="text" value="<?php echo $currency_code; ?>" placeholder="Currency Code" class="input-xlarge">
										</div>
									</div>
                                    
                      <div class="control-group">
										<label for="textarea" class="control-label">Date Format</label>
										<div class="controls">              
                                    
                       <select name="date_format" id="date_format">

                      <option value='d M,Y' <?php if($date_format == 'd M,Y') { echo 'selected="selected"'; } ?>>d M,Y</option>

                      <option value='Y-m-d' <?php if($date_format == 'Y-m-d') { echo 'selected="selected"'; } ?>>Y-m-d</option>  

                      <option value='m-d-Y' <?php if($date_format == 'm-d-Y') { echo 'selected="selected"'; } ?>>m-d-Y</option> 

                      <option value='d-m-Y' <?php if($date_format == 'd-m-Y') { echo 'selected="selected"'; } ?>>d-m-Y</option>

                      <option value='Y/m/d' <?php if($date_format == 'Y/m/d') { echo 'selected="selected"'; } ?>>Y/m/d</option> 

                      <option value='m/d/Y' <?php if($date_format == 'm/d/Y') { echo 'selected="selected"'; } ?>>m/d/Y</option>

                      <option value='d/m/Y' <?php if($date_format == 'd/m/Y') { echo 'selected="selected"'; } ?>>d/m/Y</option> 

                  </select>
                  
                  </div>
				</div>
                                    
                                    
                   <div class="control-group">
										<label for="textarea" class="control-label">Time Format</label>
										<div class="controls">    
                                        
                  <select name="time_format" id="time_format" >

                     

                      <option value='H:i a' <?php if($time_format == 'H:i a') { echo 'selected="selected"'; } ?>>H:i a</option>

                      <option value='H:i:s a' <?php if($time_format == 'H:i:s a') { echo 'selected="selected"'; } ?>>H:i:s a</option>  

                      <option value='h:i:s a' <?php if($time_format == 'h:i:s a') { echo 'selected="selected"'; } ?>>h:i:s a</option>

                    

                  </select>
                  
                    </div>
				</div>
                          
                       <div class="control-group">
										<label for="textarea" class="control-label">Date Time Format</label>
										<div class="controls">    
                                        
                                             
                   <select name="date_time_format" id="date_time_format" >

                      <option value='d M,Y H:i a' <?php if($date_time_format == 'd M,Y H:i a') { echo 'selected="selected"'; } ?>>d M,Y H:i a</option>

                      <option value='d M,Y H:i:s a' <?php if($date_time_format == 'd M,Y H:i:s a') { echo 'selected="selected"'; } ?>>d M,Y H:i:s a</option>

                      <option value='d M,Y h:i:s a' <?php if($date_time_format == 'd M,Y h:i:s a') { echo 'selected="selected"'; } ?>>d M,Y h:i:s a</option>

                      

                      <option value='Y-m-d H:i a' <?php if($date_time_format == 'Y-m-d H:i a') { echo 'selected="selected"'; } ?>>Y-m-d H:i a</option> 

                      <option value='Y-m-d H:i:s a' <?php if($date_time_format == 'Y-m-d H:i:s a') { echo 'selected="selected"'; } ?>>Y-m-d H:i:s a</option>

                      <option value='Y-m-d h:i:s a' <?php if($date_time_format == 'Y-m-d h:i:s a') { echo 'selected="selected"'; } ?>>Y-m-d h:i:s a</option>

 

                      <option value='m-d-Y H:i a' <?php if($date_time_format == 'm-d-Y H:i a') { echo 'selected="selected"'; } ?>>m-d-Y H:i a</option> 

                      <option value='m-d-Y H:i:s a' <?php if($date_time_format == 'm-d-Y H:i:s a') { echo 'selected="selected"'; } ?>>m-d-Y H:i:s a</option> 

                      <option value='m-d-Y h:i:s a' <?php if($date_time_format == 'm-d-Y h:i:s a') { echo 'selected="selected"'; } ?>>m-d-Y h:i:s a</option> 

  

                      <option value='d-m-Y H:i a' <?php if($date_time_format == 'd-m-Y H:i a') { echo 'selected="selected"'; } ?>>d-m-Y H:i a</option> 

                      <option value='d-m-Y H:i:s a' <?php if($date_time_format == 'd-m-Y H:i:s a') { echo 'selected="selected"'; } ?>>d-m-Y H:i:s a</option> 

                      <option value='d-m-Y h:i:s a' <?php if($date_time_format == 'd-m-Y h:i:s a') { echo 'selected="selected"'; } ?>>d-m-Y h:i:s a</option>

                      

                      <option value='Y/m/d H:i a' <?php if($date_time_format == 'Y/m/d H:i a') { echo 'selected="selected"'; } ?>>Y/m/d H:i a</option> 

                      <option value='Y/m/d H:i:s a' <?php if($date_time_format == 'Y/m/d H:i:s a') { echo 'selected="selected"'; } ?>>Y/m/d H:i:s a</option> 

                      <option value='Y/m/d h:i:s a' <?php if($date_time_format == 'Y/m/d h:i:s a') { echo 'selected="selected"'; } ?>>Y/m/d h:i:s a</option>

                       

                      <option value='m/d/Y H:i a' <?php if($date_time_format == 'm/d/Y H:i a') { echo 'selected="selected"'; } ?>>m/d/Y H:i a</option> 

                      <option value='m/d/Y H:i:s a' <?php if($date_time_format == 'm/d/Y H:i:s a') { echo 'selected="selected"'; } ?>>m/d/Y H:i:s a</option> 

                      <option value='m/d/Y h:i:s a' <?php if($date_time_format == 'm/d/Y h:i:s a') { echo 'selected="selected"'; } ?>>m/d/Y h:i:s a</option>

                      

                      <option value='d/m/Y H:i a' <?php if($date_time_format == 'd/m/Y H:i a') { echo 'selected="selected"'; } ?>>d/m/Y H:i a</option> 

                      <option value='d/m/Y H:i:s a' <?php if($date_time_format == 'd/m/Y H:i:s a') { echo 'selected="selected"'; } ?>>d/m/Y H:i:s a</option> 

                      <option value='d/m/Y h:i:s a' <?php if($date_time_format == 'd/m/Y h:i:s a') { echo 'selected="selected"'; } ?>>d/m/Y h:i:s a</option>

                       

                  </select>
                          </div>
				</div>
                         
                         
                          <div class="control-group">
										<label for="textarea" class="control-label">TimeZone</label>
										<div class="controls">              
                                    
                       <select name="site_timezone" id="site_timezone">

                                            
                      <option value="0" <?php if($site_timezone == '0') { echo 'selected="selected"'; } ?>>0</option>		  
			<option value="+1" <?php if($site_timezone == '+1') { echo 'selected="selected"'; } ?>>+1</option>
			<option value="+2" <?php if($site_timezone == '+2') { echo 'selected="selected"'; } ?>>+2</option>
			<option value="+3" <?php if($site_timezone == '+3') { echo 'selected="selected"'; } ?>>+3</option>
			<option value="+4" <?php if($site_timezone == '+4') { echo 'selected="selected"'; } ?>>+4</option>
			<option value="+5" <?php if($site_timezone == '+5') { echo 'selected="selected"'; } ?>>+5</option>
			<option value="+6" <?php if($site_timezone == '+6') { echo 'selected="selected"'; } ?>>+6</option>
			<option value="+7" <?php if($site_timezone == '+7') { echo 'selected="selected"'; } ?>>+7</option>
			<option value="+8" <?php if($site_timezone == '+8') { echo 'selected="selected"'; } ?>>+8</option>
			<option value="+9" <?php if($site_timezone == '+9') { echo 'selected="selected"'; } ?>>+9</option>
			<option value="+10" <?php if($site_timezone == '+10') { echo 'selected="selected"'; } ?>>+10</option>
			<option value="+11" <?php if($site_timezone == '+11') { echo 'selected="selected"'; } ?>>+11</option>
			<option value="+12" <?php if($site_timezone == '+12') { echo 'selected="selected"'; } ?>>+12</option>
			<option value="-10" <?php if($site_timezone == '-10') { echo 'selected="selected"'; } ?>>-10</option>
			<option value="-9" <?php if($site_timezone == '-9') { echo 'selected="selected"'; } ?>>-9</option>
			<option value="-8" <?php if($site_timezone == '-8') { echo 'selected="selected"'; } ?>>-8</option>
			<option value="-7" <?php if($site_timezone == '-7') { echo 'selected="selected"'; } ?>>-7</option>
			<option value="-6" <?php if($site_timezone == '-6') { echo 'selected="selected"'; } ?>>-6</option>
			<option value="-5" <?php if($site_timezone == '-5') { echo 'selected="selected"'; } ?>>-5</option>
			<option value="-4" <?php if($site_timezone == '-4') { echo 'selected="selected"'; } ?>>-4</option>
			<option value="-3" <?php if($site_timezone == '-3') { echo 'selected="selected"'; } ?>>-3</option>
			<option value="-2" <?php if($site_timezone == '-2') { echo 'selected="selected"'; } ?>>-2</option>
			<option value="-1" <?php if($site_timezone == '-1') { echo 'selected="selected"'; } ?>>-1</option>

                  </select>
                  
                  </div>
				</div>     
                                    
                                    
									<div class="form-actions">
										<button type="submit" class="button button-basic-blue">Save changes</button>
										
									</div>
                                    <input type="hidden" name="site_id" id="site_id" value="<?php echo $site_id;?>" />
								</form>
							</div>
						</div>
					</div>
				</div>
				
				
				
			</div>