<div class="page-header">
				<div class="pull-left">
					<h4><i class="icon-cogs"></i> Email Templates</h4>
				</div>
				<div class="pull-right">
					<ul class="bread">
						<li>System Setting<span class="divider">/</span></li>						
						<li class='active'>Email Templates</li>
					</ul>
				</div>
			</div>
            
            
            
            
            <div class="container-fluid" id="content-area">
            
            
            <div class="row-fluid">
								<div class="span12">
                                
                                
                                
                                
                                <?php if($msg!='') { ?>
<div class="alert alert-success">
											<button type="button" class="close" data-dismiss="alert">&times;</button>
											<strong>Success !</strong>
	<?php if($msg=='active') { ?>Email Template has been activated successfully. <?php } ?>
	<?php if($msg=='inactive') { ?>Email Template has been inactivated successfully. <?php } ?>
	<?php if($msg=='delete') { ?>Email Template has been deleted successfully. <?php } ?>	
	<?php if($msg=='insert') { ?>New Email Template has been added successfully. <?php } ?>	
	<?php if($msg=='update') { ?>Email Template has been updated successfully. <?php } ?>		
</div> 
<?php } ?>	
                                
                                
                                
									<div class="box">
										<div class="box-head">
											<i class="icon-table"></i>
											<span>Email Templates</span>
										</div>
                                        
                                        <form name="frm_listpackageitem" id="frm_listpackageitem" action="<?php echo site_url('setting/email_template_action');?>" method="post">
<input type="hidden" name="action" id="action" />
<input type="hidden" name="offset" id="offset" value="<?php echo $offset; ?>" />


										<div class="box-body box-body-nopadding">
											



											<table class="table table-nomargin table-striped table-bordered table-hover table-pagination">
												<thead>
													<tr>
														<th>Name</th>
														<th>Subject</th>
														<th>From Email</th>
														<th>Action</th>														
													</tr>
												</thead>
												<tbody>
													<?php if($result) { 
											foreach($result as $res) {
										?>
                                        
                                        
														<tr>
															<td><?php echo anchor('setting/edit_email_template/'.$res->email_template_id.'/'.$offset,ucfirst($res->task));?></td>
															<td><?php echo ucfirst($res->subject);?></td>
															<td><?php echo ucfirst($res->from_address);?></td>
															<td><a href="<?php echo site_url('setting/edit_email_template/'.$res->email_template_id.'/'.$offset);?>" class='button button-basic button-icon' rel="tooltip" title="Edit"><i class="icon-edit"></i></a></td>
															
														</tr>
                                                        
                                                        <?php } } else { ?>
									<tr><td colspan="4" align="center" valign="middle"  style="text-align:center;">No Email Template has been added yet.</td></tr>
										<?php } ?>
                                        
                                        
													</tbody>
												</table>	
                                                
												<div class="bottom-table">
													<div class="pull-left">
														
													</div>
													<div class="pull-right">
                                                   
                                                    <div class="pagination pagination-custom">
                                                    <?php echo $page_link; ?>
													</div>
                                                    
                                                    </div>
												</div>
                                                
                                               
                                                
                                                
											</div> </form>
                                            
                                            
                                            
										</div>
									</div>
								</div>
            
            
            </div>