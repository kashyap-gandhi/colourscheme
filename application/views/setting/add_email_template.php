<div class="page-header">
				<div class="pull-left">
					<h4><i class="icon-cogs"></i>Email Templates</h4>
				</div>
				<div class="pull-right">
					<ul class="bread">
						<li>System Setting<span class="divider">/</span></li>						
						<li class='active'>Email Templates</li>
					</ul>
				</div>
			</div>
            
            
            
            <div class="container-fluid" id="content-area">
				
              
              
              
             
    
                
				<div class="row-fluid">
                <div class="span12">
                
                 <?php if($error!=''){ ?>

		<div class="alert alert-error">
											<button type="button" class="close" data-dismiss="alert">&times;</button>
											<strong>Warning !</strong> <?php echo $error;?>
										</div>    <?php }?>
                                        
                                        
					
						<div class="box">
							<div class="box-head">
								<i class="icon-list-ul"></i>
								<span>Edit Email Templates</span>
							</div>
							<div class="box-body box-body-nopadding">
                            
                                   <?php
									$attributes = array('name'=>'frm_email_template/'.$email_template_id,'class'=>'form-horizontal form-bordered');
									echo form_open('setting/add_email_template',$attributes);
								  ?> 
                                  
                                  
								
									<div class="control-group">
										<label for="textfield" class="control-label">Task Name</label>
										<div class="controls">
                                        <?php echo ucfirst($task); ?>
										<input type="hidden" name="task" value="<?php echo $task; ?>" />
										</div>
									</div>
                                    
                                    
									<div class="control-group">
										<label for="password" class="control-label">From Name</label>
										<div class="controls">
											<input name="from_name" id="from_name" type="text" value="<?php echo $from_name; ?>"  placeholder="From Name" class="input-xlarge">
										</div>
									</div>
									
                                    
                                    
                                    
                                    
                                   
									<div class="control-group">
										<label for="textarea" class="control-label">From Address</label>
										<div class="controls">
											<input name="from_address" id="from_address" type="text" value="<?php echo $from_address; ?>" placeholder="From Address" class="input-xlarge">
                                          
										</div>
									</div>
                                    
                                    
                                    <div class="control-group">
										<label for="textarea" class="control-label">Reply Address</label>
										<div class="controls">
											<input name="reply_address" id="reply_address" type="text" value="<?php echo $reply_address; ?>" placeholder="Reply Address" class="input-xlarge">
                                          
										</div>
									</div>
                                    
                                    <div class="control-group">
										<label for="textarea" class="control-label">Subject</label>
										<div class="controls">
											<input name="subject" id="subject" type="text" value="<?php echo $subject; ?>" placeholder="Subject" class="input-xlarge">
										</div>
									</div>
                                    
                                    <div class="control-group">
										<label for="textarea" class="control-label">Message</label>
										<div class="controls">
											<textarea name="message" id="message" rows="5" class="input-block-level"><?php echo $message; ?></textarea>
                                            
										</div>
									</div>
                                    
                         
                                    
									<div class="form-actions">
										<button type="submit" class="button button-basic-blue">Save changes</button>
										
									</div>
                                    <input type="hidden" name="email_template_id" id="email_template_id" value="<?php echo $email_template_id;?>" />
                                                
                                                <input type="hidden" name="offset" id="offset" value="<?php echo $offset;?>" />
								</form>
							</div>
						</div>
					</div>
				</div>
				
				
				
			</div>