<div class="page-header">
				<div class="pull-left">
					<h4><i class="icon-cogs"></i> Email Setting</h4>
				</div>
				<div class="pull-right">
					<ul class="bread">
						<li>System Setting<span class="divider">/</span></li>						
						<li class='active'>Email</li>
					</ul>
				</div>
			</div>
            
            
            
            <div class="container-fluid" id="content-area">
				
              
              
              
             
    
                
				<div class="row-fluid">
                <div class="span12">
                
                 <?php if($error!=''){ ?>

		<div class="alert alert-error">
											<button type="button" class="close" data-dismiss="alert">&times;</button>
											<strong>Warning !</strong> <?php echo $error;?>
										</div>    <?php }?>
                                        
                                         <?php if($msg!=''){ ?>

		<div class="alert alert-success">
											<button type="button" class="close" data-dismiss="alert">&times;</button>
											<strong>Success !</strong> <?php echo $msg;?>
										</div>    <?php }?>
    
					
						<div class="box">
							<div class="box-head">
								<i class="icon-list-ul"></i>
								<span>Change your site setting</span>
							</div>
							<div class="box-body box-body-nopadding">
                            
                                   <?php
									$attributes = array('name'=>'frm_emailsetting','class'=>'form-horizontal form-bordered');
									echo form_open('setting/email',$attributes);
								  ?> 
                                  
								
									<div class="control-group">
										<label for="textfield" class="control-label">Mailer</label>
										<div class="controls">
                                        <select name="mailer" id="mailer"> 
							<option value="mail" <?php if($mailer=='mail') { ?> selected="selected" <?php } ?> >PHP Mail</option>
						<option value="smtp" <?php if($mailer=='smtp') { ?> selected="selected" <?php } ?> >SMTP</option>
						<option value="sendmail" <?php if($mailer=='sendmail') { ?> selected="selected" <?php } ?> >sendmail</option>
												</select> 
                                                
										
										</div>
									</div>
									<div class="control-group">
										<label for="password" class="control-label">Send Mail Path</label>
										<div class="controls">
											<input name="sendmail_path" id="sendmail_path" type="text" value="<?php echo $sendmail_path; ?>"  placeholder="Send Mail Path" class="input-xlarge"><span class="help-inline">(if Mailer is sendmail)</span>
										</div>
									</div>
									
                                    
                                    
                                    
                                    
                                   
									<div class="control-group">
										<label for="textarea" class="control-label">SMTP Port</label>
										<div class="controls">
											<input name="smtp_port" id="smtp_port" type="text" value="<?php echo $smtp_port; ?>" placeholder="SMTP Port" class="input-xlarge">
                                            <span class="help-inline">(465 or 25 or 587)</span> 
										</div>
									</div>
                                    
                                    
                                    <div class="control-group">
										<label for="textarea" class="control-label">SMTP Host</label>
										<div class="controls">
											<input name="smtp_host" id="smtp_host" type="text" value="<?php echo $smtp_host; ?>" placeholder="SMTP Host" class="input-xlarge">
                                            <span class="help-inline">(if smtp user is gmail then ssl://smtp.googlemail.com)</span> 
										</div>
									</div>
                                    
                                    <div class="control-group">
										<label for="textarea" class="control-label">SMTP Email</label>
										<div class="controls">
											<input name="smtp_email" id="smtp_email" type="text" value="<?php echo $smtp_email; ?>" placeholder="SMTP Email" class="input-xlarge">
										</div>
									</div>
                                    
                                    <div class="control-group">
										<label for="textarea" class="control-label">SMTP Password</label>
										<div class="controls">
											<input name="smtp_password" id="smtp_password" type="password" value="<?php echo $smtp_password; ?>" placeholder="SMTP Password" class="input-xlarge">
										</div>
									</div>
                                    
                         
                                    
									<div class="form-actions">
										<button type="submit" class="button button-basic-blue">Save changes</button>
										
									</div>
                                    <input type="hidden" name="email_setting_id" id="email_setting_id" value="<?php echo $email_setting_id;?>" />
								</form>
							</div>
						</div>
					</div>
				</div>
				
				
				
			</div>