<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
   "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<meta charset="utf8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
   <title><?php echo SITE_NAME; ?></title>
    <?php $base_url=base_url(); ?>	
   <!-- Bootstrap -->
	<link rel="stylesheet" href="<?php echo $base_url;?>css/bootstrap.min.css">
	<!-- Bootstrap responsive -->
	<link rel="stylesheet" href="<?php echo $base_url;?>css/bootstrap-responsive.min.css">
	<!-- Theme CSS -->
    
    <link rel="stylesheet" href="<?php echo base_url();?>css/chosen.css">
    
    	<!-- jQuery UI -->
	<link rel="stylesheet" href="<?php echo $base_url;?>css/jquery-ui.css">
	<!-- jQuery UI Theme -->
	<link rel="stylesheet" href="<?php echo $base_url;?>css/jquery.ui.theme.css">
	<!-- multi select plugin -->
	<link rel="stylesheet" href="<?php echo $base_url;?>css/multi-select.css">
    
    
	<!--[if !IE]> -->
	<link rel="stylesheet" href="<?php echo $base_url;?>css/style.css">
	<!-- <![endif]-->
	<!--[if IE]>
	<link rel="stylesheet" href="<?php echo $base_url;?>css/style_ie.css">
	<![endif]-->

	<!-- jQuery -->
	<script src="<?php echo $base_url;?>js/jquery.min.js"></script>
    <!-- Old jquery functions -->
	<script src="<?php echo $base_url;?>js/jquery.migrate.min.js"></script>
	<!-- jQuery UI Core -->
	<script src="<?php echo $base_url;?>js/jquery.ui.core.min.js"></script>
	<!-- jQuery UI Widget -->
	<script src="<?php echo $base_url;?>js/jquery.ui.widget.min.js"></script>

	<!-- jQuery UI button -->
	<script src="<?php echo $base_url;?>js/jquery.ui.button.min.js"></script>
    
	<!-- smoother animations -->
	<script src="<?php echo $base_url;?>js/jquery.easing.min.js"></script>
    
	<!-- Bootstrap -->
	<script src="<?php echo $base_url;?>js/bootstrap.min.js"></script>
	
 
	<!-- small charts plugin -->
	<script src="<?php echo $base_url;?>js/jquery.easy-pie-chart.min.js"></script>
	<!-- Charts plugin -->
	<script src="<?php echo $base_url;?>js/jquery.flot.min.js"></script>
	<!-- Pie charts plugin -->
	<script src="<?php echo $base_url;?>js/jquery.flot.pie.min.js"></script>
	<!-- Bar charts plugin -->
	<script src="<?php echo $base_url;?>js/jquery.flot.bar.order.min.js"></script>
	<!-- Charts resizable plugin -->
	<script src="<?php echo $base_url;?>js/jquery.flot.resize.min.js"></script>
	<!-- calendar plugin -->
	<script src="<?php echo $base_url;?>js/fullcalendar.min.js"></script>
	<!-- chosen plugin -->
	<script src="<?php echo $base_url;?>js/chosen.jquery.min.js"></script>
    <!-- Scrollable navigation -->
	<script src="<?php echo $base_url;?>js/jquery.nicescroll.min.js"></script>

    
    <!-- Growl Like notifications -->
	<script src="<?php echo $base_url;?>js/jquery.gritter.min.js"></script>
    
    
    <!-- Form wizard plugin -->
	<script src="<?php echo $base_url;?>js/jquery.form.wizard.min.js"></script>
	<!-- Uniform plugin -->
	<script src="<?php echo $base_url;?>js/jquery.uniform.min.js"></script>

	
	<script src="<?php echo $base_url;?>js/demonstration.min.js"></script>
	<!-- Theme framework -->
	<script src="<?php echo $base_url;?>js/eakroko.min.js"></script>
	<!-- Theme scripts -->
	<script src="<?php echo $base_url;?>js/application.min.js"></script>
    
    <script src="<?php echo $base_url; ?>js/function.js"></script> 
    
    
   <?= $_scripts ?>
   <?= $_styles ?>
</head>
<body data-layout="fixed">
  
  <?php echo $header; ?> 
  <div id="main">
  <div id="navigation"><?php echo $sidebar; ?></div>
  <div id="content"><?php echo $content; ?></div>
  </div>
  <?php echo $footer; ?>
   
   
   <div class="navi-functions">
		<div class="btn-group btn-group-custom">
			<a href="#" class="button button-square layout-not-fixed notify" rel="tooltip" title="Toggle fixed-nav" data-notify-message="Fixed nav is now {{state}}" data-notify-title="Toggled fixed nav">
				<i class="icon-unlock"></i>
			</a>
			<a href="#" class="button button-square layout-not-fluid notify button-active" rel="tooltip" title="Toggle fixed-layout" data-notify-message="Fixed layout is now {{state}}" data-notify-title="Toggled fixed layout">
				<i class="icon-exchange"></i>
			</a>
			
			
		</div>
	</div>
    
</body>
</html>