<?php $site_setting=site_setting();
$quotetype_list=quotetype_list();
$productquality_list=productquality_list();
 ?>

<div class="page-header">
				<div class="pull-left">
					<h4><i class="icon-cogs"></i> <?php if($product_id!='') {?>Edit Product<?php } else { ?>Add Product<?php }  ?></h4>
				</div>
				<div class="pull-right">
					<ul class="bread">
						<li><a href="<?php echo site_url('products/manage');?>">Manage Products</a><span class="divider">/</span></li>
                        <li class="active">Product</li>
					</ul>
				</div>
			</div>
            
            
            <div class="container-fluid" id="content-area">
				
<div class="row-fluid">
                <div class="span12">
                
                 <?php if($error!=''){ ?>

		<div class="alert alert-error">
											<button type="button" class="close" data-dismiss="alert">&times;</button>
											<strong>Warning !</strong> <?php echo $error;?>
										</div>    <?php }?>
                                        
                                        
    
					
						<div class="box">
							<div class="box-head">
								<i class="icon-list-ul"></i>
								<span>Change Product details</span>
							</div>
							<div class="box-body box-body-nopadding">
                             <?php
									$attributes = array('name'=>'frm_addproduct','class'=>'form-horizontal form-bordered');
									echo form_open('products/add_product',$attributes);
								  ?> 
                                  
                                  
                                  <div class="control-group">
										<label for="textarea" class="control-label">Quote Type</label>
										<div class="controls">    
                                        
                  <select name="quotetype_id" id="quotetype_id" >
                  <option value="">Select</option>
				<?php if(isset($quotetype_list) && !empty($quotetype_list)) { 
						foreach($quotetype_list as $type) { ?>
                	<option value="<?php echo $type->quotetype_id;?>" <?php if($quotetype_id==$type->quotetype_id) { ?> selected="selected" <?php } ?>><?php echo ucfirst($type->quotetype); ?></option>
				<?php } } ?>
                  </select>
                  
                    </div>
				</div>
                
                
                
                
                <div class="control-group">
										<label for="textarea" class="control-label">Quality Type</label>
										<div class="controls">    
                                        
                  <select name="productquality_id" id="productquality_id" >
                  <option value="">Select</option>
				<?php if(isset($productquality_list) && !empty($productquality_list)) { 
						foreach($productquality_list as $type) { ?>
                	<option value="<?php echo $type->productquality_id;?>" <?php if($productquality_id==$type->productquality_id) { ?> selected="selected" <?php } ?>><?php echo ucfirst($type->quality_desc); ?></option>
				<?php } } ?>
                  </select>
                  
                    </div>
				</div>
                                  
								
									<div class="control-group">
										<label for="textfield" class="control-label">Paint Brand</label>
										<div class="controls">
										<input name="paintbrand" id="paintbrand" type="text" value="<?php echo $paintbrand; ?>" placeholder="Paint Brand" class="input-xlarge">
                                        </div>
									</div>
                                    
                                    <div class="control-group">
										<label for="textfield" class="control-label">Description</label>
										<div class="controls">
										<input name="description" id="description" type="text" value="<?php echo $description; ?>" placeholder="Description" class="input-xlarge">
                                        </div>
									</div>
                                    
                                    
                                    <div class="control-group">
										<label for="textfield" class="control-label">Spread Quantity</label>
										<div class="controls">
										<input name="spreadqty" id="spreadqty" type="text" value="<?php echo $spreadqty; ?>" placeholder="Paint Brand" class="input-xlarge">
                                        </div>
									</div>
									
                                    
                                    
									<div class="form-actions">
										 <?php if($product_id=='') { ?>
												<button type="submit" class="button button-basic-blue">Save</button>
											<?php } else { ?>
											<button type="submit" class="button button-basic-blue">Save changes</button>
											<?php } ?>
                                            <button type="button" class="button button-basic" onClick="window.location.href='<?php echo site_url('products/manage');?>'">Cancel</button>
                                           
											<input type="hidden" name="product_id" id="product_id" value="<?php echo $product_id; ?>" />
											<input type="hidden" name="offset" id="offset" value="<?php echo $offset; ?>" />
										
									</div>
                                   
                                   
								</form>
							</div>
						</div>
					</div>
				</div>
				
				
				
			</div>