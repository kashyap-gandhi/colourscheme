<?php $site_setting = site_setting(); ?>
<div class="page-header">
    <div class="pull-left">
        <h4><i class="icon-cogs"></i>Product</h4>
    </div>
    <div class="pull-right">
        <ul class="bread">
            <li><a href="<?php echo site_url('products/manage'); ?>">Manage Products</a><span class="divider">/</span></li>
            <li class="active">Product</li>
        </ul>
    </div>
</div>




<div class="container-fluid" id="content-area">


    <div class="row-fluid">
        <div class="span12">




            <?php if ($msg != '') { ?>
                <div class="alert <?php if ($msg == 'notfound' || $msg == 'cannot') { ?>alert-danger<?php } else { ?>alert-success<?php } ?>">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    <strong><?php if ($msg == 'notfound' || $msg == 'cannot') { ?>Warning<?php } else { ?>Success<?php } ?>!</strong>
                    <?php if ($msg == 'active') { ?>Product has been activated successfully. <?php } ?>
                    <?php if ($msg == 'inactive') { ?>Product has been inactivated successfully. <?php } ?>
                    <?php if ($msg == 'delete') { ?>Product detail has been deleted successfully. <?php } ?>	
                    <?php if ($msg == 'insert') { ?>New Product has been added successfully. <?php } ?>	
                    <?php if ($msg == 'update') { ?>Product detail has been updated successfully. <?php } ?>	
                    <?php if ($msg == 'notfound') { ?>Product records not found. <?php } ?>
                    <?php if ($msg == 'cannot') { ?>Cannot delete Product because it used in many records. <?php } ?>		

                </div> 
            <?php } ?>	


            <script>
        
        function searchme(){
                
                var search_value=$.trim($("input#search_value").val());
                var search_by=$.trim($("input#search_by").val());
                
                
                 
                if(search_by!='' && search_value!='')
		{
                    
                    window.location.href='<?php echo site_url('products/search');?>/'+search_by+'/'+search_value;
		}
		else
		{
                    window.location.href='<?php echo site_url('products/manage');?>/';
                }
		
        }
        
        </script>
            


            <div class="box">
                <div class="box-head">
                    <i class="icon-table"></i>
                    <span>Manage Product</span>
                </div>



                    <div class="box-body box-body-nopadding">
                        <div class="highlight-toolbar">
                            <div class="pull-left">

                                  <input type="hidden" name="search_by" id="search_by" value="name" />
                                
                                <input type="text" value=" <?php if($keyword!=''){ echo $keyword; } ?>" name="search_value" id="search_value" style="margin: 5px;" />
                                <input type="button" name="search_btn" value="Search" class="button button-basic" onclick="searchme();" />
                                
                              
                                
                                
                            </div>

                            
                            

                <form name="frm_listproduct" id="frm_listproduct" action="<?php echo site_url('products/product_action'); ?>" method="post">
                    <input type="hidden" name="action" id="action" />
                    <input type="hidden" name="offset" id="offset" value="<?php echo $offset; ?>" />	

                            <div class="pull-right"><div class="btn-toolbar">
                                    <div class="btn-group">
                                        <a href="<?php echo site_url('products/add_product'); ?>" class='button button-basic button-icon' rel="tooltip" title="Add"><i class="icon-plus-sign"></i></a>
                                        <a href="javascript:void(0)" class='button button-basic button-icon' rel="tooltip" title="Delete" onclick="setaction('chk[]','delete', 'If Product is linked to any records then it will not delete record. \n\nAre you sure, you want to delete selected record(s)?', 'frm_listproduct')"><i class="icon-trash"></i></a>

                                    </div>
                                </div></div>

                        </div>
                        <table class="table table-nomargin table-striped table-bordered table-hover table-pagination">
                            <thead>
                                <tr>
                                    <th><input type="checkbox" class="checkall" /></th> 
                                    <th>Name</th>
                                    <th>Spread Qty.</th>
                                    <th>Cost</th>
                                    <th>Description</th>
                                    <th>Quote Type</th>
                                    <th>Quality</th>
                                    <th>Action</th>														
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if ($result) {

                                    foreach ($result as $res) {
                                        ?>

                                        <tr> 
                                            <td><input type="checkbox" id='chk' name="chk[]" value="<?php echo $res->product_id; ?>" /></td> 
                                            <td><?php echo anchor('products/edit_product/' . $res->product_id . '/' . $offset, $res->paintbrand, ' rel="tooltip" title="Edit" '); ?></td>												                                             
                                            <td><?php echo $res->spreadqty; ?></td>

                                            <td><a href="<?php echo site_url('products/managecost/' . $res->product_id); ?>" class='button button-basic button-icon' rel="tooltip" title="Manange Costs"><i class="icon-plus-sign-alt"></i></a>
                                            </td>

                                            <td><?php echo $res->description; ?></td>
                                            <td><?php echo $res->quotetype; ?></td>
                                            <td><?php echo $res->quality_desc; ?></td>  
                                            <td><a href="<?php echo site_url('products/edit_product/' . $res->product_id . '/' . $offset); ?>" class='button button-basic button-icon' rel="tooltip" title="Edit"><i class="icon-edit"></i></a>
                                            </td>

                                        </tr> 



                                    <?php } ?>
                                <?php } else { ?>
                                    <tr><td colspan="9" align="center" valign="middle"  style="text-align:center;">No Product has been added yet.</td></tr>
                                <?php } ?>

                            </tbody>
                        </table>
                        <div class="bottom-table">
                            <div class="pull-left">

                            </div>
                            <div class="pull-right"><div class="pagination pagination-custom">
                                    <?php echo $page_link; ?>
                                </div></div>
                        </div>
                    </div>

                </form>


            </div>
        </div>
    </div>


</div>