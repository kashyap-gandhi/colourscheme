<?php
$site_setting = site_setting();
$product_list = product_list();
$site_pst_tax = $site_setting->past_tax;
?>

<div class="page-header">
    <div class="pull-left">
        <h4><i class="icon-cogs"></i> <?php if ($productcost_id != '') { ?>Edit Product Cost<?php } else { ?>Add Product Cost<?php } ?></h4>
    </div>
    <div class="pull-right">
        <ul class="bread">
            <li><a href="<?php echo site_url('products/manage'); ?>">Manage Products</a><span class="divider">/</span></li>
            <li class="active">Product Cost</li>
        </ul>
    </div>
</div>


<div class="container-fluid" id="content-area">

    <div class="row-fluid">
        <div class="span12">

            <?php if ($error != '') { ?>

                <div class="alert alert-error">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    <strong>Warning !</strong> <?php echo $error; ?>
                </div>    <?php } ?>




            <div class="box">
                <div class="box-head">
                    <i class="icon-list-ul"></i>
                    <span>Change Product cost details  :: <?php echo anchor('products/managecost/' . $product_id, ucfirst($product_detail->paintbrand)); ?></span>
                </div>
                <div class="box-body box-body-nopadding">
                    <?php
                    $attributes = array('name' => 'frm_addproductcost', 'class' => 'form-horizontal form-bordered');
                    echo form_open('products/add_cost/' . $product_id, $attributes);
                    ?> 


                    <?php /* ?><div class="control-group">
                      <label for="textarea" class="control-label">Product</label>
                      <div class="controls">

                      <select name="product_id" id="product_id" >
                      <option value="">Select</option>
                      <?php if(isset($product_list) && !empty($product_list)) {
                      foreach($product_list as $type) { ?>
                      <option value="<?php echo $type->product_id;?>" <?php if($product_id==$type->product_id) { ?> selected="selected" <?php } ?>><?php echo ucfirst($type->paintbrand)."&nbsp;&nbsp;||&nbsp;&nbsp;".ucfirst($type->description); ?></option>
                      <?php } } ?>
                      </select>

                      </div>
                      </div><?php */ ?>

                    <input type="hidden" name="product_id" id="product_id" value="<?php echo $product_id; ?>" />





                    <!-- datepicker plugin -->
                    <link rel="stylesheet" href="<?php echo base_url(); ?>css/bootstrap-datetimepicker.min.css">


                    <!-- datepicker plugin -->
                    <script src="<?php echo base_url(); ?>js/bootstrap-datetimepicker.min.js"></script>


                    <script type="text/javascript">
                        $(function() {
                            $('.datetimepicker').datetimepicker();
                        });
                    </script>      


                    <div class="control-group">
                        <label for="password" class="control-label">Start Date</label>
                        <div class="controls">


                            <div  class="input-append date datetimepicker">
                                <input data-format="dd-MM-yyyy hh:mm:ss" name="start_date" id="start_date" type="text" value="<?php
                    if ($start_date != '') {
                        echo date('d-m-Y H:i:s', strtotime($start_date));
                    }
                    ?>" placeholder="Start Date" class="input-xlarge" />
                                <span class="add-on">
                                    <i data-time-icon="icon-time" data-date-icon="icon-calendar">
                                    </i>
                                </span>
                            </div>


                        </div>
                    </div>

                    <div class="control-group">
                        <label for="password" class="control-label">End Date</label>
                        <div class="controls">

                            <div class="input-append date datetimepicker">
                                <input data-format="dd-MM-yyyy hh:mm:ss" name="end_date" id="end_date" type="text" value="<?php
                    if ($end_date != '') {
                        echo date('d-m-Y H:i:s', strtotime($end_date));
                    }
                    ?>" placeholder="End Date" class="input-xlarge" />
                                <span class="add-on">
                                    <i data-time-icon="icon-time" data-date-icon="icon-calendar">
                                    </i>
                                </span>
                            </div>



                        </div>
                    </div>





                    <div class="control-group">
                        <label for="textfield" class="control-label">Cost</label>
                        <div class="controls">
                            <div class="input-append">
                                <input name="cost" id="cost" type="text" value="<?php echo $cost; ?>" placeholder="Cost" class="input-small">
                                <span class="add-on"><?php echo $site_setting->currency_symbol; ?></span>
                            </div>
                        </div>
                    </div>




                    
                    
                    
                      <div class="control-group">
                        <label for="textarea" class="control-label">PST Tax(%)</label>
                        <div class="controls">  

                            <input name="pst_tax" id="pst_tax" type="text" value="<?php if($pst_tax!='') { echo $pst_tax; } else { echo $site_pst_tax; } ?>" placeholder="Cost" class="input-small">

                        </div>
                    </div>

                    
                    
                    <div class="control-group">
                        <label for="textarea" class="control-label">Markup Cost(%)</label>
                        <div class="controls">  

                            <select name="markup_cost" id="markup_cost" class="span1">
                                <?php if (!empty($all_markup_cost)) {
                                    foreach ($all_markup_cost as $key => $mcost) { ?>
                                        <option value="<?php echo $mcost; ?>" <?php if ($markup_cost == $mcost) { ?> selected="selected" <?php } ?>><?php echo $mcost; ?></option>
    <?php }
} ?>
                            </select>

                        </div>
                    </div>



                    <div class="control-group">
                        <label for="textarea" class="control-label">Status</label>
                        <div class="controls">    

                            <select name="active" id="active" >
                                <option value="0" <?php if ($active == 0) { ?> selected="selected" <?php } ?>>Inactive</option>
                                <option value="1" <?php if ($active == 1) { ?> selected="selected" <?php } ?>>Active</option>
                            </select>

                        </div>
                    </div>



                    <div class="form-actions">
                        <?php if ($productcost_id == '') { ?>
                            <button type="submit" class="button button-basic-blue">Save</button>
<?php } else { ?>
                            <button type="submit" class="button button-basic-blue">Save changes</button>
<?php } ?>
                        <button type="button" class="button button-basic" onClick="window.location.href='<?php echo site_url('products/managecost/' . $product_id); ?>'">Cancel</button>

                        <input type="hidden" name="productcost_id" id="productcost_id" value="<?php echo $productcost_id; ?>" />
                        <input type="hidden" name="offset" id="offset" value="<?php echo $offset; ?>" />

                    </div>


                    </form>
                </div>
            </div>
        </div>
    </div>



</div>