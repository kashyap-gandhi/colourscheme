<?php $site_setting = site_setting(); ?>
<div class="page-header">
    <div class="pull-left">
        <h4><i class="icon-cogs"></i>Product Cost</h4>
    </div>
    <div class="pull-right">
        <ul class="bread">
            <li><a href="<?php echo site_url('products/manage'); ?>">Manage Products</a><span class="divider">/</span></li>
            <li class="active">Product Cost</li>
        </ul>
    </div>
</div>




<div class="container-fluid" id="content-area">


    <div class="row-fluid">
        <div class="span12">




            <?php if ($msg != '') { ?>
                <div class="alert <?php if ($msg == 'notfound' || $msg == 'cannot') { ?>alert-danger<?php } else { ?>alert-success<?php } ?>">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    <strong><?php if ($msg == 'notfound' || $msg == 'cannot') { ?>Warning<?php } else { ?>Success<?php } ?>!</strong>
                    <?php if ($msg == 'active') { ?>Product Cost has been activated successfully. <?php } ?>
                    <?php if ($msg == 'inactive') { ?>Product Cost has been inactivated successfully. <?php } ?>
                    <?php if ($msg == 'delete') { ?>Product Cost detail has been deleted successfully. <?php } ?>	
                    <?php if ($msg == 'insert') { ?>New Product Cost has been added successfully. <?php } ?>	
                    <?php if ($msg == 'update') { ?>Product Cost detail has been updated successfully. <?php } ?>	
                    <?php if ($msg == 'notfound') { ?>Product Cost records not found. <?php } ?>
                    <?php if ($msg == 'cannot') { ?>Cannot delete Product Cost because it used in many records. <?php } ?>		

                </div> 
            <?php } ?>	



            <div class="box">
                <div class="box-head">
                    <i class="icon-table"></i>
                    <span>Manage Product Cost :: <?php echo anchor('products/manage/', ucfirst($product_detail->paintbrand)); ?></span>
                </div>




                <form name="frm_listproductcost" id="frm_listproductcost" action="<?php echo site_url('products/cost_action/' . $product_id); ?>" method="post">
                    <input type="hidden" name="action" id="action" />
                    <input type="hidden" name="offset" id="offset" value="<?php echo $offset; ?>" />	

                    <div class="box-body box-body-nopadding">
                        <div class="highlight-toolbar">
                            <div class="pull-left">

                            </div>

                            <div class="pull-right"><div class="btn-toolbar">
                                    <div class="btn-group">
                                        <a href="<?php echo site_url('products/add_cost/' . $product_id); ?>" class='button button-basic button-icon' rel="tooltip" title="Add"><i class="icon-plus-sign"></i></a>
                                        
                                        
                                        <a href="javascript:void(0)" class='button button-basic button-icon' rel="tooltip" title="Active" onclick="setaction('chk[]','active', 'Are you sure, you want to active selected record(s)?', 'frm_listproductcost')"><i class="icon-ok"></i></a>
                                        <a href="javascript:void(0)" class='button button-basic button-icon' rel="tooltip" title="Inactive" onclick="setaction('chk[]','inactive', 'Are you sure, you want to inactive selected record(s)?', 'frm_listproductcost')"><i class="icon-minus-sign"></i></a>
                                        
                                        <a href="javascript:void(0)" class='button button-basic button-icon' rel="tooltip" title="Delete" onclick="setaction('chk[]','delete', 'If Product is linked to any records then it will not delete record. \n\nAre you sure, you want to delete selected record(s)?', 'frm_listproductcost')"><i class="icon-trash"></i></a>

                                    </div>
                                </div></div>

                        </div>
                        <table class="table table-nomargin table-striped table-bordered table-hover table-pagination">
                            <thead>
                                <tr>
                                    <th><input type="checkbox" class="checkall" /></th> 

                                    <th>Cost(<?php echo $site_setting->currency_symbol; ?>)</th>
                                    <th>Markup Cost(%)</th>
                                    <th>PST Tax(%)</th>                                    
                                    <th>Start Date</th>
                                    <th>End Date</th>
                                    <th>Status</th>
                                    <th>Action</th>												
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if ($result) {

                                    foreach ($result as $res) {
                                        ?>

                                        <tr> 
                                            <td><input type="checkbox" id='chk' name="chk[]" value="<?php echo $res->productcost_id; ?>" /></td> 

                                            <td><?php echo anchor('products/edit_cost/' . $product_id . '/' . $res->productcost_id . '/' . $offset, $res->cost, ' rel="tooltip" title="Edit Cost" '); ?></td>		
                                            <td><?php echo $res->markup_cost; ?></td>	
                                            <td><?php echo $res->pst_tax; ?></td>	
                                            <td><?php if ($res->start_date != '') {
                                            echo date($site_setting->date_time_format, strtotime($res->start_date));
                                        } ?></td> 
                                            <td><?php if ($res->end_date != '') {
                                            echo date($site_setting->date_time_format, strtotime($res->end_date));
                                        } ?></td> 

                                            <td><?php if ($res->active == 1) { ?><a href="javascript:void(0)" class="button button-basic button-icon" rel="tooltip" title="Active"><i class="icon-ok"></i></a><?php } ?></td> 


                                            <td><a href="<?php echo site_url('products/edit_cost/' . $product_id . '/' . $res->productcost_id . '/' . $offset); ?>" class='button button-basic button-icon' rel="tooltip" title="Edit"><i class="icon-edit"></i></a>
                                            </td>

                                        </tr> 



    <?php } ?>
<?php } else { ?>
                                    <tr><td colspan="8" align="center" valign="middle"  style="text-align:center;">No Product Cost has been added yet.</td></tr>
<?php } ?>

                            </tbody>
                        </table>
                        <div class="bottom-table">
                            <div class="pull-left">

                            </div>
                            <div class="pull-right"><div class="pagination pagination-custom">
<?php echo $page_link; ?>
                                </div></div>
                        </div>
                    </div>

                </form>


            </div>
        </div>
    </div>


</div>