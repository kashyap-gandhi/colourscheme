<?php $site_setting=site_setting();
$leadsource_list=leadsource_list(); ?>

<div class="page-header">
				<div class="pull-left">
					<h4><i class="icon-cogs"></i> <?php if($client_id=='') {?>Add Client<?php }  else { ?>Edit Client<?php  } ?></h4>
				</div>
				<div class="pull-right">
					<ul class="bread">
						<li><a href="<?php echo site_url('clients/manage');?>">Manage Clients</a><span class="divider">/</span></li>
						<li class='active'>Detail</li>
					</ul>
				</div>
			</div>
            
            
            <div class="container-fluid" id="content-area">
				
<div class="row-fluid">
                <div class="span12">
                
                 <?php if($error!=''){ ?>

		<div class="alert alert-error">
											<button type="button" class="close" data-dismiss="alert">&times;</button>
											<strong>Warning !</strong> <?php echo $error;?>
										</div>    <?php }?>
                                        
                                        
    
					
						<div class="box">
							<div class="box-head">
								<i class="icon-list-ul"></i>
								<span>Change Client details</span>
							</div>
							<div class="box-body box-body-nopadding">
                             <?php
									$attributes = array('name'=>'frm_addclient','class'=>'form-horizontal form-bordered');
									echo form_open('clients/add_client',$attributes);
								  ?> 
                                  
								
									<div class="control-group">
										<label  class="control-label">Company</label>
										<div class="controls">
											<input name="first_name" id="first_name" type="text" value="<?php echo $first_name; ?>" placeholder="Company" class="input-xlarge">
                                            <span class="help-inline"></span> 
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">Contact</label>
										<div class="controls">
											<input name="last_name" id="last_name" type="text" value="<?php echo $last_name; ?>" placeholder="Contact" class="input-xlarge">
										</div>
									</div>									
                                    
                      				<div class="control-group">
										<label class="control-label">Email</label>
										<div class="controls">
											<input name="email" id="email" type="text" value="<?php echo $email; ?>" placeholder="Email" class="input-xlarge">
                                             <span class="help-inline">(Email must be unique.)</span> 
										</div>
									</div>
                      				
                                    <div class="control-group">&nbsp;</div>
                                    
                                    
                      				<div class="control-group">
										<label class="control-label">Cell Phone</label>
										<div class="controls">
											<input name="cell_phone" id="cell_phone" type="text" value="<?php echo $cell_phone; ?>" placeholder="Cell Phone" class="input-xlarge">
                                             <span class="help-inline">(Cell Phone must be unique.)</span> 
										</div>
									</div>
                      				
                                    <div class="control-group">
										<label class="control-label">Business Phone</label>
										<div class="controls">
											<input name="business_phone" id="business_phone" type="text" value="<?php echo $business_phone; ?>" placeholder="Business Phone" class="input-xlarge">
                                             <span class="help-inline">(Business Phone must be unique.)</span> 
										</div>
									</div>
                                    
                                    
                                    <div class="control-group">
										<label class="control-label">Fax</label>
										<div class="controls">
											<input name="fax" id="fax" type="text" value="<?php echo $fax; ?>" placeholder="Fax" class="input-xlarge">
                                             <span class="help-inline">(Fax must be unique.)</span> 
										</div>
									</div>
                                    
                                    
                                    <div class="control-group">
										<label class="control-label">Home Phone</label>
										<div class="controls">
											<input name="home_phone" id="home_phone" type="text" value="<?php echo $home_phone; ?>" placeholder="Home Phone" class="input-xlarge">
                                             <span class="help-inline">(Home Phone must be unique.)</span> 
										</div>
									</div>
                                    
                                     <div class="control-group">&nbsp;</div>
                                     
                                     
                                     
                                     
                                     <div class="control-group">
										<label class="control-label">Address</label>
										<div class="controls">
											
                                            <textarea name="address" id="address" rows="5" placeholder="Address" class="input-block-level"><?php echo $address; ?></textarea>
                                            
                                             <span class="help-inline"></span> 
										</div>
									</div>
                                     
                                     
                                     <div class="control-group">
										<label class="control-label">City</label>
										<div class="controls">
											<input name="city" id="city" type="text" value="<?php echo $city; ?>" placeholder="City" class="input-xlarge">
										</div>
									</div>
                                     
                                     
                                     <div class="control-group">
										<label class="control-label">Province</label>
										<div class="controls">
											<input name="province" id="province" type="text" value="<?php echo $province; ?>" placeholder="Province" class="input-xlarge">
										</div>
									</div>
                                     
                                     
                                     
                                     <div class="control-group">
										<label class="control-label">Postal Code</label>
										<div class="controls">
											<input name="postal_code" id="postal_code" type="text" value="<?php echo $postal_code; ?>" placeholder="Postal Code" class="input-xlarge">
										</div>
									</div>
                                     
                                     
                                     
                                     
                                     
                                     <div class="control-group">&nbsp;</div>
                                     
                                     
                                     
                      
                      
                    				<div class="control-group">
										<label  class="control-label">Source</label>
										<div class="controls">    
                                        
                                          <select name="source" id="source" >
                                          <option value="">Select</option>
                                        <?php if(isset($leadsource_list) && !empty($leadsource_list)) { 
                                                foreach($leadsource_list as $leadsource) { ?>
                                            <option value="<?php echo $leadsource->leadsource_id;?>" <?php if($source==$leadsource->leadsource_id) { ?> selected="selected" <?php } ?>><?php echo ucfirst($leadsource->leadsource); ?></option>
                                        <?php } } ?>
                                          </select>
                                          
                                            </div>
                                        </div>
                                        
                 
                
   
           
           
           
           
									<div class="form-actions">
										 <?php if($client_id=='') { ?>
												<button type="submit" class="button button-basic-blue">Save</button>
											<?php } else { ?>
											<button type="submit" class="button button-basic-blue">Save changes</button>
											<?php } ?>
                                            <button type="button" class="button button-basic" onClick="window.location.href='<?php echo site_url('clients/manage');?>'">Cancel</button>
                                           
											<input type="hidden" name="client_id" id="client_id" value="<?php echo $client_id; ?>" />
											<input type="hidden" name="offset" id="offset" value="<?php echo $offset; ?>" />
										
									</div>
                                   
                                   
								</form>
							</div>
						</div>
					</div>
				</div>
				
				
				
			</div>