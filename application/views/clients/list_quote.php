<?php $site_setting=site_setting();?>
<div class="page-header">
				<div class="pull-left">
					<h4><i class="icon-cogs"></i>Client Quotes</h4>
				</div>
				<div class="pull-right">
					<ul class="bread">
						<li><a href="<?php echo site_url('clients/manage');?>">Manage Clients</a><span class="divider">/</span></li>
                        <li class="active">Quotes</li>
					</ul>
				</div>
			</div>
            
            
            
            
            <div class="container-fluid" id="content-area">
            
            
            <div class="row-fluid">
								<div class="span12">
                                
                                
                                
                                
                                <?php if($msg!='') { ?>
<div class="alert <?php if($msg=='notfound' || $msg=='cannot') {?>alert-danger<?php } else { ?>alert-success<?php } ?>">
											<button type="button" class="close" data-dismiss="alert">&times;</button>
											<strong><?php if($msg=='notfound' || $msg=='cannot') {?>Warning<?php } else { ?>Success<?php } ?>!</strong>
<?php if($msg=='active') { ?>Quote has been activated successfully. <?php } ?>
	<?php if($msg=='inactive') { ?>Quote has been inactivated successfully. <?php } ?>
	<?php if($msg=='delete') { ?>Quote has been deleted successfully. <?php } ?>	
	<?php if($msg=='insert') { ?>New Quote has been added successfully. <?php } ?>	
	<?php if($msg=='update') { ?>Quote detail has been updated successfully. <?php } ?>	
    <?php if($msg=='notfound') { ?>Quote records not found. <?php } ?>
    <?php if($msg=='cannot') { ?>Cannot delete Quote because it used in many records. <?php } ?>		
  
</div> 
<?php } ?>	
                                
                                
                                
									<div class="box">
										<div class="box-head">
											<i class="icon-table"></i>
											<span>Manage Quotes</span>
                                            
                                            <div class="pull-right"><?php if($client_detail->first_name!='') { echo ucfirst($client_detail->first_name.' : '); }  if($client_detail->last_name!='') { echo ucfirst($client_detail->last_name); } ?></div>
                                             
										</div>
                                        

									                                        

	 <form name="frm_listquotes" id="frm_listquotes" action="<?php echo site_url('clients/quote_action/'.$client_id);?>" method="post">
<input type="hidden" name="action" id="action" />
<input type="hidden" name="offset" id="offset" value="<?php echo $offset; ?>" />	
                                        
                                        <div class="box-body box-body-nopadding">
											<div class="highlight-toolbar">
												<div class="pull-left">
													
												</div>
                                                
                                                <div class="pull-right"><div class="btn-toolbar">
													<div class="btn-group">
														<a href="javascript:void(0)" class='button button-basic button-icon addquote' rel="tooltip" title="Add Quote" data-id="<?php echo $client_id;?>" data-company="<?php if($client_detail->first_name!='') { echo $client_detail->first_name.'&nbsp;'; } if($client_detail->last_name!='') { echo $client_detail->last_name; } ?>" ><i class="icon-plus-sign"></i></a>
                                                          <a href="javascript:void(0)" class='button button-basic button-icon' rel="tooltip" title="Delete" onclick="setaction('chk[]','delete', 'If Quote is linked to any records then it will not delete record. \n\nAre you sure, you want to delete selected record(s)?', 'frm_listquotes')"><i class="icon-trash"></i></a>
												
													</div>
												</div></div>
												
											</div>
											<table class="table table-nomargin table-striped table-bordered table-hover table-pagination">
												<thead>
													<tr>
														<th><input type="checkbox" class="checkall" /></th>                                                         
														<th>Quote ID</th>
                                                        <th>Quote Type</th>
                                                        <th>Type</th>
                                                        <th>Sales Lead</th>
                                                        <th>Won</th>
                                                        <th>Date</th>
														<th>Action</th>														
													</tr>
												</thead>
												<tbody>
									<?php if($result) { 
											
											foreach($result as $res) {
											
										?>
										
                        <tr> 
                            <td><input type="checkbox" id='chk' name="chk[]" value="<?php echo $res->quote_id;?>" /></td> 
                            <td><?php echo anchor('quotes/add_quote/'.$res->quote_id,$res->quote_unique_id,' rel="tooltip" title="Edit" ');?></td> 
                        
                           
                            <td><?php if($res->quotetype_id>0) { $quote_type=quotetype_by_id($res->quotetype_id); if(!empty($quote_type)) { echo ucfirst($quote_type->quotetype); } } ?></td>
                            <td><?php if($res->bustype_id>0){$bustype_type=bustype_by_id($res->bustype_id); if(!empty($bustype_type)) { echo ucfirst($bustype_type->description); } } ?></td>
                            
                            <td><?php if($res->team_id>0) { $team_detail=team_by_id($res->team_id); if(!empty($team_detail)) { echo ucfirst($team_detail->name); } } ?></td>
                            
                            
                            <td><?php if($res->wonreason_id>0) { $wonreason_detail=winloss_by_id($res->wonreason_id); if(!empty($wonreason_detail)) { echo ucfirst($wonreason_detail->reason); } } ?></td>
                            
                            <td><?php if($res->date!='') { echo date($site_setting->date_time_format,strtotime($res->date)); } ?></td> 
                            
                            <td>                                               
                            <a href="<?php echo site_url('quotes/add_quote/'.$res->quote_id);?>" class='button button-basic button-icon' rel="tooltip" title="Edit"><i class="icon-edit"></i></a>
                           </td>
                            
                        </tr> 
                         
										 
										
										<?php   } ?>
										<?php } else { ?>
										<tr><td colspan="8" align="center" valign="middle" style="text-align:center;">No Quote Type has been added yet.</td></tr>
										<?php } ?>
														
													</tbody>
												</table>
												<div class="bottom-table">
													<div class="pull-left">
														
													</div>
													<div class="pull-right"><div class="pagination pagination-custom">
														<?php echo $page_link; ?>
													</div></div>
												</div>
											</div>
										
                                        </form>
                                    
                                        
                                        </div>
									</div>
								</div>
            
            
            </div>
            
            
            
            
            
            
              <?php 
			$quotetype_list=quotetype_list();
			$bustype_list=bustype_list();
			$team_list=team_list_by_role_id(1);
                        
                        
                          $quote_paint_1=quote_paint(1);
                $quote_paint_2=quote_paint(2);
		
                        
                        ?>
            <!-- Modal -->
            
<div class="modal" id="addquote" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display:none;">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Add Quote</h4>
      </div>
      <div class="modal-body">
      
      
       <form name="frmaddquote" class="form-horizontal">
   		
        <div class="control-group">
            <label class="control-label"><b>Client</b></label>
            <div class="controls">              
               <input type="text" id="company_name" value="" readonly="readonly" />
               <input type="hidden" name="client_id" id="client_id" value="" />
            </div>
        </div>
                                    
                                    
        <div class="control-group">
            <label class="control-label"><b>Quote Type</b></label>
            <div class="controls">
                <select name="quotetype_id" id="quotetype_id" class="input-large">
                    <?php if(!empty($quotetype_list)){ 
					  foreach($quotetype_list as $type) {?>
					  <option value="<?php echo $type->quotetype_id;?>"><?php echo ucfirst($type->quotetype);?></option>
					  <?php } } ?>
                </select>
            </div>
        </div>
                                    
           <div class="control-group">
            <label class="control-label"><b>Comm/Res</b></label>
            <div class="controls">
                <select name="bustype_id" id="bustype_id" class="input-large">
                    <?php if(!empty($bustype_list)){ 
					  foreach($bustype_list as $type) {?>
					  <option value="<?php echo $type->bustype_id;?>"><?php echo ucfirst($type->description);?></option>
					  <?php } } ?>
                </select>
            </div>
        </div>  
           
           
           
           
           
                    
                    <div class="control-group">
                        <label class="control-label"><b>Paint</b></label>
                        <div class="controls paint1" style="width:210px;">
                            <select name="PaintQuality_1" id="PaintQuality_1" class="input-large chosen-select">
                                        <option value="">---Select Interior---</option>
<?php if (!empty($quote_paint_1)) {
    foreach ($quote_paint_1 as $paint) { ?>
                                                <option value="<?php echo $paint->productcost_id; ?>" ><?php echo ucfirst(trim($paint->description)) . '&nbsp;&nbsp;|&nbsp;&nbsp;' . ucfirst(trim($paint->paintbrand)); ?></option>
                                        <?php }
                                    } ?>
                                    </select>
                        </div>
                        
                        <div class="controls paint2" style="width:210px; display: none;">
                            <select name="PaintQuality_2" id="PaintQuality_2" class="input-large chosen-select">
                                        <option value="">---Select Exterior---</option>
<?php if (!empty($quote_paint_2)) {
    foreach ($quote_paint_2 as $paint) { ?>
                                                <option value="<?php echo $paint->productcost_id; ?>" ><?php echo ucfirst(trim($paint->description)) . '&nbsp;&nbsp;|&nbsp;&nbsp;' . ucfirst(trim($paint->paintbrand)); ?></option>
                                        <?php }
                                    } ?>
                                    </select>
                        </div>
                        
                        
                    </div>                         

                    
                    
                     <div class="control-group">
                        <label class="control-label"><b>Job Rate</b></label>
                        <div class="controls">
                                <select name="rate" id="rate" class="input-large">
                                    
<?php for ($i = 75; $i >= 46; $i--) { ?>
                                        <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
<?php } ?>
                                </select>
                    
                     </div>
                    </div>                         

                    

           
           
               
       
       <div class="control-group">
            <label class="control-label"><b>Sales Lead</b></label>
            <div class="controls" style="height: 150px; width:210px;">
                <select name="team_id" id="team_id" class="input-large chosen-select">
                    <?php if(!empty($team_list)){ 
					  foreach($team_list as $team) {?>
					  <option value="<?php echo $team->team_id;?>"><?php echo ucfirst($team->name);?></option>
					  <?php } } ?>
                </select>
            </div>
        </div>
        
       </form>
       
       <style>
	   .chzn-results { height:80px; } 
	   </style> 
     
      
      
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary savequote">Save changes</button>
      </div>
    </div>
  </div>
</div>

 <!-- Modal -->
             <script type="text/javascript">
			$(".addquote").on("click",function(){
				var client_id=$(this).attr('data-id');
				var company = $(this).attr('data-company');
				$("#client_id").val(client_id);
				$("#company_name").val(company);
				$("#addquote").modal('show');
				
			});
                        
                        
    $("#quotetype_id").live("change",function(){
       
       var paint_type=$("#quotetype_id option:selected").val();
       
       if(paint_type==1){
           $("div.paint1").show();
           $("div.paint2").hide();
       } else if(paint_type==2){
           $("div.paint2").show();
           $("div.paint1").hide();
       } else {
           $("div.paint1").show();
           $("div.paint2").hide();
       }
       
    });
    
			
			$(".savequote").on("click",function(){
				var quote_id=0;
				
				
				var client_id=$('#client_id').val();
				var quotetype_id=$('#quotetype_id').val();
				var bustype_id=$('#bustype_id').val();
				var team_id=$('#team_id').val();
				
				if(client_id=='' || client_id==0) {
					alert('Client cannot be blank');
					return false;
				}
				if(quotetype_id=='' || quotetype_id==0) {
					alert('Quote type cannot be blank');
					return false;
				}
				if(bustype_id=='' || bustype_id==0) {
					alert('Bus type cannot be blank');
					return false;
				}
				if(team_id=='' || team_id==0) {
					alert('Sales Lead cannot be blank');
					return false;
				}
                                
                                
                                
        var paint_type=$("#quotetype_id option:selected").val();
       
      
       
       if(paint_type==1){
           var paint=$("#PaintQuality_1 option:selected").val();
       } else if(paint_type==2){
           var paint=$("#PaintQuality_2 option:selected").val();
       } else {
           var paint=$("#PaintQuality_1 option:selected").val();
       }
        
        if(paint=='' || paint==0){
            alert('Please select paint.');
            return false;
        }
        
      var rate=$("#rate option:selected").val();
      if(rate=='' || rate==0){
            alert('Please select Job rate.');
            return false;
        }
	
				
				var res = $.ajax({						
					type: 'POST',
					url: '<?php echo site_url('quotes/add_client_quote/');?>',
					data: {client_id:client_id,quotetype_id:quotetype_id,bustype_id:bustype_id,team_id:team_id,PaintQuality:paint,job_rate:rate},
					dataType: 'json', 
					cache: false,
					async: false                     
				}).responseText;							
			 
		
			 	
				var rcom_res = jQuery.parseJSON(res);
	
				if(rcom_res.status=='success')
				{
					quote_id=rcom_res.quote_id;					
				}
				else {
					alert("Unable to create Quote. Please do once again.");
					return false;
				}
				
				window.location.href="<?php echo site_url('quotes/add_quote/');?>/"+quote_id;
			});
			
			</script>