<?php $site_setting = site_setting(); ?>
<div class="page-header">
    <div class="pull-left">
        <h4><i class="icon-cogs"></i>Clients</h4>
    </div>
    <div class="pull-right">
        <ul class="bread">
            <li><a href="<?php echo site_url('clients/manage'); ?>">Manage Clients</a><span class="divider">/</span></li>
            <li class="active">Clients</li>
        </ul>
    </div>
</div>




<div class="container-fluid" id="content-area">


    <div class="row-fluid">
        <div class="span12">




            <?php if ($msg != '') { ?>
                <div class="alert <?php if ($msg == 'notfound') { ?>alert-danger<?php } else { ?>alert-success<?php } ?>">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    <strong><?php if ($msg == 'notfound') { ?>Warning<?php } else { ?>Success<?php } ?>!</strong>
                    <?php if ($msg == 'active') { ?>Client has been activated successfully. <?php } ?>
                    <?php if ($msg == 'inactive') { ?>Client has been inactivated successfully. <?php } ?>
                    <?php if ($msg == 'delete') { ?>Client account has been deleted successfully. <?php } ?>	
                    <?php if ($msg == 'insert') { ?>New Client has been added successfully. <?php } ?>	
                    <?php if ($msg == 'update') { ?>Client account has been updated successfully. <?php } ?>	
                    <?php if ($msg == 'rights') { ?>Client right access has been updated successfully. <?php } ?>	
                    <?php if ($msg == 'notfound') { ?>Client records not found. <?php } ?>	

                </div> 
            <?php } ?>	


            <script>
        
        function searchme(){
                
                
                var search_by=$("select#search_by option:selected").val();
                var search_value=$("input#search_value").val();
                
                if(search_by==0)
		{
                    window.location.href='<?php echo site_url('clients/manage');?>/';
		}
		else
		{
			
			window.location.href='<?php echo site_url('clients/search');?>/'+search_by+'/'+search_value;
		}
	
            
        }
        
        </script>
            
            

            <div class="box">
                <div class="box-head">
                    <i class="icon-table"></i>
                    <span>Manage Client</span>
                </div>

                
                    <div class="box-body box-body-nopadding">
                        <div class="highlight-toolbar">
                            <div class="pull-left">
                                <select name="search_by" id="search_by" style="margin: 5px;">
                                    <option value="0">All</option>
                                    <option value="first_name" <?php if($option=='first_name'){?> selected <?php } ?>>Company</option>
                                    <option value="last_name" <?php if($option=='last_name'){?> selected <?php } ?>>Contact</option>
                                    <option value="email" <?php if($option=='email'){?> selected <?php } ?>>Email</option>
                                    <option value="cell_phone" <?php if($option=='cell_phone'){?> selected <?php } ?>>Cell Phone</option>
                                    <option value="home_phone" <?php if($option=='home_phone'){?> selected <?php } ?>>Home Phone</option>
                                    <option value="business_phone" <?php if($option=='business_phone'){?> selected <?php } ?>>Business Phone</option>
                                    <option value="fax" <?php if($option=='fax'){?> selected <?php } ?>>Fax</option>
                                   
                                </select>
                                <input type="text" value=" <?php if($keyword!=''){ echo $keyword; } ?>" name="search_value" id="search_value" style="margin: 5px;" />
                                <input type="button" name="search_btn" value="Search" class="button button-basic" onclick="searchme();" />
                            </div>

                            
                            <form name="frm_listclient" id="frm_listclient" action="<?php echo site_url('clients/client_action'); ?>" method="post">
                    <input type="hidden" name="action" id="action" />
                    <input type="hidden" name="offset" id="offset" value="<?php echo $offset; ?>" />


                    
                            <div class="pull-right"><div class="btn-toolbar">
                                    <div class="btn-group">
                                        <a href="<?php echo site_url('clients/add_client'); ?>" class='button button-basic button-icon' rel="tooltip" title="Add"><i class="icon-plus-sign"></i></a>

                                        <a href="javascript:void(0)" class='button button-basic button-icon' rel="tooltip" title="Delete" onclick="setaction('chk[]','delete', 'If Client is linked to any records then it will not delete record. \n\nAre you sure, you want to delete selected record(s)?', 'frm_listclient')"><i class="icon-trash"></i></a>
                                    </div>
                                </div></div>

                        </div>
                        <table class="table table-nomargin table-striped table-bordered table-hover table-pagination">
                            <thead>
                                <tr>
                                    <th><input type="checkbox" class="checkall" /></th> 
                                    <th>Company</th>
                                    <th>Contact</th>
                                    <th>Email</th>

                                    <th>Quote</th>

                                    <th>Cell Ph.</th>
                                    <th>Home Ph.</th>
                                    <th>Business Ph.</th>

                                    <th>Area</th>
                                    <th>Postal</th>

                                    <th>Action</th>														
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if ($result) {

                                    foreach ($result as $row) {
                                        ?>

                                        <tr> 
                                            <td><input type="checkbox" id='chk' name="chk[]" value="<?php echo $row->client_id; ?>" /></td> 


                                            <td><?php if ($row->first_name != '') {
                                            echo anchor('clients/edit_client/' . $row->client_id, $row->first_name, ' rel="tooltip" title="Edit" ');
                                        } ?></td> 
                                            <td><?php if ($row->last_name != '') {
                                            echo anchor('clients/edit_client/' . $row->client_id, $row->last_name, ' rel="tooltip" title="Edit" ');
                                        } ?></td>
                                            <td><?php echo $row->email; ?></td>


                                            <td><a href="<?php echo site_url('clients/quotes/' . $row->client_id); ?>" class='button button-basic button-icon' rel="tooltip" title="Quotes"><i class="icon-file-alt"></i></a></td>


                                            <td><?php echo $row->cell_phone; ?></td>
                                            <td><?php echo $row->home_phone; ?></td>
                                            <td><?php echo $row->business_phone; ?></td>                                      
                                            <td><?php if ($row->city != '' && $row->province != '') {
                                            echo $row->city . ',' . $row->province;
                                        } elseif ($row->city != '') {
                                            echo $row->city;
                                        } elseif ($row->province != '') {
                                            echo $row->province;
                                        } ?></td>                                      
                                            <td><?php echo $row->postal_code; ?></td>                                     




                                            <td>                                               
                                                <a href="<?php echo site_url('clients/edit_client/' . $row->client_id . '/' . $offset); ?>" class='button button-basic button-icon' rel="tooltip" title="Edit"><i class="icon-edit"></i></a>
                                            </td>
                                        </tr> 



    <?php } ?>
<?php } else { ?>
                                    <tr><td colspan="11" align="center" valign="middle">No Clients has been added yet.</td></tr>
                                    <?php } ?>

                            </tbody>
                        </table>
                        <div class="bottom-table">
                            <div class="pull-left">

                            </div>
                            <div class="pull-right"><div class="pagination pagination-custom">
<?php echo $page_link; ?>
                                </div></div>
                        </div>
                    </div>

                </form>

            </div>
        </div>
    </div>


</div>