<?php $site_setting=site_setting();
 ?>

<div class="page-header">
				<div class="pull-left">
					<h4><i class="icon-cogs"></i> <?php if($salesstage_id!='') {?>Edit Sales Stage<?php } else { ?>Add Sales Stage<?php }  ?></h4>
				</div>
				<div class="pull-right">
					<ul class="bread">
						<li><a href="<?php echo site_url('others/bustype');?>">Others</a><span class="divider">/</span></li>
                        <li class="active">Sales Stages</li>
					</ul>
				</div>
			</div>
            
            
            <div class="container-fluid" id="content-area">
				
<div class="row-fluid">
                <div class="span12">
                
                 <?php if($error!=''){ ?>

		<div class="alert alert-error">
											<button type="button" class="close" data-dismiss="alert">&times;</button>
											<strong>Warning !</strong> <?php echo $error;?>
										</div>    <?php }?>
                                        
                                        
    
					
						<div class="box">
							<div class="box-head">
								<i class="icon-list-ul"></i>
								<span>Change Sales Stage details</span>
							</div>
							<div class="box-body box-body-nopadding">
                             <?php
									$attributes = array('name'=>'frm_addsalesstage','class'=>'form-horizontal form-bordered');
									echo form_open('others/add_salesstage',$attributes);
								  ?> 
                                  
								
									<div class="control-group">
										<label for="textfield" class="control-label">Name</label>
										<div class="controls">
											<input name="sales_stage" id="sales_stage" type="text" value="<?php echo $sales_stage; ?>" placeholder="Name" class="input-xlarge">
                                            
										</div>
									</div>
									
                       
                      
                      
                                    
                                    
                   
                          
                       
                         
                         
                               
                                    
                                    
									<div class="form-actions">
										 <?php if($salesstage_id=='') { ?>
												<button type="submit" class="button button-basic-blue">Save</button>
											<?php } else { ?>
											<button type="submit" class="button button-basic-blue">Save changes</button>
											<?php } ?>
                                            <button type="button" class="button button-basic" onClick="window.location.href='<?php echo site_url('others/salesstage');?>'">Cancel</button>
                                           
											<input type="hidden" name="salesstage_id" id="salesstage_id" value="<?php echo $salesstage_id; ?>" />
											<input type="hidden" name="offset" id="offset" value="<?php echo $offset; ?>" />
										
									</div>
                                   
                                   
								</form>
							</div>
						</div>
					</div>
				</div>
				
				
				
			</div>