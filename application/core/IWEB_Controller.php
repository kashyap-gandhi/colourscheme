<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	require_once(APPPATH.'helpers/custom_helper'.EXT);
	
	
class  IWEB_Controller  extends  CI_Controller  {


	
    public function __construct()
	{
	
		// get the CI superobject
		$CI =& get_instance();
		parent::__construct();
		
		
		
		/* google map setting **/
		
		$site_setting=site_setting();
		
		$site_timezone=tzOffsetToName($site_setting->site_timezone);
		date_default_timezone_set($site_timezone);
		
		if(!defined('SITE_NAME')){ define('SITE_NAME',$site_setting->site_name); }
	
      }
		
	
} 

// END IWEB_Controller class

/* End of file IWEB_Controller.php */
/* Location: ./application/core/IWEB_Controller.php */