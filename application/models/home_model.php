<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Home_model extends IWEB_Model  {
	
	public function __construct()
	{
		parent::__construct();
	}	
	
	function chk_login()
	{				
			$query=$this->db->query("select * from ".$this->db->dbprefix('team')." where username='".$this->input->post('username')."' and password='".$this->input->post('password')."' and active=1 ");
			
		if($query->num_rows()>0)
		{
		
			$detail=$query->row();
		
			$data=array(
		
			'team_id' => $detail->team_id,
			'username' => $detail->username,
			'email' => $detail->email,
			'name' => $detail->name,
                        'role1' => $detail->role1,
                        'role2' => $detail->role2
			
			);
		
		$this->session->set_userdata($data);
		
		
		$data_login=array(
		'team_id'=>$detail->team_id,
		'login_date'=>date('Y-m-d H:i:s'),
		'login_ip'=>$_SERVER['REMOTE_ADDR']		
		);
		
		$this->db->insert('team_login',$data_login);
		
		
		return 1;	
	
	    }
	
	else {
	
	
		return 0;
	}


	}
	
	
	
	function forgot_email()
	{
		$email = $this->input->post('email');
		$site_setting = site_setting();
		
		
		
			$query = $this->db->get_where('team',array('email'=>$email));
			
			if($query->num_rows()>0)
			{
			
				$row = $query->row();
			
				if($row->email != "")
				{
					
					$email_template=$this->db->query("select * from ".$this->db->dbprefix('email_template')." where task='Forgot Password Admin'");
					$email_temp=$email_template->row();
					
					
					if($email_temp) {
						$email_address_from=$email_temp->from_address;
						$email_address_reply=$email_temp->reply_address;
						
						$email_subject=$email_temp->subject;										
						$email_subject=str_replace('{site_name}',$site_setting->site_name,$email_subject);	
						
						$email_message=$email_temp->message;
						
						$username =$row->username;
						$password = $row->password;
						$email = $row->email;
						$email_to=$email;
						
						$login_link=anchor(site_url(),site_url());
						
						$email_message=str_replace('{break}','<br/><br/>',$email_message);
						$email_message=str_replace('{username}',$username,$email_message);
						$email_message=str_replace('{password}',$password,$email_message);
						$email_message=str_replace('{email}',$email,$email_message);
						$email_message=str_replace('{login_link}',$login_link,$email_message);
						$email_message=str_replace('{site_name}',$site_setting->site_name,$email_message);
						
						$str=$email_message;
				
					
							/** custom_helper email function **/
						
							email_send($email_address_from,$email_address_reply,$email_to,$email_subject,$str);
					}
						
					
				
					return '1';
					
				}
				else{
					return '0';
				}
			}	
			
			else
			{
				return 2;
			}
		
		
		
		
	}
	
	
	
} 
