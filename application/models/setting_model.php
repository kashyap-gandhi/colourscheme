<?php
class Setting_model extends CI_Model
{

	function Setting_model()
	{
		parent::__construct();
		
	}
	
	
	function email_template_insert()
	{
		$data = array(		
			'from_name' => $this->input->post('from_name'),	
			'from_address' => $this->input->post('from_address'),
			'reply_address' => $this->input->post('reply_address'),
			'subject' => $this->input->post('subject'),
			'message' => $this->input->post('message')
		);
		
		$this->db->insert('email_template',$data);
	}		
	
	
	function email_template_update()
	{
		$data = array(		
			'from_name' => $this->input->post('from_name'),	
			'from_address' => $this->input->post('from_address'),
			'reply_address' => $this->input->post('reply_address'),
			'subject' => $this->input->post('subject'),
			'message' => $this->input->post('message')
		);
		$this->db->where('email_template_id',$this->input->post('email_template_id'));
		$this->db->update('email_template',$data);
	}		
	
	function get_one_email_template($id)
	{
		$query = $this->db->get_where('email_template',array('email_template_id'=>$id));
		if($query->num_rows()>0)
		{
			return $query->row();	
		}
		
		return 0;
	}
	
	
	
	function get_total_email_template_count()
	{
		$query=$this->db->query("select * from ".$this->db->dbprefix('email_template'));
		
		if($query->num_rows()>0)
		{
			return $query->num_rows();	
		}
		
		return 0;
	}
	
	function get_all_email_template($offset, $limit)
	{
		$query=$this->db->query("select * from ".$this->db->dbprefix('email_template')." order by email_template_id asc limit ".$limit." offset ".$offset);
		
		if($query->num_rows()>0)
		{
			return $query->result();	
		}
		
		return 0;
	}
	
	
	
	
	
	function get_email_setting()
	{
		$query=$this->db->query("select * from ".$this->db->dbprefix('email_setting'));
		return $query->row();
	}
	
	function update_email_setting()
	{
		
		$data = array(			
			'mailer' => $this->input->post('mailer'),
			'sendmail_path' => $this->input->post('sendmail_path'),
			'smtp_port' => $this->input->post('smtp_port'),
			'smtp_host' => $this->input->post('smtp_host'),
			'smtp_email' => $this->input->post('smtp_email'),
			'smtp_password' => $this->input->post('smtp_password'),
		);
		
		$this->db->where("email_setting_id",$this->input->post('email_setting_id'));
		$this->db->update('email_setting',$data);
	
	}
	
	
	
	function get_site_setting()
	{
		$query=$this->db->query("select * from ".$this->db->dbprefix('site_setting'));
		return $query->row();
	}
	
	function update_site_setting()
	{
		
		$data=array(
		"site_name" => $this->input->post('site_name'),
		"currency_symbol" => $this->input->post('currency_symbol'),
		"currency_code" => $this->input->post('currency_code'),
		"date_format" => $this->input->post('date_format'),
		"time_format" => $this->input->post('time_format'),
		"date_time_format" => $this->input->post('date_time_format'),
		"site_timezone" => $this->input->post('site_timezone'),
		"site_version" => $this->input->post('site_version'),
		);
		
		$this->db->where("site_setting_id",$this->input->post('site_id'));
		$this->db->update('site_setting',$data);
	
	}
}

?>