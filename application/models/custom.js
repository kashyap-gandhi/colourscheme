// Flex slider full width
$(window).load(function() {
  $('.flexslider').flexslider({
    animation: "slide",
  });

    /*------------------------------------------*/
    /*  Responsive Select Navigation Script
     /*------------------------------------------*/
    selectnav('header-nav', {
        label: 'Go to...',
        nested: true,
        indent: '-'
    });

    selectnav('footer-nav', {
        label: 'Go to...',
        nested: true,
        indent: '-'
    });

    $(".rem").click(function(){
        $( this ).toggleClass( "red" );
    });

});


  /*------------------------------------------*/
  /*  Form validate plugin
  /*------------------------------------------*/
    /*------------------------------------------*/
    /*  Navigation scroll function
    /*------------------------------------------*/
    $('#navigation a').click(function(e){
        $('#navigation a.active').removeClass("active");
        $(this).addClass('active');
        var target = $(this).attr("href");
        var target_offset = $(target).offset().top;
        $('html, body').animate({scrollTop:target_offset}, 600);
    });

    $('#selectnav1').on('change',function(){
        var value = $(this).val();
        var hash_index = value.indexOf('#');
        var target = value.substr(hash_index);
        console.log(target);
        var target_offset = $(target).offset().top;
        $('html, body').animate({scrollTop:target_offset}, 'slow');
    });

    /*------------------------------------------*/
    /*  Scroll to top function
    /*------------------------------------------*/
    $(function() {
        $(window).scroll(function () {
            if ($(this).scrollTop() > 50) {
                $('#back-to-top').fadeIn();
            } else {
                $('#back-to-top').fadeOut();
            }           
        });

        $('#back-to-top').on('click', function(){
            $('html, body').animate({scrollTop:0}, 'slow' );
            return false;
        });
    });
