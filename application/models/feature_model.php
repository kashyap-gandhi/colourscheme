<?php
class Feature_model extends IWEB_Model
{

	function Feature_model()
	{
		parent::__construct();
	}
	
	
	
	////===coat type====
	
	function coat_insert()
	{
			
			$data=array(
			'rate_type' => $this->input->post('rate_type'),
			'quotetype_id' => $this->input->post('quotetype_id'),
			'rate' => $this->input->post('rate')
			);
			
			
			$this->db->insert('featurecoat',$data);
			
	}
	
	
	function coat_update()
	{
			
		$data=array(
			'rate_type' => $this->input->post('rate_type'),
			'quotetype_id' => $this->input->post('quotetype_id'),
			'rate' => $this->input->post('rate')
			);
			
			$this->db->where('featurecoat_id',$this->input->post('featurecoat_id'));
			$this->db->update('featurecoat',$data);
			
	}
	
	function get_coat_detail($id)
	{
		$query=$this->db->query("select * from ".$this->db->dbprefix('featurecoat')." where featurecoat_id='".$id."'");
		return $query->row();
	}
	
	
	
	function get_total_coat_count()
	{
		$query=$this->db->query("select * from ".$this->db->dbprefix('featurecoat rt')." left join ".$this->db->dbprefix("quotetype qt")." on rt.quotetype_id=qt.quotetype_id");		
		return $query->num_rows();		
	}
	
	function get_all_coat($offset, $limit)
	{
		$query=$this->db->query("select * from ".$this->db->dbprefix('featurecoat rt')." left join ".$this->db->dbprefix("quotetype qt")." on rt.quotetype_id=qt.quotetype_id order by  rt.featurecoat_id desc limit ".$limit." offset ".$offset);		
		
		if($query->num_rows()>0)
		{
			return $query->result();		
		}		
		
		return 0;
	}
	
	function delete_coat($id)
	{
		$query=$this->db->query("select * from ".$this->db->dbprefix('featurecoat')." where featurecoat_id='".$id."'");		
		
		if($query->num_rows()>0)
		{	
			
			/*$check_roomdetail=$this->db->query("select * from ".$this->db->dbprefix('roomdetail')." where featurecoat_id='".$id."'");	
			
			
		
			if($check_roomdetail->num_rows()>0) { 
				
				///==we dont delete record becuase it used somewhere
				
			} else {*/
				$this->db->delete('featurecoat',array('featurecoat_id' => $id));	
			/*}*/
		}		
		
		return 0;
	
	}
	
	
	
	
	
	
	
	
	
	////===footage type====
	
	function footage_insert()
	{
			
			$data=array(
			'description' => $this->input->post('description'),
			'quotetype_id' => $this->input->post('quotetype_id'),
			'default_footage' => $this->input->post('default_footage')
			);
			
			
			$this->db->insert('feature',$data);
			
	}
	
	
	function footage_update()
	{
			
		$data=array(
			'description' => $this->input->post('description'),
			'quotetype_id' => $this->input->post('quotetype_id'),
			'default_footage' => $this->input->post('default_footage')
			);
			
			$this->db->where('feature_id',$this->input->post('feature_id'));
			$this->db->update('feature',$data);
			
	}
	
	function get_footage_detail($id)
	{
		$query=$this->db->query("select * from ".$this->db->dbprefix('feature')." where feature_id='".$id."'");
		return $query->row();
	}
	
	
	
	function get_total_footage_count()
	{
		$query=$this->db->query("select * from ".$this->db->dbprefix('feature rt')." left join ".$this->db->dbprefix("quotetype qt")." on rt.quotetype_id=qt.quotetype_id");		
		return $query->num_rows();		
	}
	
	function get_all_footage($offset, $limit)
	{
		$query=$this->db->query("select * from ".$this->db->dbprefix('feature rt')." left join ".$this->db->dbprefix("quotetype qt")." on rt.quotetype_id=qt.quotetype_id order by  rt.feature_id desc limit ".$limit." offset ".$offset);		
		
		if($query->num_rows()>0)
		{
			return $query->result();		
		}		
		
		return 0;
	}
	
	function delete_footage($id)
	{
		$query=$this->db->query("select * from ".$this->db->dbprefix('feature')." where feature_id='".$id."'");		
		
		if($query->num_rows()>0)
		{	
			
			$check_roomdetail=$this->db->query("select * from ".$this->db->dbprefix('roomdetail')." where feature_id='".$id."'");	
			
			
		
			if($check_roomdetail->num_rows()>0) { 
				
				///==we dont delete record becuase it used somewhere
				
			} else {
				$this->db->delete('feature',array('feature_id' => $id));	
			}
		}		
		
		return 0;
	
	}
	
	
	
	
	
	
	
}

?>