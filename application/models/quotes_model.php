<?php
class Quotes_model extends IWEB_Model
{

	function Quotes_model()
	{
		parent::__construct();
	}
	
	
	
	////====quote updates========
        
        
        //=======step 4================
        
        function quote_step_4_financial_update(){
            
            $quote_id = $this->input->post('quote_id');		
            
            
            $data_financial['AdditionalCostDesc1'] = $this->input->post('AdditionalCostDesc1');
            $data_financial['ActualCostAmt1'] = $this->input->post('ActualCostAmt1');	

            $data_financial['AdditionalCostDesc2'] = $this->input->post('AdditionalCostDesc2');
            $data_financial['ActualCostAmt2'] = $this->input->post('ActualCostAmt2');	

            $data_financial['AdditionalCostDesc3'] = $this->input->post('AdditionalCostDesc3');
            $data_financial['ActualCostAmt3'] = $this->input->post('ActualCostAmt3');	

            $data_financial['AdditionalCostDesc4'] = $this->input->post('AdditionalCostDesc4');
            $data_financial['ActualCostAmt4'] = $this->input->post('ActualCostAmt4');	

            $data_financial['AdditionalCostDesc5'] = $this->input->post('AdditionalCostDesc5');
            $data_financial['ActualCostAmt5'] = $this->input->post('ActualCostAmt5');	
            
            $data_financial['FinalMaterialCostwPST']  = $this->input->post('FinalMaterialCostwPST');	
            
            $data_financial['FinalCost']  = $this->input->post('FinalCost');
            $data_financial['AdditionalHourlyChargeAmt']  = $this->input->post('AdditionalHourlyChargeAmt');	
            $data_financial['ExtraWorkMaterials'] = $this->input->post('ExtraWorkMaterials');
            
            $data_financial['TotalCustPayment']  = $this->input->post('TotalCustPayment');	
            $data_financial['FinalProfit']  = $this->input->post('FinalProfit');	
            $data_financial['FinalLDH']  = $this->input->post('FinalLDH');	
            
            
            $this->db->where('quote_id',$quote_id);                        
            $this->db->update('quotes',$data_financial);
            
        }
        
        ///======step 3================
        
        
        
		
	function quote_financial_update()
	{
		
		
		
				
                        $quote_id = $this->input->post('quote_id');		
		
		
		
                        //===commited
                        
                        
                        $data_financial['CalculatedTotal'] = $this->input->post('CalculatedTotal');
                        $data_financial['AdjustedTotal'] = $this->input->post('AdjustedTotal');
                        $data_financial['DiscountRate'] = $this->input->post('DiscountRate');
                        $data_financial['SellAmount'] = $this->input->post('SellAmount');
                        $data_financial['LDH'] = $this->input->post('LDH');
                        $data_financial['budget_time_material'] = $this->input->post('budget_time_material');
			
			
			$data_financial['AdditionalCostDesc1'] = $this->input->post('AdditionalCostDesc1');
                        $data_financial['AdditionalCostAmt1'] = $this->input->post('AdditionalCostAmt1');	
                        
                        $data_financial['AdditionalCostDesc2'] = $this->input->post('AdditionalCostDesc2');
                        $data_financial['AdditionalCostAmt2'] = $this->input->post('AdditionalCostAmt2');	
                        
                        $data_financial['AdditionalCostDesc3'] = $this->input->post('AdditionalCostDesc3');
                        $data_financial['AdditionalCostAmt3'] = $this->input->post('AdditionalCostAmt3');	
                        
                        $data_financial['AdditionalCostDesc4'] = $this->input->post('AdditionalCostDesc4');
                        $data_financial['AdditionalCostAmt4'] = $this->input->post('AdditionalCostAmt4');	
                        
                        $data_financial['AdditionalCostDesc5'] = $this->input->post('AdditionalCostDesc5');
                        $data_financial['AdditionalCostAmt5'] = $this->input->post('AdditionalCostAmt5');	
                        
                        
                        //==optional 1 part
                       
                        $data_financial['OptionalCostDesc11'] = $this->input->post('OptionalCostDesc11');
                        $data_financial['OptionalCostAmt11'] = $this->input->post('OptionalCostAmt11');	

                        $data_financial['OptionalCostDesc12'] = $this->input->post('OptionalCostDesc12');
                        $data_financial['OptionalCostAmt12'] = $this->input->post('OptionalCostAmt12');	

                        $data_financial['OptionalCostDesc13'] = $this->input->post('OptionalCostDesc13');
                        $data_financial['OptionalCostAmt13'] = $this->input->post('OptionalCostAmt13');	

                        $data_financial['OptionalCostDesc14'] = $this->input->post('OptionalCostDesc14');
                        $data_financial['OptionalCostAmt14'] = $this->input->post('OptionalCostAmt14');	

                        $data_financial['OptionalCostDesc15'] = $this->input->post('OptionalCostDesc15');
                        $data_financial['OptionalCostAmt15'] = $this->input->post('OptionalCostAmt15');	
                        
                        $data_financial['CalculatedOptTotal1'] = $this->input->post('CalculatedOptTotal1');
                        $data_financial['AdjustedOptTotal1'] = $this->input->post('AdjustedOptTotal1');
                        $data_financial['DiscountOptRate1'] = $this->input->post('DiscountOptRate1');
                        $data_financial['SellOptAmount1'] = $this->input->post('SellOptAmount1');
                        $data_financial['OptLDH1'] = $this->input->post('OptLDH1');
			
                          //==optional 2 part
                       
                        $data_financial['OptionalCostDesc21'] = $this->input->post('OptionalCostDesc21');
                        $data_financial['OptionalCostAmt21'] = $this->input->post('OptionalCostAmt21');	

                        $data_financial['OptionalCostDesc22'] = $this->input->post('OptionalCostDesc22');
                        $data_financial['OptionalCostAmt22'] = $this->input->post('OptionalCostAmt22');	

                        $data_financial['OptionalCostDesc23'] = $this->input->post('OptionalCostDesc23');
                        $data_financial['OptionalCostAmt23'] = $this->input->post('OptionalCostAmt23');	

                        $data_financial['OptionalCostDesc24'] = $this->input->post('OptionalCostDesc24');
                        $data_financial['OptionalCostAmt24'] = $this->input->post('OptionalCostAmt24');	

                        $data_financial['OptionalCostDesc25'] = $this->input->post('OptionalCostDesc25');
                        $data_financial['OptionalCostAmt25'] = $this->input->post('OptionalCostAmt25');	
                        
                        $data_financial['CalculatedOptTotal2'] = $this->input->post('CalculatedOptTotal2');
                        $data_financial['AdjustedOptTotal2'] = $this->input->post('AdjustedOptTotal2');
                        $data_financial['DiscountOptRate2'] = $this->input->post('DiscountOptRate2');
                        $data_financial['SellOptAmount2'] = $this->input->post('SellOptAmount2');
                        $data_financial['OptLDH2'] = $this->input->post('OptLDH2');
                        
                        
                        
                          //==optional 3 part
                       
                        $data_financial['OptionalCostDesc31'] = $this->input->post('OptionalCostDesc31');
                        $data_financial['OptionalCostAmt31'] = $this->input->post('OptionalCostAmt31');	

                        $data_financial['OptionalCostDesc32'] = $this->input->post('OptionalCostDesc32');
                        $data_financial['OptionalCostAmt32'] = $this->input->post('OptionalCostAmt32');	

                        $data_financial['OptionalCostDesc33'] = $this->input->post('OptionalCostDesc33');
                        $data_financial['OptionalCostAmt33'] = $this->input->post('OptionalCostAmt33');	

                        $data_financial['OptionalCostDesc34'] = $this->input->post('OptionalCostDesc34');
                        $data_financial['OptionalCostAmt34'] = $this->input->post('OptionalCostAmt34');	

                        $data_financial['OptionalCostDesc35'] = $this->input->post('OptionalCostDesc35');
                        $data_financial['OptionalCostAmt35'] = $this->input->post('OptionalCostAmt35');	
                        
                        $data_financial['CalculatedOptTotal3'] = $this->input->post('CalculatedOptTotal3');
                        $data_financial['AdjustedOptTotal3'] = $this->input->post('AdjustedOptTotal3');
                        $data_financial['DiscountOptRate3'] = $this->input->post('DiscountOptRate3');
                        $data_financial['SellOptAmount3'] = $this->input->post('SellOptAmount3');
                        $data_financial['OptLDH3'] = $this->input->post('OptLDH3');
                        
                        
                        
                        
                          //==optional 4 part
                       
                        $data_financial['OptionalCostDesc41'] = $this->input->post('OptionalCostDesc41');
                        $data_financial['OptionalCostAmt41'] = $this->input->post('OptionalCostAmt41');	

                        $data_financial['OptionalCostDesc42'] = $this->input->post('OptionalCostDesc42');
                        $data_financial['OptionalCostAmt42'] = $this->input->post('OptionalCostAmt42');	

                        $data_financial['OptionalCostDesc43'] = $this->input->post('OptionalCostDesc43');
                        $data_financial['OptionalCostAmt43'] = $this->input->post('OptionalCostAmt43');	

                        $data_financial['OptionalCostDesc44'] = $this->input->post('OptionalCostDesc44');
                        $data_financial['OptionalCostAmt44'] = $this->input->post('OptionalCostAmt44');	

                        $data_financial['OptionalCostDesc45'] = $this->input->post('OptionalCostDesc45');
                        $data_financial['OptionalCostAmt45'] = $this->input->post('OptionalCostAmt45');	
                        
                        $data_financial['CalculatedOptTotal4'] = $this->input->post('CalculatedOptTotal4');
                        $data_financial['AdjustedOptTotal4'] = $this->input->post('AdjustedOptTotal4');
                        $data_financial['DiscountOptRate4'] = $this->input->post('DiscountOptRate4');
                        $data_financial['SellOptAmount4'] = $this->input->post('SellOptAmount4');
                        $data_financial['OptLDH4'] = $this->input->post('OptLDH4');
                        
                        
                        $confirm_opt1=0;
                        if($this->input->post('confirm_opt1')){
                            if($this->input->post('confirm_opt1')==1){
                                $confirm_opt1=1;
                            }
                        }
                        
                        $data_financial['confirm_opt1'] = $confirm_opt1;
                        
                        $confirm_opt2=0;
                        if($this->input->post('confirm_opt2')){
                            if($this->input->post('confirm_opt2')==1){
                                $confirm_opt2=1;
                            }
                        }
                        
                        $data_financial['confirm_opt2'] = $confirm_opt2;
                        
                        
                        $confirm_opt3=0;
                        if($this->input->post('confirm_opt3')){
                            if($this->input->post('confirm_opt3')==1){
                                $confirm_opt3=1;
                            }
                        }
                        
                        $data_financial['confirm_opt3'] = $confirm_opt3;
                        
                        
                        $confirm_opt4=0;
                        if($this->input->post('confirm_opt4')){
                            if($this->input->post('confirm_opt4')==1){
                                $confirm_opt4=1;
                            }
                        }
                        
                        $data_financial['confirm_opt4'] = $confirm_opt4;
                        
                        
				
                        $this->db->where('quote_id',$quote_id);                        
                        $this->db->update('quotes',$data_financial);

			
			
	
	}
	
	
        
	
	
	///======step 1================
	
	
	function get_feature_configs($feature_id)
	{
		
		
                /*if($feature_id==SC_INT || $feature_id==SC_EXT){
                  
                  
                     
                   $sql="SELECT * FROM ".$this->db->dbprefix('feature_configure')." WHERE `feature_id` = ".$feature_id." ORDER BY FIElD(`rate`, '1.25', '1.5', '2','1','0.5','0.75')";
                             $query=$this->db->query($sql);
                             
                   
                } else {*/
                    
                    $this->db->select('*');
                    $this->db->from('feature_configure');
                    $this->db->where('feature_id',$feature_id);
                    $this->db->order_by('corder','asc');
                    $query=$this->db->get();
                //}
		
		
		 
                
		if($query->num_rows()>0)
		{
			$res=$query->result();
			
			return $res;
		}
		return false;
	}
	
	function get_rate_configs($config_id)
	{
		$this->db->select('*');
		$this->db->from('feature_configure');
		$this->db->where('feature_configure_id',$config_id);
		
		
		$query=$this->db->get();
		
		if($query->num_rows()>0)
		{
			$res=$query->row();
			
			return $res;
		}
		return false;
	}
	
	
	function feature_rate($feature_id)
	{
		$this->db->select('*');
		$this->db->from('feature_configure');
		$this->db->where('feature_id',$feature_id);
		$this->db->order_by('rate','asc');
		
		$query=$this->db->get();
		
		if($query->num_rows()>0)
		{
			$res=$query->result();
			
			return $res;
		}
		return false;
	}
	
	function feature_upg($feature_id)
	{
		$this->db->select('*');
		$this->db->from('feature_configure');
		$this->db->where('feature_id',$feature_id);
		$this->db->where('addSqft != ','0');
		$this->db->order_by('rate','asc');
		
		$query=$this->db->get();
		
		if($query->num_rows()>0)
		{
			$res=$query->result();
			
			return $res;
		}
		return false;
	}

	
	
	function quote_detail_update()
	{
		$data['QuoteContact']=$this->input->post('QuoteContact');	
		$data['QuoteAddress']=$this->input->post('QuoteAddress');	
		$data['QuoteContactPhone']=$this->input->post('QuoteContactPhone');	
		$data['short_description']=$this->input->post('short_description');	
		
		$data['leadsource_id']=$this->input->post('leadsource_id');	
		$data['ReferralName']=$this->input->post('ReferralName');	
		
		$data['rate']=$this->input->post('rate');		
		$data['date']=date('Y-m-d H:i:s',strtotime($this->input->post('date')));		
		$data['PaintQuality']=$this->input->post('PaintQuality');			
		
		$data['team_id']=$this->input->post('team_id');			
		$data['wonreason_id']=$this->input->post('wonreason_id');	
		$data['salesstage_id']=$this->input->post('salesstage_id');	
		$data['Probability']=$this->input->post('Probability');	
		
		$this->db->where('quote_id',$this->input->post('quote_id'));
		$this->db->update('quotes',$data);
		
		$this->quote_room_update();
			
	}
	
	
	
	function quote_room_update()
	{
		
		
		//pr($_POST); 
		//die;
		
		
		$room_ids='';
		
				
    	$quote_id = $this->input->post('quote_id');		
		$total_roomcnt = $this->input->post('roomcnt');
		
		
		if($total_roomcnt>0){
		
			for($i=1;$i<=$total_roomcnt;$i++){
			
			//echo "===".$i."====<br/>";
			//if($quotedetail_id){
			
			$quotedetail_id = $this->input->post('quotedetail_id'.$i);
			
						
			////===if greater than 0 then update it and store all new & old  id in one array  at the end put delete code if id not in and quote_id=  same for feature id
			
			$data_room['roomtype_id'] = $this->input->post('roomtype_id'.$i);
			
			if($data_room['roomtype_id']!=''){
			
			$data_room['additionalroomdescription'] = $this->input->post('additionalroomdescription'.$i);
			
			$data_room['prep'] = $this->input->post('prep'.$i);
			$data_room['setclean'] = $this->input->post('setclean'.$i);
			//$data_room['closets'] = $this->input->post('closets'.$i);
			//$data_room['misc'] = $this->input->post('misc'.$i);
			
			$data_room['optprep'] = $this->input->post('optprep'.$i);
			$data_room['optsetclean'] = $this->input->post('optsetclean'.$i);
			//$data_room['optclosets'] = $this->input->post('optclosets'.$i);
			//$data_room['optmisc'] = $this->input->post('optmisc'.$i);
			
			
			
			if($quotedetail_id!=''){
				
				$this->db->where('quote_id',$quote_id);
				$this->db->where('quotedetail_id',$quotedetail_id);
				$this->db->update('quotedetail',$data_room);
						
			} else {
				$data_room['quote_id'] = $quote_id;
				
				$this->db->insert('quotedetail',$data_room);
				$quotedetail_id=mysql_insert_id();
				
			}
			
			$room_ids.="'".$quotedetail_id."',";
			
			//pr($data_room);
			//die;
			
			///====feature part=====
			
			$feature_ids='';
			
			$roomfeaturecnt = $this->input->post('roomfeaturecnt'.$i);
			
			//echo "===".$roomfeaturecnt."====<br/>";
			for($j=1;$j<=$roomfeaturecnt;$j++){
				
				//echo "===".$j."===".$i."====<br/>";					
				//if($this->input->post('roomdetail_id'.$j.'-'.$i)){
			
					///===for updates check is > 0
					$roomdetail_id = $this->input->post('roomdetail_id'.$j.'-'.$i);
												
													//echo 'feature_id'.$j.'-'.$i."====<br/>";	
					 $f_txt='feature_id'.$j.'-'.$i;
					 $data_room_features['feature_id'] = $this->input->post($f_txt);
					
					if($data_room_features['feature_id']!=''){
						
						
						$data_room_features['quote_id'] = $quote_id;
						
						$len_txt='Length'.$j.'-'.$i;
						$data_room_features['Length'] = $this->input->post($len_txt);
						
						$wid_txt='Width'.$j.'-'.$i;
						$data_room_features['Width'] = $this->input->post($wid_txt);
						
						$hig_txt='Height'.$j.'-'.$i;
						$data_room_features['Height'] = $this->input->post($hig_txt);
						
						$qty_txt='Quantity'.$j.'-'.$i;
						$data_room_features['Quantity'] = $this->input->post($qty_txt);
						
						$addsqft_txt='addSqft'.$j.'-'.$i;
						$data_room_features['addSqft'] = $this->input->post($addsqft_txt);
						
						$procid_txt='coat_type'.$j.'-'.$i;
						$data_room_features['coat_type'] = $this->input->post($procid_txt);
						
						$rate_txt='rate'.$j.'-'.$i;
						$data_room_features['rate'] = $this->input->post($rate_txt);
						
						$multi_txt='Multiplyer'.$j.'-'.$i;
						$data_room_features['Multiplyer'] = $this->input->post($multi_txt);
						
						$sheen_txt='sheen_id'.$j.'-'.$i;
						$data_room_features['sheen_id'] = $this->input->post($sheen_txt);
						
						$sqft_txt='sqfootage'.$j.'-'.$i;
						$data_room_features['sqfootage'] = $this->input->post($sqft_txt);
						
						$tot_txt='totTime'.$j.'-'.$i;
						$data_room_features['totTime'] = $this->input->post($tot_txt);
						
						$sqftc_txt='sqfootagec'.$j.'-'.$i;
						$data_room_features['sqfootagec'] = $this->input->post($sqftc_txt);
						
						$totc_txt='totTimec'.$j.'-'.$i;
						$data_room_features['totTimec'] = $this->input->post($totc_txt);
						
						$desc_txt='description'.$j.'-'.$i;
						$data_room_features['description'] = $this->input->post($desc_txt);
						
						
						$com_txt='commit'.$j.'-'.$i;
						if($this->input->post($com_txt)){					
							$commit = $this->input->post($com_txt);
						} else{
							$commit = 0;
						}
						
						$data_room_features['commit']=$commit;
                                                
                                               $opttype_txt='opttype'.$j.'-'.$i;
                                               $data_room_features['opttype'] = $this->input->post($opttype_txt);
						
						if($roomdetail_id!=''){
					
							$this->db->where('quotedetail_id',$quotedetail_id);
							$this->db->where('roomdetail_id',$roomdetail_id);
							$this->db->update('roomdetail',$data_room_features);
							
							
						} else {
							
							$data_room_features['quotedetail_id']=$quotedetail_id;
							
							$this->db->insert('roomdetail',$data_room_features);
							$roomdetail_id=mysql_insert_id();
							
						}
						//pr($data_room_features);
						
						$feature_ids.="'".$roomdetail_id."',";
					} /// not blank
									
				//} ///====feature id exists
				
			}
			
			
			///======delete other room ids where quote id 
			if($feature_ids!=''){
				
				$feature_ids=substr($feature_ids,0,-1);
				
				$this->db->query("delete from ".$this->db->dbprefix("roomdetail")." where quote_id=".$quote_id." and  quotedetail_id=".$quotedetail_id." and roomdetail_id not in (".$feature_ids.")");
				
			}
			
			
			///====feature part=====
			
			
			
				} //===not blank
			
			//	} ///=====if room id exits
			
			
			}
		}
		
		
		///======delete other room ids where quote id 
		if($room_ids!=''){
			
			$room_ids=substr($room_ids,0,-1);
		
			$this->db->query("delete from ".$this->db->dbprefix("roomdetail")." where quote_id=".$quote_id." and  quotedetail_id not in (".$room_ids.")");
			
			$this->db->query("delete from ".$this->db->dbprefix("quotedetail")." where quote_id=".$quote_id." and quotedetail_id not in (".$room_ids.")");
			
		}
		
		
		//die;
	
	}
	
	///======step 2================
	
	function get_commited_material($quote_id)
	{
		
		$query=$this->db->query("select rd.feature_id, ft.description, rd.sheen_id, sh.sheen_description, sum(rd.sqfootage) as total_sqft from ".$this->db->dbprefix("roomdetail rd")." left join ".$this->db->dbprefix("feature ft")." on rd.feature_id=ft.feature_id left join ".$this->db->dbprefix("sheen sh")." on rd.sheen_id=sh.sheen_id where rd.quote_id=".$quote_id." and rd.commit=1 group by rd.feature_id, rd.sheen_id");
		
		if($query->num_rows()>0){
			return $query->result();
		}
		
		return false;
	}
	
	function get_optional_material($quote_id)
	{
		$query=$this->db->query("select rd.feature_id, ft.description, rd.sheen_id, sh.sheen_description, sum(rd.sqfootagec) as total_sqftc from ".$this->db->dbprefix("roomdetail rd")." left join ".$this->db->dbprefix("feature ft")." on rd.feature_id=ft.feature_id left join ".$this->db->dbprefix("sheen sh")." on rd.sheen_id=sh.sheen_id where rd.quote_id=".$quote_id." and rd.commit=0 group by rd.feature_id, rd.sheen_id");
		
		if($query->num_rows()>0){
			return $query->result();
		}
		
		return false;
	}
	
	
		
	function quote_order_update()
	{
		
		
		//pr($_POST); 
		//die;
		
		
		$order_ids='';
		
				
    	$quote_id = $this->input->post('quote_id');		
		$total_ordercnt = $this->input->post('ordercnt');
		
		
		if($total_ordercnt>0){
		
			for($i=1;$i<=$total_ordercnt;$i++){
			
			
			
			$orderdetail_id = $this->input->post('orderdetail_id'.$i);
			
						
			////===if greater than 0 then update it and store all new & old  id in one array  at the end put delete code if id not in and quote_id=  same for feature id
			
			$data_order['productcost_id'] = $this->input->post('productcost_id'.$i);
			
			if($data_order['productcost_id']!=''){
			
			$data_order['notes'] = $this->input->post('notes'.$i);
			
			$data_order['qty'] = $this->input->post('qty'.$i);
			$data_order['ncqty'] = $this->input->post('ncqty'.$i);
			$data_order['sheen_id'] = $this->input->post('sheen_id'.$i);
			$data_order['colour'] = $this->input->post('colour'.$i);
			
			$data_order['pst_tax'] = $this->input->post('pst_tax'.$i);
			$data_order['markup_cost'] = $this->input->post('markup_cost'.$i);
			$data_order['productcostsum'] = $this->input->post('productcostsum'.$i);
			$data_order['optionalcostsum'] = $this->input->post('optionalcostsum'.$i);
                        
                        
                        $data_order['opttype'] = $this->input->post('opttype'.$i);
			
                        
                        if($data_order['opttype']>0){
                            $data_order['ncqty']=$data_order['qty'];
                            $data_order['optionalcostsum']=$data_order['productcostsum'];
                        } else {
                            $data_order['ncqty']=0;
                            $data_order['optionalcostsum']=0;
                        }
			
			
			if($orderdetail_id!=''){
				
				$this->db->where('quote_id',$quote_id);
				$this->db->where('orderdetail_id',$orderdetail_id);
				$this->db->update('orderdetails',$data_order);
						
			} else {
			
				$data_order['quote_id'] = $quote_id;
				
				$this->db->insert('orderdetails',$data_order);
				$orderdetail_id=mysql_insert_id();
				
			}
			
			
			
			$order_ids.="'".$orderdetail_id."',";
						
			
				} //===not blank
			
		
			
			
			}
		}
		
	
		///======delete other order ids where quote id 
		if($order_ids!=''){
			
			$order_ids=substr($order_ids,0,-1);
		
			$this->db->query("delete from ".$this->db->dbprefix("orderdetails")." where quote_id=".$quote_id." and  orderdetail_id not in (".$order_ids.")");
			
		}
		
		
		//die;
	
	}
	
	
	///======step 5================
	
	function followup_update()
	{
		$date=$this->input->post('satisfactioncalldate');
		$data["satisfactioncalldate"] = date('Y-m-d H:i:s',strtotime($date));			
		$data["satisfactionrating"] = $this->input->post('satisfactionrating'); 
		$data["satisfactioncall"] = $this->input->post('satisfactioncall');
		
		$data["satisfactionleadpainter"] = $this->input->post('satisfactionleadpainter');
		$data["satisfactionname"] = $this->input->post('satisfactionname');
		$data["satisfactionnotes"] = $this->input->post('satisfactionnotes');
		
		
		
		$this->db->where('quote_id',$this->input->post('quote_id'));
		$this->db->update('quotes',$data);
		
	}
	
	
	///=====quote details===
	
	
	function quote_details($id)
	{
		$query=$this->db->query("select * from ".$this->db->dbprefix('quotes qts')." left join ".$this->db->dbprefix('client ct')." on qts.client_id=ct.client_id where  qts.quote_id='".$id."'");		
		
		if($query->num_rows()>0)
		{
			return $query->row();
		}
		return false;
	}
	
	function room_details($id)
	{
		$query=$this->db->query("select * from ".$this->db->dbprefix('quotedetail')." where quote_id='".$id."'");		
		
		if($query->num_rows()>0)
		{
			return $query->result();
		}
		return false;
	}
	
	function room_feature_details($id)
	{
		$query=$this->db->query("select * from ".$this->db->dbprefix('roomdetail')." where quotedetail_id='".$id."'");		
		
		if($query->num_rows()>0)
		{
			return $query->result();
		}
		return false;
	}
	
	function order_details($id)
	{
		$query=$this->db->query("select * from ".$this->db->dbprefix('orderdetails')." where quote_id='".$id."'");		
		
		if($query->num_rows()>0)
		{
			return $query->result();
		}
		return false;
	}
	
	///=====delete quote====
	
	function delete_quote($id)
	{
		$query=$this->db->query("select * from ".$this->db->dbprefix('quotes')." where quote_id='".$id."'");		
		
		if($query->num_rows()>0)
		{	
			
			//====quote details====
			$check_quotesdetail=$this->db->query("select * from ".$this->db->dbprefix('quotedetail')." where quote_id='".$id."'");
			
			if($check_quotesdetail->num_rows()>0)
			{
				
				$quote_detail=$check_quotesdetail->result();
				
				foreach($quote_detail as $room) {
					
					///====quote room details===
					$check_quotesroomdetail=$this->db->query("select * from ".$this->db->dbprefix('roomdetail')." where quotedetail_id='".$room->quotedetail_id."'");
				
					if($check_quotesroomdetail->num_rows()>0)
					{
						$this->db->delete('roomdetail',array('quotedetail_id' => $room->quotedetail_id));	
					}
					///====quote room details===
					
					
				}				
				
				//===delete quote details====
				$this->db->delete('quotedetail',array('quote_id' => $id));	
			}
			//====quote details====
			
			
			///====quote timecharges details===
			$check_quotestimecharge=$this->db->query("select * from ".$this->db->dbprefix('timecharges')." where quote_id='".$id."'");
		
			if($check_quotestimecharge->num_rows()>0)
			{
				$this->db->delete('timecharges',array('quote_id' => $id));	
			}
			///====quote timecharges details===
			
			
			///====quote order details===
			$check_quotesorder=$this->db->query("select * from ".$this->db->dbprefix('orderdetails')." where quote_id='".$id."'");
		
			if($check_quotesorder->num_rows()>0)
			{
				$this->db->delete('orderdetails',array('quote_id' => $id));	
			}
			///====quote order details===
			
			
			///====quote costtotals details===
			$check_quotesorder=$this->db->query("select * from ".$this->db->dbprefix('costtotals')." where quote_id='".$id."'");
		
			if($check_quotesorder->num_rows()>0)
			{
				$this->db->delete('costtotals',array('quote_id' => $id));	
			}
			///====quote costtotals details===
			
			
			
			///===delete from main quote
			$this->db->delete('quotes',array('quote_id' => $id));	
			
		}		
		
		return 0;
	}
	
	//=====manage quote==========
	
	function get_total_quote_count($option, $keyword)
	{
		/*$query=$this->db->query("select * from ".$this->db->dbprefix('quotes qts')." left join ".$this->db->dbprefix('client clt')." on qts.client_id=clt.client_id");		
		return $query->num_rows();*/
                
                
                $keyword = str_replace('"', '', str_replace(array("'", ",", "%", "$", "&", "*", "#", "(", ")", ":", ";", ">", "<", "/"), '', $keyword));


                $this->db->select('*');
                $this->db->from('quotes qts');
                $this->db->join('client clt','qts.client_id=clt.client_id','left');


                if ($option != '' && $keyword != '') {


                    if ($option == 'name') {

                        $this->db->or_like('qts.QuoteContact', $keyword);
                        $this->db->or_like('clt.first_name', $keyword);
                        $this->db->or_like('clt.last_name', $keyword);


                        if (substr_count($keyword, ' ') >= 1) {
                            $ex = explode(' ', $keyword);

                            foreach ($ex as $val) {


                                $this->db->or_like('qts.QuoteContact', $val);
                                $this->db->or_like('clt.first_name', $val);
                                $this->db->or_like('clt.last_name', $val);
                            }
                        }




                        $this->db->order_by('clt.last_name', "asc");
                    } else {
                        $this->db->or_like($option, $keyword);


                        if (substr_count($keyword, ' ') >= 1) {
                            $ex = explode(' ', $keyword);

                            foreach ($ex as $val) {

                                $this->db->or_like($option, $val);
                            }
                        }




                        $this->db->order_by($option, "asc");
                    }
                } else {
                    $this->db->order_by('qts.quote_id', "desc");
                }


                $query = $this->db->get();


                return $query->num_rows();
                
	}
	
	function get_all_quote($offset, $limit,$option, $keyword)
	{
		//$query=$this->db->query("select * from ".$this->db->dbprefix('quotes qts')." left join ".$this->db->dbprefix('client clt')." on qts.client_id=clt.client_id order by  qts.quote_id desc limit ".$limit." offset ".$offset);		
		
                
                
                $keyword = str_replace('"', '', str_replace(array("'", ",", "%", "$", "&", "*", "#", "(", ")", ":", ";", ">", "<", "/"), '', $keyword));


                $this->db->select('*');
                $this->db->from('quotes qts');
                $this->db->join('client clt','qts.client_id=clt.client_id','left');


                if ($option != '' && $keyword != '') {


                    if ($option == 'name') {

                        $this->db->or_like('qts.QuoteContact', $keyword);
                        $this->db->or_like('clt.first_name', $keyword);
                        $this->db->or_like('clt.last_name', $keyword);


                        if (substr_count($keyword, ' ') >= 1) {
                            $ex = explode(' ', $keyword);

                            foreach ($ex as $val) {


                                $this->db->or_like('qts.QuoteContact', $val);
                                $this->db->or_like('clt.first_name', $val);
                                $this->db->or_like('clt.last_name', $val);
                            }
                        }




                        $this->db->order_by('clt.last_name', "asc");
                    } else {
                        $this->db->or_like($option, $keyword);


                        if (substr_count($keyword, ' ') >= 1) {
                            $ex = explode(' ', $keyword);

                            foreach ($ex as $val) {

                                $this->db->or_like($option, $val);
                            }
                        }




                        $this->db->order_by($option, "asc");
                    }
                } else {
                    $this->db->order_by('qts.quote_id', "desc");
                }

                $this->db->limit($limit, $offset);
                
                $query = $this->db->get();
                
                
               // echo $this->db->last_query();
                
                
		if($query->num_rows()>0)
		{
			return $query->result();		
		}		
		
		return 0;
	}
	
	///=====add client====
	
	function add_client()
	{
		$first_name=$this->input->post('first_name');
		$last_name=$this->input->post('last_name');
		$email=$this->input->post('email');
		
		$cell_phone=$this->input->post('cell_phone');
		$home_phone=$this->input->post('home_phone');
		$business_phone=$this->input->post('business_phone');
		$fax=$this->input->post('fax');
		
		$address=$this->input->post('address');
		$city=$this->input->post('city');
		$province=$this->input->post('province');
		$postal_code=$this->input->post('postal_code');
		
		
		$data=array();
			
		if($first_name!='') {				
			$data['first_name']=$first_name;
		}
		if($last_name!='') {				
			$data['last_name']=$last_name;
		}
		if($email!='') {				
			$data['email']=$email;
		}
		
		if($cell_phone!='') {				
			$data['cell_phone']=$cell_phone;
		}
		if($home_phone!='') {				
			$data['home_phone']=$home_phone;
		}
		if($business_phone!='') {				
			$data['business_phone']=$business_phone;
		}
		if($fax!='') {				
			$data['fax']=$fax;
		}
		
		
		if($address!='') {				
			$data['address']=$address;
		}
		if($city!='') {				
			$data['city']=$city;
		}
		if($province!='') {				
			$data['province']=$province;
		}
		if($postal_code!='') {				
			$data['postal_code']=$postal_code;
		}
		
		
		if(!empty($data)) {
			if($this->db->insert('client',$data))
			{		
				return $this->db->insert_id();
			}		
		}
		return 0;
	}
	
	
	function add_client_quote()
	{
		
		$last_quote_id=1;
		
		$last_quote=$this->db->query("select quote_unique_id from ".$this->db->dbprefix("quotes")." order by quote_id desc limit 1");
		
		if($last_quote->num_rows()>0)
		{
			$last_quote_detail=$last_quote->row();
			$last_quote_get_id=$last_quote_detail->quote_unique_id;
			
			if($last_quote_get_id!='') {			
				$last_quote_id=substr($last_quote_get_id,3,strlen($last_quote_get_id));
				(int) $last_quote_id = (int) $last_quote_id + 1;
			}
			
		}
		
		
		$last_quote_id=sprintf("%04d", $last_quote_id);
		
		$quote_unique_id=date('y').'0'.$last_quote_id;
		
		
		$client_id=$this->input->post('client_id');
		
		
		
		$data=array(
		'client_id' => $client_id,
		'quotetype_id' => $this->input->post('quotetype_id'),
		'bustype_id' => $this->input->post('bustype_id'),
		'team_id' => $this->input->post('team_id'),
		'date' => date('Y-m-d H:i:s'),
		'quote_unique_id' => $quote_unique_id,
                'PaintQuality' => $this->input->post('PaintQuality'),   
                'rate' => $this->input->post('job_rate')
		);
		
		$client_detail=client_by_id($client_id);
                
               
		
		if($client_detail){
			if($client_detail->address!='') {				
				$data['Address']=$client_detail->address;
			}
			if($client_detail->city!='') {				
				$data['City']=$client_detail->city;
			}
			if($client_detail->province!='') {				
				$data['Province']=$client_detail->province;
			}
			if($client_detail->postal_code!='') {				
				$data['PostalCode']=$client_detail->postal_code;
			}
		}
		
		
		if($this->db->insert('quotes',$data))
		{
			return $this->db->insert_id();
		}
		return 0;
	}
	
	//=====find client====
	
	
	function find_client()
	{
		$first_name=$this->input->post('first_name');
		$last_name=$this->input->post('last_name');
		$email=$this->input->post('email');
		
		$cell_phone=$this->input->post('cell_phone');
		$home_phone=$this->input->post('home_phone');
		$business_phone=$this->input->post('business_phone');
		$fax=$this->input->post('fax');
		
		$address=$this->input->post('address');
		$city=$this->input->post('city');
		$province=$this->input->post('province');
		$postal_code=$this->input->post('postal_code');
		
		
		$this->db->select('*');
		$this->db->from('client');
		
		
		if($first_name!='') {				
			//$this->db->or_like('first_name',$first_name);
                        $this->db->like('first_name',$first_name);
		}
		if($last_name!='') {				
			//$this->db->or_like('last_name',$last_name);
                        $this->db->like('last_name',$last_name);
		}
		if($email!='') {				
			//$this->db->or_like('email',$email);
                        $this->db->like('email',$email);
		}
		
		if($cell_phone!='') {				
			//$this->db->or_like('cell_phone',$cell_phone);
                        $this->db->like('cell_phone',$cell_phone);
		}
		if($home_phone!='') {				
			//$this->db->or_like('home_phone',$home_phone);
                        $this->db->like('home_phone',$home_phone);
		}
		if($business_phone!='') {				
			//$this->db->or_like('business_phone',$business_phone);
                        $this->db->like('business_phone',$business_phone);
		}
		if($fax!='') {				
			//$this->db->or_like('fax',$fax);
                        $this->db->like('fax',$fax);
		}
		
		
		if($address!='') {				
			//$this->db->or_like('address',$address);
                        $this->db->like('address',$address);
		}
		if($city!='') {				
			//$this->db->or_like('city',$city);
                        $this->db->like('city',$city);
		}
		if($province!='') {				
			//$this->db->or_like('province',$province);
                        $this->db->like('province',$province);
		}
		if($postal_code!='') {				
			//$this->db->or_like('postal_code',$postal_code);
                        $this->db->like('postal_code',$postal_code);
		}
		
		
		$this->db->order_by('client_id','DESC');
		
		
		$query=$this->db->get();
		
		//echo $this->db->last_query(); die;
		
		if($query->num_rows()>0)
		{
			return $query->result();
		}
		
		return false;
		
	}
	
	///==========time charge============
	
	
	function complete_quote($id,$is_complete)
	{
		$data["is_complete"] = $is_complete;	
		
		$this->db->where('quote_id',$id);
		if($this->db->update('quotes',$data))
		{
			return true;
		}
		
		return false;
	}
	
	
	
	function quote_first($id)
	{
		$query=$this->db->query("select * from ".$this->db->dbprefix('timecharges tcr')." where tcr.quote_id=".$id." order by tcr.timecode_id asc limit 1");		
		
		if($query->num_rows()>0)
		{
			return $query->row();		
		}		
		
		return 0;
	}
	
	
	function quote_last($id)
	{
		$query=$this->db->query("select * from ".$this->db->dbprefix('timecharges tcr')." where tcr.quote_id=".$id." order by tcr.timecode_id desc limit 1");		
		
		if($query->num_rows()>0)
		{
			return $query->row();		
		}		
		
		return 0;
	}
	
	
	
	function get_quote_total_timecharge_count($id)
	{
		$query=$this->db->query("select * from ".$this->db->dbprefix('timecharges tcr')." left join ".$this->db->dbprefix('team tm')." on tcr.team_id=tm.team_id where tcr.quote_id=".$id);		
		return $query->num_rows();		
	}
	
	function get_quote_all_timecharge($offset, $limit,$id)
	{
		$query=$this->db->query("select * from ".$this->db->dbprefix('timecharges tcr')." left join ".$this->db->dbprefix('team tm')." on tcr.team_id=tm.team_id where tcr.quote_id=".$id." order by tcr.timecode_id desc");		
		
		if($query->num_rows()>0)
		{
			return $query->result();		
		}		
		
		return 0;
	}
	
	function timecharge_insert()
	{
		
			
		$date= date('Y-m-d H:i:s',strtotime($this->input->post('date')));	
		
		$begin_time=convertToHoursMins($this->input->post('begin_time'),'%02d:%02d');
		if($begin_time=='') { $begin_time=0.0; }
		
		$end_time=convertToHoursMins($this->input->post('end_time'),'%02d:%02d');
		if($end_time=='') { $end_time=0.0; }
		
		$break=convertToHoursMins($this->input->post('break'),'%02d:%02d');
		if($break=='') { $break=0.0; }	
			
		$data=array(
		'quote_id' => $this->input->post('quote_id'),
		'team_id' => $this->input->post('team_id'),
		'role_id' => $this->input->post('role_id'),
		'chargetype_id' => $this->input->post('chargetype_id'),
		'date' => $date,
		'begin_time' => $begin_time,
		'end_time' => $end_time,
		'break' => $break,
		'hours' => $this->input->post('hours'),
		'resource_cost' => $this->input->post('resource_cost'),
		);
		
		
		$this->db->insert('timecharges',$data);
			
	}
	
	function timecharge_update()
	{
	
	}
	
	
	function delete_timecharge($id)
	{
		$check_quotestimecharge=$this->db->query("select * from ".$this->db->dbprefix('timecharges')." where timecode_id='".$id."'");
		
		if($check_quotestimecharge->num_rows()>0)
		{
			$this->db->delete('timecharges',array('timecode_id' => $id));	
		}
	}
	
	////===type====
	
	
	
	function type_insert()
	{
			
			$data=array(
			'quotetype' => $this->input->post('quotetype')
			);
			
			
			$this->db->insert('quotetype',$data);
			
	}
	
	
	function type_update()
	{
			
		$data=array(
			'quotetype' => $this->input->post('quotetype')
			);
			
			$this->db->where('quotetype_id',$this->input->post('quotetype_id'));
			$this->db->update('quotetype',$data);
			
	}
	
	function get_type_detail($id)
	{
		$query=$this->db->query("select * from ".$this->db->dbprefix('quotetype')." where quotetype_id='".$id."'");
		return $query->row();
	}
	
	
	
	function get_total_type_count()
	{
		$query=$this->db->query("select * from ".$this->db->dbprefix('quotetype'));		
		return $query->num_rows();		
	}
	
	function get_all_type($offset, $limit)
	{
		$query=$this->db->query("select * from ".$this->db->dbprefix('quotetype')." order by quotetype_id desc limit ".$limit." offset ".$offset);		
		
		if($query->num_rows()>0)
		{
			return $query->result();		
		}		
		
		return 0;
	}
	
	function delete_type($id)
	{
		$query=$this->db->query("select * from ".$this->db->dbprefix('quotetype')." where quotetype_id='".$id."'");		
		
		if($query->num_rows()>0)
		{	
			
			$check_feature=$this->db->query("select * from ".$this->db->dbprefix('feature')." where quotetype_id='".$id."'");	
			$check_featurecoat=$this->db->query("select * from ".$this->db->dbprefix('featurecoat')." where quotetype_id='".$id."'");	
			
			$check_product=$this->db->query("select * from ".$this->db->dbprefix('product')." where quotetype_id='".$id."'");
			
			$check_quotes=$this->db->query("select * from ".$this->db->dbprefix('quotes')." where quotetype_id='".$id."'");
			$check_roomtype=$this->db->query("select * from ".$this->db->dbprefix('roomtype')." where quotetype_id='".$id."'");
			
		
			if($check_feature->num_rows()>0 || $check_featurecoat->num_rows()>0 || $check_product->num_rows()>0 || $check_quotes->num_rows()>0 || $check_roomtype->num_rows()>0) { 
				
				///==we dont delete record becuase it used somewhere
				
			} else {
				$this->db->delete('quotetype',array('quotetype_id' => $id));	
			}
		}		
		
		return 0;
	
	}
	
	
	
	
}

?>