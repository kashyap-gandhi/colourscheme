<?php
class Leadsource_model extends IWEB_Model
{

	function Leadsource_model()
	{
		parent::__construct();
	}
	
	
	////===type====
	
	
	
	function type_insert()
	{
			
			$data=array(
			'leadsourcetype' => $this->input->post('leadsourcetype')
			);
			
			
			$this->db->insert('leadsourcetype',$data);
			
	}
	
	
	function type_update()
	{
			
		$data=array(
			'leadsourcetype' => $this->input->post('leadsourcetype')
			);
			
			$this->db->where('leadsourcetype_id',$this->input->post('leadsourcetype_id'));
			$this->db->update('leadsourcetype',$data);
			
	}
	
	function get_type_detail($id)
	{
		$query=$this->db->query("select * from ".$this->db->dbprefix('leadsourcetype')." where leadsourcetype_id='".$id."'");
		return $query->row();
	}
	
	
	
	function get_total_type_count()
	{
		$query=$this->db->query("select * from ".$this->db->dbprefix('leadsourcetype'));		
		return $query->num_rows();		
	}
	
	function get_all_type($offset, $limit)
	{
		$query=$this->db->query("select * from ".$this->db->dbprefix('leadsourcetype')." order by leadsourcetype_id desc limit ".$limit." offset ".$offset);		
		
		if($query->num_rows()>0)
		{
			return $query->result();		
		}		
		
		return 0;
	}
	
	function delete_type($id)
	{
		$query=$this->db->query("select * from ".$this->db->dbprefix('leadsourcetype')." where leadsourcetype_id='".$id."'");		
		
		if($query->num_rows()>0)
		{	
			
			$check_leadsource=$this->db->query("select * from ".$this->db->dbprefix('leadsource')." where leadsourcetype_id='".$id."'");	
			
		
			if($check_leadsource->num_rows()>0) { 
				
				///==we dont delete record becuase it used somewhere
				
			} else {
				$this->db->delete('leadsourcetype',array('leadsourcetype_id' => $id));	
			}
		}		
		
		return 0;
	
	}
	
	
	///=============lead source===============
	
	
	function lead_insert()
	{
			
			$data=array(
			'leadsource' => $this->input->post('leadsource'),
			'leadsourcetype_id' => $this->input->post('leadsourcetype_id'),
			'design' => $this->input->post('design'),
			'quantity' => $this->input->post('quantity'),
			'printing' => $this->input->post('printing'),
			'distribution_cost' => $this->input->post('distribution_cost')
			);
			if($this->input->post('beginpgmdate')!='') {
				$data['beginpgmdate']=date('Y-m-d H:i:s',strtotime($this->input->post('beginpgmdate')));
			}
			if($this->input->post('endpgmdate')!='') {
				$data['endpgmdate']=date('Y-m-d H:i:s',strtotime($this->input->post('endpgmdate')));
			}
			
			
			$this->db->insert('leadsource',$data);
			
	}
	
	
	function lead_update()
	{
			
			$data=array(
			'leadsource' => $this->input->post('leadsource'),
			'leadsourcetype_id' => $this->input->post('leadsourcetype_id'),
			'design' => $this->input->post('design'),
			'quantity' => $this->input->post('quantity'),
			'printing' => $this->input->post('printing'),
			'distribution_cost' => $this->input->post('distribution_cost')
			);
			
			if($this->input->post('beginpgmdate')!='') {
				$data['beginpgmdate']=date('Y-m-d H:i:s',strtotime($this->input->post('beginpgmdate')));
			}
			if($this->input->post('endpgmdate')!='') {
				$data['endpgmdate']=date('Y-m-d H:i:s',strtotime($this->input->post('endpgmdate')));
			}
			
			$this->db->where('leadsource_id',$this->input->post('leadsource_id'));
			$this->db->update('leadsource',$data);
			
	}
	
	function get_lead_detail($id)
	{
		$query=$this->db->query("select * from ".$this->db->dbprefix('leadsource')." where leadsource_id='".$id."'");
		return $query->row();
	}
	
	
	function get_total_leadsource_count()
	{
		$query=$this->db->query("select * from ".$this->db->dbprefix('leadsource ls')." left join ".$this->db->dbprefix('leadsourcetype lst')." on ls.leadsourcetype_id=lst.leadsourcetype_id");		
		return $query->num_rows();		
	}
	
	function get_all_leadsource($offset, $limit)
	{
		$query=$this->db->query("select * from ".$this->db->dbprefix('leadsource ls')." left join ".$this->db->dbprefix('leadsourcetype lst')." on ls.leadsourcetype_id=lst.leadsourcetype_id order by ls.leadsource_id desc limit ".$limit." offset ".$offset);		
		
		if($query->num_rows()>0)
		{
			return $query->result();		
		}		
		
		return 0;
	}
	
	
	function delete_lead($id)
	{
		$query=$this->db->query("select * from ".$this->db->dbprefix('leadsource')." where leadsource_id='".$id."'");		
		
		if($query->num_rows()>0)
		{	
			
			$check_quotes=$this->db->query("select * from ".$this->db->dbprefix('quotes')." where leadsource_id='".$id."'");	
			
		
			if($check_quotes->num_rows()>0) { 
				
				///==we dont delete record becuase it used somewhere
				
			} else {
				$this->db->delete('leadsource',array('leadsource_id' => $id));	
			}
		}		
		
		return 0;
	
	}
	
}

?>