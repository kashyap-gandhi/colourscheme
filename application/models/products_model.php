<?php
class Products_model extends IWEB_Model
{

	function Products_model()
	{
		parent::__construct();
	}
	
	
	////===product cost====
	
	
	
	function product_cost_insert()
	{
	
                ///==make all inactive
		if($this->input->post('active')==1) {
			
			$data_status_update=array(
			'active' => 0,
			);
			$this->db->where('product_id',$this->input->post('product_id'));
			$this->db->update('productdetails',$data_status_update);
		}
	
			
			$data=array(
			'product_id' => $this->input->post('product_id'),
			'cost' => $this->input->post('cost'),
                        'markup_cost' => $this->input->post('markup_cost'),
                        'pst_tax' => $this->input->post('pst_tax'),
			'active' => $this->input->post('active')
			);
			if($this->input->post('start_date')!='') {
				$data['start_date']=date('Y-m-d H:i:s',strtotime($this->input->post('start_date')));
			}
			if($this->input->post('end_date')!='') {
				$data['end_date']=date('Y-m-d H:i:s',strtotime($this->input->post('end_date')));
			}
			
			
			$this->db->insert('productdetails',$data);
			
	}
	
	
	function product_cost_update()
	{
			
		///==make all inactive
		if($this->input->post('active')==1) {
			
			$data_status_update=array(
			'active' => 0,
			);
			$this->db->where('product_id',$this->input->post('product_id'));
			$this->db->update('productdetails',$data_status_update);
		}
		
		
		$data=array(
			'product_id' => $this->input->post('product_id'),
			'cost' => $this->input->post('cost'),
                        'markup_cost' => $this->input->post('markup_cost'),
                        'pst_tax' => $this->input->post('pst_tax'),
			'active' => $this->input->post('active')
			);
			if($this->input->post('start_date')!='') {
				$data['start_date']=date('Y-m-d H:i:s',strtotime($this->input->post('start_date')));
			}
                        
                       
                       
			if($this->input->post('end_date')!='') {
                             $data['end_date']=date('Y-m-d H:i:s',strtotime($this->input->post('end_date')));
			}
			
			$this->db->where('productcost_id',$this->input->post('productcost_id'));
			$this->db->update('productdetails',$data);
			
	}
	
	function get_product_cost_detail($id)
	{
		$query=$this->db->query("select * from ".$this->db->dbprefix('productdetails')." where productcost_id='".$id."'");
		return $query->row();
	}
	
	
	
	function get_total_product_cost_count($product_id)
	{
		$query=$this->db->query("select * from ".$this->db->dbprefix('productdetails pd')." left join ".$this->db->dbprefix("product pr")." on pd.product_id=pr.product_id where pd.product_id=".$product_id);		
		return $query->num_rows();		
	}
	
	function get_all_product_cost($product_id, $offset, $limit)
	{
		$query=$this->db->query("select * from ".$this->db->dbprefix('productdetails pd')." left join ".$this->db->dbprefix("product pr")." on pd.product_id=pr.product_id where pd.product_id=".$product_id." order by pd.productcost_id desc limit ".$limit." offset ".$offset);		
		
		if($query->num_rows()>0)
		{
			return $query->result();		
		}		
		
		return 0;
	}
	
	function delete_cost($id)
	{
		$query=$this->db->query("select * from ".$this->db->dbprefix('productdetails')." where productcost_id='".$id."'");		
		
		if($query->num_rows()>0)
		{	
			
			$check_orderdetails=$this->db->query("select * from ".$this->db->dbprefix('orderdetails')." where productcost_id='".$id."'");
			$check_roomdetail=$this->db->query("select * from ".$this->db->dbprefix('roomdetail')." where productcost_id='".$id."'");
			
				
			
		
			if($check_orderdetails->num_rows()>0 || $check_roomdetail->num_rows()>0) { 
				
				///==we dont delete record becuase it used somewhere
				
			} else {
				$this->db->delete('productdetails',array('productcost_id' => $id));	
			}
		}		
		
		return 0;
	
	}
        
        function active_cost($id)
	{
		$query=$this->db->query("select * from ".$this->db->dbprefix('productdetails')." where productcost_id='".$id."'");		
		
		if($query->num_rows()>0)
		{	
                    
                    $row=$query->row();
                    
                    
			
			$data_status_update=array(
			'active' => 0,
			);
			$this->db->where('product_id',$row->product_id);
			$this->db->update('productdetails',$data_status_update);	
			
                        
                        $data_status_update2=array(
			'active' => 1,
			);
			$this->db->where('productcost_id',$id);
			$this->db->update('productdetails',$data_status_update2);	
			
                        
                        
			
		}		
		
		return 0;
	
	}
	
        
        function deactive_cost($id)
	{
		$query=$this->db->query("select * from ".$this->db->dbprefix('productdetails')." where productcost_id='".$id."'");		
		
		if($query->num_rows()>0)
		{	
                    
                    $row=$query->row();
                        
                        $data_status_update2=array(
			'active' => 0,
			);
			$this->db->where('productcost_id',$id);
			$this->db->update('productdetails',$data_status_update2);	
			
                        
                        
                        $query2=$this->db->query("select * from ".$this->db->dbprefix('productdetails')." where product_id='".$row->product_id."' order by productcost_id desc limit 1");                              
                        if($query2->num_rows()>0)
                        {	
                            
                           
                    
                            $row2=$query2->row();
                        
                        
                            $data_status_update=array(
                            'active' => 1,
                            );
                            $this->db->where('productcost_id',$row2->productcost_id);
                            $this->db->update('productdetails',$data_status_update);	
			
                             
                        
                        }   
                        
			
		}		
		
		return 0;
	
	}
	
	
	
	////===product manage====
	
	
	
	function product_insert()
	{
			
			$data=array(
			'paintbrand' => $this->input->post('paintbrand'),
			'spreadqty' => $this->input->post('spreadqty'),
			'description' => $this->input->post('description'),
			'quotetype_id' => $this->input->post('quotetype_id'),
			'productquality_id' => $this->input->post('productquality_id')
			);
			
			
			$this->db->insert('product',$data);
			
	}
	
	
	function product_update()
	{
			
		$data=array(
			'paintbrand' => $this->input->post('paintbrand'),
			'spreadqty' => $this->input->post('spreadqty'),
			'description' => $this->input->post('description'),
			'quotetype_id' => $this->input->post('quotetype_id'),
			'productquality_id' => $this->input->post('productquality_id')
			);
			
			$this->db->where('product_id',$this->input->post('product_id'));
			$this->db->update('product',$data);
			
	}
	
	function get_product_detail($id)
	{
		$query=$this->db->query("select * from ".$this->db->dbprefix('product')." where product_id='".$id."'");
		return $query->row();
	}
	
	
	
	function get_total_product_count($option,$keyword)
	{
            
              $keyword=str_replace('"','',str_replace(array("'",",","%","$","&","*","#","(",")",":",";",">","<","/"),'',$keyword));
          
              
              
            $this->db->select('*');
            $this->db->from('product pr');
            $this->db->join('quotetype qt','pr.quotetype_id=qt.quotetype_id','left');
            $this->db->join('productquality pq','pr.productquality_id=pq.productquality_id','left');
		
		
		if($option!='' && $keyword!=''){		
		
                    
                    if($option=='name') {
                    
                        $this->db->or_like('pr.description',$keyword);
                        $this->db->or_like('pr.paintbrand', $keyword);


                    if(substr_count($keyword,' ')>=1)
                    {
                            $ex=explode(' ',$keyword);

                            foreach($ex as $val)
                            {

                                    $this->db->or_like('pr.description',$val);
                                    $this->db->or_like('pr.paintbrand', $val);

                            }	
                    }		
		
                    $this->db->order_by('paintbrand', "asc"); 
                    
                    
                } else {
                
                    
                    $this->db->or_like($option,$keyword);


                    if(substr_count($keyword,' ')>=1)
                    {
                            $ex=explode(' ',$keyword);

                            foreach($ex as $val)
                            {

                                    $this->db->or_like($option,$val);

                            }	
                    }		
		
                    $this->db->order_by($option, "asc"); 
                    
                }
                    
                
        } else {
            $this->db->order_by('product_id', "desc"); 
        }
		
		
		$query = $this->db->get();
		
	
              //  echo $this->db->last_query(); die;
		/*$query=$this->db->query("select * from ".$this->db->dbprefix('product pr')." left join ".$this->db->dbprefix("quotetype qt")." on pr.quotetype_id=qt.quotetype_id left join ".$this->db->dbprefix('productquality pq')." on pr.productquality_id=pq.productquality_id");		*/
                
                
		return $query->num_rows();		
	}
	
	function get_all_product($offset, $limit, $option,$keyword)
	{
            
              $keyword=str_replace('"','',str_replace(array("'",",","%","$","&","*","#","(",")",":",";",">","<","/"),'',$keyword));
          
		
              
              
              $keyword=str_replace('"','',str_replace(array("'",",","%","$","&","*","#","(",")",":",";",">","<","/"),'',$keyword));
          
              
              
            $this->db->select('*');
            $this->db->from('product pr');
            $this->db->join('quotetype qt','pr.quotetype_id=qt.quotetype_id','left');
            $this->db->join('productquality pq','pr.productquality_id=pq.productquality_id','left');
		
		
		if($option!='' && $keyword!=''){		
		
                    
                    if($option=='name') {
                    
                        $this->db->or_like('pr.description',$keyword);
                        $this->db->or_like('pr.paintbrand', $keyword);


                    if(substr_count($keyword,' ')>=1)
                    {
                            $ex=explode(' ',$keyword);

                            foreach($ex as $val)
                            {

                                    $this->db->or_like('pr.description',$val);
                                    $this->db->or_like('pr.paintbrand', $val);

                            }	
                    }		
		
                    $this->db->order_by('paintbrand', "asc"); 
                    
                    
                } else {
                
                    
                    $this->db->or_like($option,$keyword);


                    if(substr_count($keyword,' ')>=1)
                    {
                            $ex=explode(' ',$keyword);

                            foreach($ex as $val)
                            {

                                    $this->db->or_like($option,$val);

                            }	
                    }		
		
                    $this->db->order_by($option, "asc"); 
                    
                }
                    
                
        } else {
            $this->db->order_by('product_id', "desc"); 
        }
		
         $this->db->limit($limit,$offset);
		
		$query = $this->db->get();
		
	
              
              
              /*$query=$this->db->query("select * from ".$this->db->dbprefix('product pr')." left join ".$this->db->dbprefix("quotetype qt")." on pr.quotetype_id=qt.quotetype_id left join ".$this->db->dbprefix('productquality pq')." on pr.productquality_id=pq.productquality_id order by pr.product_id desc limit ".$limit." offset ".$offset);		*/
              
              
              
		
		if($query->num_rows()>0)
		{
			return $query->result();		
		}		
		
		return 0;
	}
	
	function delete_product($id)
	{
		$query=$this->db->query("select * from ".$this->db->dbprefix('product')." where product_id='".$id."'");		
		
		if($query->num_rows()>0)
		{	
			
			$check_productdetails=$this->db->query("select * from ".$this->db->dbprefix('productdetails')." where product_id='".$id."'");	
			
		
			if($check_productdetails->num_rows()>0) { 
				
				///==we dont delete record becuase it used somewhere
				
			} else {
				$this->db->delete('product',array('product_id' => $id));	
			}
		}		
		
		return 0;
	
	}
	
	
	////===type====
	
	
	
	function type_insert()
	{
			
			$data=array(
			'quality_desc' => $this->input->post('quality_desc')
			);
			
			
			$this->db->insert('productquality',$data);
			
	}
	
	
	function type_update()
	{
			
		$data=array(
			'quality_desc' => $this->input->post('quality_desc')
			);
			
			$this->db->where('productquality_id',$this->input->post('productquality_id'));
			$this->db->update('productquality',$data);
			
	}
	
	function get_type_detail($id)
	{
		$query=$this->db->query("select * from ".$this->db->dbprefix('productquality')." where productquality_id='".$id."'");
		return $query->row();
	}
	
	
	
	function get_total_type_count()
	{
		$query=$this->db->query("select * from ".$this->db->dbprefix('productquality'));		
		return $query->num_rows();		
	}
	
	function get_all_type($offset, $limit)
	{
		$query=$this->db->query("select * from ".$this->db->dbprefix('productquality')." order by productquality_id desc limit ".$limit." offset ".$offset);		
		
		if($query->num_rows()>0)
		{
			return $query->result();		
		}		
		
		return 0;
	}
	
	function delete_type($id)
	{
		$query=$this->db->query("select * from ".$this->db->dbprefix('productquality')." where productquality_id='".$id."'");		
		
		if($query->num_rows()>0)
		{	
			
			$check_product=$this->db->query("select * from ".$this->db->dbprefix('product')." where productquality_id='".$id."'");	
			
		
			if($check_product->num_rows()>0) { 
				
				///==we dont delete record becuase it used somewhere
				
			} else {
				$this->db->delete('productquality',array('productquality_id' => $id));	
			}
		}		
		
		return 0;
	
	}
	
	
	
	
}

?>