<?php
class Clients_model extends IWEB_Model
{

	function Clients_model()
	{
		parent::__construct();
	}
	
	
	
	//=====manage quote==========
	
	function get_total_quote_count($client_id)
	{
		$query=$this->db->query("select * from ".$this->db->dbprefix('quotes qts')." left join ".$this->db->dbprefix('client clt')." on qts.client_id=clt.client_id where  qts.client_id=".$client_id);		
		return $query->num_rows();		
	}
	
	function get_all_quote($offset, $limit, $client_id)
	{
		$query=$this->db->query("select * from ".$this->db->dbprefix('quotes qts')." left join ".$this->db->dbprefix('client clt')." on qts.client_id=clt.client_id where  qts.client_id=".$client_id." order by  qts.quote_id desc limit ".$limit." offset ".$offset);		
		
		if($query->num_rows()>0)
		{
			return $query->result();		
		}		
		
		return 0;
	}
	
	
	///////////////================user============
	
	
	function email_unique($email)
	{
		if($this->input->post('client_id')=='')
		{
			$query=$this->db->query("select * from  ".$this->db->dbprefix('client')." where email='".$email."'");
		}
		else
		{
			$query=$this->db->query("select * from  ".$this->db->dbprefix('client')." where client_id!='".$this->input->post('client_id')."' and email='".$email."'");
		}
		
		
		if($query->num_rows()>0)
		{
			return FALSE;
		}
		
		return TRUE;
		
		
	}
	
	
	function cellphone_unique($number)
	{
		if($this->input->post('client_id')=='')
		{
			$query=$this->db->query("select * from  ".$this->db->dbprefix('client')." where cell_phone='".$number."'");
		}
		else
		{
			$query=$this->db->query("select * from  ".$this->db->dbprefix('client')." where client_id!='".$this->input->post('client_id')."' and cell_phone='".$number."'");
		}
		
		
		if($query->num_rows()>0)
		{
			return FALSE;
		}
		
		return TRUE;
		
		
	}
	
	
	function homephone_unique($number)
	{
		if($this->input->post('client_id')=='')
		{
			$query=$this->db->query("select * from  ".$this->db->dbprefix('client')." where home_phone='".$number."'");
		}
		else
		{
			$query=$this->db->query("select * from  ".$this->db->dbprefix('client')." where client_id!='".$this->input->post('client_id')."' and home_phone='".$number."'");
		}
		
		
		if($query->num_rows()>0)
		{
			return FALSE;
		}
		
		return TRUE;
		
		
	}
	
	
	function businessphone_unique($number)
	{
		if($this->input->post('client_id')=='')
		{
			$query=$this->db->query("select * from  ".$this->db->dbprefix('client')." where business_phone='".$number."'");
		}
		else
		{
			$query=$this->db->query("select * from  ".$this->db->dbprefix('client')." where client_id!='".$this->input->post('client_id')."' and business_phone='".$number."'");
		}
		
		
		if($query->num_rows()>0)
		{
			return FALSE;
		}
		
		return TRUE;
		
		
	}
	
	function faxphone_unique($number)
	{
		if($this->input->post('client_id')=='')
		{
			$query=$this->db->query("select * from  ".$this->db->dbprefix('client')." where fax='".$number."'");
		}
		else
		{
			$query=$this->db->query("select * from  ".$this->db->dbprefix('client')." where client_id!='".$this->input->post('client_id')."' and fax='".$number."'");
		}
		
		
		if($query->num_rows()>0)
		{
			return FALSE;
		}
		
		return TRUE;
		
		
	}

	
	
	
	function client_insert()
	{
	
		$data=array(			
			'first_name' => $this->input->post('first_name'),
			'last_name' => $this->input->post('last_name'),
			'email' => $this->input->post('email'),
			'cell_phone' => $this->input->post('cell_phone'),
			'home_phone' => $this->input->post('home_phone'),
			'business_phone' => $this->input->post('business_phone'),
			'fax' => $this->input->post('fax'),		
			'address' => $this->input->post('address'),		
			'city' => $this->input->post('city'),		
			'province' => $this->input->post('province'),		
			'postal_code' => $this->input->post('postal_code'),		
			'source' => $this->input->post('source'),			
			'client_date' => date('Y-m-d H:i:s'),
			'client_ip' =>$_SERVER['REMOTE_ADDR'],
		
		);
		
		
		$this->db->insert('client',$data);
			
	}
	
	
	function client_update()
	{
			
						
			
			$data=array(			
			'first_name' => $this->input->post('first_name'),
			'last_name' => $this->input->post('last_name'),
			'email' => $this->input->post('email'),
			'cell_phone' => $this->input->post('cell_phone'),
			'home_phone' => $this->input->post('home_phone'),
			'business_phone' => $this->input->post('business_phone'),
			'fax' => $this->input->post('fax'),		
			'address' => $this->input->post('address'),		
			'city' => $this->input->post('city'),		
			'province' => $this->input->post('province'),		
			'postal_code' => $this->input->post('postal_code'),		
			'source' => $this->input->post('source'),						
			);
			
			
			
			$this->db->where('client_id',$this->input->post('client_id'));
			$this->db->update('client',$data);
			
	}
	
	function get_client_detail($id)
	{
		$query=$this->db->query("select * from ".$this->db->dbprefix('client')." where client_id='".$id."'");
		return $query->row();
	}
	
	
	
	function get_total_client_count($option,$keyword)
	{
            
            $keyword=str_replace('"','',str_replace(array("'",",","%","$","&","*","#","(",")",":",";",">","<","/"),'',$keyword));
            
            
            $this->db->select('*');
            $this->db->from('client');
		
		
		if($option!='' && $keyword!=''){		
		

            $this->db->or_like($option,$keyword);


            if(substr_count($keyword,' ')>=1)
            {
                    $ex=explode(' ',$keyword);

                    foreach($ex as $val)
                    {

                            $this->db->or_like($option,$val);

                    }	
            }

			
		
		
		$this->db->order_by($option, "asc"); 
                
        } else {
            $this->db->order_by('client_id', "desc"); 
        }
		
		
		$query = $this->db->get();
		
		
		return $query->num_rows();
                
                
		
		
	}
	
	function get_all_client($offset, $limit,$option,$keyword)
	{
		
            
            
            $keyword=str_replace('"','',str_replace(array("'",",","%","$","&","*","#","(",")",":",";",">","<","/"),'',$keyword));
            
            
            $this->db->select('*');
            $this->db->from('client');
		
		
		if($option!='' && $keyword!=''){		
		

            $this->db->or_like($option,$keyword);


            if(substr_count($keyword,' ')>=1)
            {
                    $ex=explode(' ',$keyword);

                    foreach($ex as $val)
                    {

                            $this->db->or_like($option,$val);

                    }	
            }

			
		
		
		$this->db->order_by($option, "asc"); 
                
        } else {
            $this->db->order_by('client_id', "desc"); 
        }
		
		
                $this->db->limit($limit,$offset);
        
		$query = $this->db->get();
            
		
		if($query->num_rows()>0)
		{
			return $query->result();		
		}		
		
		return 0;
	}
	
	function delete_client($id)
	{
		$query=$this->db->query("select * from ".$this->db->dbprefix('client')." where client_id='".$id."'");		
		
		if($query->num_rows()>0)
		{
										
			$check_quotes=$this->db->query("select * from ".$this->db->dbprefix('quotes')." where client_id='".$id."'");	
		
			
			if($check_quotes->num_rows()>0) { 
			
			///==we dont delete record becuase it used somewhere
			
			} else {
		
				$this->db->delete('client',array('client_id' => $id));	
				
			}
				
		}		
		
		return 0;
	
	}
	
}

?>