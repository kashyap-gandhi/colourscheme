<?php

class User_model extends IWEB_Model {

    function User_model() {
        parent::__construct();
    }

    ///////////////================user============


    function email_unique($email) {
        if ($this->input->post('team_id') == '') {
            $query = $this->db->query("select * from  " . $this->db->dbprefix('team') . " where email='" . $email . "'");
        } else {
            $query = $this->db->query("select * from  " . $this->db->dbprefix('team') . " where team_id!='" . $this->input->post('team_id') . "' and email='" . $email . "'");
        }


        if ($query->num_rows() > 0) {
            return FALSE;
        }

        return TRUE;
    }

    function user_unique($username) {
        if ($this->input->post('team_id') == '') {
            $query = $this->db->query("select * from  " . $this->db->dbprefix('team') . " where username='" . $username . "'");
        } else {
            $query = $this->db->query("select * from  " . $this->db->dbprefix('team') . " where team_id!='" . $this->input->post('team_id') . "' and username='" . $username . "'");
        }


        if ($query->num_rows() > 0) {
            return FALSE;
        }

        return TRUE;
    }

    function team_insert() {


        $data = array(
            'username' => $this->input->post('username'),
            'name' => $this->input->post('name'),
            'email' => $this->input->post('email'),
            'password' => $this->input->post('password'),
            'active' => $this->input->post('active'),
            'role2' => $this->input->post('role2'),
            'role1' => $this->input->post('role1'),
            'team_date' => date('Y-m-d H:i:s'),
            'team_ip' => $_SERVER['REMOTE_ADDR'],
        );
        if ($this->input->post('begin_date') != '') {
            $data['begin_date'] = date('Y-m-d H:i:s', strtotime(str_replace('/','-',$this->input->post('begin_date'))));
        }
        if ($this->input->post('end_date') != '') {
            $data['end_date'] = date('Y-m-d H:i:s', strtotime(str_replace('/','-',$this->input->post('end_date'))));
        }

        $this->db->insert('team', $data);
    }

    function team_update() {



        $data = array(
            'username' => $this->input->post('username'),
            'name' => $this->input->post('name'),
            'email' => $this->input->post('email'),
            'active' => $this->input->post('active'),
            'role2' => $this->input->post('role2'),
            'role1' => $this->input->post('role1')
        );

        if ($this->input->post('begin_date') != '') {
            $data['begin_date'] = date('Y-m-d H:i:s', strtotime(str_replace('/','-',$this->input->post('begin_date'))));
        }
        if ($this->input->post('end_date') != '') {
            $data['end_date'] = date('Y-m-d H:i:s', strtotime(str_replace('/','-',$this->input->post('end_date'))));
        }

        if ($this->input->post('password') != '') {
            $data['password'] = $this->input->post('password');
        }

        $this->db->where('team_id', $this->input->post('team_id'));
        $this->db->update('team', $data);
    }

    function get_team_detail($id) {
        $query = $this->db->query("select * from " . $this->db->dbprefix('team') . " where team_id='" . $id . "'");
        return $query->row();
    }

    function get_total_team_count($option, $keyword) {
        //$query=$this->db->query("select * from ".$this->db->dbprefix('team'));		
        //return $query->num_rows();		


        $keyword = str_replace('"', '', str_replace(array("'", ",", "%", "$", "&", "*", "#", "(", ")", ":", ";", ">", "<", "/"), '', $keyword));


        $this->db->select('*');
        $this->db->from('team');


        if ($option != '' && $keyword != '') {


           

          
                $this->db->or_like($option, $keyword);


                if (substr_count($keyword, ' ') >= 1) {
                    $ex = explode(' ', $keyword);

                    foreach ($ex as $val) {

                        $this->db->or_like($option, $val);
                    }
                }




                $this->db->order_by($option, "asc");
            
        } else {
            $this->db->order_by('team_id', "desc");
        }


        $query = $this->db->get();


        return $query->num_rows();
    }

    function get_all_team($offset, $limit, $option, $keyword) {
        //$query=$this->db->query("select * from ".$this->db->dbprefix('team')." order by team_id desc limit ".$limit." offset ".$offset);		


        $keyword = str_replace('"', '', str_replace(array("'", ",", "%", "$", "&", "*", "#", "(", ")", ":", ";", ">", "<", "/"), '', $keyword));


        $this->db->select('*');
        $this->db->from('team');


        if ($option != '' && $keyword != '') {


            
                $this->db->or_like($option, $keyword);


                if (substr_count($keyword, ' ') >= 1) {
                    $ex = explode(' ', $keyword);

                    foreach ($ex as $val) {

                        $this->db->or_like($option, $val);
                    }
                }




                $this->db->order_by($option, "asc");
          
        } else {
            $this->db->order_by('team_id', "desc");
        }


        $this->db->limit($limit, $offset);

        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->result();
        }

        return 0;
    }

    function delete_team($id) {
        $query = $this->db->query("select * from " . $this->db->dbprefix('team') . " where team_id='" . $id . "'");

        if ($query->num_rows() > 0) {


            $check_wage = $this->db->query("select * from " . $this->db->dbprefix('wagehistory') . " where team_id='" . $id . "'");
            $check_timecharges = $this->db->query("select * from " . $this->db->dbprefix('timecharges') . " where team_id='" . $id . "'");
            $check_quotes = $this->db->query("select * from " . $this->db->dbprefix('quotes') . " where team_id='" . $id . "'");
            $check_jobhourtracker = $this->db->query("select * from " . $this->db->dbprefix('jobhourtracker') . " where employee_id='" . $id . "'");


            if ($check_wage->num_rows() > 0 || $check_timecharges->num_rows() > 0 || $check_quotes->num_rows() > 0 || $check_jobhourtracker->num_rows() > 0) {

                ///==we dont delete record becuase it used somewhere
            } else {

                $check_rights = $this->db->query("select * from " . $this->db->dbprefix('rights_assign') . " where team_id='" . $id . "'");
                $chk_login = $this->db->query("select * from " . $this->db->dbprefix('team_login') . " where team_id='" . $id . "'");

                if ($chk_login->num_rows() > 0) {
                    $this->db->delete('team_login', array('team_id' => $id));
                }

                $this->db->delete('team', array('team_id' => $id));
            }
        }

        return 0;
    }

    function get_total_team_login_count() {
        $query = $this->db->query("select ad.username,ad.name,ad.email,adl.* from " . $this->db->dbprefix('team_login adl') . " left join " . $this->db->dbprefix('team ad') . " on adl.team_id=ad.team_id");
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        }
        return 0;
    }

    function get_all_team_login($offset, $limit) {
        $query = $this->db->query("select ad.username,ad.name,ad.email,adl.* from " . $this->db->dbprefix('team_login adl') . " left join " . $this->db->dbprefix('team ad') . " on adl.team_id=ad.team_id order by login_id desc limit " . $limit . " offset " . $offset);
        if ($query->num_rows() > 0) {
            return $query->result();
        }
        return 0;
    }

    ///====rights part====
    function get_assign_rights($id) {
        $query = $this->db->query("select * from " . $this->db->dbprefix('rights_assign') . " where team_id='" . $id . "'");

        if ($query->num_rows() > 0) {
            $rights = $query->result();

            $temp = array();

            foreach ($rights as $rig) {
                if ($rig->rights_set == 1 || $rig->rights_set == '1') {
                    $temp[] = $rig->rights_id;
                }
            }

            return $temp;
        } else {
            return 0;
        }
    }

    function get_rights() {
        $query = $this->db->query("select * from " . $this->db->dbprefix('rights') . " order by rights_name asc");
        return $query->result();
    }

    function add_rights() {

        $get_rights = $this->db->query("select * from " . $this->db->dbprefix('rights') . " order by rights_id asc");
        $all_rights = $get_rights->result();


        $rights_id = $this->input->post('rights_id');
        $team_id = $this->input->post('team_id');

        if ($rights_id) {
            foreach ($all_rights as $rig) {

                if (in_array($rig->rights_id, $rights_id)) {



                    $detail = $this->db->query("select * from " . $this->db->dbprefix('rights_assign') . " where rights_id='" . $rig->rights_id . "' and team_id='" . $team_id . "'");

                    if ($detail->num_rows() > 0) {
                        $update = $this->db->query("update " . $this->db->dbprefix('rights_assign') . " set rights_set='1' where rights_id='" . $rig->rights_id . "' and team_id='" . $team_id . "'");
                    } else {
                        $insert = $this->db->query("insert into " . $this->db->dbprefix('rights_assign') . " (`team_id`,`rights_id`,`rights_set`)values('" . $team_id . "','" . $rig->rights_id . "','1')");
                    }
                } else {
                    $detail = $this->db->query("select * from " . $this->db->dbprefix('rights_assign') . " where rights_id='" . $rig->rights_id . "' and team_id='" . $team_id . "'");

                    if ($detail->num_rows() > 0) {

                        $remove = $this->db->query("update " . $this->db->dbprefix('rights_assign') . " set rights_set='0' where rights_id='" . $rig->rights_id . "' and team_id='" . $team_id . "'");
                    } else {

                        $insert_remove = $this->db->query("insert into " . $this->db->dbprefix('rights_assign') . " (`team_id`,`rights_id`,`rights_set`)values('" . $team_id . "','" . $rig->rights_id . "','0')");
                    }
                }
            }
        } else {

            $remove = $this->db->query("delete from " . $this->db->dbprefix('rights_assign') . "  where team_id='" . $team_id . "'");
        }
    }

    ////===roles====



    function role_insert() {

        $data = array(
            'role' => $this->input->post('role')
        );


        $this->db->insert('roles', $data);
    }

    function role_update() {

        $data = array(
            'role' => $this->input->post('role')
        );

        $this->db->where('role_id', $this->input->post('role_id'));
        $this->db->update('roles', $data);
    }

    function get_role_detail($id) {
        $query = $this->db->query("select * from " . $this->db->dbprefix('roles') . " where role_id='" . $id . "'");
        return $query->row();
    }

    function get_total_role_count() {
        $query = $this->db->query("select * from " . $this->db->dbprefix('roles'));
        return $query->num_rows();
    }

    function get_all_role($offset, $limit) {
        $query = $this->db->query("select * from " . $this->db->dbprefix('roles') . " order by role_id desc limit " . $limit . " offset " . $offset);

        if ($query->num_rows() > 0) {
            return $query->result();
        }

        return 0;
    }

    function delete_role($id) {
        $query = $this->db->query("select * from " . $this->db->dbprefix('roles') . " where role_id='" . $id . "'");

        if ($query->num_rows() > 0) {
            $this->db->delete('roles', array('role_id' => $id));
        }

        return 0;
    }

    ////===wages====



    function wage_insert() {

        $data = array(
            'wage' => $this->input->post('wage'),
            'burden' => $this->input->post('burden'),
            'team_id' => $this->input->post('team_id')
        );

        if ($this->input->post('beginwage') != '') {
            $data['beginwage'] = date('Y-m-d H:i:s', strtotime(str_replace('/','-',$this->input->post('beginwage'))));
        }
        if ($this->input->post('endwage') != '') {
            $data['endwage'] = date('Y-m-d H:i:s', strtotime(str_replace('/','-',$this->input->post('endwage'))));
        }


        $this->db->insert('wagehistory', $data);
    }

    function wage_update() {

        $data = array(
            'wage' => $this->input->post('wage'),
            'burden' => $this->input->post('burden'),
            'team_id' => $this->input->post('team_id')
        );

        if ($this->input->post('beginwage') != '') {
            $data['beginwage'] = date('Y-m-d H:i:s', strtotime(str_replace('/','-',$this->input->post('beginwage'))));
        }
        if ($this->input->post('endwage') != '') {
            $data['endwage'] = date('Y-m-d H:i:s', strtotime(str_replace('/','-',$this->input->post('endwage'))));
        }



        $this->db->where('wage_id', $this->input->post('wage_id'));
        $this->db->update('wagehistory', $data);
    }

    function get_wage_detail($id) {
        $query = $this->db->query("select * from " . $this->db->dbprefix('wagehistory') . " where wage_id='" . $id . "'");
        return $query->row();
    }

    function get_total_wage_count() {
        $query = $this->db->query("select * from " . $this->db->dbprefix('wagehistory wh') . " left join " . $this->db->dbprefix("team tm") . " on wh.team_id=tm.team_id");
        return $query->num_rows();
    }

    function get_all_wage($offset, $limit) {
        $query = $this->db->query("select * from " . $this->db->dbprefix('wagehistory wh') . " left join " . $this->db->dbprefix("team tm") . " on wh.team_id=tm.team_id order by wage_id desc limit " . $limit . " offset " . $offset);

        if ($query->num_rows() > 0) {
            return $query->result();
        }

        return 0;
    }

    function get_team_all_wage($team_id) {
        $query = $this->db->query("select * from " . $this->db->dbprefix('wagehistory wh') . " where wh.team_id='" . $team_id . "' order by wage_id desc");

        if ($query->num_rows() > 0) {
            return $query->result();
        }

        return 0;
    }

}

?>