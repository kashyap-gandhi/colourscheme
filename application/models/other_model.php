<?php
class Other_model extends IWEB_Model
{

	function Other_model()
	{
		parent::__construct();
	}
	
	
	////===room type====
	
	function roomtype_insert()
	{
			
			$data=array(
			'description' => $this->input->post('description'),
			'quotetype_id' => $this->input->post('quotetype_id')
			);
			
			
			$this->db->insert('roomtype',$data);
			
	}
	
	
	function roomtype_update()
	{
			
		$data=array(
			'description' => $this->input->post('description'),
			'quotetype_id' => $this->input->post('quotetype_id')
			);
			
			$this->db->where('roomtype_id',$this->input->post('roomtype_id'));
			$this->db->update('roomtype',$data);
			
	}
	
	function get_roomtype_detail($id)
	{
		$query=$this->db->query("select * from ".$this->db->dbprefix('roomtype')." where roomtype_id='".$id."'");
		return $query->row();
	}
	
	
	
	function get_total_roomtype_count()
	{
		$query=$this->db->query("select * from ".$this->db->dbprefix('roomtype rt')." left join ".$this->db->dbprefix("quotetype qt")." on rt.quotetype_id=qt.quotetype_id");		
		return $query->num_rows();		
	}
	
	function get_all_roomtype($offset, $limit)
	{
		$query=$this->db->query("select * from ".$this->db->dbprefix('roomtype rt')." left join ".$this->db->dbprefix("quotetype qt")." on rt.quotetype_id=qt.quotetype_id order by  rt.roomtype_id desc limit ".$limit." offset ".$offset);		
		
		if($query->num_rows()>0)
		{
			return $query->result();		
		}		
		
		return 0;
	}
	
	function delete_roomtype($id)
	{
		$query=$this->db->query("select * from ".$this->db->dbprefix('roomtype')." where roomtype_id='".$id."'");		
		
		if($query->num_rows()>0)
		{	
			
			$check_quotedetail=$this->db->query("select * from ".$this->db->dbprefix('quotedetail')." where roomtype_id='".$id."'");	
			
			
		
			if($check_quotedetail->num_rows()>0) { 
				
				///==we dont delete record becuase it used somewhere
				
			} else {
				$this->db->delete('roomtype',array('roomtype_id' => $id));	
			}
		}		
		
		return 0;
	
	}
	
	
	
	
	
	
	////===winloss ====
	
	
	
	function winloss_insert()
	{
			$wonlost=0;
			
			//if(isset($this->input->post('billable')) {
				if($this->input->post('wonlost')==1) { $wonlost=1;  }
			//}
			
			$data=array(
			'reason' => $this->input->post('reason'),
			'wonlost'=>$wonlost
			);
			
			
			$this->db->insert('winloss',$data);
			
	}
	
	
	function winloss_update()
	{
			
		$wonlost=0;
			
			//if(isset($this->input->post('billable')) {
				if($this->input->post('wonlost')==1) { $wonlost=1;  }
			//}
			
			$data=array(
			'reason' => $this->input->post('reason'),
			'wonlost'=>$wonlost
			);
			
			$this->db->where('wonreason_id',$this->input->post('wonreason_id'));
			$this->db->update('winloss',$data);
			
	}
	
	function get_winloss_detail($id)
	{
		$query=$this->db->query("select * from ".$this->db->dbprefix('winloss')." where wonreason_id='".$id."'");
		return $query->row();
	}
	
	
	
	function get_total_winloss_count()
	{
		$query=$this->db->query("select * from ".$this->db->dbprefix('winloss'));		
		return $query->num_rows();		
	}
	
	function get_all_winloss($offset, $limit)
	{
		$query=$this->db->query("select * from ".$this->db->dbprefix('winloss')." order by wonreason_id desc limit ".$limit." offset ".$offset);		
		
		if($query->num_rows()>0)
		{
			return $query->result();		
		}		
		
		return 0;
	}
	
	function delete_winloss($id)
	{
		$query=$this->db->query("select * from ".$this->db->dbprefix('winloss')." where wonreason_id='".$id."'");		
		
		if($query->num_rows()>0)
		{	
			
			$check_quotes=$this->db->query("select * from ".$this->db->dbprefix('quotes')." where wonreason_id='".$id."'");	
		
			if($check_quotes->num_rows()>0) { 
				
				///==we dont delete record becuase it used somewhere
				
			} else {
				$this->db->delete('winloss',array('wonreason_id' => $id));	
			}
		}		
		
		return 0;
	
	}
	
	
	
		
	////===sheen====
	
	
	
	function sheen_insert()
	{
			
			$data=array(
			'sheen_description' => $this->input->post('sheen_description')
			);
			
			
			$this->db->insert('sheen',$data);
			
	}
	
	
	function sheen_update()
	{
			
		$data=array(
			'sheen_description' => $this->input->post('sheen_description')
			);
			
			$this->db->where('sheen_id',$this->input->post('sheen_id'));
			$this->db->update('sheen',$data);
			
	}
	
	function get_sheen_detail($id)
	{
		$query=$this->db->query("select * from ".$this->db->dbprefix('sheen')." where sheen_id='".$id."'");
		return $query->row();
	}
	
	
	
	function get_total_sheen_count()
	{
		$query=$this->db->query("select * from ".$this->db->dbprefix('sheen'));		
		return $query->num_rows();		
	}
	
	function get_all_sheen($offset, $limit)
	{
		$query=$this->db->query("select * from ".$this->db->dbprefix('sheen')." order by sheen_id desc limit ".$limit." offset ".$offset);		
		
		if($query->num_rows()>0)
		{
			return $query->result();		
		}		
		
		return 0;
	}
	
	function delete_sheen($id)
	{
		$query=$this->db->query("select * from ".$this->db->dbprefix('sheen')." where sheen_id='".$id."'");		
		
		if($query->num_rows()>0)
		{	
			
			$check_orderdetails=$this->db->query("select * from ".$this->db->dbprefix('orderdetails')." where sheen_id='".$id."'");	
			
			$check_roomdetail=$this->db->query("select * from ".$this->db->dbprefix('roomdetail')." where sheen_id='".$id."'");	
		
			if($check_orderdetails->num_rows()>0 || $check_roomdetail->num_rows()>0) { 
				
				///==we dont delete record becuase it used somewhere
				
			} else {
				$this->db->delete('sheen',array('sheen_id' => $id));	
			}
		}		
		
		return 0;
	
	}
	
	
	
	
	
	
	////===salesstage====
	
	
	
	function salesstage_insert()
	{
			
			$data=array(
			'sales_stage' => $this->input->post('sales_stage')
			);
			
			
			$this->db->insert('salesstage',$data);
			
	}
	
	
	function salesstage_update()
	{
			
		$data=array(
			'sales_stage' => $this->input->post('sales_stage')
			);
			
			$this->db->where('salesstage_id',$this->input->post('salesstage_id'));
			$this->db->update('salesstage',$data);
			
	}
	
	function get_salesstage_detail($id)
	{
		$query=$this->db->query("select * from ".$this->db->dbprefix('salesstage')." where salesstage_id='".$id."'");
		return $query->row();
	}
	
	
	
	function get_total_salesstage_count()
	{
		$query=$this->db->query("select * from ".$this->db->dbprefix('salesstage'));		
		return $query->num_rows();		
	}
	
	function get_all_salesstage($offset, $limit)
	{
		$query=$this->db->query("select * from ".$this->db->dbprefix('salesstage')." order by salesstage_id desc limit ".$limit." offset ".$offset);		
		
		if($query->num_rows()>0)
		{
			return $query->result();		
		}		
		
		return 0;
	}
	
	function delete_salesstage($id)
	{
		$query=$this->db->query("select * from ".$this->db->dbprefix('salesstage')." where salesstage_id='".$id."'");		
		
		if($query->num_rows()>0)
		{	
			
			$check_quotes=$this->db->query("select * from ".$this->db->dbprefix('quotes')." where salesstage_id='".$id."'");	
		
			if($check_quotes->num_rows()>0) { 
				
				///==we dont delete record becuase it used somewhere
				
			} else {
				$this->db->delete('salesstage',array('salesstage_id' => $id));	
			}
		}		
		
		return 0;
	
	}
	
	
	
	
	
	////===chargetype====
	
	
	
	function chargetype_insert()
	{
			$billable=0;
			
			//if(isset($this->input->post('billable')) {
				if($this->input->post('billable')==1) { $billable=1;  }
			//}
			
			$data=array(
			'charge_description' => $this->input->post('charge_description'),
			'billable'=>$billable
			);
			
			
			$this->db->insert('chargetype',$data);
			
	}
	
	
	function chargetype_update()
	{
			
		$billable=0;
			
			//if(isset($this->input->post('billable')) {
				if($this->input->post('billable')==1) { $billable=1;  }
			//}
			
			$data=array(
			'charge_description' => $this->input->post('charge_description'),
			'billable'=>$billable
			);
			
			$this->db->where('chargetype_id',$this->input->post('chargetype_id'));
			$this->db->update('chargetype',$data);
			
	}
	
	function get_chargetype_detail($id)
	{
		$query=$this->db->query("select * from ".$this->db->dbprefix('chargetype')." where chargetype_id='".$id."'");
		return $query->row();
	}
	
	
	
	function get_total_chargetype_count()
	{
		$query=$this->db->query("select * from ".$this->db->dbprefix('chargetype'));		
		return $query->num_rows();		
	}
	
	function get_all_chargetype($offset, $limit)
	{
		$query=$this->db->query("select * from ".$this->db->dbprefix('chargetype')." order by chargetype_id desc limit ".$limit." offset ".$offset);		
		
		if($query->num_rows()>0)
		{
			return $query->result();		
		}		
		
		return 0;
	}
	
	function delete_chargetype($id)
	{
		$query=$this->db->query("select * from ".$this->db->dbprefix('chargetype')." where chargetype_id='".$id."'");		
		
		if($query->num_rows()>0)
		{	
			
			$check_timecharges=$this->db->query("select * from ".$this->db->dbprefix('timecharges')." where chargetype_id='".$id."'");	
		
			if($check_timecharges->num_rows()>0) { 
				
				///==we dont delete record becuase it used somewhere
				
			} else {
				$this->db->delete('chargetype',array('chargetype_id' => $id));	
			}
		}		
		
		return 0;
	
	}
	
	
	
	////===bustype====
	
	
	
	function bustype_insert()
	{
			
			$data=array(
			'description' => $this->input->post('description')
			);
			
			
			$this->db->insert('bustype',$data);
			
	}
	
	
	function bustype_update()
	{
			
		$data=array(
			'description' => $this->input->post('description')
			);
			
			$this->db->where('bustype_id',$this->input->post('bustype_id'));
			$this->db->update('bustype',$data);
			
	}
	
	function get_bustype_detail($id)
	{
		$query=$this->db->query("select * from ".$this->db->dbprefix('bustype')." where bustype_id='".$id."'");
		return $query->row();
	}
	
	
	
	function get_total_bustype_count()
	{
		$query=$this->db->query("select * from ".$this->db->dbprefix('bustype'));		
		return $query->num_rows();		
	}
	
	function get_all_bustype($offset, $limit)
	{
		$query=$this->db->query("select * from ".$this->db->dbprefix('bustype')." order by bustype_id desc limit ".$limit." offset ".$offset);		
		
		if($query->num_rows()>0)
		{
			return $query->result();		
		}		
		
		return 0;
	}
	
	function delete_bustype($id)
	{
		$query=$this->db->query("select * from ".$this->db->dbprefix('bustype')." where bustype_id='".$id."'");		
		
		if($query->num_rows()>0)
		{	
			
			$check_quotes=$this->db->query("select * from ".$this->db->dbprefix('quotes')." where bustype_id='".$id."'");	
		
			if($check_quotes->num_rows()>0) { 
				
				///==we dont delete record becuase it used somewhere
				
			} else {
				$this->db->delete('bustype',array('bustype_id' => $id));	
			}
		}		
		
		return 0;
	
	}
	
	
	
	
	
}

?>