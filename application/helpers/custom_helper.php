<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');


	
	
	// --------------------------------------------------------------------

	/**
	 * Site Base Path
	 *
	 * @access	public
	 * @param	string	the Base Path string
	 * @return	string
	 */
	function base_path()
	{		
		$CI =& get_instance();
		return $base_path = $CI->config->slash_item('base_path');		
	}
	
	
	
	/**
	 * Check user login
	 *
	 * @return	boolen
	 */
	function check_admin_authentication()
	{		
		$CI =& get_instance();
		
			if($CI->session->userdata('team_id')!='')
			{
				return true;
			}
			else
			{
				return false;
			}
	
	}
	
	
	// --------------------------------------------------------------------

	/**
	 * get login user id
	 *
	 * @return	integer
	 */
	function get_authenticateUserID()
	{		
		$CI =& get_instance();
		return $CI->session->userdata('team_id');
	}
	
	
	/***** get last admin login detail
	**
	***/
	
	function get_last_admin_login_detail()
	{
		$CI =& get_instance();
		
		$CI->db->select('adl.*,ad.username,ad.email,ad.admin_type');
		$CI->db->from('admin_login adl');
		$CI->db->join('admin ad','adl.admin_id=ad.admin_id','left');
		$CI->db->order_by('adl.login_id','desc');
		$CI->db->limit(1);
		
		$query=$CI->db->get();
		
		if($query->num_rows()>0)
		{
			$res=$query->row();
			
			return $res->login_ip.'('.date('d M, Y h:i:s A',strtotime($res->login_date)).')';
		}
	
		return $_SERVER['REMOTE_ADDR'].'('.date('d M, Y h:i:s A').')';
		
	}
	
	
	
	
	/**
	 * @param DateTime $date A given date
	 * @param int $firstDay 0-6, Sun-Sat respectively
	 * @return DateTime
	 */
	function get_first_day_of_week($date) 
	{
		 $day_of_week = date('N', strtotime($date)); 
		 $week_first_day = date('Y-m-d', strtotime($date . " - " . ($day_of_week - 1) . " days")); 
		 return $week_first_day;
	}

	
	function get_last_day_of_week($date)
	{
		 $day_of_week = date('N', strtotime($date)); 
		 $week_last_day = date('Y-m-d', strtotime($date . " + " . (7 - $day_of_week) . " days"));   
    	 return $week_last_day;
	}
	
	
	
	
	/*** load assigns right setting
	*  return number 1 or 0
	**/
	function get_rights($rights_name)
	{
		$CI =& get_instance();
		$right_detail = $CI->db->get_where("rights",array('rights_name'=>trim($rights_name)));
		
		if($right_detail->num_rows()>0)
			{
			
				$right_result=$right_detail->row();
				$rights_id=$right_result->rights_id;

			$query=$CI->db->get_where("rights_assign",array('rights_id'=>$rights_id,'team_id'=>$CI->session->userdata('team_id')));
			
			if($query->num_rows()>0)
			{
				$result=$query->row();
				
				if($result->rights_set=='1' || $result->rights_set==1)
				{
					return 1;
				}
				else
				{
					return 0;
				}					
			}
			else
			{
				return 0;
			}	
		}
		else
		{
			return 0;		
		}
	
	}
	

	
	
	
	/*** get force_download 
	*  return string cityname
	**/
	
	function force_download($filename = '', $data = false, $enable_partial = true, $speedlimit = 0)
    {
        if ($filename == '')
        {
            return FALSE;
        }
        
        if($data === false && !file_exists($filename))
            return FALSE;

        // Try to determine if the filename includes a file extension.
        // We need it in order to set the MIME type
        if (FALSE === strpos($filename, '.'))
        {
            return FALSE;
        }
    
        // Grab the file extension
        $x = explode('.', $filename);
        $extension = end($x);

        // Load the mime types
        @include(APPPATH.'config/mimes'.EXT);
    
        // Set a default mime if we can't find it
        if ( ! isset($mimes[$extension]))
        {
            if (ereg('Opera(/| )([0-9].[0-9]{1,2})', $_SERVER['HTTP_USER_AGENT']))
                $UserBrowser = "Opera";
            elseif (ereg('MSIE ([0-9].[0-9]{1,2})', $_SERVER['HTTP_USER_AGENT']))
                $UserBrowser = "IE";
            else
                $UserBrowser = '';
            
            $mime = ($UserBrowser == 'IE' || $UserBrowser == 'Opera') ? 'application/octetstream' : 'application/octet-stream';
        }
        else
        {
            $mime = (is_array($mimes[$extension])) ? $mimes[$extension][0] : $mimes[$extension];
        }
        
        $size = $data === false ? filesize($filename) : strlen($data);
        
        if($data === false)
        {
            $info = pathinfo($filename);
            $name = $info['basename'];
        }
        else
        {
            $name = $filename;
        }
        
        // Clean data in cache if exists
        @ob_end_clean();
        
        // Check for partial download
        if(isset($_SERVER['HTTP_RANGE']) && $enable_partial)
        {
            list($a, $range) = explode("=", $_SERVER['HTTP_RANGE']);
            list($fbyte, $lbyte) = explode("-", $range);
            
            if(!$lbyte)
                $lbyte = $size - 1;
            
            $new_length = $lbyte - $fbyte;
            
            header("HTTP/1.1 206 Partial Content", true);
            header("Content-Length: $new_length", true);
            header("Content-Range: bytes $fbyte-$lbyte/$size", true);
        }
        else
        {
            header("Content-Length: " . $size);
        }
        
        // Common headers
        header('Content-Type: ' . $mime, true);
        header('Content-Disposition: attachment; filename="' . $name . '"', true);
        header("Expires: 0", true);
        header('Accept-Ranges: bytes', true);
        header("Cache-control: private", true);
        header('Pragma: private', true);header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        
        // Open file
        if($data === false) {
            $file = fopen($filename, 'r');
            
            if(!$file)
                return FALSE;
        }
        
        // Cut data for partial download
        if(isset($_SERVER['HTTP_RANGE']) && $enable_partial)
            if($data === false)
                fseek($file, $range);
            else
                $data = substr($data, $range);
        
        // Disable script time limit
        @set_time_limit(0);
        
        // Check for speed limit or file optimize
        if($speedlimit > 0 || $data === false)
        {
            if($data === false)
            {
                $chunksize = $speedlimit > 0 ? $speedlimit * 1024 : 512 * 1024;
            
                while(!feof($file) and (connection_status() == 0))
                {
                    $buffer = fread($file, $chunksize);
                    echo $buffer;
                    flush();
                    
                    if($speedlimit > 0)
                        sleep(1);
                }
                
                fclose($file);
            }
            else
            {
                $index = 0;
                $speedlimit *= 1024; //convert to kb
                
                while($index < $size and (connection_status() == 0))
                {
                    $left = $size - $index;
                    $buffersize = min($left, $speedlimit);
                    
                    $buffer = substr($data, $index, $buffersize);
                    $index += $buffersize;
                    
                    echo $buffer;
                    flush();
                    sleep(1);
                }
            }
        }
        else
        {
            echo $data;
        }
		
		//$this->db->cache_delete_all();
		ob_clean();
        flush();
		
    }
	
	
	

	
	
	
	
	
	/**
 * Array to CSV
 *
 * download == "" -> return CSV string
 * download == "toto.csv" -> download file toto.csv
 */
	

    function array_to_csv($array, $download = "")
    {
        if ($download != "")
        {    
            header('Content-Type: application/csv');
            header('Content-Disposition: attachement; filename="' . $download . '"');
        }        

        ob_start();
        $f = fopen('php://output', 'w') or show_error("Can't open php://output");
        $n = 0;        
        foreach ($array as $line)
        {
            $n++;
            if (!fputcsv($f, $line))
            {
                show_error("Can't write line $n: $line");
            }
        }
        fclose($f) or show_error("Can't close php://output");
        $str = ob_get_contents();
        ob_end_clean();

        if ($download == "")
        {
            return $str;    
        }
        else
        {    
            echo $str;
        }        
    }


// ------------------------------------------------------------------------

/**
 * Query to CSV
 *
 * download == "" -> return CSV string
 * download == "toto.csv" -> download file toto.csv
 */

    function query_to_csv($query, $headers = TRUE, $download = "")
    {
        if ( ! is_object($query) OR ! method_exists($query, 'list_fields'))
        {
            show_error('invalid query');
        }
        
        $array = array();
        
        if ($headers)
        {
            $line = array();
            foreach ($query->list_fields() as $name)
            {
                $line[] = $name;
            }
            $array[] = $line;
        }
        
        foreach ($query->result_array() as $row)
        {
            $line = array();
            foreach ($row as $item)
            {
                $line[] = $item;
            }
            $array[] = $line;
        }

        echo array_to_csv($array, $download);
    }

	function pr($data=array())
	{
		echo "<pre>";
		print_r($data);
		echo "</pre>";
	}
        
        
        
        
        function check_role_permission($module_name){
            
            $CI =& get_instance();
            
            $permission=false;
            
            $role1= (int) $CI->session->userdata('role1');
            $role2= (int) $CI->session->userdata('role2');
            
            $role_permission1=false;            
            if($role1>0){                
                $get_permission1=$CI->config->item($role1, 'roles');
                
                if(in_array($module_name, $get_permission1)){
                    $role_permission1=true;
                }              
            }
            
            $role_permission2=false;            
            if($role2>0){                
                $get_permission2=$CI->config->item($role2, 'roles');
                
                if(in_array($module_name, $get_permission2)){
                    $role_permission2=true;
                }              
            }
            
            
            if($role_permission1==true || $role_permission2==true){
                $permission=true;
            }
            
            
            
            return $permission;
            
        }
        
        
        
        function quote_actual_job_hours($quote_id,$chargetype_id=0)
	{		
		$CI =& get_instance();
                
             if($chargetype_id>0){
             
                       $query = $CI->db->query("SELECT TIME_FORMAT(SEC_TO_TIME(SUM(TIME_TO_SEC(TIME_FORMAT(hours,'%H:%i')))),'%H:%i') as total_hours FROM ".$CI->db->dbprefix("timecharges")." WHERE quote_id='".$quote_id."' and chargetype_id='".$chargetype_id."' ");
                 
             } else {
                    $query = $CI->db->query("SELECT TIME_FORMAT(SEC_TO_TIME(SUM(TIME_TO_SEC(TIME_FORMAT(hours,'%H:%i')))),'%H:%i') as total_hours FROM ".$CI->db->dbprefix("timecharges")." WHERE quote_id='".$quote_id."' ");
             }
                
                
		if($query->num_rows()>0){
			$res=$query->row();
                        return $res->total_hours; 
		} 
		return 0;
	}
        
        function quote_actual_labour_costs($quote_id)
	{		
		$CI =& get_instance();
                
             
                    $query = $CI->db->query("SELECT SUM(resource_cost) as total_costs FROM ".$CI->db->dbprefix("timecharges")." WHERE quote_id='".$quote_id."' ");
                
                
		if($query->num_rows()>0){
			$res=$query->row();
                        return $res->total_costs; 
		} 
		return 0;
	}
	
        
        
        function quotetotalhours_by_id($quote_id,$commit,$opttype)
	{		
		$CI =& get_instance();
                
                if($commit==1){
                    $query = $CI->db->query("select SUM(totTime) as TotalHours from ".$CI->db->dbprefix("roomdetail")." where commit=1 and quote_id='".$quote_id."' and opttype=0 ");
                } else {
                    $query = $CI->db->query("select SUM(totTimeC) as TotalHours from ".$CI->db->dbprefix("roomdetail")." where commit=0 and quote_id='".$quote_id."' and opttype='".$opttype."'");
                }
                
		if($query->num_rows()>0){
			$res=$query->row();
                        return $res->TotalHours; 
		} 
		return 0;
	}
        
        
        
        
        function quotetotalprep_by_id($quote_id,$commit)
	{		
		$CI =& get_instance();
                
                if($commit==1){
                    $query = $CI->db->query("select SUM(prep) as TotalPrep from ".$CI->db->dbprefix("quotedetail")." where quote_id='".$quote_id."'");
                } else {
                    $query = $CI->db->query("select SUM(optprep) as TotalPrep from ".$CI->db->dbprefix("quotedetail")." where quote_id='".$quote_id."'");
                }
                
		if($query->num_rows()>0){
			$res=$query->row();
                        return $res->TotalPrep; 
		} 
		return 0;
	}
        
         function quotetotalclosets_by_id($quote_id,$commit)
	{		
		$CI =& get_instance();
                
                if($commit==1){
                    $query = $CI->db->query("select SUM(closets) as TotalClosets from ".$CI->db->dbprefix("quotedetail")." where quote_id='".$quote_id."'");
                } else {
                    $query = $CI->db->query("select SUM(optclosets) as TotalClosets from ".$CI->db->dbprefix("quotedetail")." where quote_id='".$quote_id."'");
                }
                
		if($query->num_rows()>0){
			$res=$query->row();
                        return $res->TotalClosets; 
		} 
		return 0;
	}
        
          function quotetotalmisc_by_id($quote_id,$commit)
	{		
		$CI =& get_instance();
                
                if($commit==1){
                    $query = $CI->db->query("select SUM(misc) as TotalMisc from ".$CI->db->dbprefix("quotedetail")." where quote_id='".$quote_id."'");
                } else {
                    $query = $CI->db->query("select SUM(optmisc) as TotalMisc from ".$CI->db->dbprefix("quotedetail")." where quote_id='".$quote_id."'");
                }
                
		if($query->num_rows()>0){
			$res=$query->row();
                        return $res->TotalMisc; 
		} 
		return 0;
	}
        
          function quotetotalsetclean_by_id($quote_id,$commit)
	{		
		$CI =& get_instance();
                
                if($commit==1){
                    $query = $CI->db->query("select SUM(setclean) as TotalSetClean from ".$CI->db->dbprefix("quotedetail")." where quote_id='".$quote_id."'");
                } else {
                    $query = $CI->db->query("select SUM(optsetclean) as TotalSetClean from ".$CI->db->dbprefix("quotedetail")." where quote_id='".$quote_id."'");
                }
                
		if($query->num_rows()>0){
			$res=$query->row();
                        return $res->TotalSetClean; 
		} 
		return 0;
	}
        
        
        
        
        function quotetotalmaterial_by_id($quote_id,$commit,$opttype)
	{		
		$CI =& get_instance();
                
                if($commit==1){
                    $query = $CI->db->query("select SUM(productcostsum) as TotalProductSum from ".$CI->db->dbprefix("orderdetails")." where quote_id='".$quote_id."' and opttype=0 ");
                } else {
                    $query = $CI->db->query("select SUM(optionalcostsum) as TotalProductSum from ".$CI->db->dbprefix("orderdetails")." where quote_id='".$quote_id."' and opttype='".$opttype."'");
                }
                
		if($query->num_rows()>0){
			$res=$query->row();
                        return $res->TotalProductSum; 
		} 
		return 0;
	}
	
	
	function sheen_by_id($id)
	{		
		$CI =& get_instance();
		$query = $CI->db->query("select * from ".$CI->db->dbprefix("sheen")." where sheen_id='".$id."'");
		if($query->num_rows()>0){
			return $query->row();
		} 
		return false;
	}

	function sheen_list()
	{		
		$CI =& get_instance();
		
                $CI->db->select("*");
		$CI->db->from("sheen");
		$CI->db->order_by("sheen_description",'ASC');		
		$query = $CI->db->get();
                
                
		if($query->num_rows()>0){
			return $query->result();
		} 
		return false;
	}
	
	
	function quotetype_product_list($quotetype_id)
	{
		$CI =& get_instance();
		
		/*$CI->db->select("*");
		$CI->db->from("product pr");
		$CI->db->join("productdetails prd","pr.product_id=prd.product_id",'left');
		$CI->db->where("pr.quotetype_id",$quotetype_id);	
		$CI->db->where("prd.active",1);	
	
		$query = $CI->db->get();
		*/
		
		
		$query = $CI->db->query("SELECT ProductDetails.productcost_id, Product.description, Product.paintbrand, Product.product_id, ProductDetails.cost,ProductDetails.markup_cost,ProductDetails.pst_tax FROM ".$CI->db->dbprefix("product Product")." RIGHT JOIN ".$CI->db->dbprefix("productdetails ProductDetails")." ON Product.product_id=ProductDetails.product_id WHERE (ProductDetails.active=1 or ProductDetails.active=0) and Product.quotetype_id=".$quotetype_id." And  Product.productquality_id>0 ORDER BY Product.paintbrand, Product.productquality_id");
		
		
		if($query->num_rows()>0){
			return $query->result();
		} 
		return false;
	}
	
	function quotetype_room_list($quotetype_id)
	{
		$CI =& get_instance();
		$CI->db->select("*");
		$CI->db->from("roomtype");
		$CI->db->where("quotetype_id",$quotetype_id);		
		$query = $CI->db->get();
		if($query->num_rows()>0){
			return $query->result();
		} 
		return false;
	}
	
	function quotetype_feature_list($quotetype_id)
	{
		$CI =& get_instance();
		$CI->db->select("*");
		$CI->db->from("feature");
		$CI->db->where("quotetype_id",$quotetype_id);	
                $CI->db->order_by('description','asc');
		$query = $CI->db->get();
		if($query->num_rows()>0){
			return $query->result();
		} 
		return false;
	}
	
	
	function quotetype_featurecoat_list($quotetype_id)
	{
		$CI =& get_instance();
		$CI->db->select("*");
		$CI->db->from("featurecoat");
		$CI->db->where("quotetype_id",$quotetype_id);		
		$query = $CI->db->get();
		if($query->num_rows()>0){
			return $query->result();
		} 
		return false;
	}
	
	
        
        
        function product_by_id($id)
	{		
		$CI =& get_instance();
		$query = $CI->db->query("select * from ".$CI->db->dbprefix("product")." where product_id='".$id."'");
                
                echo "select * from ".$CI->db->dbprefix("product")." where product_id='".$id."'"; die;
		if($query->num_rows()>0){
			return $query->row();
		} 
		return false;
	}

/*** load product setting
	*  return records array
	**/
	
	function product_list()
	{		
		$CI =& get_instance();
		$query = $CI->db->get("product");
		if($query->num_rows()>0){
			return $query->result();
		} 
		return false;
	}
	


/*** load product quality setting
	*  return records array
	**/
	
	function productquality_list()
	{		
		$CI =& get_instance();
		$query = $CI->db->get("productquality");
		if($query->num_rows()>0){
			return $query->result();
		} 
		return false;
	}
	
	
	function bustype_by_id($id)
	{		
		$CI =& get_instance();
		$query = $CI->db->query("select * from ".$CI->db->dbprefix("bustype")." where bustype_id='".$id."'");
		if($query->num_rows()>0){
			return $query->row();
		} 
		return false;
	}
	
	function bustype_list()
	{		
		$CI =& get_instance();
		$CI->db->select("*");
		$CI->db->from("bustype");
		$CI->db->order_by("description","asc");
		$query = $CI->db->get();
		if($query->num_rows()>0){
			return $query->result();
		} 
		return false;
	}
	
	
	/*** load team setting
	*  return records array
	**/
	
	
	function quote_paint($quotetype_id)
	{
		
		$CI =& get_instance();
		$query = $CI->db->query("SELECT ProductDetails.productcost_id, Product.description, Product.paintbrand FROM ".$CI->db->dbprefix("product Product")." RIGHT JOIN ".$CI->db->dbprefix("productdetails ProductDetails")." ON Product.product_id=ProductDetails.product_id WHERE ProductDetails.active=1 and Product.quotetype_id=".$quotetype_id." And  Product.productquality_id>0 ORDER BY Product.paintbrand asc");
                // order by Product.productquality_id asc
		
		
		if($query->num_rows()>0){
			return $query->result();
		} 
		return false;
		
	}
	
        
        function product_by_productcost_id($productcost_id){
            
            $CI =& get_instance();
		$query = $CI->db->query("SELECT Product.*,ProductDetails.* FROM ".$CI->db->dbprefix("product Product")." RIGHT JOIN ".$CI->db->dbprefix("productdetails ProductDetails")." ON Product.product_id=ProductDetails.product_id WHERE ProductDetails.active=1 and ProductDetails.productcost_id=".$productcost_id." ");
              
		
		
		if($query->num_rows()>0){
			return $query->row();
		} 
		return false;
		
        }
        
        
	function quotetype_by_id($id)
	{		
		$CI =& get_instance();
		$query = $CI->db->query("select * from ".$CI->db->dbprefix("quotetype")." where quotetype_id='".$id."'");
		if($query->num_rows()>0){
			return $query->row();
		} 
		return false;
	}
	
	function quotetype_list()
	{		
		$CI =& get_instance();
		$query = $CI->db->get("quotetype");
		if($query->num_rows()>0){
			return $query->result();
		} 
		return false;
	}
	
	
	function quote_list()
	{		
		$CI =& get_instance();
		$CI->db->select("qt.quote_id,qt.quote_unique_id,qt.date,qt.QuoteContact,ct.first_name,ct.last_name,bt.description,qtp.quotetype");
		$CI->db->from("quotes qt");
		$CI->db->join("client ct","qt.client_id=ct.client_id","left");
		$CI->db->join("bustype bt","qt.bustype_id=bt.bustype_id","left");
		$CI->db->join("quotetype qtp","qt.quotetype_id=qtp.quotetype_id","left");
		$CI->db->order_by("qt.quote_id","desc");
		$query = $CI->db->get();
		if($query->num_rows()>0){
			return $query->result();
		} 
		return false;
	}
	
	
	
	
	
	/*** load team setting
	*  return records array
	**/
	
	function team_by_id($id)
	{		
		$CI =& get_instance();
		$query = $CI->db->query("select * from ".$CI->db->dbprefix("team")." where team_id='".$id."'");
		if($query->num_rows()>0){
			return $query->row();
		} 
		return false;
	}
	
	function team_list()
	{		
		$CI =& get_instance();
		$CI->db->select("*");
		$CI->db->from("team");
                $CI->db->where("active",1);
		$CI->db->order_by("name","asc");
		$query = $CI->db->get();
		if($query->num_rows()>0){
			return $query->result();
		} 
		return false;
	}
        
        function team_list_by_role_id($role_id)
	{		
		$CI =& get_instance();
		$CI->db->select("*");
		$CI->db->from("team");
                //$CI->db->where("active",1);
                $CI->db->where(" active=1 and (role1='".$role_id."' or role2='".$role_id."') ",null, false);
		$CI->db->order_by("name","asc");
		$query = $CI->db->get();
		if($query->num_rows()>0){
			return $query->result();
		} 
		return false;
	}
	
	function winloss_by_id($id)
	{		
		$CI =& get_instance();
		$query = $CI->db->query("select * from ".$CI->db->dbprefix("winloss")." where wonreason_id='".$id."'");
		if($query->num_rows()>0){
			return $query->row();
		} 
		return false;
	}
	
	function winloss_list()
	{		
		$CI =& get_instance();
		$CI->db->select("*");
		$CI->db->from("winloss");
                $CI->db->where("is_active",1);	
		$CI->db->order_by("wonreason_id","asc");
		$query = $CI->db->get();
		if($query->num_rows()>0){
			return $query->result();
		} 
		return false;
	}
	
	
	function salesstage_list()
	{				
		$CI =& get_instance();
		$CI->db->select("*");
		$CI->db->from("salesstage");
		$CI->db->order_by("salesstage_id","asc");
		$query = $CI->db->get();
		
		if($query->num_rows()>0){
			return $query->result();
		} 
		return false;
	}
	
	
	
	/*** load leadsourcetype list
	*  return records array
	**/
	
	function leadsource_list()
	{				
		$CI =& get_instance();
		$CI->db->select("*");
		$CI->db->from("leadsource");
		$CI->db->order_by("leadsource_id","desc");
		$query = $CI->db->get();
		
		if($query->num_rows()>0){
			return $query->result();
		} 
		return false;
	}
	
	/*** load leadsourcetype list
	*  return records array
	**/
	
	function leadsource_type_list()
	{		
		$CI =& get_instance();
		$query = $CI->db->get("leadsourcetype");
		if($query->num_rows()>0){
			return $query->result();
		} 
		return false;
	}
	
	
	/*** load role setting
	*  return records array
	**/
	
	function role_list()
	{		
		$CI =& get_instance();
		$query = $CI->db->get("roles");
		if($query->num_rows()>0){
			return $query->result();
		} 
		return false;
	}
	
	
	/*** load role setting
	*  return records array
	**/
	
	function chargetype_list()
	{		
		$CI =& get_instance();
		$query = $CI->db->get("chargetype");
		if($query->num_rows()>0){
			return $query->result();
		} 
		return false;
	}
        
        
        function chargetype_by_id($id)
	{		
		$CI =& get_instance();
		$query = $CI->db->query("select * from ".$CI->db->dbprefix("chargetype")." where chargetype_id='".$id."'");
		if($query->num_rows()>0){
			return $query->row();
		} 
		return false;
	}
	
	
	function client_by_id($id)
	{		
		$CI =& get_instance();
		$query = $CI->db->query("select * from ".$CI->db->dbprefix("client")." where client_id='".$id."'");
		if($query->num_rows()>0){
			return $query->row();
		} 
		return false;
	}
		
	/*** load site setting
	*  return single record array
	**/
	
	function site_setting()
	{		
		$CI =& get_instance();
		$query = $CI->db->get("site_setting");
		return $query->row();
	
	}
	
	
	
	function email_setting()
	{
		$CI =& get_instance();
		$query = $CI->db->get("email_setting");
		return $query->row();
	}
	
	
	/**
	 * send email
	 *
	 * @return	integer
	 */
	 
	 
	
	function email_send($email_address_from,$email_address_reply,$email_to,$email_subject,$str,$email_from_name='')
	{
				
		$CI =& get_instance();
		$query = $CI->db->get_where("email_setting",array('email_setting_id'=>1));
		$email_set=$query->row();
					
									
		$CI->load->library(array('email'));
			
		///////====smtp====
		
		if($email_set->mailer=='smtp')
		{
		
			$config['protocol']='smtp';  
			$config['smtp_host']=trim($email_set->smtp_host);  
			$config['smtp_port']=trim($email_set->smtp_port);  
			$config['smtp_timeout']='30';  
			$config['smtp_user']=trim($email_set->smtp_email);  
			$config['smtp_pass']=trim($email_set->smtp_password);  
					
		}
		
		/////=====sendmail======
		
		elseif(	$email_set->mailer=='sendmail')
		{	
		
			$config['protocol'] = 'sendmail';
			$config['mailpath'] = trim($email_set->sendmail_path);
			
		}
		
		/////=====php mail default======
		
		else
		{
		
		}
			
			
		$config['wordwrap'] = TRUE;	
		$config['mailtype'] = 'html';
		$config['crlf'] = '\n\n';
		$config['newline'] = '\n\n';
		
		$CI->email->initialize($config);	
				
		if($email_from_name!='')
		{
			$CI->email->from($email_address_from,$email_from_name);
		}
		else
		{
			$CI->email->from($email_address_from);
		}
	
		$CI->email->reply_to($email_address_reply);
		$CI->email->to($email_to);
		$CI->email->subject($email_subject);
		$CI->email->message($str);
		$CI->email->send();

	}
	
	
	
	
	
	/**
	 * generate random code
	 *
	 * @return	string
	 */
	
	function randomCode()
	{
		$alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
		$pass = array(); 
		
		for ($i = 0; $i < 12; $i++) {
		$n = rand(0, strlen($alphabet)-1); //use strlen instead of count
		$pass[$i] = $alphabet[$n];
		}
		return implode($pass); //turn the array into a string
	}
	
	
	function getrandomCode($length=12) 
	{
     
	 	$code='';
		//Ascii Code for number, lowercase, uppercase and special characters
		$no = range(48,57); 
		$lo = range(97,122);
		$up = range(65,90); 
		 
		//exclude character I, l, 1, 0, O
		$eno = array(48, 49);
		$elo = array(108);
		$eup = array(73,79);
		$no = array_diff($no,$eno);
		$lo = array_diff($lo,$elo);
		$up = array_diff($up,$eup);
		$chr = array_merge($no, $lo, $up);
	 
		 
		for ($i=1;$i<=$length;$i++) {
			 
			$code.= chr($chr[rand(0,count($chr)-1)]);   
		}
		return $code;
	}
	
	
	
	
	
	
  
  	/****  create seo friendly url 
	* var string $text
	**/ 	  
  
  	function clean_url($text) 
	{ 
	
		$text=strtolower($text); 
		$code_entities_match = array( '&quot;' ,'!' ,'@' ,'#' ,'$' ,'%' ,'^' ,'&' ,'*' ,'(' ,')' ,'+' ,'{' ,'}' ,'|' ,':' ,'"' ,'<' ,'>' ,'?' ,'[' ,']' ,'' ,';' ,"'" ,',' ,'.' ,'_' ,'/' ,'*' ,'+' ,'~' ,'`' ,'=' ,' ' ,'---' ,'--','--'); 
		$code_entities_replace = array('' ,'-' ,'-' ,'' ,'' ,'' ,'-' ,'-' ,'' ,'' ,'' ,'' ,'' ,'' ,'' ,'-' ,'' ,'' ,'' ,'' ,'' ,'' ,'' ,'' ,'' ,'-' ,'' ,'-' ,'-' ,'' ,'' ,'' ,'' ,'' ,'-' ,'-' ,'-','-'); 
		$text = str_replace($code_entities_match, $code_entities_replace, $text); 
		return $text; 
	} 

	
	/********get original client ip
	** return ip string
	***/
	
	 function getRealIP() 
	 {
	   if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
			$ip=$_SERVER['HTTP_CLIENT_IP'];
	   } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
			$ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
	   } else {
			 $ip=$_SERVER['REMOTE_ADDR'];
	   }
	  return $ip;
	}
	
	
	/*********get user location from ip
	*** return array
	***/
	
	function getip2location($user_ip)
	{
		$url = 'http://www.geoplugin.net/json.gp?ip=';
		$url .=$user_ip;
			
		if(function_exists('curl_init'))
		{		
			$defaults = array(
				CURLOPT_HEADER => 0,
				CURLOPT_URL => $url,
				CURLOPT_FRESH_CONNECT => 1,
				CURLOPT_RETURNTRANSFER => 1,
				CURLOPT_FORBID_REUSE => 1,
				CURLOPT_TIMEOUT => 15
			);
			
			$ch = curl_init();
			curl_setopt_array($ch, $defaults);
			$result = curl_exec($ch);
			curl_close($ch);
			$response = json_decode($result);	
			return $response;	
			
		} else if(function_exists('file_get_contents'))	{
					
			$response = json_decode(file_get_contents($url));
			return $response;		
		}		
		
		return false;
	}
	
        
        //http://www.calculatorsoup.com/calculators/time/time-to-decimal-calculator.php
        
        function conver_time_to_decimal($time=''){
            
            $res=0;
            $sum=0;
            
            if($time!=''){
                                
                $exp=explode(':', $time);
                
                if(isset($exp[0]) && $exp[0]!=''){
                    $sum=$sum+$exp[0];
                }
                if(isset($exp[1]) && $exp[1]!=''){
                    $sum=$sum+($exp[1]/60);
                }
                if(isset($exp[2]) && $exp[2]!=''){
                    $sum=$sum+($exp[2]/3600);
                }
                
                $res=$sum;
                
            }
            return $res;
            
        }

	
	
	function convertToHoursMins($time, $format = '%d:%d') {
		settype($time, 'integer');
		if ($time < 1) {
			return;
		}
		$hours = floor($time / 60);
		$minutes = ($time % 60);
		return sprintf($format, $hours, $minutes);
	}
	
	
	 /***  get  duration between two dates
       * var date date
       * var date curdate
       * var integer diff
       * var integer years
       * var integer months
       * var integer days
       * var integer hours
       * var integer mins
       * return var integetr ago
       **/
       function getDuration($date)
       {
               $curdate = date('Y-m-d H:i:s');
               $diff = abs(strtotime($date) - strtotime($curdate));
               $years = floor($diff / (365*60*60*24));
               $months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
               $days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));
               $hours = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24 )/ (60*60));
               $mins = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24 - $hours*60*60)/ (60));
               
               $ago = '';
               if($years != 0){ if($years > 1) {$ago =  $years.' years';} else { $ago =  $years.' year';}}
               elseif($months != 0){ if($months > 1) {$ago =  $months.' months';} else { $ago =  $months.' month';}}
               elseif($days != 0) { if($days > 1) {$ago =  $days.' days';} else { $ago =  $days.' day';}}
               elseif($hours != 0){ if($hours > 1) {$ago =  $hours.' hours';} else { $ago =  $hours.' hour';}}
               else{ if($mins > 1) {$ago =  $mins.' minutes';} else { $ago =  $mins.' minute';}}
               return $ago.' ago';
       }
	   
	   
	   
	   
	   
	 
	    /***  get  duration between two dates in reverse count
	  ** $start_date ( any date), $end_date (any date) 
       * return string of hours-minute-sec
       **/
	   
	   function getReverseDuration($start_date,$end_date)
	   {
	   
	   		$auto_complete_date=$end_date;
			$task_timezone_date=$start_date;
			
		   	$dateg=$auto_complete_date;
			$date1 = $auto_complete_date;
			$date2=$task_timezone_date;
			$diff = abs(strtotime($task_timezone_date) - strtotime($auto_complete_date));
			$test = floor($diff / (60*60*24));
			$str = '';
		
	
				
				$hours = 0;
				$minuts = 0;
				$dategg = $dateg;
			

				if(strtotime(date('Y-m-d H:i:s',strtotime($dateg))) > strtotime($task_timezone_date)) 
				{					
					
					//echo $date2;
					$diff2 = abs(strtotime($dategg) - strtotime($date2));
					$day1 = floor($diff2 / (60*60*24));
					
				
					$hours   = floor(($diff2 - $day1*60*60*24)/ (60*60));  
					$minuts  = floor(($diff2 - $day1*60*60*24 - $hours*60*60)/ 60); 
					$seconds = floor(($diff2 - $day1*60*60*24 - $hours*60*60 - $minuts*60)); 
					
					$str = $hours."-". $minuts."-".$seconds;
				}
				
				
			
				return $str;
	 
	 
	 }
	 
	 
	    /*********detect bad words from content
	** return bad words array
	***/
	
	function BadWords($str)
	{
				   
		$arrya=array();
		 
		  
		$temp['badwords']='';
		$temp['str']=$str;
			   
			    
		$CI =& get_instance();
	
		
		$supported_cache=check_supported_cache_driver();
		
		if(isset($supported_cache))
		{
			if($supported_cache!='' && $supported_cache!='none')
			{
				////===load cache driver===
				$CI->load->driver('cache');
				
				if($CI->cache->$supported_cache->get('spam_word'))
				{
					$arrya= (object)$CI->cache->$supported_cache->get('spam_word');				
				}
				else
				{
										
					$query = $CI->db->query("select spam_word from ".$CI->db->dbprefix('spam_word')." order by spam_word asc");
					
					if($query->num_rows()>0)
					{
						$res = $query->result();
				
						foreach($res as $spm)
						{
							$arrya[]=$spm->spam_word;
						}
				
						$CI->cache->$supported_cache->save('spam_word',$arrya,CACHE_VALID_SEC);	
						
					}
					
							
				}
			
			}
			
			else
			{
				$query = $CI->db->query("select spam_word from ".$CI->db->dbprefix('spam_word')." order by spam_word asc");
				
				if($query->num_rows()>0)
				{
					$res = $query->result();
				
					foreach($res as $spm)
					{
						$arrya[]=$spm->spam_word;
					}
				}
				
			}
		}
		else
		{
			
			$query = $CI->db->query("select spam_word from ".$CI->db->dbprefix('spam_word')." order by spam_word asc");
			
			if($query->num_rows()>0)
			{
				$res = $query->result();
				
				foreach($res as $spm)
				{
					$arrya[]=$spm->spam_word;
				}
			}
			
		
		}
		//////////====end cache part
		  
		   
		   
		   $str=strtolower($str);
		   $cleanstr = $str;
		   
		   $badwords=array();
		   
		  /* $charlist = array("|3"=>"b","13"=>"b","l3"=>"b","|)"=>"d","1)"=>"d","[)"=>"d","|("=>"k","1("=>"k","$"=>"s","("=>"c","1"=>"i","+"=>"t","|"=>"i","!"=>"i","#"=>"h","<"=>"c","@"=>"a","0"=>"o","{"=>"c","["=>"c");
		   
		   foreach($charlist as $char=>$value)
		   {
			 $cleanstr = strtolower(str_replace($char, $value, $cleanstr));
		   }*/
		   
		 
		   
		   $badarray = $arrya;
		   
		//   print_r($badarray);
		  // die;
		   
		  if($badarray)
		  {
			   foreach($badarray as $naughty) 
			   {
					   if (preg_match("/$naughty/", $str) or preg_match("/$naughty/", $cleanstr))
					   {
						  //return true;
						  $badwords[]=$naughty;
						  $cleanstr	= str_replace($naughty,'',$cleanstr);
					   }
			   }
			   
			   $temp['badwords']=array_unique($badwords);
			   $temp['str']=$cleanstr;
			    return $temp;
		  }
		  
		   return $temp;
		   
   } // end of bad word
     
//    Example :
//		 $str="you will fuck by him ,do not smash with them fucking jig";
//       $ret=BadWords($str);
//       if(is_array($ret))
//       {
//       echo "Some bad words detected like..".implode(",",$ret);
//       }
//	   
	 
	   
	 /*** load captcha
	*  return captcha image
	**/
	
	
	function load_captcha()
	{
		$CI =& get_instance();
		$CI->load->helper('captcha');
			
		$vals = array(
	  
		'img_path'	 => './captcha/',
		'img_url'	 => base_url().'captcha/',
		'font_path'	 => base_path().'captcha/fonts/texb.ttf',
		'img_width'	 => '150',
		'img_height' => 30,
		'expiration' => 7200
		);
	
		$cap = create_captcha($vals);
		
		 if ( $cap ) {
		  $data = array(
			'captcha_id' => '',
			'captcha_time' => $cap['time'],
			'ip_address' => $_SERVER['REMOTE_ADDR'],
			'word' => $cap['word'] ,
			);
		  $query = $CI->db->insert_string( 'captcha', $data );
		  $CI->db->query($query );
		}else {
		  return "Umm captcha not work" ;
		}
		return $cap['image'] ;
	
	}
	
	/*** check captcha validation
	*  return boolen
	**/	
	
	function check_capthca()
    {
		$CI =& get_instance();
		// Delete old data ( 2hours)
		$expiration = time()-7200 ;
		$sql = " DELETE FROM ".$CI->db->dbprefix('captcha')." WHERE captcha_time < ? ";
		$binds = array($expiration);
		$query =$CI->db->query($sql, $binds);
	 
		//checking input
		$sql = "SELECT COUNT(*) AS count FROM  ".$CI->db->dbprefix('captcha')." WHERE word = ? AND ip_address = ? AND captcha_time > ?";
		$binds = array($_POST['captcha'], $_SERVER['REMOTE_ADDR'], $expiration);
		$query = $CI->db->query($sql, $binds);
		$row = $query->row();
	 
	  if ( $row -> count > 0 )
		{
		  return true;
		}
		return false;
	 
  }
	  
		
		/********offset to timezone
	***  var $offset
	** return string timezone name
	***/
	
	function tzOffsetToName($offset, $isDst = null)
    {
        if ($isDst === null)
        {
            $isDst = date('I');
        }

        $offset *= 3600;
        $zone    = timezone_name_from_abbr('', $offset, $isDst);

        if ($zone === false)
        {
            foreach (timezone_abbreviations_list() as $abbr)
            {
                foreach ($abbr as $city)
                {
                    if ((bool)$city['dst'] === (bool)$isDst &&
                        strlen($city['timezone_id']) > 0    &&
                        $city['offset'] == $offset)
                    {
                        $zone = $city['timezone_id'];
                        break;
                    }
                }

                if ($zone !== false)
                {
                    break;
                }
            }
        }
    
        return $zone;
    }
	
    
    function round_up ( $value, $precision ) { 
    $pow = pow ( 10, $precision ); 
    return ( ceil ( $pow * $value ) + ceil ( $pow * $value - ceil ( $pow * $value ) ) ) / $pow; 
} 

		
		
/* End of file custom_helper.php */
/* Location: ./system/application/helpers/custom_helper.php */

?>