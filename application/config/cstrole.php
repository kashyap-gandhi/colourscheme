<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| Custom Role Sections
| -------------------------------------------------------------------------
|
|
*/



 
 //sales
 $role[1]=array('manage_report','report_work_order','report_time_sheet','report_client_proposals','report_material_proposals','manage_client','manage_order','manage_quote');
 
 //team lead
 $role[2]=array('manage_report','report_work_order','report_time_sheet','report_client_proposals','report_material_proposals','manage_order');
 
 //painters
 $role[3]=array('manage_report','report_work_order','report_time_sheet','report_client_proposals','report_material_proposals');
 
 //administration
 $role[4]=array('manage_report','report_work_order','report_time_sheet','report_client_proposals','report_material_proposals','manage_client','manage_order','manage_quote','system_setting','site_setting','email_setting','email_templates_setting','manage_others','manage_bus_type','manage_charge_type','manage_room_type','manage_sales_stage','manage_sheen','manage_winloss','manage_feature','manage_product','manage_team','manage_leadsource');
 
 //office managers
 $role[6]=array('manage_team','manage_leadsource');




 $config['roles'] = $role;


/* End of file cstrole.php */
/* Location: ./application/config/cstrole.php */