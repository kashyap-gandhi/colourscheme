<?php 

$main_layout_width='900';
$main_font_family='calibri';

$main_title='CSPI Work Order';
$main_title_font_color='6A7DA5';
$main_title_font_size='24';
$main_title_font_style='normal';
$main_title_font_weight='bold';
$main_title_font_family='calibri';


$sub_title_font_color='8B97BB';
$sub_title_font_size='18';
$sub_title_font_style='normal';
$sub_title_font_weight='normal';
$sub_title_font_family='calibri';

$sub_val_font_color='000000';
$sub_val_font_size='18';
$sub_val_font_style='normal';
$sub_val_font_weight='normal';


?>
<table border="0" cellpadding="0" cellspacing="0" width="<?php echo $main_layout_width; ?>px" style="font-family: <?php echo $main_font_family; ?>;">
    <tr>
        <td valign="top">
            <table border="0" cellpadding="0" cellspacing="0">
                <tr><td colspan="2" style="color:#<?php echo $main_title_font_color; ?>; font-size: <?php echo $main_title_font_size; ?>px; font-weight: <?php echo $main_title_font_weight; ?>; font-style:<?php echo $main_title_font_style; ?>;font-family: <?php echo $main_title_font_family; ?>;"><?php echo $main_title; ?></td></tr>
                
                <tr><td colspan="2" height="5"></td></tr>
                <tr>
                    <td colspan="2" style="color:#<?php echo $sub_title_font_color; ?>; font-size: <?php echo $sub_title_font_size; ?>px; font-weight: <?php echo $sub_title_font_weight; ?>; font-style:<?php echo $sub_title_font_style; ?>;font-family: <?php echo $sub_title_font_family; ?>;">Colour Scheme Professionals Inc.</td>
                </tr>
                <tr><td colspan="2" height="2"></td></tr>
                <tr>
                    <td colspan="2" style="color:#<?php echo $sub_title_font_color; ?>; font-size: <?php echo $sub_title_font_size; ?>px; font-weight: <?php echo $sub_title_font_weight; ?>; font-style:<?php echo $sub_title_font_style; ?>;font-family: <?php echo $sub_title_font_family; ?>;">2nd Floor, 521 Hargrave St.</td>
                </tr>
                <tr><td colspan="2" height="2"></td></tr>
                 <tr>
                    <td colspan="2" style="color:#<?php echo $sub_title_font_color; ?>; font-size: <?php echo $sub_title_font_size; ?>px; font-weight: <?php echo $sub_title_font_weight; ?>; font-style:<?php echo $sub_title_font_style; ?>;font-family: <?php echo $sub_title_font_family; ?>;">Winnipeg, Manitoba R3A 0Y1</td>
                </tr>
                <tr><td colspan="2" height="2"></td></tr>
                
            </table>            
            
        </td>
        <td width="20"></td>
        <td align="right" valign="middle" width="255"><img src="invoice-logo.png" border="0" /></td>
    </tr>
</table>


<table border="0" cellpadding="0" cellspacing="0" width="<?php echo $main_layout_width; ?>px">
    <tr><td height="7"></td></tr>
    <tr>
        <td height="5" style="background: #<?php echo $main_title_font_color; ?>;"></td>
    </tr>
</table>

<table border="0" cellpadding="0" cellspacing="0" width="<?php echo $main_layout_width; ?>px">
    <tr><td height="7"></td></tr>
</table>

<table border="0" cellpadding="0" cellspacing="0" width="<?php echo $main_layout_width; ?>px" style="font-family: <?php echo $main_font_family; ?>;">
    <tr>
        <td width="40%" align="left" valign="top">
            <table border="0" cellpadding="2" cellspacing="0" width="100%" style="font-weight: bold;">
                <tr>
                    <td align="left" valign="top" width="30%" style="color:#<?php echo $sub_title_font_color; ?>; font-size: <?php echo $sub_title_font_size; ?>px; font-weight: bold; font-style:<?php echo $sub_title_font_style; ?>;font-family: <?php echo $sub_title_font_family; ?>;">Client</td>  
                    <td>David Stern</td>
                </tr> 
                <tr>
                    <td></td>
                    <td align="left" valign="top">8721 Roblin Blvd.</td>
                </tr> 
                <tr>
                    <td></td>
                    <td align="left" valign="top">Heading Ley MB RJH417</td>
                </tr>
            </table>
        </td>
        <td width="20%"></td>
        <td width="40%" align="right" valign="top">
            <table border="0" cellpadding="3" cellspacing="0" width="100%">
                 <tr>
                     <td align="right" valign="top" style="font-weight: bold;">Date:</td>
                     <td align="left" valign="top">13-Jul-2015</td>
                 </tr>
                 <tr>
                     <td align="right" valign="top" style="font-weight: bold;">Com/Res:</td>
                     <td align="left" valign="top">Residential</td>
                 </tr>
                 <tr>
                     <td align="right" valign="top" style="font-weight: bold;">Int/Ext:</td>
                     <td align="left" valign="top">Interior</td>
                 </tr>
                 <tr>
                     <td align="right" valign="top" style="font-weight: bold;">Paint:</td>
                     <td align="left" valign="top">INT SW 2 SW HARMONY</td>
                 </tr>
            </table>
        </td>
    </tr>
</table>


<table border="0" cellpadding="0" cellspacing="0" width="<?php echo $main_layout_width; ?>px" style="font-family: <?php echo $main_font_family; ?>;">
    <tr>
       <td align="left" valign="top" width="30%" style="color:#<?php echo $sub_title_font_color; ?>; font-size: <?php echo $sub_title_font_size; ?>px; font-weight: bold; font-style:<?php echo $sub_title_font_style; ?>;font-family: <?php echo $sub_title_font_family; ?>;">Job Description</td>
    </tr>
    <tr>
        <td style="border: 1px solid #8B97BB; padding: 3px 4px;">
            welcome to colour scheme professionals painters Inc. We are suited at Winnipeg, Monitoba, 8724 Canada. Please if any issue then contact on 708-4058-8747
        </td>
    </tr>
    
</table>



