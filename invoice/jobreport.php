<?php 

$main_layout_width='900';
$main_font_family='calibri';

$main_title='Job Hour Tracker Report - Emp';
$main_title_font_color='6A7DA5';
$main_title_font_size='24';
$main_title_font_style='normal';
$main_title_font_weight='bold';
$main_title_font_family='calibri';


$sub_title_font_color='8B97BB';
$sub_title_font_size='18';
$sub_title_font_style='normal';
$sub_title_font_weight='normal';
$sub_title_font_family='calibri';

$sub_val_font_color='000000';
$sub_val_font_size='18';
$sub_val_font_style='normal';
$sub_val_font_weight='normal';
$sub_val_font_family='calibri';


?>

<table border="0" cellpadding="0" cellspacing="0" width="<?php echo $main_layout_width; ?>px" style="font-family: <?php echo $main_font_family; ?>;">
    <tr>
        <td valign="top">
            <table border="0" cellpadding="0" cellspacing="0">
                <tr><td colspan="2" style="color:#<?php echo $main_title_font_color; ?>; font-size: <?php echo $main_title_font_size; ?>px; font-weight: <?php echo $main_title_font_weight; ?>; font-style:<?php echo $main_title_font_style; ?>;font-family: <?php echo $main_title_font_family; ?>;"><?php echo $main_title; ?></td></tr>
                
                <tr><td colspan="2" height="15"></td></tr>
                <tr>
                    <td style="color:#<?php echo $sub_title_font_color; ?>; font-size: <?php echo $sub_title_font_size; ?>px; font-weight: <?php echo $sub_title_font_weight; ?>; font-style:<?php echo $sub_title_font_style; ?>;font-family: <?php echo $sub_title_font_family; ?>;">Begin Date</td>
                    <td style="color:#<?php echo $sub_val_font_color; ?>; font-size: <?php echo $sub_val_font_size; ?>px; font-weight: <?php echo $sub_val_font_weight; ?>; font-style:<?php echo $sub_val_font_style; ?>;font-family: <?php echo $sub_val_font_family; ?>;">13-Jul-2015</td>
                </tr>
                <tr><td colspan="2" height="2"></td></tr>
                 <tr>
                    <td style="color:#<?php echo $sub_title_font_color; ?>; font-size: <?php echo $sub_title_font_size; ?>px; font-weight: <?php echo $sub_title_font_weight; ?>; font-style:<?php echo $sub_title_font_style; ?>;font-family: <?php echo $sub_title_font_family; ?>;">End Date</td>                    
                    <td style="color:#<?php echo $sub_val_font_color; ?>; font-size: <?php echo $sub_val_font_size; ?>px; font-weight: <?php echo $sub_val_font_weight; ?>; font-style:<?php echo $sub_val_font_style; ?>;font-family: <?php echo $sub_val_font_family; ?>;">13-Jul-2015</td>
                </tr>
            </table>            
            
        </td>
        <td width="20"></td>
        <td align="right" valign="middle" width="255"><img src="invoice-logo.png" border="0" /></td>
    </tr>
</table>

<table border="0" cellpadding="0" cellspacing="0" width="<?php echo $main_layout_width; ?>px">
    <tr><td height="7"></td></tr>
    <tr>
        <td height="5" style="background: #<?php echo $main_title_font_color; ?>;"></td>
    </tr>
</table>
